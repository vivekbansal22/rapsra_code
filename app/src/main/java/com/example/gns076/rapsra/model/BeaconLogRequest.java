package com.example.gns076.rapsra.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class BeaconLogRequest implements Serializable,Cloneable{

    @SerializedName("beacon_logs")
    private List<BeaconLogInfo> beaconLogInfoList = new ArrayList<>();

    public List<BeaconLogInfo> getBeaconLogInfoList() {
        return beaconLogInfoList;
    }

    public void setBeaconLogInfoList(List<BeaconLogInfo> beaconLogInfoList) {
        this.beaconLogInfoList = beaconLogInfoList;
    }
    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

}
