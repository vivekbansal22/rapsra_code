package com.example.gns076.rapsra.activities;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.example.gns076.rapsra.R;
import com.example.gns076.rapsra.model.ProspectsFollowUpInfoModel;
import com.example.gns076.rapsra.model.ProspectFollowDataModel;
import  com.example.gns076.rapsra.model.ProspectsTableResponse;
import com.example.gns076.rapsra.model.SetProspectsFollowUpResponse;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CalendarProspectsUpdateData extends AppCompatActivity {
    private APIInterface apiInterface;
    private ProspectsFollowUpInfoModel prospectsFollowUpInfoModel[];
    private TextView   nameOfPropectUpDate;
    private TextView cityOfProspectUpDate;
    private TextView stateOfProspectUpDate;
    private EditText followUpNewDate;
    private EditText followUpNewNote;
    private int mYear, mMonth, mDay;
    private String previousDate;
    private ProspectFollowDataModel prospectFollowDataModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calendar_prospects_update_data_new1);
        init();

        apiInterface = APIClient.getClient().create(APIInterface.class);
        final TableLayout tableFollowUpInfo = findViewById(R.id.dataDisplayTable);
        try {

            Call<ProspectsTableResponse> call2 = apiInterface.getProspectsFollowUpsInfo(prospectFollowDataModel.getId());

            call2.enqueue(new Callback<ProspectsTableResponse>() {
                @Override
                public void onResponse(Call<ProspectsTableResponse> call, Response<ProspectsTableResponse> response) {

                    ProspectsTableResponse res = response.body();
                    if (res != null) {
                        prospectsFollowUpInfoModel=res.getObj();
                        if(prospectsFollowUpInfoModel!=null) {
                            TableRow tbrow0 = new TableRow(getApplicationContext());
                            tbrow0.addView(getTextView("PostedOn"));
                            tbrow0.addView(getTextView("FollowUpNote"));

                            tbrow0.setBackgroundColor(Color.GRAY);

                            tableFollowUpInfo.addView(tbrow0);
                            //  ProspectsFollowUpInfoModel m;
                            for (ProspectsFollowUpInfoModel m:prospectsFollowUpInfoModel) {
                                TableRow tbrow = new TableRow(getApplicationContext());

                                TextView updatedAt = new TextView(getApplicationContext());
                                TextView dateNotes = new TextView(getApplicationContext());

                                updatedAt.setText(m.getUpdatedAt());
                                updatedAt.setTextColor(Color.BLACK);

                                updatedAt.setGravity(Gravity.CENTER);
                                updatedAt.setMaxLines(3);
                                updatedAt.setMaxWidth(10);
                                updatedAt.setSingleLine(false);
                                tbrow.addView(updatedAt);

                                dateNotes.setText(m.getDate_notes());
                                dateNotes.setTextColor(Color.BLACK);
                                dateNotes.setGravity(Gravity.CENTER);
                                dateNotes.setMaxLines(3);
                                dateNotes.setMaxWidth(10);
                                dateNotes.setSingleLine(false);
                                tbrow.addView(dateNotes);
                                tableFollowUpInfo.addView(tbrow);
                            }//for loop
                        }
                        else{
                            TableRow tbrow0 = new TableRow(getApplicationContext());
                            tbrow0.addView(getTextView("PostedOn"));
                            tbrow0.addView(getTextView("FollowUpNote"));

                            tbrow0.setBackgroundColor(Color.GRAY);
                            tableFollowUpInfo.addView(tbrow0);
                        }

                        //   setUpCalendarAdapter();
                    }
                    else{
                        TableRow tbrow0 = new TableRow(getApplicationContext());

                        tbrow0.addView(getTextView("PostedOn"));
                        tbrow0.addView(getTextView("FollowUpNote"));

                        tbrow0.setBackgroundColor(Color.GRAY);
                        tableFollowUpInfo.addView(tbrow0);
                    }

                }

                @Override
                public void onFailure(Call<ProspectsTableResponse> call, Throwable t) {
                    //   setUpCalendarAdapter();
                    call.cancel();
                    TableRow tbrow0 = new TableRow(getApplicationContext());

                    tbrow0.addView(getTextView("PostedOn"));
                    tbrow0.addView(getTextView("FollowUpNote"));

                    tbrow0.setBackgroundColor(Color.GRAY);
                    tableFollowUpInfo.addView(tbrow0);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    /*
    * This code is for intilization
    * */
    public void init(){
        nameOfPropectUpDate = (TextView)findViewById(R.id.nameOfProspectUpDate);
        cityOfProspectUpDate   = (TextView)findViewById(R.id.cityOfProspectUpdate);
        stateOfProspectUpDate  = (TextView)findViewById(R.id.stateOfProspectUpdate);
        followUpNewDate = (EditText)findViewById(R.id.followUpDateOfProspectUpdate);
        followUpNewNote = (EditText)findViewById(R.id.followUpNoteOfProspectUpdate);
        prospectFollowDataModel = (ProspectFollowDataModel)getIntent().getExtras().get("selectedProspect");
        previousDate = (String)getIntent().getExtras().get("previousDate");
        nameOfPropectUpDate.setText(prospectFollowDataModel.getFirst_name()+" "+prospectFollowDataModel.getLast_name());
        cityOfProspectUpDate.setText(prospectFollowDataModel.getCity());
        stateOfProspectUpDate.setText(prospectFollowDataModel.getState());
    }
    /*
     * This code is to make common textview
     * */
    public TextView getTextView(String text){
        TextView textView = new TextView(getApplicationContext());
        textView.setText(text.toString());
        textView.setTextColor(Color.BLACK);
        textView.setGravity(Gravity.CENTER);


        return textView;
    }



    public void onClickFollowDate(View v) {

        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        CharSequence strDate = null;
                        c.set(year,monthOfYear,dayOfMonth);
                        Date chosenDate = c.getTime();
//Date d = new Date(year, monthOfYear, dayOfMonth);
                        SimpleDateFormat dateFormatter = new SimpleDateFormat(
                                "yyyy-MM-dd");
                        strDate = dateFormatter.format(chosenDate);
                        followUpNewDate.setText( strDate);

                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();

        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
// }

    }

    public void onUpdateButtonClick(View v) {
        final Intent intentRegister = new Intent(getApplicationContext(), CustomCalendarActivity.class);

        if (followUpNewNote.getText().toString().isEmpty() || followUpNewNote.getText().toString() == null || followUpNewNote.getText().toString().equalsIgnoreCase(" ")) {
            Toast.makeText(getApplicationContext(),"Please Enter Valid Note", Toast.LENGTH_LONG).show();
        }
        else{
           try {


                Date chosenTime=  Calendar.getInstance().getTime();

                SimpleDateFormat dateFormatter = new SimpleDateFormat(
                        "yyyy-MM-dd HH:mm:ss");
                String time = dateFormatter.format(chosenTime);
                Call<SetProspectsFollowUpResponse> call2 = apiInterface.setProspectsFollowUps(prospectFollowDataModel.getId(),

                        previousDate.toString(),followUpNewNote.getText().toString().trim(), followUpNewDate.getText().toString().trim(),time);
                call2.enqueue(new Callback<SetProspectsFollowUpResponse>() {
                    @Override
                    public void onResponse(Call<SetProspectsFollowUpResponse> call, Response<SetProspectsFollowUpResponse> response) {
                        System.out.println(response.body().toString());
                        SetProspectsFollowUpResponse res = response.body();
                        if (!res.isError()) {

                            Toast.makeText(getApplicationContext(), "Data Updated Successfully", Toast.LENGTH_LONG).show();
                            //  emptySOInputEditText();
                            getApplicationContext().startActivity(intentRegister);

                        } else {
                            Toast.makeText(getApplicationContext(), res.getMessage(), Toast.LENGTH_LONG).show();

                        }
                    }

                    @Override
                    public void onFailure(Call<SetProspectsFollowUpResponse> call, Throwable t) {
                        Toast.makeText(getApplicationContext(), "Failure.No Network Connection", Toast.LENGTH_LONG).show();
                        call.cancel();
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }

        }//else
    }

}
