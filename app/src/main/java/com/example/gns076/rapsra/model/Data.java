package com.example.gns076.rapsra.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;


public class Data {

    @SerializedName("daily")
    private ArrayList<MyTeamStatus> daily= new ArrayList<MyTeamStatus>();
    @SerializedName("weekly")
    private ArrayList<MyTeamStatus> weekly= new ArrayList<MyTeamStatus>();
    @SerializedName("monthly")
    private ArrayList<MyTeamStatus> monthly= new ArrayList<MyTeamStatus>();
    @SerializedName("yearly")
    private ArrayList<MyTeamStatus> yearly= new ArrayList<MyTeamStatus>();

    public ArrayList<MyTeamStatus> getDaily() {
        return daily;
    }

    public void setDaily(ArrayList<MyTeamStatus> daily) {
        this.daily = daily;
    }

    public ArrayList<MyTeamStatus> getWeekly() {
        return weekly;
    }

    public void setWeekly(ArrayList<MyTeamStatus> weekly) {
        this.weekly = weekly;
    }

    public ArrayList<MyTeamStatus> getMonthly() {
        return monthly;
    }

    public void setMonthly(ArrayList<MyTeamStatus> monthly) {
        this.monthly = monthly;
    }

    public ArrayList<MyTeamStatus> getYearly() {
        return yearly;
    }

    public void setYearly(ArrayList<MyTeamStatus> yearly) {
        this.yearly = yearly;
    }
}
