package com.example.gns076.rapsra.adapters;

import android.graphics.Color;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.gns076.rapsra.R;
import com.example.gns076.rapsra.model.MyTeamStatus;

import com.example.gns076.rapsra.sql.DatabaseHelper;

import java.util.List;
import java.util.Map;

public class MyStatusWeeklyAdapter extends RecyclerView.Adapter<MyStatusWeeklyAdapter.weeklyStatusHolder> {

    private static final String TAG = MyStatusWeeklyAdapter.class.getSimpleName();

    public void setListWeeklyStatus(List<MyTeamStatus> listWeeklyStatus) {
        this.listWeeklyStatus = listWeeklyStatus;
    }

    private List<MyTeamStatus> listWeeklyStatus;

    DatabaseHelper helper;

    Map<String,String> statusColorList;

    public MyStatusWeeklyAdapter(List<MyTeamStatus> listWeeklyStatus, DatabaseHelper helper) {
        this.listWeeklyStatus = listWeeklyStatus;
        this.helper = helper;
    }

    @Override
    public MyStatusWeeklyAdapter.weeklyStatusHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // inflating recycler item view
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_status_weekly_new, parent, false);

        return new MyStatusWeeklyAdapter.weeklyStatusHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyStatusWeeklyAdapter.weeklyStatusHolder holder, int position) {

        holder.textViewName.setText(listWeeklyStatus.get(position).getName());
        holder.textViewPercentage.setText(listWeeklyStatus.get(position).getPercentage());
        holder.textViewUps.setText(listWeeklyStatus.get(position).getUps());
        holder.textViewClosed.setText(listWeeklyStatus.get(position).getClosed());

        if((helper.getAllStatus().size()>0)){
            Log.i(TAG,"size:"+helper.getAllStatus().size());
            statusColorList = helper.getAllStatus();


            Log.i(TAG,"status color size:"+statusColorList.size());
            for (Map.Entry<String,String> entry : statusColorList.entrySet()){
                Log.i(TAG,"Status Name:"+listWeeklyStatus.get(position).getStatus());
                Log.i(TAG,"database Status Name:"+entry.getKey());
                if(listWeeklyStatus.get(position).getStatus().equalsIgnoreCase(entry.getKey())){
                    Log.i(TAG,"Status Name:"+listWeeklyStatus.get(position).getName());
                    holder.statusCircle.setColorFilter(Color.parseColor(entry.getValue()));
                }
            }
        }




        //     holder.statusCircle.setColorFilter(Color.GREEN);
    }

    @Override
    public int getItemCount() {
        Log.v(UsersRecyclerAdapter.class.getSimpleName(), "" + listWeeklyStatus.size());
        return listWeeklyStatus.size();
    }


    public class weeklyStatusHolder extends RecyclerView.ViewHolder {

        public TextView textViewName;
        public TextView textViewPercentage;
        public TextView textViewUps;
        public TextView textViewClosed;
        public ImageView statusCircle;

        public weeklyStatusHolder(View view) {
            super(view);
            textViewName = view.findViewById(R.id.status_name_new);
            textViewPercentage = view.findViewById(R.id.percent_value);
            textViewUps = view.findViewById(R.id.ups_value);
            textViewClosed = view.findViewById(R.id.closed_value);
            statusCircle = view.findViewById(R.id.status_icon);

        }
    }
}
