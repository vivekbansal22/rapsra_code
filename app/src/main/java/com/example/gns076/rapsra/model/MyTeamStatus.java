package com.example.gns076.rapsra.model;

import com.google.gson.annotations.SerializedName;

public class MyTeamStatus {
    /* @SerializedName("qno")
     private int qno;*/
    @SerializedName("id")
    private String id;


    @SerializedName("name")

    private  String name;

    @SerializedName("status")
    private  String status;

    @SerializedName("ups")
    private  String ups;

    @SerializedName("closed")
    private  String closed;

    @SerializedName("percentage")
    private  String percentage;


    public MyTeamStatus() {
    }

    public MyTeamStatus(String id, String name, String status, String ups, String closed, String percentage) {
        this.id = id;
        this.name = name;
        this.status = status;
        this.ups = ups;
        this.closed = closed;
        this.percentage = percentage;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUps() {
        return ups;
    }

    public void setUps(String ups) {
        this.ups = ups;
    }

    public String getClosed() {
        return closed;
    }

    public void setClosed(String closed) {
        this.closed = closed;
    }

    public String getPercentage() {
        return percentage;
    }

    public void setPercentage(String percentage) {
        this.percentage = percentage;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


}
