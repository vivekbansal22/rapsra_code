package com.example.gns076.rapsra.adapters;

import android.content.Context;
import android.graphics.Color;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.gns076.rapsra.R;
import com.example.gns076.rapsra.model.AppPreference;
import com.example.gns076.rapsra.model.ProspectFollowDataModel;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class GridAdapter extends ArrayAdapter {
    private static final String TAG = GridAdapter.class.getSimpleName();
    private LayoutInflater mInflater;
    private List<Date> monthlyDates;
    private Calendar currentDate;
    private Map<Integer,LinkedList<String>> prospectDateMap;
    public AppPreference mAppPreference;
    private ProspectFollowDataModel followDataModels[];
    public Context mcontext;
    //public static boolean flag = false;
  //  public static String dealerId;
  //  private List<EventObjects> allEvents;
    public GridAdapter(Context context, List<Date> monthlyDates, Calendar currentDate,ProspectFollowDataModel followDataModels[]) {
        super(context, R.layout.single_cell_layout);
        this.monthlyDates = monthlyDates;
        this.currentDate = currentDate;
        mcontext = context;
    //    this.allEvents = allEvents;
        mInflater = LayoutInflater.from(context);
        this.followDataModels = followDataModels;

    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Date mDate = monthlyDates.get(position);
        Calendar dateCal = Calendar.getInstance();

        dateCal.setTime(mDate);
        int dayValue = dateCal.get(Calendar.DAY_OF_MONTH);
        int displayMonth = dateCal.get(Calendar.MONTH) + 1;
        int displayYear = dateCal.get(Calendar.YEAR);
        int currentMonth = currentDate.get(Calendar.MONTH) + 1;
        int currentYear = currentDate.get(Calendar.YEAR);
        View view = convertView;
        if(view == null){
            view = mInflater.inflate(R.layout.single_cell_layout, parent, false);
        }
        if(displayMonth == currentMonth && displayYear == currentYear){
            view.setBackgroundColor(Color.parseColor("#cccccc"));
            if(dayValue == 01 || dayValue == 1){
                setProspectDateMap(getData());
            }

            TextView date = (TextView)view.findViewById(R.id.calendar_date);
            TextView prospectName1 = (TextView)view.findViewById(R.id.first_person);
            TextView prospectName2 = (TextView)view.findViewById(R.id.second_person);
            TextView prospectName3 = (TextView)view.findViewById(R.id.third_person);


            date.setText(String.valueOf(dayValue));

            LinkedList<String> dateData = prospectDateMap.get(dayValue);
            if(prospectDateMap!=null) {
                if (dateData!=null) {

                    if(dateData.size()==1)
                    {
                        prospectName1.setText(dateData.get(0));
                        prospectName2.setText("");
                        prospectName3.setText("");
                    }
                    else if(dateData.size()==2){
                        prospectName1.setText(dateData.get(0));
                        prospectName2.setText(dateData.get(1));
                        prospectName3.setText("");
                    }
                    else if(dateData.size()==3){
                        prospectName1.setText(dateData.get(0));
                        prospectName2.setText(dateData.get(1));
                        prospectName3.setText(dateData.get(2));
                    }
                    else{
                        prospectName1.setText(dateData.get(0));
                        prospectName2.setText(dateData.get(1));
                        prospectName3.setText(dateData.get(2)+"..");
                    }
                } else {
                    view.setEnabled(false);
                    view.setOnClickListener(null);
                    prospectName1.setText("");
                    prospectName2.setText("");
                    prospectName3.setText("");
                }
            }
            else{
                view.setEnabled(false);
                view.setOnClickListener(null);
                prospectName1.setText("");
                prospectName2.setText("");
                prospectName3.setText("");
            }

        }else{
            TextView date = (TextView)view.findViewById(R.id.calendar_date);
            date.setText("");
            TextView prospectName1 = (TextView)view.findViewById(R.id.first_person);
            TextView prospectName2 = (TextView)view.findViewById(R.id.second_person);
            TextView prospectName3 = (TextView)view.findViewById(R.id.third_person);
            prospectName1.setText("");
            prospectName2.setText("");
            prospectName3.setText("");
            view.setBackgroundColor(Color.parseColor("#cccccc"));
            view.setEnabled(false);
            view.setOnClickListener(null);
        }

        return view;
    }




    public  Map<Integer,LinkedList<String>> getData( ){
        Map<Integer,LinkedList<String>>prospectDateMap = new HashMap<Integer,LinkedList<String>>();
        if(followDataModels != null) {
            for (int i = 0; i < followDataModels.length; i++) {
                String stringDate = followDataModels[i].getFollowDate().substring(8);
                if (prospectDateMap.containsKey(Integer.parseInt(stringDate))) {
                    prospectDateMap.get(Integer.parseInt(stringDate)).add(followDataModels[i].getFirst_name());
                } else {
                    LinkedList<String> arrayList = new LinkedList<String>();
                    arrayList.add(followDataModels[i].getFirst_name());
                    prospectDateMap.put(Integer.parseInt(stringDate), arrayList);

                }
            }

        }
        return prospectDateMap;
    }

    public void setProspectDateMap( Map<Integer,LinkedList<String>>prospectDateMap){
        this.prospectDateMap =  prospectDateMap;
    }
    @Override
    public int getCount() {
        return monthlyDates.size();
    }
    @Nullable
    @Override
    public Object getItem(int position) {
        return monthlyDates.get(position);
    }
    @Override
    public int getPosition(Object item) {
        return monthlyDates.indexOf(item);
    }
}
