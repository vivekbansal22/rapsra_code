package com.example.gns076.rapsra.model;

/**
 * Created by
 */

public class Message {
    public String sender;
    public String senderId;
    public String sendermailId;

    public Message(String sender, String senderId, String message, String msgtime,String sendermailId) {
        this.sender = sender;
        this.senderId = senderId;
        this.sendermailId = sendermailId;
        this.message = message;
        this.msgtime = msgtime;
    }

    public Message() {
    }

    public String getSendermailId() {
        return sendermailId;
    }

    public void setSendermailId(String sendermailId) {
        this.sendermailId = sendermailId;
    }

    /* public Message(String sender, String senderId, String message, String msgtime) {
            this.sender = sender;
            this.senderId = senderId;
            this.message = message;
            this.msgtime = msgtime;
        }
    */
    public String message;

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    public String getMsgtime() {
        return msgtime;
    }

    public void setMsgtime(String msgtime) {
        this.msgtime = msgtime;
    }

    public String msgtime;


    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
