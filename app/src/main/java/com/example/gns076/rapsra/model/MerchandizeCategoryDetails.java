package com.example.gns076.rapsra.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class MerchandizeCategoryDetails implements Serializable {

    @SerializedName("merchandize_category")
    private MerchandizeCategory merchandizeCategory;
    @SerializedName("subcategories")
    private ArrayList<MerchandizeSubCategory> subCategories =  new ArrayList<MerchandizeSubCategory>();

    public MerchandizeCategory getMerchandizeCategory() {
        return merchandizeCategory;
    }

    public void setMerchandizeCategory(MerchandizeCategory merchandizeCategory) {
        this.merchandizeCategory = merchandizeCategory;
    }

    public ArrayList<MerchandizeSubCategory> getSubCategories() {
        return subCategories;
    }

    public void setSubCategories(ArrayList<MerchandizeSubCategory> subCategories) {
        this.subCategories = subCategories;
    }
}
