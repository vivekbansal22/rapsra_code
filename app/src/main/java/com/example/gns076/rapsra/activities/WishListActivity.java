package com.example.gns076.rapsra.activities;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.RelativeSizeSpan;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.example.gns076.rapsra.R;
import com.example.gns076.rapsra.model.AppPreference;

public class WishListActivity extends AppCompatActivity implements View.OnClickListener {

    private AppPreference mAppPreference;

    private Button taskButtonWishlistPage;
    private Button bothTaskWishListButtonWishlistPage;
    private Button wishListReturn;
    private String token;
    private Button menuButton;
    private Button virtualAssist;
    private Button editButtonWishList;
    private TextView wishListPhoneNo;

    private TextView firstLastNameText;

    private String prospectorId;
    private String prospectFirstName;
    private String prospectLastName;
    private String prospectPhNo;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wishlist_x);

        mAppPreference = new AppPreference(this);
        token=mAppPreference.getToken();

        taskButtonWishlistPage = (Button) findViewById(R.id.task_button_wishlist_page_x);
        bothTaskWishListButtonWishlistPage = (Button) findViewById(R.id.task_wishlist_both_button_wishlist_page_x);
        wishListReturn = (Button)findViewById(R.id.wishlist_return);
        menuButton =(Button)findViewById(R.id.wishlist_menu_button);
        virtualAssist = (Button)findViewById(R.id.wishlist_Virtualassist);

        editButtonWishList = findViewById(R.id.edit_button_wishlist_x);

        prospectorId =  getIntent().getExtras().getString("prospect_id");
        prospectFirstName =  getIntent().getExtras().getString("prospector_firstname");
        prospectLastName =  getIntent().getExtras().getString("prospector_lastname");
        prospectPhNo =  getIntent().getExtras().getString("prospector_phone");

        wishListPhoneNo = (TextView) findViewById(R.id.wishlist_phoneno);
        firstLastNameText = (TextView) findViewById(R.id.firstname_lastname_wishlist_x);
        firstLastNameText.setText(prospectFirstName+" "+prospectLastName);

        wishListPhoneNo.setText(prospectPhNo);

        setListeners();

        menuButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Creating the instance of PopupMenu
                PopupMenu popup = new PopupMenu(WishListActivity.this, menuButton);
                //Inflating the Popup using xml file

                menuButton.setBackgroundResource(R.drawable.exit_menu);

                Menu gamesMenu =popup.getMenu();


                popup.getMenuInflater()
                        .inflate(R.menu.menu_home, gamesMenu);
                gamesMenu.removeItem(R.id.logout);

                gamesMenu.removeItem(R.id.add_edit_so);
                gamesMenu.removeItem(R.id.scheduled_followups);


                for(int i = 0; i < gamesMenu.size(); i++) {
                    MenuItem item = gamesMenu.getItem(i);


                    SpannableString spanString = new SpannableString(gamesMenu.getItem(i).getTitle().toString());
                    int end = spanString.length();

                    //   spanString.setSpan(new RelativeSizeSpan(1.5f), 0, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    spanString.setSpan(new RelativeSizeSpan(0.04f), 0, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    item.setTitle(spanString);

                }


                popup.setOnDismissListener(new PopupMenu.OnDismissListener() {
                    @Override
                    public void onDismiss(PopupMenu popupMenu) {
                        menuButton.setBackgroundResource(R.drawable.menu);
                    }
                });


                //registering popup with OnMenuItemClickListener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {

                        Intent intentViewScheduledFollowUps;
                        switch (item.getItemId()){

                            case R.id.main_page:
                                intentViewScheduledFollowUps = new Intent(getApplicationContext(), SalesRadarActivityNew.class);
                                startActivity(intentViewScheduledFollowUps);
                                break;
/*
                            case R.id.add_edit_so:
                                intentViewScheduledFollowUps = new Intent(getApplicationContext(), AddEditSoActivity.class);
                                startActivity(intentViewScheduledFollowUps);
                                break;*/

                            case R.id.add_edit_prospect:
                                intentViewScheduledFollowUps = new Intent(getApplicationContext(), ProspectorActivity.class);
                                intentViewScheduledFollowUps.putExtra("Token",mAppPreference.getToken() );
                                startActivity(intentViewScheduledFollowUps);
                                break;

                            case R.id.rap_gamify:
                                intentViewScheduledFollowUps = new Intent(getApplicationContext(), GamificationActivity.class);
                                startActivity(intentViewScheduledFollowUps);
                                break;
                            case R.id.my_rank:
                                intentViewScheduledFollowUps = new Intent(getApplicationContext(), WrittenActivity.class);
                                startActivity(intentViewScheduledFollowUps);
                                break;
                            case R.id.chat_menu:
                                intentViewScheduledFollowUps = new Intent(getApplicationContext(), ChatActivity.class);
                                startActivity(intentViewScheduledFollowUps);
                                break;

                            case R.id.ar_mode:
                                intentViewScheduledFollowUps = new Intent(getApplicationContext(), ArViewActivityNew.class);
                                startActivity(intentViewScheduledFollowUps);
                                break;

                            case R.id.floor_map_mode:
                                intentViewScheduledFollowUps = new Intent(getApplicationContext(), FloorActivity.class);
                                startActivity(intentViewScheduledFollowUps);
                                break;

                            case R.id.fun_games:
                                intentViewScheduledFollowUps = new Intent(getApplicationContext(), FunGames.class);
                                startActivity(intentViewScheduledFollowUps);
                                break;


                        }
                        return true;
                    }
                });

                popup.show(); //showing popup menu


            }
        });


    }

    public void setListeners() {

        taskButtonWishlistPage.setOnClickListener(this);
        bothTaskWishListButtonWishlistPage.setOnClickListener(this);
        editButtonWishList.setOnClickListener(this);

        wishListReturn.setOnClickListener(this);
        virtualAssist.setOnClickListener(this);
    }


    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.task_button_wishlist_page_x:
                Intent intentTaskButton = new Intent(this, TaskActivity.class);
                intentTaskButton.putExtra("Token",mAppPreference.getToken() );

                intentTaskButton.putExtra("prospect_id",prospectorId );
                intentTaskButton.putExtra("prospector_firstname",prospectFirstName);
                intentTaskButton.putExtra("prospector_lastname",prospectLastName);
                intentTaskButton.putExtra("prospector_phone",prospectPhNo);
                startActivity(intentTaskButton);
                break;

            case R.id.task_wishlist_both_button_wishlist_page_x:
                Intent intentBothButton = new Intent(this, TaskWishListBothActivity.class);

                intentBothButton.putExtra("prospect_id",prospectorId );
                intentBothButton.putExtra("prospector_firstname",prospectFirstName);
                intentBothButton.putExtra("prospector_lastname",prospectLastName);
                intentBothButton.putExtra("prospector_phone",prospectPhNo);

                startActivity(intentBothButton);
                break;

            case R.id.wishlist_return: Intent intentProspector = new Intent(getApplicationContext(), ProspectorActivity.class);
                intentProspector.putExtra("Token",token );
                startActivity(intentProspector);
                finish();
                break;

            case R.id.wishlist_Virtualassist:
                Intent  intentVirtualAssist= new Intent(getApplicationContext(), VirtualAssistantActivity.class);
                startActivity(intentVirtualAssist);
                finish();
                break;

            case R.id.edit_button_wishlist_x:
                Intent intentTask1 = new Intent(getApplicationContext(), ProspectorActivity.class);
                intentTask1.putExtra("prospect_id",prospectorId );
                startActivity(intentTask1);
                finish();
                break;


        }
    }

    @Override
    public void onBackPressed() {
        Intent intentProspector = new Intent(getApplicationContext(), ProspectorActivity.class);
        intentProspector.putExtra("Token",token );
        startActivity(intentProspector);
        finish();
    }
}
