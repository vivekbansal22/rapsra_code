package com.example.gns076.rapsra.model;


public class SubCategoryPopUpModel {
    public SubCategoryPopUpModel(String subCategoryId, String subCategoryName,boolean isSelected) {
        this.isSelected = isSelected;
        this.subCategoryId = subCategoryId;
        this.subCategoryName = subCategoryName;
    }

    private boolean isSelected;
    private String subCategoryId;

    public String getSubCategorYId() {
        return subCategoryId;
    }

    public void setSubCategorYId(String subCategorYId) {
        this.subCategoryId = subCategorYId;
    }

    public String getSubCategoryName() {
        return subCategoryName;
    }

    public void setSubCategoryName(String subCategoryName) {
        this.subCategoryName = subCategoryName;
    }

    private String subCategoryName;

    public boolean getSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
