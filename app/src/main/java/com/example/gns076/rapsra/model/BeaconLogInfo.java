package com.example.gns076.rapsra.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class BeaconLogInfo implements Serializable {

    @SerializedName("beaconid")
    private String beaconIdentifier;
    @SerializedName("distance")
    private String distance;
    @SerializedName("logstring")
    private String logString;
    @SerializedName("timestamp")
    private String timestamp;
    @SerializedName("agent_id")
    private String agentID;

    public String getBeaconIdentifier() {
        return beaconIdentifier;
    }

    public void setBeaconIdentifier(String beaconIdentifier) {
        this.beaconIdentifier = beaconIdentifier;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getLogString() {
        return logString;
    }

    public void setLogString(String logString) {
        this.logString = logString;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getAgentID() {
        return agentID;
    }

    public void setAgentID(String agentID) {
        this.agentID = agentID;
    }
}
