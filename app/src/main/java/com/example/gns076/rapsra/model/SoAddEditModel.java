package com.example.gns076.rapsra.model;

import com.google.gson.annotations.SerializedName;

public class SoAddEditModel {
    @SerializedName("sales_order_no")
    public int sales_order_no;
    @SerializedName("first_name")
    public String first_name;
    @SerializedName("last_name")
    public String last_name;
    @SerializedName("email")
    public int email;
    @SerializedName("street1")
    public String street1;
    @SerializedName("street2")
    public String street2;
    @SerializedName("zip")
    public int zip;
    @SerializedName("dealer_id")
    public String dealer_id;
    @SerializedName("store_id")
    public String store_id;
    @SerializedName("agent_id")
    public int agent_id;

    public int getSales_order_no() {
        return sales_order_no;
    }

    public void setSales_order_no(int sales_order_no) {
        this.sales_order_no = sales_order_no;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public int getEmail() {
        return email;
    }

    public void setEmail(int email) {
        this.email = email;
    }

    public String getStreet1() {
        return street1;
    }

    public void setStreet1(String street1) {
        this.street1 = street1;
    }

    public String getStreet2() {
        return street2;
    }

    public void setStreet2(String street2) {
        this.street2 = street2;
    }

    public int getZip() {
        return zip;
    }

    public void setZip(int zip) {
        this.zip = zip;
    }

    public String getDealer_id() {
        return dealer_id;
    }

    public void setDealer_id(String dealer_id) {
        this.dealer_id = dealer_id;
    }

    public String getStore_id() {
        return store_id;
    }

    public void setStore_id(String store_id) {
        this.store_id = store_id;
    }

    public int getAgent_id() {
        return agent_id;
    }

    public void setAgent_id(int agent_id) {
        this.agent_id = agent_id;
    }


}
