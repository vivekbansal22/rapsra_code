package com.example.gns076.rapsra.model;

import com.google.gson.annotations.SerializedName;

public class ChatAgentModel {

    @SerializedName("id")
    private String senderId;

    @SerializedName("name")
    private  String senderName;

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public String getSenderEmail() {
        return senderEmail;
    }

    public void setSenderEmail(String senderEmail) {
        this.senderEmail = senderEmail;
    }

    public String getSenderStatus() {
        return senderStatus;
    }

    public void setSenderStatus(String senderStatus) {
        this.senderStatus = senderStatus;
    }

    @SerializedName("email")
    private String senderEmail;

    @SerializedName("status")
    private String senderStatus;

}
