package com.example.gns076.rapsra.model;

import com.google.gson.annotations.SerializedName;

public class StateModel {

    @SerializedName("id")
    private String stateId;

    public String getStateId() {
        return stateId;
    }

    public void setStateId(String stateId) {
        this.stateId = stateId;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public String getStateShortName() {
        return stateShortName;
    }

    public void setStateShortName(String stateShortName) {
        this.stateShortName = stateShortName;
    }

    @SerializedName("state_name")
    private  String stateName;

    @SerializedName("state_short_name")
    private String stateShortName;
}
