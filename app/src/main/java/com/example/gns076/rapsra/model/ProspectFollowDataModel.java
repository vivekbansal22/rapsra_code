package com.example.gns076.rapsra.model;

import com.google.gson.annotations.SerializedName;

public class ProspectFollowDataModel implements java.io.Serializable {
    @SerializedName("first_name")
    public String first_name;
    @SerializedName("last_name")
    public String last_name;
    @SerializedName("email")
    public String email;

    public ProspectFollowDataModel(String first_name, String last_name, String email, String followDate, String dateNotes, String phoneNo, String id) {
        this.first_name = first_name;
        this.last_name = last_name;
        this.email = email;
        this.followDate = followDate;
        this.dateNotes = dateNotes;
        this.phoneNo = phoneNo;
        this.id = id;
    }

    @SerializedName("FollowDate")
    public String followDate;
    @SerializedName("date_notes")
    public String dateNotes;
    @SerializedName("phone")
    public String phoneNo;
    @SerializedName("id")
    public  String id;

    @SerializedName("city")
    public String city;

    @SerializedName("state")
    public String state;

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFollowDate() {
        return followDate;
    }

    public void setFollowDate(String followDate) {
        this.followDate = followDate;
    }

    public String getDateNotes() {
        return dateNotes;
    }

    public void setDateNotes(String dateNotes) {
        this.dateNotes = dateNotes;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    @Override
    public String toString() {
        return getFirst_name()+" "+getLast_name()+"  "+getEmail()+"  "+ getFollowDate()+"  "+getPhoneNo()+"  "+getDateNotes()+"   "+getId();
    }
}
