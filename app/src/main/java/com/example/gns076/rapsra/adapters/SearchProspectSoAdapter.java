package com.example.gns076.rapsra.adapters;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.gns076.rapsra.R;
import com.example.gns076.rapsra.model.ProspectSoSearchModel;

import java.util.List;

public class SearchProspectSoAdapter extends RecyclerView.Adapter<SearchProspectSoAdapter.prospectsoHolder> {


    private final OnItemClickListener listener;

    public interface OnItemClickListener {
        void onItemClick(ProspectSoSearchModel item);
    }

    private List<ProspectSoSearchModel> listProspectSoSearch;

    public void setListProspectSoSearch(List<ProspectSoSearchModel> listProspectSoSearch) {
        this.listProspectSoSearch = listProspectSoSearch;
    }


    public SearchProspectSoAdapter(List<ProspectSoSearchModel> listProspectSoSearch, OnItemClickListener listener) {
        this.listProspectSoSearch = listProspectSoSearch;
        this.listener = listener;
    }

    @Override
    public SearchProspectSoAdapter.prospectsoHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // inflating recycler item view
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.prospect_so_list_new, parent, false);

        return new SearchProspectSoAdapter.prospectsoHolder(itemView);
    }

    @Override
    public void onBindViewHolder(SearchProspectSoAdapter.prospectsoHolder holder, int position) {

        holder.textViewFirstName.setText(listProspectSoSearch.get(position).getFirst_name());
        holder.textViewLastName.setText(listProspectSoSearch.get(position).getLast_name());
        holder.textViewPhone.setText(listProspectSoSearch.get(position).getPhNo());

        holder.bind(listProspectSoSearch.get(position),listener);
        //     holder.statusCircle.setColorFilter(Color.GREEN);
    }

    @Override
    public int getItemCount() {
        Log.v(UsersRecyclerAdapter.class.getSimpleName(), "" + listProspectSoSearch.size());
        return listProspectSoSearch.size();
    }


    public class prospectsoHolder extends RecyclerView.ViewHolder {

        public TextView textViewFirstName;
        public TextView textViewLastName;
        public TextView textViewPhone;


        public prospectsoHolder(View view) {
            super(view);
            textViewFirstName = view.findViewById(R.id.valueName);
            textViewLastName = view.findViewById(R.id.valueLastname);
            textViewPhone = view.findViewById(R.id.valuePhone);


        }

        public void bind(final ProspectSoSearchModel item, final OnItemClickListener listener) {

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(item);
                }
            });
        }
    }
}
