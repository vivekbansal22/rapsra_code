package com.example.gns076.rapsra.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class BeaconInfoResponse implements  java.io.Serializable{

    @SerializedName("error")
    public boolean error;

    @SerializedName("beacon_info")
    public ArrayList<BeaconModel> beaconInfo = new ArrayList<>();

    @SerializedName("message")
    public String message;

    public ArrayList<BeaconModel> getBeaconInfo() {
        return beaconInfo;
    }

    public void setBeaconInfo(ArrayList<BeaconModel> beaconInfo) {
        this.beaconInfo = beaconInfo;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


}
