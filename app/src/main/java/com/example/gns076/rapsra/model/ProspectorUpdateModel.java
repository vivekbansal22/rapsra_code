package com.example.gns076.rapsra.model;

import com.google.gson.annotations.SerializedName;

public class ProspectorUpdateModel {

    @SerializedName("first_name")
    private String firstName;

    @SerializedName("last_name")
    private String lastName;

    @SerializedName("sales_order_number")
    private String salesOrder;

    @SerializedName("phone")
    private String phoneNumber;

    @SerializedName("email")
    private String emailId;

    @SerializedName("city")
    private String city;

    @SerializedName("street")
    private String street;

    @SerializedName("state")
    private String state;

    @SerializedName("gender")
    private String gender;

    @SerializedName("age")
    private String age;

    @SerializedName("zip")
    private String zipCode;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getSalesOrder() {
        return salesOrder;
    }

    public void setSalesOrder(String salesOrder) {
        this.salesOrder = salesOrder;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getReasonForVisit() {
        return reasonForVisit;
    }

    public void setReasonForVisit(String reasonForVisit) {
        this.reasonForVisit = reasonForVisit;
    }

    public String getContactMethod() {
        return contactMethod;
    }

    public void setContactMethod(String contactMethod) {
        this.contactMethod = contactMethod;
    }

    public String getMerchantCategoryIdString() {
        return merchantCategoryIdString;
    }

    public void setMerchantCategoryIdString(String merchantCategoryIdString) {
        this.merchantCategoryIdString = merchantCategoryIdString;
    }

    public String getSubMerchantCategoryIdString() {
        return subMerchantCategoryIdString;
    }

    public void setSubMerchantCategoryIdString(String subMerchantCategoryIdString) {
        this.subMerchantCategoryIdString = subMerchantCategoryIdString;
    }

    public String getIsProspectorPlanSold() {
        return isProspectorPlanSold;
    }

    public void setIsProspectorPlanSold(String isProspectorPlanSold) {
        this.isProspectorPlanSold = isProspectorPlanSold;
    }

    @SerializedName("reason_for_visit")
    private String reasonForVisit;

    @SerializedName("contact_method")
    private String contactMethod;

    @SerializedName("merchant_category_id")
    private String merchantCategoryIdString;

    @SerializedName("merchant_sub_category_ids")
    private String subMerchantCategoryIdString;

    @SerializedName("is_plan_sold")
    private String isProspectorPlanSold;



}
