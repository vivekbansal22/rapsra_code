package com.example.gns076.rapsra.activities;

import android.content.Intent;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.example.gns076.rapsra.R;
import com.example.gns076.rapsra.adapters.BeaconRecycleAdapter;
import com.example.gns076.rapsra.model.AppPreference;
import com.example.gns076.rapsra.model.BeaconInfo;
import com.google.android.gms.nearby.Nearby;
import com.google.android.gms.nearby.messages.BleSignal;
import com.google.android.gms.nearby.messages.Distance;
import com.google.android.gms.nearby.messages.Message;
import com.google.android.gms.nearby.messages.MessageFilter;
import com.google.android.gms.nearby.messages.MessageListener;
import com.google.android.gms.nearby.messages.Strategy;
import com.google.android.gms.nearby.messages.SubscribeOptions;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;

import java.util.ArrayList;
import java.util.List;

public class NearbyActivity extends YouTubeBaseActivity {

    private AppPreference mAppPreference;
    private MessageListener mMessageListener;
    private static final String TAG = NearbyActivity.class.getSimpleName();
    private Message mMessage;
    private String token;

    private BeaconInfo beaconInfo= new BeaconInfo();
    private List<BeaconInfo> beaconList = new ArrayList<>();
    private RecyclerView recyclerView;
    private BeaconRecycleAdapter bAdapter;


    YouTubePlayerView youTubePlayerView;
    YouTubePlayer activeYouTubePlayer = null;
    // Button play;
    YouTubePlayer.OnInitializedListener onInitializedListener = null;

    private String strForVideoPlaying= "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nearby);
        Intent dasInt=this.getIntent();
        Bundle bundle =dasInt.getExtras();
        token=bundle.getString("Token");

        youTubePlayerView = (YouTubePlayerView) findViewById(R.id.youtube1);

        youTubePlayerView.setVisibility(View.GONE);


        mMessageListener = new MessageListener() {
            @Override
            public void onFound(Message message) {
                Log.i(TAG, "Message found: " + message);
                Log.i(TAG, "Message string: " + new String(message.getContent()));
                Log.i(TAG, "Message namespaced type: " + message.getNamespace() +
                        "/" + message.getType());


                youTubePlayerView.setVisibility(View.VISIBLE);

                if(message.getType().equals("video")|| message.getType().equals("newvideo"))
                {
                    Bundle bundle = new Bundle();
                    String youtubeVideoString = new String(message.getContent());
                    String onlyId = youtubeVideoString.replace("https://www.youtube.com/watch?v=", "");
                    strForVideoPlaying=onlyId;


                    Intent intent = new Intent(getApplicationContext(),YoutubeActivity.class);
                    bundle.putString("Token",token );

                    Log.i(TAG,"Starting activity");

                    if(activeYouTubePlayer != null && activeYouTubePlayer.isPlaying()) {
                        activeYouTubePlayer.pause();
                        activeYouTubePlayer.loadVideo(strForVideoPlaying);
                    }
                    else {
                        onInitializedListener = new YouTubePlayer.OnInitializedListener() {

                            @Override
                            public void onInitializationSuccess(YouTubePlayer.Provider provider,final YouTubePlayer youTubePlayer, boolean b) {
                                activeYouTubePlayer = youTubePlayer;
                                youTubePlayer.loadVideo(strForVideoPlaying);

                                youTubePlayer.setPlayerStateChangeListener(new YouTubePlayer.PlayerStateChangeListener() {
                                    @Override
                                    public void onLoading() {

                                    }

                                    @Override
                                    public void onLoaded(String s) {

                                    }

                                    @Override
                                    public void onAdStarted() {

                                    }

                                    @Override
                                    public void onVideoStarted() {

                                    }

                                    @Override
                                    public void onVideoEnded() {
                                        youTubePlayerView.setVisibility(View.INVISIBLE);
                                        youTubePlayer.release();
                                    }

                                    @Override
                                    public void onError(YouTubePlayer.ErrorReason errorReason) {

                                    }
                                });
                            }



                            @Override
                            public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {

                            }
                        };


                        youTubePlayerView.initialize(PlayerConfig.API_KEY, onInitializedListener);
                    }

                }

            }


            @Override
            public void onBleSignalChanged(final Message message, final BleSignal bleSignal) {
                Log.i(TAG, "Message: " + message + " has new BLE signal information: " + bleSignal);

            }

            @Override
            public void onDistanceChanged(final Message message, final Distance distance) {
                Log.i(TAG, "Distance changed, message: " + message + ", new distance: " + distance);

            }


            @Override
            public void onLost(Message message) {
                Log.d(TAG, "Lost sight of message: " + new String(message.getContent()));

            }




        };


    }

    @Override
    protected void onStart() {
        super.onStart();
        SubscribeOptions options = new SubscribeOptions.Builder() .setStrategy(Strategy.BLE_ONLY)
                .setFilter(
                        new MessageFilter.Builder()
                                .includeNamespacedType("gnsbeacon-1", "")
                                .build()
                )
                .build();
        //beacontest-222719
        //gnsbeacon-1
        Nearby.getMessagesClient(this).subscribe(mMessageListener,options);

    }

    @Override
    protected void onStop() {
        Nearby.getMessagesClient(this).unsubscribe(mMessageListener);
        super.onStop();
    }


}
