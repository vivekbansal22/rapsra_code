package com.example.gns076.rapsra.activities;


import com.example.gns076.rapsra.model.BeaconInfoResponse;
import com.example.gns076.rapsra.model.BeaconLogResponse;
import com.example.gns076.rapsra.model.CalendarDataResponse;
import com.example.gns076.rapsra.model.ChatApiResponse;
import com.example.gns076.rapsra.model.DeleteTaskResponse;
import com.example.gns076.rapsra.model.LoginResponse;
import com.example.gns076.rapsra.model.LogoutResponse;
import com.example.gns076.rapsra.model.MerchandiseBeaconInfoResponse;
import com.example.gns076.rapsra.model.MerchandizeCateoryApiResponse;
import com.example.gns076.rapsra.model.MyRankResponse;
import com.example.gns076.rapsra.model.NewsCrawler;
import com.example.gns076.rapsra.model.NextStepCategoryApiResponse;
import com.example.gns076.rapsra.model.ProspectSoResponse;
import com.example.gns076.rapsra.model.ProspectorApiResponse;
import com.example.gns076.rapsra.model.ProspectorUpdateApiResponse;
import com.example.gns076.rapsra.model.ProspectsTableResponse;
import com.example.gns076.rapsra.model.SoResponse;
import com.example.gns076.rapsra.model.SetProspectsFollowUpResponse;
import com.example.gns076.rapsra.model.SpinnerStateApiResponse;
import com.example.gns076.rapsra.model.TaskResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import androidx.appcompat.app.AppCompatActivity;
interface APIInterface {


    @FormUrlEncoded
    @POST("/apis/api.php?apicall=login")
    Call<LoginResponse> checkUser(@Field("username") String username,
                                  @Field("password") String password);

    @FormUrlEncoded
    @POST("/apis/api.php?apicall=logout")
    Call<LogoutResponse> logoutUser(@Field("agent_id") String agentid);

    @FormUrlEncoded
    @POST("/apis/api.php?apicall=getcrawler")
    Call<NewsCrawler> getcrawler(@Field("dealer_id") String dealer_id,
                                 @Field("current_date_time")String currentTime);


    @FormUrlEncoded
    @POST("/apis/api.php?apicall=getSearchRecords")
    Call<ProspectSoResponse> getProspectSoDetails(@Field("dealer_id") String dealer_id, @Field("search_data") String search_data);

    @FormUrlEncoded
    @POST("/apis/api.php?apicall=getProspectsFollowUps")
    Call<CalendarDataResponse> getCalendarData(@Field("agent_id") String agent_id,
                                               @Field("dealer_id") String dealerId,
                                               @Field("month") String month,
                                               @Field("year") String year);



    @FormUrlEncoded
    @POST("/apis/api.php?apicall=getProspectsFollowUpsInfo")
    Call<ProspectsTableResponse>  getProspectsFollowUpsInfo(@Field("ref_id") String ref_id);


    @FormUrlEncoded
    @POST("/apis/api.php?apicall=setProspectsFollowUps")
    Call<SetProspectsFollowUpResponse> setProspectsFollowUps(@Field("ref_id") String refId,
                                                             @Field("selectedDate") String selectedDate,
                                                             @Field("date_notes") String dateNotes,
                                                             @Field("prospectNextFollowDate") String prospect_NextFollow_Date,
                                                             @Field("current_time") String current_time);

    @FormUrlEncoded
    @POST("/apis/api.php?apicall=setstatus")
    Call<LogoutResponse> setstatus(@Field("agent_id") String agent_id,
                                   @Field("status") String status,
                                   @Field("dealer_id") String dealer_id,
                                   @Field("store_id") String store_id);

    @FormUrlEncoded
    @POST("/apis/api.php?apicall=setsalesorder")
    Call<SoResponse> setsalesorder(@Field("sales_order_no") String sales_order_no,
                                   @Field("first_name") String first_name,
                                   @Field("last_name") String last_name,
                                   @Field("phone") String phone,
                                   @Field("email") String email,
                                   @Field("street1") String street1,
                                   @Field("street2") String street2,
                                   @Field("city") String city,
                                   @Field("state") String state,
                                   @Field("zip") String zip,
                                   @Field("dealer_id") String dealer_id,
                                   @Field("store_id") String store_id,
                                   @Field("agent_id") String agent_id,
                                   @Field("soFollowDate") String soFollowDate,
                                   @Field("notes") String notes,
                                   @Field("merchant_category") String merchant_category);

    @FormUrlEncoded
    @POST("/apis/api.php?apicall=setbeaconlogs")
    Call<BeaconLogResponse> setBeaconlogs(@Field("beaconid") String beaconId,
                                          @Field("distance") String distance,
                                          @Field("logstring") String logString,
                                          @Field("agent_id") String agentId,
                                          @Field("timestamp") String timeStamp);

    @FormUrlEncoded
    @POST("/apis/api.php?apicall=setprospects")
    Call<SoResponse> setprospects(@Field("first_name") String first_name,
                                  @Field("last_name") String last_name,
                                  @Field("phone") String phone,
                                  @Field("email") String email,
                                  @Field("street1") String street1,
                                  @Field("street2") String street2,
                                  @Field("city") String city,
                                  @Field("state") String state,
                                  @Field("zip") String zip,
                                  @Field("contact_date") String contact_date,
                                  @Field("dealer_id") String dealer_id,
                                  @Field("store_id") String store_id,
                                  @Field("agent_id") String agent_id,
                                  @Field("prospectFollowDate") String prospectFollowDate,
                                  @Field("notes") String notes,
                                  @Field("merchant_category") String merchant_category ,
                                  @Field("next_step") String next_step );

    @FormUrlEncoded
    @POST("/apis/api.php?apicall=getBeaconInfo")
    Call<BeaconInfoResponse> getBeaconInfo(@Field("Beacon_Id") String beacon_id, @Field("User_Role") String user_role);

    @FormUrlEncoded
    @POST("/apis/api.php?apicall=getBeaconInfo")
    Call<BeaconInfoResponse> getFloorMap(@Field("Beacon_Id") String beacon_id, @Field("User_Role") String user_role);

    @FormUrlEncoded
    @POST("/apis/api.php?apicall=getmerchandizecategory")
    Call<MerchandizeCateoryApiResponse> getMerchandizeCategory(@Field("id") String id);

    @FormUrlEncoded
    @POST("/apis/api.php?apicall=getnextstep")
    Call<NextStepCategoryApiResponse> getNextStep(@Field("id") String id);

    @FormUrlEncoded
    @POST("/apis/api.php?apicall=getuserstatus")
    Call<ChatApiResponse> getChatData(@Field("dealer_id") String dealer_id);

    @FormUrlEncoded
    @POST("/apis/api.php?apicall=getmerchandisebeacons")
    Call<MerchandiseBeaconInfoResponse> getMerchandiseBeaconInfo(@Field("user_id") String user_role);



    @FormUrlEncoded
    @POST("/apis/api.php?apicall=getrank")
    Call<MyRankResponse> getRank(@Field("dealer_id") String dealer_id);

    @FormUrlEncoded
    @POST("/apis/api.php?apicall=getmerchandizecategory")
    Call<MerchandizeCateoryApiResponse> getMerchandizeCategories(@Field("dealer_id") String dealer_id);

    @FormUrlEncoded
    @POST("/apis/api.php?apicall=setprospects")
    Call<ProspectorApiResponse> setProspectsData(@Field("id") String prosepect_id,
                                                 @Field("store_id") String store_id,
                                                 @Field("dealer_id") String dealer_id,
                                                 @Field("agent_id") String agent_id,
                                                 @Field("first_name") String firstname,
                                                 @Field("last_name") String lastName,
                                                 @Field("phone") String phoneNo,
                                                 @Field("email") String emailId,
                                                 @Field("street") String street,
                                                 @Field("city") String cityName,
                                                 @Field("state") String stateName,
                                                 @Field("zip") String zipCode,
                                                 @Field("age") String age,
                                                 @Field("gender") String gender,
                                                 @Field("reason_for_visit") String reasonForVisit,
                                                 @Field("contact_method") String contactMethod,
                                                 @Field("merchant_category_id") String merchantCatId,
                                                 @Field("merchant_sub_cat_id") String merchantSubCatId,
                                                 @Field("plan_sold") String planSold,
                                                 @Field("sales_order_number") String salesOrderNumber);

    @FormUrlEncoded
    @POST("/apis/api.php?apicall=getstates")
    Call<SpinnerStateApiResponse> getSpinnerForState(@Field("store_id") String store_id);


    @FormUrlEncoded
    @POST("/apis/api.php?apicall=savetask")
    Call<TaskResponse> saveTask(@Field("prospect_id")String prospectorId,
                                @Field("task_name") String taskName,
                                @Field("task_date") String taskDate,
                                @Field("action") String action,
                                @Field("contact_method") String contactMethod,
                                @Field("notes") String notes);

    @FormUrlEncoded
    @POST("/apis/api.php?apicall=getProspectorData")
    Call<ProspectorUpdateApiResponse> getProspectorData(@Field("prospector_id") String prospectorId);

    @FormUrlEncoded
    @POST("/apis/api.php?apicall=deletetask")
    Call<DeleteTaskResponse> deleteTask(@Field("task_id")String taskId);

   /* @GET("/api/users?")
    Call<UserList> doGetUserList(@Query("page") String page);*/

   /* @FormUrlEncoded
    @POST("/api/users?")
    Call<UserList> doCreateUserWithField(@Field("name") String name, @Field("job") String job);*/
}
