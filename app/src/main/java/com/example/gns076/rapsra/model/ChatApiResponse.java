package com.example.gns076.rapsra.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ChatApiResponse {

    @SerializedName("error")
    private boolean error;

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<ChatAgentModel> getChatAgentModelArrayList() {
        return chatAgentModelArrayList;
    }

    public void setChatAgentModelArrayList(ArrayList<ChatAgentModel> chatAgentModelArrayList) {
        this.chatAgentModelArrayList = chatAgentModelArrayList;
    }

    @SerializedName("message")
    private String message;

    @SerializedName("data")
    private ArrayList<ChatAgentModel> chatAgentModelArrayList;
}
