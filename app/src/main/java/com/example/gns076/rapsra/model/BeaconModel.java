package com.example.gns076.rapsra.model;

import com.google.gson.annotations.SerializedName;

public class BeaconModel implements  java.io.Serializable{

    @SerializedName("beacon_uid")
    public String beaconId;

    @SerializedName("role")
    public String role;

    @SerializedName("beacon_attachment")
    public String beaconAttachment;


    public String getBeaconId() {
        return beaconId;
    }

    public void setBeaconId(String beaconId) {
        this.beaconId = beaconId;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getBeaconAttachment() {
        return beaconAttachment;
    }

    public void setBeaconAttachment(String beaconAttachment) {
        this.beaconAttachment = beaconAttachment;
    }
}
