package com.example.gns076.rapsra.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ProspectSoResponse {

    @SerializedName("error")
    private boolean error;


    @SerializedName("message")
    private String message;

    @SerializedName("followups")
    private ArrayList<ProspectSoSearchModel> followups =new ArrayList<>();

  /*  @SerializedName("sales_order")
    public String sales_order_no;
    @SerializedName("first_name")
    public String first_name;
    @SerializedName("last_name")
    public String last_name;
    @SerializedName("email")
    public String email;
    @SerializedName("phoneNo")
    public String phNo;
    @SerializedName("street1")
    public String address;
    @SerializedName("street2")
    public String unit;
    @SerializedName("zip")
    public int zip;
    @SerializedName("dealer_id")
    public String dealer_id;
    @SerializedName("city")
    public String city;
    @SerializedName("state")
    public String state;
    @SerializedName("store_id")
    public String store_id;
    @SerializedName("agent_id")
    public String agent_id;
    @SerializedName("notes")
    public String notes;
    @SerializedName("merchant_category")
    public String merchant_category;
    @SerializedName("type")
    public String type;*/

    public ArrayList<ProspectSoSearchModel> getFollowups() {
        return followups;
    }

    public void setFollowups(ArrayList<ProspectSoSearchModel> followups) {
        this.followups = followups;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
