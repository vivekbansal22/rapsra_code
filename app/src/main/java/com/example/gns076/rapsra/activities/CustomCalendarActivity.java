package com.example.gns076.rapsra.activities;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.example.gns076.rapsra.R;
import com.example.gns076.rapsra.model.AppPreference;

public class CustomCalendarActivity extends BaseActivity {
    private AppPreference mAppPreference;

    private static final String TAG = CustomCalendarActivity.class.getSimpleName();
    private APIInterface apiInterface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAppPreference = new AppPreference(this);
        setContentView(R.layout.activity_custom_calendar);
        CalendarCustomView mView = (CalendarCustomView)findViewById(R.id.custom_calendar);
    }

    @Override
    protected void onStart() {
        super.onStart();
        setContentView(R.layout.activity_custom_calendar);
        CalendarCustomView mView = (CalendarCustomView)findViewById(R.id.custom_calendar);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater= getMenuInflater();
        inflater.inflate(R.menu.menu_home,menu);
        menu.removeItem(R.id.scheduled_followups);
        menu.removeItem(R.id.logout);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        return super.onOptionsItemSelected(item);
    }
}
