package com.example.gns076.rapsra.activities;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.text.style.RelativeSizeSpan;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;

import com.example.gns076.rapsra.R;
import com.example.gns076.rapsra.helpers.InputValidation;
import com.example.gns076.rapsra.model.AppPreference;
import com.example.gns076.rapsra.model.DeleteTaskResponse;
import com.example.gns076.rapsra.model.TaskModel;
import com.example.gns076.rapsra.model.TaskResponse;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TaskActivity extends BaseActivity implements View.OnClickListener{
    private AppPreference mAppPreference;

    private Button addTask;
    private TableLayout tableTask;
    private EditText taskName;
    private TextView taskDate;
    private Spinner taskAction;
    private Spinner taskContactMethod;
    private EditText taskNote;
    private TextView displayprospectPhoneNo;
    private TextView displayFirstLastName;

    private Button saveButton;
    private Button clearTaskButton;
    private Button menuButton;
    private Button returnButton;
    private Button taskSelectedButton;
    private Button wishListButtonTaskPage;
    private Button bothTaskBasketButtonTaskPage;
    private Button editProspect;
    private Button virtualAssist;
    private Button deleteTaskButton;

    private int mYear, mMonth, mDay;

    private String prospectorId;

    ArrayList<TaskModel> taskList = new ArrayList<TaskModel>();
    TaskModel taskModel;
    private InputValidation inputValidation;
    String prpspectorId;
    String prospectFirstName;
    String prospectLastName;
    String prospectPhNo;
    private  List<String> actionItems;
    private  List<String> contactMethods;

    boolean flag = false;
    List<TableRow> tableRowList = new ArrayList<>();
    int counterTableRow = 0;
    int rowCount = 14;

    TableRow tbrow;
    TableRow tbrowHeader;

    private int taskId;
    private int tableRowId;


    private String token;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_x);

        mAppPreference = new AppPreference(this);
        token=mAppPreference.getToken();

        inputValidation =  new InputValidation(this);



        taskName  = (EditText)findViewById(R.id.taskName);
        taskDate = (TextView)findViewById(R.id.textView9);
        taskAction = (Spinner)findViewById(R.id.taskActionSpinner);
        taskContactMethod =  (Spinner)findViewById(R.id.taskContactMethod);
        taskNote = (EditText)findViewById(R.id.taskNotes);

        /* changed from activity_task to activity_task_x*/

        prospectorId =  getIntent().getExtras().getString("prospect_id");
        prospectFirstName =  getIntent().getExtras().getString("prospector_firstname");
        prospectLastName =  getIntent().getExtras().getString("prospector_lastname");
        prospectPhNo =  getIntent().getExtras().getString("prospector_phone");



        displayprospectPhoneNo =  (TextView)findViewById(R.id.prospectPhNo_x);
        displayFirstLastName = (TextView)findViewById(R.id.prospectFirstLastName_x);
        editProspect = (Button)findViewById(R.id.editProspectButton_x);


        displayprospectPhoneNo.setText(prospectPhNo);
        displayFirstLastName.setText(prospectFirstName+" "+prospectLastName);


        /* changed from activity_task to activity_task_x*/


        addTask = (Button)findViewById(R.id.addTaskButton);
        tableTask = (TableLayout)findViewById(R.id.task_table);
        //   saveButton = (Button)findViewById(R.id.button6);
        // clearTaskButton = (Button)findViewById(R.id.button7);
        menuButton =  (Button)findViewById(R.id.taskmenu);
        //    virtualAssist = (Button)findViewById(R.id.taskVirtualassist);
        returnButton =  (Button)findViewById(R.id.taskReturnButton);
        taskSelectedButton = (Button)findViewById(R.id.taskSelectedButton);
        wishListButtonTaskPage = (Button)findViewById(R.id.wishlist_button_task_page_x);
        bothTaskBasketButtonTaskPage = (Button)findViewById(R.id.task_wishlist_both_button_task_page_x) ;
        deleteTaskButton = (Button)findViewById(R.id.deleteTaskButton);

        /* changed from activity_task to activity_task_x*/



        /* changed from activity_task to activity_task_x*/

        //setDataForSpinner();
        setDataForSpinnerNew();

        addTaskHeaderToTable();
        addTaskRowToTableNew();

        setListeners();

        menuButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Creating the instance of PopupMenu
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);


                PopupMenu popup = new PopupMenu(TaskActivity.this, menuButton);
                //Inflating the Popup using xml file

                menuButton.setBackgroundResource(R.drawable.exit_menu);

                Menu gamesMenu =popup.getMenu();


                popup.getMenuInflater()
                        .inflate(R.menu.menu_home, gamesMenu);
                gamesMenu.removeItem(R.id.logout);

                gamesMenu.removeItem(R.id.add_edit_so);
                gamesMenu.removeItem(R.id.scheduled_followups);


                for(int i = 0; i < gamesMenu.size(); i++) {
                    MenuItem item = gamesMenu.getItem(i);


                    SpannableString spanString = new SpannableString(gamesMenu.getItem(i).getTitle().toString());
                    int end = spanString.length();

                    //   spanString.setSpan(new RelativeSizeSpan(1.5f), 0, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    spanString.setSpan(new RelativeSizeSpan(0.04f), 0, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    item.setTitle(spanString);

                }


                popup.setOnDismissListener(new PopupMenu.OnDismissListener() {
                    @Override
                    public void onDismiss(PopupMenu popupMenu) {
                        menuButton.setBackgroundResource(R.drawable.menu);
                    }
                });


                //registering popup with OnMenuItemClickListener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {

                        Intent intentButtonClick;
                        switch (item.getItemId()){

                            case R.id.main_page:
                                intentButtonClick = new Intent(getApplicationContext(), SalesRadarActivityNew.class);
                                startActivity(intentButtonClick);
                                break;

                         /*   case R.id.add_edit_so:
                                intentButtonClick = new Intent(getApplicationContext(), AddEditSoActivity.class);
                                startActivity(intentButtonClick);
                                break;*/

                            case R.id.add_edit_prospect:
                                intentButtonClick = new Intent(getApplicationContext(), ProspectorActivity.class);
                                intentButtonClick.putExtra("Token",mAppPreference.getToken() );
                                startActivity(intentButtonClick);
                                break;

                            case R.id.rap_gamify:
                                intentButtonClick = new Intent(getApplicationContext(), GamificationActivity.class);
                                startActivity(intentButtonClick);
                                break;
                            case R.id.my_rank:
                                intentButtonClick = new Intent(getApplicationContext(), WrittenActivity.class);
                                startActivity(intentButtonClick);
                                break;
                            case R.id.chat_menu:
                                intentButtonClick = new Intent(getApplicationContext(), ChatActivity.class);
                                startActivity(intentButtonClick);
                                break;

                            case R.id.ar_mode:
                                intentButtonClick = new Intent(getApplicationContext(), ArViewActivityNew.class);
                                startActivity(intentButtonClick);
                                break;

                            case R.id.floor_map_mode:
                                intentButtonClick = new Intent(getApplicationContext(), FloorActivity.class);
                                startActivity(intentButtonClick);
                                break;

                            case R.id.fun_games:
                                intentButtonClick = new Intent(getApplicationContext(), FunGames.class);
                                startActivity(intentButtonClick);
                                break;

                        }
                        return true;
                    }
                });

                popup.show(); //showing popup menu


            }
        });


    }

    public void addTaskHeaderToTable(){

        TableRow.LayoutParams wrapWrapTableRowParams = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.MATCH_PARENT);
        int[] fixedColumnWidths = new int[]{20, 10, 20, 10, 50};
        int fixedHeaderHeight = 60;

        tbrowHeader = new TableRow(getApplicationContext());
        //     tbrowHeader.setLayoutParams(wrapWrapTableRowParams);
    /*    tbrowHeader.addView(makeTableHeaderWithText("Task Name", fixedColumnWidths[0], fixedHeaderHeight));
        tbrowHeader.addView(makeTableHeaderWithText("Follow Up Date", fixedColumnWidths[1], fixedHeaderHeight));
        tbrowHeader.addView(makeTableHeaderWithText("Action", fixedColumnWidths[2], fixedHeaderHeight));
        tbrowHeader.addView(makeTableHeaderWithText("Contact Method", fixedColumnWidths[3], fixedHeaderHeight));
        tbrowHeader.addView(makeTableHeaderWithText("Notes", fixedColumnWidths[4], fixedHeaderHeight));
        tableTask.addView(tbrowHeader);
*/
        tbrowHeader.setLayoutParams(wrapWrapTableRowParams);

        tbrowHeader.setBackgroundColor(getResources().getColor(R.color.colorForActionBar));


        tbrowHeader.addView(getTextViewForColumnText("Task Name", 20));
        tbrowHeader.addView(getTextViewForColumnText("Follow Up Date", 8));
        tbrowHeader.addView(getTextViewForColumnText("Action", 30));
        tbrowHeader.addView(getTextViewForColumnText("Contact Method", 10));
        tbrowHeader.addView(getTextViewForColumnText("Notes", 150));


        /*tbrowHeader.addView(getTextViewWithButton("Task Name", 90,true),new TableRow.LayoutParams(1));
        tbrowHeader.addView(getTextViewWithButton("Follow Up Date", 80,true),new TableRow.LayoutParams(2));
        tbrowHeader.addView(getTextViewWithButton("Action", 60,true),new TableRow.LayoutParams(3));
        tbrowHeader.addView(getTextViewWithButton("Contact Method", 90,true),new TableRow.LayoutParams(4));
        tbrowHeader.addView(getTextViewWithButton("Notes", 150,false),new TableRow.LayoutParams(5));*/
        tableTask.addView(tbrowHeader);
    }


    /*public void addTaskRowToTable(){

        for(int i=0; i<rowCount; i++){

            TableRow tbrow = new TableRow(getApplicationContext());

            tbrow.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,TableRow.LayoutParams.WRAP_CONTENT));
            // tbrow.setBackgroundColor(getResources().getColor(R.color.colorForMeetingStatus));
            tbrow.setBackground(getResources().getDrawable(R.drawable.task_basket_row_image));
            // tbrow.setBackgroundColor(getResources().getColor(R.color.tasktablerowcolor));
            tableTask.addView(tbrow);
            tableRowList.add(tbrow);
        }


    }*/

    public void addTaskRowToTableNew(){



        for(int i=0; i<rowCount; i++){

            tbrow = new TableRow(getApplicationContext());

            tbrow.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT));

            tbrow.setBackground(getResources().getDrawable(R.drawable.task_basket_row_image));

            tableTask.addView(tbrow);

            tableRowList.add(tbrow);
        }


        //   tableRelativeLayout.addView(tableTask);
    }


    public void addrowDataToTableNew(final TaskModel tm, List<TableRow> tableRowList){
        //  TableRow tbrow = new TableRow(getApplicationContext());;
        if(counterTableRow < rowCount) {

            tbrow = tableRowList.get(counterTableRow);
            tbrow.setId(counterTableRow);
            tbrow.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,TableRow.LayoutParams.WRAP_CONTENT));
            tbrow.addView(getTextView(tm.getTaskName(), R.color.colorForActionBar, 20));
            tbrow.addView(getTextView(tm.getTaskDate(), R.color.colorForActionBar, 8));
            tbrow.addView(getTextView(tm.getTaskAction(), R.color.colorForActionBar, 30));
            tbrow.addView(getTextView(tm.getTaskContactMethod(), R.color.colorForActionBar, 10));
            tbrow.addView(getTextView(tm.getTaskNotes(), R.color.colorForActionBar, 150));
            //    tableTask.addView(tbrow);

            //    tableDataList.add(tm);  // new code

            //      drawTable();


        }

        else{

            //     tableDataList.add(tm);


            tbrow = new TableRow(getApplicationContext());
            tbrow.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,TableRow.LayoutParams.WRAP_CONTENT));
            tbrow.setBackground(getResources().getDrawable(R.drawable.task_basket_row_image));
            tbrow.addView(getTextView(tm.getTaskName(), R.color.colorForActionBar,20));
            tbrow.addView(getTextView(tm.getTaskDate(), R.color.colorForActionBar,8));
            tbrow.addView(getTextView(tm.getTaskAction(), R.color.colorForActionBar,30));
            tbrow.addView(getTextView(tm.getTaskContactMethod(), R.color.colorForActionBar,10));
            tbrow.addView(getTextView(tm.getTaskNotes(), R.color.colorForActionBar,150));
            tbrow.setId(counterTableRow);
            tableTask.addView(tbrow);
            tableRowList.add(tbrow);

            /* New code*/
           /* addTaskHeaderToTable();
            for(TaskModel taskModel: tableDataList) {

                tbrow = new TableRow(getApplicationContext());
                tbrow.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT));
                tbrow.setBackground(getResources().getDrawable(R.drawable.task_basket_row_image));
                tbrow.addView(getTextView(taskModel.getTaskName(), R.color.colorForActionBar, 20));
                tbrow.addView(getTextView(taskModel.getTaskDate(), R.color.colorForActionBar, 10));
                tbrow.addView(getTextView(taskModel.getTaskAction(), R.color.colorForActionBar, 30));
                tbrow.addView(getTextView(taskModel.getTaskContactMethod(), R.color.colorForActionBar, 10));
                tbrow.addView(getTextView(taskModel.getTaskNotes(), R.color.colorForActionBar, 100));
                tbrow.setId(counterTableRow);
                tableTask.addView(tbrow);
                tableRowList.add(tbrow);
            }*/
            /* New code*/



        }

        tbrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //   Toast.makeText(getApplicationContext(), "Clicked on row" + view.getId(), Toast.LENGTH_LONG).show();
                tbrow = (TableRow)view;
                setTaskRowDetails(tm);
                //      tableTask.removeViewAt(view.getId());

            }
        });

        counterTableRow++;
    }

    public void setTaskRowDetails(TaskModel tm){


        taskId = tm.getTaskId();
        taskName.setText(tm.getTaskName());
        taskDate.setText(tm.getTaskDate());

        for(int i=0; i < actionItems.size();i++){

            if(actionItems.get(i).equalsIgnoreCase(tm.getTaskAction())){
                taskAction.setSelection(i);
                break;
            }

        }

        for(int i=0; i < contactMethods.size();i++){

            if(contactMethods.get(i).equalsIgnoreCase(tm.getTaskContactMethod())){
                taskContactMethod.setSelection(i);
                break;
            }

        }

        taskNote.setText(tm.getTaskNotes());


    }


    public TextView makeTableRowWithText(String text, int widthInPercentOfScreenWidth, int fixedHeightInPixels,int textColor) {
        TextView recyclableTextView = new TextView(getApplicationContext());
        int screenWidth = getResources().getDisplayMetrics().widthPixels;
        recyclableTextView.setText(text);
        recyclableTextView.setTextColor(textColor);
        recyclableTextView.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 15);
        recyclableTextView.setGravity(Gravity.CENTER);
        recyclableTextView.setWidth(widthInPercentOfScreenWidth * screenWidth / 100);
        recyclableTextView.setHeight(fixedHeightInPixels);
        return recyclableTextView;
    }


    public TextView makeTableHeaderWithText(String text, int widthInPercentOfScreenWidth, int fixedHeightInPixels) {
        TextView recyclableTextView = new TextView(getApplicationContext());
        int screenWidth = getResources().getDisplayMetrics().widthPixels;
        recyclableTextView.setText(text);
        recyclableTextView.setTextColor(Color.WHITE);
        recyclableTextView.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 15);
        recyclableTextView.setBackgroundColor(getResources().getColor(R.color.colorForActionBar));
        recyclableTextView.setTypeface(null, Typeface.BOLD);
        recyclableTextView.setGravity(Gravity.CENTER);
        recyclableTextView.setWidth(widthInPercentOfScreenWidth * screenWidth / 100);
        recyclableTextView.setHeight(fixedHeightInPixels);
        return recyclableTextView;
    }

    public LinearLayout getTextViewWithButton(String text, int width, boolean scrollButton){
        LinearLayout linearLayout =  new LinearLayout(getApplicationContext());
        linearLayout.setOrientation(LinearLayout.HORIZONTAL);
        LinearLayout.LayoutParams linearLayoutParams = new LinearLayout.LayoutParams(
                android.widget.LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        linearLayout.setLayoutParams(linearLayoutParams);
        //  linearLayout.set


        TextView textView = new TextView(getApplicationContext());

        //   textView.setLayoutParams(new TableRow.LayoutParams(width, 60, 1f));
        //     textView.setLayoutParams(new TableLayout.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, TableLayout.LayoutParams.WRAP_CONTENT, 1f));
        textView.setText(text);
        textView.setTextColor(Color.WHITE);
        // textView.setBackgroundColor(getResources().getColor(R.color.colorForActionBar));
        textView.setTypeface(null, Typeface.BOLD);
        textView.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 13);
        textView.setGravity(Gravity.CENTER);
        textView.setWidth(width);
        textView.setHeight(55);

        linearLayout.addView(textView);

        if(scrollButton) {

            Button sortButton = new Button(getApplicationContext());
            //   sortButton.setLayoutParams(new LinearLayout.LayoutParams(20,10));
            // sortButton.setWidth(30);
            //   sortButton.setHeight(20);

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    15,
                    5
            );

            params.setMargins(2, 28, 2, 20);

            sortButton.setBackground(getResources().getDrawable(R.drawable.sort_triangle));
            sortButton.setGravity(Gravity.RIGHT);
            //    sortButton.

            //  linearLayoutParams.setMargins(2,10,2,10);
            linearLayout.addView(sortButton, params);
        }
        return linearLayout;
    }


    public TextView getTextViewForColumnText(String text, int width){
        TextView textView = new TextView(getApplicationContext());

        //   textView.setLayoutParams(new TableRow.LayoutParams(width, 60, 1f));
        //     textView.setLayoutParams(new TableLayout.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, TableLayout.LayoutParams.WRAP_CONTENT, 1f));
        textView.setText(text);
        textView.setTextColor(Color.WHITE);
        // textView.setBackgroundColor(getResources().getColor(R.color.colorForActionBar));
        textView.setTypeface(null, Typeface.BOLD);
        textView.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 15);
        textView.setGravity(Gravity.CENTER);
        textView.setWidth(width);
        return textView;
    }


    public TextView getTextView(String text,int textColor, int width){
        TextView textView = new TextView(getApplicationContext());

        textView.setLayoutParams(new TableRow.LayoutParams(width, TableRow.LayoutParams.WRAP_CONTENT, 1f));
        textView.setText(text);
        textView.setTextColor(textColor);
        textView.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 15);

        //    textView.setBackground(getResources().getDrawable(R.drawable.information_field));
        //textView.setBackgroundColor(getResources().getDrawable(R.drawable.information_field));
        textView.setGravity(Gravity.CENTER);
        return textView;
    }

    public void onClickTaskDate(View v) {

        //   if (v == prospectFollowDate) {

        // Get Current Date
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog = new DatePickerDialog(this,R.style.CalendarDialogTheme,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {

                        CharSequence strDate = null;
                        c.set(year,monthOfYear,dayOfMonth);
                        Date chosenDate =  c.getTime();
                        //Date d = new Date(year, monthOfYear, dayOfMonth);
                        SimpleDateFormat dateFormatter = new SimpleDateFormat(
                                "MM/dd/yy");
                        strDate = dateFormatter.format(chosenDate);
                        taskDate.setText( strDate);
                        //prospectFollowDate.setText( year+ "-" +  (monthOfYear + 1) + "-" + dayOfMonth );

                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
        //    }

    }


    public void setDataForSpinnerNew(){
        actionItems = new ArrayList<String>();

        actionItems.add("Select Action");
        actionItems.add("Call for Sale");
        actionItems.add("Call for promotion");

        ArrayAdapter<String> dataAdapterAction = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, actionItems)
        { @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent)
        {
            View v = null;

            // If this is the initial dummy entry, make it hidden
            if (position == 0) {
                TextView tv = new TextView(getContext());
                tv.setHeight(0);
                tv.setVisibility(View.GONE);
                v = tv;
            }
            else {
                // Pass convertView as null to prevent reuse of special case views
                v = super.getDropDownView(position, null, parent);
            }

            // Hide scroll bar because it appears sometimes unnecessarily, this does not prevent scrolling
            parent.setVerticalScrollBarEnabled(false);
            return v;
        }


            @RequiresApi(api = Build.VERSION_CODES.O)
            @NonNull
            @Override
            public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
// Typeface myFont = Typeface.createFromAsset(getAssets(), "font/lucidagrande_normal.ttf");
                Typeface myFont = getResources().getFont(R.font.lucidagrande_bold);
                TextView v = (TextView) super.getView(position, convertView, parent);
                v.setTextSize(14.27f);
                v.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                v.setTextColor(getResources().getColor(R.color.colorForTaskBasketSpinner));
                v.setSingleLine(false);
                v.setTypeface(myFont);
                return v;
            }
        };
        dataAdapterAction.setDropDownViewResource(R.layout.spinner_item_action_contacmethod_textcolor);
        taskAction.setAdapter(dataAdapterAction);


        contactMethods = new ArrayList<String>();

        contactMethods.add("Select Contact Method");
        contactMethods.add("Phone");
        contactMethods.add("E-mail");
        contactMethods.add("Mail");

        ArrayAdapter<String> dataAdapterContactMethod = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, contactMethods)
        { @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent)
        {
            View v = null;

            // If this is the initial dummy entry, make it hidden
            if (position == 0) {
                TextView tv = new TextView(getContext());
                tv.setHeight(0);
                tv.setVisibility(View.GONE);
                v = tv;
            }
            else {
                // Pass convertView as null to prevent reuse of special case views
                v = super.getDropDownView(position, null, parent);
            }

            // Hide scroll bar because it appears sometimes unnecessarily, this does not prevent scrolling
            parent.setVerticalScrollBarEnabled(false);
            return v;
        }


            @RequiresApi(api = Build.VERSION_CODES.O)
            @NonNull
            @Override
            public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
// Typeface myFont = Typeface.createFromAsset(getAssets(), "font/lucidagrande_normal.ttf");
                Typeface myFont = getResources().getFont(R.font.lucidagrande_bold);
                TextView v = (TextView) super.getView(position, convertView, parent);
                v.setTextSize(14.27f);
                v.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                v.setTextColor(getResources().getColor(R.color.colorForTaskBasketSpinner));
                v.setSingleLine(false);
                v.setTypeface(myFont);
                return v;
            }
        };
        dataAdapterContactMethod.setDropDownViewResource(R.layout.spinner_item_action_contacmethod_textcolor);
        taskContactMethod.setAdapter(dataAdapterContactMethod);

    }

    public void setDataForSpinner(){

        List<String> actionItems = new ArrayList<String>();

        List<String> contactMethods =  new ArrayList<String>();

        actionItems.add("Select Action");
        actionItems.add("Call for Sale");
        actionItems.add("Call for promotion");

        //taskAction.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, actionItems));
        ArrayAdapter<String> dataAdapterAction = new ArrayAdapter<String>(this,  R.layout.simple_spinner_item_custom, actionItems);
        dataAdapterAction.setDropDownViewResource(R.layout.spinner_item_action_contact_method);
        taskAction.setAdapter(dataAdapterAction);

        contactMethods.add("Select Contact Method");
        contactMethods.add("Phone");
        contactMethods.add("E-mail");
        contactMethods.add("Mail");

        //taskContactMethod.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line,contactMethods));

        ArrayAdapter<String> dataAdapterContactMethod = new ArrayAdapter<String>(this,  R.layout.simple_spinner_item_custom, contactMethods);
        dataAdapterContactMethod.setDropDownViewResource(R.layout.spinner_item_action_contact_method);
        taskContactMethod.setAdapter(dataAdapterContactMethod);

    }

    public void setListeners(){

        addTask.setOnClickListener(this);
        //  saveButton.setOnClickListener(this);
        //   clearTaskButton.setOnClickListener(this);
        returnButton.setOnClickListener(this);
        //       virtualAssist.setOnClickListener(this);
        taskSelectedButton.setOnClickListener(this);
        wishListButtonTaskPage.setOnClickListener(this);
        bothTaskBasketButtonTaskPage.setOnClickListener(this);
        deleteTaskButton.setOnClickListener(this);

        /* changed from activity_task to activity_task_x*/
        editProspect.setOnClickListener(this);
        /* changed from activity_task to activity_task_x*/

        taskName.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {

                Pattern p = Pattern.compile("[^a-z0-9 ]", Pattern.CASE_INSENSITIVE);
                Matcher m = p.matcher(taskName.getText().toString());
                boolean b = m.find();

                if (b) {
                    String text = taskName.getText().toString();
                    if(text.length()==1) {
                        taskName.setText("");
                    }
                    else{
                        taskName.setText(text.substring(0, text.length() - 1));
                    }

                    showDialogForNotification("Special Characters not allowed in Sales No");
                }

            }
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {    }
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }
        });
    }


    public void showDialogForNotification(String msgNotification){
        TextView cancelDialogButton;
        final Dialog dialog = new Dialog(TaskActivity.this);
        TextView textViewMessage;
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.notification_popup_new);
        // dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        textViewMessage  = (TextView)dialog.findViewById(R.id.notificationMessage);
        textViewMessage.setText(msgNotification);
        /*dialog.show();

        final Timer t = new Timer();
        t.schedule(new TimerTask() {
            public void run() {
                dialog.dismiss(); // when the task active then close the dialog
                t.cancel(); // also just top the timer thread, otherwise, you may receive a crash report
            }
        }, 1000);
*/

        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);


        cancelDialogButton = dialog.findViewById(R.id.cancelNotificationButton);
        cancelDialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                dialog.dismiss();

            }

        });


        Window window = dialog.getWindow();
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.TOP);
        dialog.show();

    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.addTaskButton:

                if(validateTaskForm()) {
                    //        tableTask.removeAllViews();

                    //  addTaskHeaderToTable;
                    taskModel = new TaskModel();
                    taskModel.setTaskName(taskName.getText().toString());
                    taskModel.setTaskAction(taskAction.getSelectedItem().toString());
                    taskModel.setTaskDate(taskDate.getText().toString());
                    taskModel.setTaskContactMethod(taskContactMethod.getSelectedItem().toString());
                    taskModel.setTaskNotes(taskNote.getText().toString());
                    taskList.add(taskModel);
                    addrowDataToTableNew(taskModel,tableRowList);
                    saveTask(taskModel);
                    //addTaskHeaderToTable(true, taskModel);

                }
                break;

            case R.id.deleteTaskButton:

                showDialogForDeleteTask();
                break;

            /*  case R.id.button6: *//* if(validateTaskForm()) {*//*
                //saveTask(taskList);
                // }
                break;
            case R.id.button7: showDialogForClearTask();
                break;
*/
            case R.id.taskReturnButton: Intent intentProspector = new Intent(getApplicationContext(), ProspectorActivity.class);
                intentProspector.putExtra("Token",token );
                startActivity(intentProspector);
                finish();
                break;

            case R.id.wishlist_button_task_page_x:
                Intent intentBasket = new Intent(this, WishListActivity.class);

                intentBasket.putExtra("prospect_id",prospectorId );
                intentBasket.putExtra("prospector_firstname",prospectFirstName);
                intentBasket.putExtra("prospector_lastname",prospectLastName);
                intentBasket.putExtra("prospector_phone",prospectPhNo);
                startActivity(intentBasket);
                finish();
                break;

            case R.id.task_wishlist_both_button_task_page_x:

                Intent intentBoth = new Intent(this, TaskWishListBothActivity.class);
                intentBoth.putExtra("prospect_id",prospectorId );
                intentBoth.putExtra("prospector_firstname",prospectFirstName);
                intentBoth.putExtra("prospector_lastname",prospectLastName);
                intentBoth.putExtra("prospector_phone",prospectPhNo);
                startActivity(intentBoth);
                break;
            case R.id.taskSelectedButton:
                Intent intentTask = new Intent(this, TaskActivity.class);
                startActivity(intentTask);
                break;
            case  R.id.editProspectButton_x :
                Intent intentTask1 = new Intent(getApplicationContext(), ProspectorActivity.class);
                intentTask1.putExtra("prospect_id",prospectorId );
                startActivity(intentTask1);
                finish();
                break;

            case R.id.taskVirtualassist:
                Intent  intentVirtualAssist= new Intent(getApplicationContext(), VirtualAssistantActivity.class);
                startActivity(intentVirtualAssist);
                finish();
                break;



        }
    }

    public void saveTask(TaskModel tm){


        try {

            Call<TaskResponse> call2 = apiInterface.saveTask("4",tm.getTaskName(),
                    tm.getTaskDate(),
                    tm.getTaskAction(),
                    tm.getTaskContactMethod(),
                    tm.getTaskNotes()
            );

            call2.enqueue(new Callback<TaskResponse>() {
                @Override
                public void onResponse(Call<TaskResponse> call, Response<TaskResponse> response) {
                    System.out.println(response.body().toString());
                    TaskResponse res=response.body();
                    if (!res.isError()) {
                        // Toast.makeText(getApplicationContext(), "Tasks added successfully" , Toast.LENGTH_LONG).show();
                        // showDialogForSaveTask();

                    } else{
                        Toast.makeText(getApplicationContext(), "invalid parameters" , Toast.LENGTH_LONG).show();

                    }


                }

                @Override
                public void onFailure(Call<TaskResponse> call, Throwable t) {

                    call.cancel();
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    public void deleteTaskRowDetails(){
       /* int dataNo=0;
        for(int i=0;i< tableDataList.size();i++){
            if(tableDataList.get(i).getTaskId()== taskId){
                dataNo = i;
                break;
            }
        }
        tableDataList.remove(dataNo);

        drawTable();*/
        tableTask.removeView(tbrow);

//        tableTask.removeViewAt(tableRowId+1);
        tableRowList.remove(tableRowId);
        counterTableRow--;
        rowCount--;
        // taskList.remove();

        try {

            Call<DeleteTaskResponse> call2 = apiInterface.deleteTask(String.valueOf(taskId)
            );

            call2.enqueue(new Callback<DeleteTaskResponse>() {
                @Override
                public void onResponse(Call<DeleteTaskResponse> call, Response<DeleteTaskResponse> response) {
                    System.out.println(response.body().toString());
                    DeleteTaskResponse res=response.body();
                    if (!res.isError()) {
                        //    Toast.makeText(getApplicationContext(), "Task Deleted successfully" , Toast.LENGTH_LONG).show();
                        //    showDialogForDeleteSuccessTask();

                    } else{
                        Toast.makeText(getApplicationContext(), "invalid parameters" , Toast.LENGTH_LONG).show();

                    }


                }

                @Override
                public void onFailure(Call<DeleteTaskResponse> call, Throwable t) {

                    call.cancel();
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }

    }



    public boolean validateTaskForm() {
        Boolean isFormValid = true;

        if(taskName.getText().toString().trim().isEmpty()){
            Toast.makeText(getApplicationContext(), "Task Name is empty", Toast.LENGTH_LONG).show();
            return false;
        }/*else if(inputValidation.isAlphaNumeric(taskName))){

        }*/

        if(taskDate.getText().toString().trim().isEmpty()) {
            Toast.makeText(getApplicationContext(), "Task Date is empty", Toast.LENGTH_LONG).show();
            return false;
        }

        if(taskAction.getSelectedItem().toString().equalsIgnoreCase("Select Action")){
            Toast.makeText(getApplicationContext(), "Please select Task Action", Toast.LENGTH_LONG).show();
            return false;
        }

        if(taskContactMethod.getSelectedItem().toString().equalsIgnoreCase("Select Contact Method")){
            Toast.makeText(getApplicationContext(), "Please Select Contact Method", Toast.LENGTH_LONG).show();
            return false;
        }

        if(taskNote.getText().toString().trim().isEmpty()){
            Toast.makeText(getApplicationContext(), "Task Note is empty", Toast.LENGTH_LONG).show();
            return false;
        }

        return isFormValid;
    }

    public void clearTaskData(){

        if(taskName!=null){taskName.setText(null);}
        if(taskDate!=null){taskDate.setText(null);}
        if(!taskAction.getSelectedItem().toString().isEmpty())
        {
            //   taskAction.setSelected(false);
            taskAction.setSelection(0);

        }

        if(!taskContactMethod.getSelectedItem().toString().isEmpty())
        {
            //   taskAction.setSelected(false);
            taskContactMethod.setSelection(0);

        }
        //   if(taskAction!=null){taskAction.set}
        if(taskName!=null){taskName.setText(null);}
        if(taskNote!=null){taskNote.setText(null);}
    }



    public void showDialogForSaveTask(){
        TextView cancelDialogButton;
        final Dialog dialog = new Dialog(TaskActivity.this);

        dialog.setCancelable(false);
        dialog.setContentView(R.layout.save_task_dialog);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

        // dialog.show();

    /*    final Timer t = new Timer();
        t.schedule(new TimerTask() {
            public void run() {
                dialog.dismiss(); // when the task active then close the dialog
                t.cancel(); // also just top the timer thread, otherwise, you may receive a crash report
            }
        }, 2000);
*/

        cancelDialogButton = dialog.findViewById(R.id.okNotificationButton);
        cancelDialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                dialog.dismiss();

            }

        });

        dialog.show();
    }

    public void showDialogForClearTask(){

        Button continueDialogButton;
        Button cancelDialogButton;

        final Dialog dialog = new Dialog(TaskActivity.this);

        dialog.setCancelable(false);
        dialog.setContentView(R.layout.clear_task_dialog);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

        continueDialogButton = dialog.findViewById(R.id.continueDialog);
        continueDialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clearTaskData();
                dialog.dismiss();
            }
        });

        cancelDialogButton = dialog.findViewById(R.id.cancelDialog);
        cancelDialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                dialog.dismiss();

            }

        });


        dialog.show();



    }

    public void showDialogForDeleteTask(){

        TextView continueDialogButton;
        TextView cancelDialogButton;

        final Dialog dialog = new Dialog(TaskActivity.this);

        dialog.setCancelable(false);
        dialog.setContentView(R.layout.clear_task_dialog);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

        continueDialogButton = dialog.findViewById(R.id.continueDialog);
        continueDialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteTaskRowDetails();
                clearTaskData();
                dialog.dismiss();
            }
        });

        cancelDialogButton = dialog.findViewById(R.id.cancelDialog);
        cancelDialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                dialog.dismiss();

            }

        });

        Window window = dialog.getWindow();
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.TOP);
        dialog.show();



    }

    @Override
    public void onBackPressed() {
        Intent intentProspector = new Intent(getApplicationContext(), ProspectorActivity.class);
        intentProspector.putExtra("Token",token );
        startActivity(intentProspector);
        finish();
    }

    public void showDialogForDeleteSuccessTask(){

        final Dialog dialog = new Dialog(TaskActivity.this);

        dialog.setCancelable(false);
        dialog.setContentView(R.layout.delete_task_success_dialog);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

        dialog.show();

        final Timer t = new Timer();
        t.schedule(new TimerTask() {
            public void run() {
                dialog.dismiss(); // when the task active then close the dialog
                t.cancel(); // also just top the timer thread, otherwise, you may receive a crash report
            }
        }, 2000);

    }
}
