package com.example.gns076.rapsra.activities;

import android.Manifest;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.hardware.Camera;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.RelativeSizeSpan;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.gns076.rapsra.R;
import com.example.gns076.rapsra.model.AppPreference;
import com.example.gns076.rapsra.model.BeaconLogInfo;
import com.example.gns076.rapsra.model.BeaconLogResponse;
import com.example.gns076.rapsra.model.BeaconModel;
import com.example.gns076.rapsra.model.BeaconModelFloorPlan;

import com.example.gns076.rapsra.model.MerchandiseBeaconInfoResponse;
import com.example.gns076.rapsra.sql.DatabaseHelper;

import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.lang.reflect.Type;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ArViewActivityNew extends YouTubeBaseActivity implements SurfaceHolder.Callback,YouTubePlayer.OnInitializedListener {

    private static final String TAG = ArViewActivityNew.class.getSimpleName();
    Camera camera;
    SurfaceView surfaceView;

    SurfaceHolder surfaceHolder;

    private AppPreference mAppPreference;

    private String role;
    public APIInterface apiInterface;
    boolean previewing = false;
    LayoutInflater controlInflater = null;

    Calendar c = Calendar.getInstance();


    Timer scheduletimer;

    private String beaconAttachment = "";
    private String messageDisplayed;
    //  private BeaconScanningApp scanningApp;


    YouTubePlayer.OnInitializedListener onInitializedListener;

    YouTubePlayerView youTubePlayerView;
    YouTubePlayer youTubePlayer = null;

    TextView displayMessageRecievedTextView ;

    private List<BeaconModel> beaconInfoList = new ArrayList<BeaconModel>();
    private SharedPreferences sharedPreferences;
    List<BeaconModelFloorPlan> beaconDistanceList =  new ArrayList<>();
    Gson gson = new Gson();


    private DatabaseHelper databaseHelper;

    private BeaconLogInfo beaconLogInfoGetsqldata = new BeaconLogInfo();

    SimpleDateFormat simpleDateFormat;
    StringBuilder afterAddingsqldata = new StringBuilder();

    private Button refreshButton;
    private MerchandiseBeaconInfoResponse merchandiseBeaconInfoResponse;
    private BeaconLogInfo beaconLogInfoAftersqldatadd = new BeaconLogInfo();
    String fetchBeaconContentTime;

    private BeaconLogInfo beaconLogInfoAttachment =  new BeaconLogInfo();

    Dialog dialog;

    String orientationlogBeaconID ;
    String orientationlogBeaconAttachhmt ;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.i(TAG, "inside onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera_nearby_youtube);

        simpleDateFormat = new SimpleDateFormat("YYYY-MM-dd hh:mm:ss");
      /*  scanningApp = (BeaconScanningApp)getApplicationContext();
        scanningApp.bind();*/
        apiInterface = APIClient.getClient().create(APIInterface.class);
        mAppPreference = new AppPreference(this);
        databaseHelper = new DatabaseHelper(this);

        dialog = new Dialog(ArViewActivityNew.this);
        refreshButton =  findViewById(R.id.refresh_button);
        fetchBeaconContentTime = "10";

       /* if (getResources().getConfiguration().orientation == ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE) {
            //do work for landscape screen mode.
            Toast.makeText(this, "landscape", Toast.LENGTH_SHORT).show();
        } else if (getResources().getConfiguration().orientation == ActivityInfo.SCREEN_ORIENTATION_PORTRAIT) {
            //Do work for portrait screen mode.
            Toast.makeText(this, "portrait", Toast.LENGTH_SHORT).show();
        }*/
        refreshButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Creating the instance of PopupMenu
                PopupMenu popup = new PopupMenu(ArViewActivityNew.this, refreshButton);
                //Inflating the Popup using xml file

                refreshButton.setBackgroundResource(R.drawable.exit_menu);
              //  isCross = true;
                Menu arMenu =popup.getMenu();


                popup.getMenuInflater()
                        .inflate(R.menu.menu_ar_page, arMenu);


                for(int i = 0; i < arMenu.size(); i++) {
                    MenuItem item = arMenu.getItem(i);


                    SpannableString spanString = new SpannableString(arMenu.getItem(i).getTitle().toString());
                    int end = spanString.length();

                    //   spanString.setSpan(new RelativeSizeSpan(1.5f), 0, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    spanString.setSpan(new RelativeSizeSpan(0.04f), 0, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    item.setTitle(spanString);

                }


                popup.setOnDismissListener(new PopupMenu.OnDismissListener() {
                    @Override
                    public void onDismiss(PopupMenu popupMenu) {
                        refreshButton.setBackgroundResource(R.drawable.menu);
                    }
                });


                //registering popup with OnMenuItemClickListener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {

                        Intent intentButtonClick;
                        switch (item.getItemId()){

                            case R.id.main_page:
                                releaseYoutubePlayer();
                                scheduletimer.cancel();
                                intentButtonClick = new Intent(getApplicationContext(), SalesRadarActivityNew.class);
                                startActivity(intentButtonClick);
                                break;

                            case R.id.refresh_page:
                             //   intentButtonClick = new Intent(getApplicationContext(), SalesRadarActivityNew.class);
                             //   startActivity(intentButtonClick);
                                scheduletimer.cancel();
                                releaseYoutubePlayer();

                                AsyncTaskRunner task = new AsyncTaskRunner();
                                //task.doInBackground(String.valueOf(40000));
                                task.execute(String.valueOf(fetchBeaconContentTime));

                                break;



                        }
                        return true;
                    }
                });

                popup.show(); //showing popup menu


            }
        }); //closi


        role = mAppPreference.getRole();
        surfaceView = (SurfaceView) findViewById(R.id.camerapreview1);
        surfaceHolder = surfaceView.getHolder();
        surfaceHolder.addCallback(this);
        controlInflater = LayoutInflater.from(getBaseContext());
        final View viewControl = controlInflater.inflate(R.layout.control, null);

        youTubePlayerView = (YouTubePlayerView) viewControl.findViewById(R.id.youtube2);
        //   youTubePlayerView.setVisibility(View.INVISIBLE);
        final ViewGroup.LayoutParams layoutParamsControl
                = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);


        // for Displaying text
        displayMessageRecievedTextView = (TextView) viewControl.findViewById(R.id.textViewMessageForBeacon);
        displayMessageRecievedTextView.setVisibility(View.INVISIBLE);

        this.addContentView(viewControl, layoutParamsControl);

        surfaceView.setZOrderMediaOverlay(false);
        surfaceView.setZOrderOnTop(false);
        onInitializedListener = this;

        sharedPreferences = getSharedPreferences("rapsra_pref_beacon", MODE_PRIVATE);


        surfaceView.setVisibility(View.VISIBLE);


        //Added code for  adding logs
        beaconLogInfoGetsqldata.setAgentID(mAppPreference.getAgentId());
        beaconLogInfoGetsqldata.setDistance("0");
        beaconLogInfoGetsqldata.setBeaconIdentifier("0");
        Date date = Calendar.getInstance().getTime();
        beaconLogInfoGetsqldata.setTimestamp(simpleDateFormat.format(date));


        // Added getting sqlite Beacon data  for RAPSRA proximity changes.
        if (databaseHelper.getBeaconInfo().size() > 0) {

            beaconInfoList = databaseHelper.getBeaconInfo();

            if (mAppPreference.getBeaconLogFlag().equalsIgnoreCase("y")) {

                //Added code for adding logs
                afterAddingsqldata.append("Getting SQLite Data  ::");
                for (BeaconModel bm : beaconInfoList) {
                    afterAddingsqldata.append(" For Beacon ID:" + bm.getBeaconId() + " With Attachment "
                            + bm.getBeaconAttachment());
                }
                beaconLogInfoGetsqldata.setLogString(afterAddingsqldata.toString());
                setUserBeaconLogs(beaconLogInfoGetsqldata);
            }
        }


       scheduleBeaconTimer(0);


    }


    public void scheduleBeaconTimer(int startTime){

        scheduletimer = new Timer();
        scheduletimer.schedule(new TimerTask() {

            @Override
            public void run() {
                //Do something after 100ms
                timedAction();

            }
        }, startTime, 8000);
    }

    // Commented code for timer
    private void timedAction(){
        this.runOnUiThread(timer);
    }

    private Runnable timer =new Runnable() {
        @Override
        public void run() {


            String json = sharedPreferences.getString("BeaconInfoList", "");

            Type type =  new TypeToken<ArrayList<BeaconModelFloorPlan>>(){}.getType();

            beaconDistanceList =  gson.fromJson(json,type);

            sharedPreferences.edit().remove("BeaconInfoList").commit();

            if(beaconDistanceList!=null) {
                Collections.sort(beaconDistanceList);

                if(mAppPreference.getBeaconLogFlag().equalsIgnoreCase("y")){
                    beaconLogInfoGetsqldata = new BeaconLogInfo();
                    beaconLogInfoGetsqldata.setAgentID(mAppPreference.getAgentId());
                    beaconLogInfoGetsqldata.setDistance("0");
                    beaconLogInfoGetsqldata.setBeaconIdentifier("0");
                    Date date = Calendar.getInstance().getTime();
                    beaconLogInfoGetsqldata.setTimestamp(simpleDateFormat.format(date));
                    afterAddingsqldata =  new StringBuilder();

                    afterAddingsqldata.append("Scanned Beacons with ");
                    for(BeaconModelFloorPlan beaconModelFloorPlan:beaconDistanceList){
                        afterAddingsqldata.append(" Beacon ID:" + beaconModelFloorPlan.getBeaconId());
                        afterAddingsqldata.append(" Beacon Distance:" + beaconModelFloorPlan.getMeanDistance());
                    }
                    beaconLogInfoGetsqldata.setLogString(afterAddingsqldata.toString());
                    setUserBeaconLogs(beaconLogInfoGetsqldata);



                }

                handleResponceFromBeacon(beaconInfoList);




            }



        }

    };


    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width,
                               int height) {
// TODO Auto-generated method stub

        if(previewing){
            camera.stopPreview();
            previewing = false;
        }

        if (camera != null){
            try {
                camera.setPreviewDisplay(surfaceHolder);
            } catch (IOException e) {
                e.printStackTrace();
            }
            camera.startPreview();
            previewing = true;
            // surfaceView.setZOrderMediaOverlay(false);
            //surfaceView.setZOrderOnTop(true);
        }
        surfaceView.setZOrderMediaOverlay(false);
        Log.i(TAG,"inside surface changed");
        surfaceView.setZOrderOnTop(false);
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
// TODO Auto-generated method stub
        camera = Camera.open();
        camera.setDisplayOrientation(90);

        Log.i(TAG,"inside surface created");
        //  surfaceView.setZOrderOnTop(false);

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
// TODO Auto-generated method stub
        if(camera!=null) {
            camera.stopPreview();
            camera.release();
            camera = null;
            previewing = false;

        }
        Log.i(TAG,"inside surface destroyed");
    }



    public void playYoutubeVideo(String youTubeLink){
        if(youTubePlayer != null) {
            Log.i(TAG, "Beacon Attachment:: " + youTubeLink);
            String onlyId = youTubeLink.replace("https://www.youtube.com/watch?v=", "");
            Log.i(TAG, "Youtube Id:: " + onlyId);
            //strForVideoPlaying = onlyId;
            youTubePlayerView.setVisibility(View.VISIBLE);
            youTubePlayerView.bringToFront();


            Log.i(TAG, "id for youtube: " + onlyId);

            youTubePlayer.setPlayerStyle(YouTubePlayer.PlayerStyle.MINIMAL);

            youTubePlayer.loadVideo(onlyId);

        }

    }

    public void handleResponceFromBeacon(List<BeaconModel> beaconInfoList) {


        afterAddingsqldata = new StringBuilder();
        afterAddingsqldata.append("Handling Beacon Responses----------------- ");


        //  merchandiseBeaconInfoResponse = apiResponse.body();
        //   beaconInfoList = merchandiseBeaconInfoResponse.getBeaconInfo();
        String beaconAttachmentData = "";
        orientationlogBeaconAttachhmt = "";
        orientationlogBeaconID = "";

        compareBeaconDistance: for(BeaconModelFloorPlan beaconModelFloorPlan:beaconDistanceList) {
            for (BeaconModel beacon : beaconInfoList) {
                if(beaconModelFloorPlan.getBeaconId().equals(beacon.getBeaconId())) {

                    orientationlogBeaconAttachhmt = beacon.getBeaconAttachment();
                    orientationlogBeaconID = beacon.getBeaconId();
                    beaconAttachmentData = beacon.getBeaconAttachment();
                    Log.i(TAG,"Beacon ID:::"+beacon.getBeaconId()+ "with attachment : "+beaconAttachmentData);
                    break compareBeaconDistance;
                }

            }

        }

        afterAddingsqldata.append("Displayed Beacon Attachment Data:"+beaconAttachmentData);

        if(beaconAttachmentData.contains("youtube")) {
            messageDisplayed = "youtube";
            displayMessageRecievedTextView.setVisibility(View.INVISIBLE);

            if(dialog.isShowing()){
                dialog.dismiss();
            }
            //  Toast.makeText(getApplicationContext(),"YouTube data", Toast.LENGTH_SHORT).show();

      //      afterAddingsqldata.append("Youtube Attachment Data with Player value::::"+youTubePlayer);
            if(youTubePlayer == null) {
                youTubePlayerView.initialize(PlayerConfig.API_KEY, onInitializedListener);
                beaconAttachment = beaconAttachmentData;
            }
            else
            if( !youTubePlayer.isPlaying() && youTubePlayer != null ) {


                displayMessageRecievedTextView.setVisibility(View.INVISIBLE);
                youTubePlayerView.setVisibility(View.VISIBLE);

                playYoutubeVideo(beaconAttachmentData);
                beaconAttachment = beaconAttachmentData;
            }
            else if(youTubePlayer != null && youTubePlayer.isPlaying()){
                if(!beaconAttachment.equals(beaconAttachmentData)){
                    playYoutubeVideo(beaconAttachmentData);
                    beaconAttachment = beaconAttachmentData;
                }
            }
        }
        else{

           /* if(displayMessageRecievedTextView.getVisibility()==(View.INVISIBLE))
                displayMessageRecievedTextView.setVisibility(View.VISIBLE);
            displayMessageRecievedTextView.setText(beaconAttachmentData);
            messageDisplayed = "text";*/
          //  afterAddingsqldata.append("Text Attachment Data with  Player value::::"+youTubePlayer);
            if(youTubePlayer != null && youTubePlayer.isPlaying()) {
                youTubePlayer.release();
                youTubePlayerView.setVisibility(View.GONE);
                youTubePlayer =  null;
            }

            if(!beaconAttachmentData.isEmpty())
                showDialogForNotification(beaconAttachmentData);

            // youTubePlayerView.setVisibility(View.GONE);


        }

        if (mAppPreference.getBeaconLogFlag().equalsIgnoreCase("y")) {
            beaconLogInfoGetsqldata = new BeaconLogInfo();
            beaconLogInfoGetsqldata.setAgentID(mAppPreference.getAgentId());
            beaconLogInfoGetsqldata.setDistance("0");
            beaconLogInfoGetsqldata.setBeaconIdentifier("0");
            Date date = Calendar.getInstance().getTime();
            beaconLogInfoGetsqldata.setTimestamp(simpleDateFormat.format(date));
            beaconLogInfoGetsqldata.setLogString(afterAddingsqldata.toString());
            setUserBeaconLogs(beaconLogInfoGetsqldata);

        }
    }

    public void showDialogForNotification(String msgNotification){
        TextView cancelDialogButton;

        TextView textViewMessage;
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.notification_ar_view);
        // dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        textViewMessage  = (TextView)dialog.findViewById(R.id.notificationMessageForArView);
        textViewMessage.setText(msgNotification);
        /*dialog.show();

        final Timer t = new Timer();
        t.schedule(new TimerTask() {
            public void run() {
                dialog.dismiss(); // when the task active then close the dialog
                t.cancel(); // also just top the timer thread, otherwise, you may receive a crash report
            }
        }, 1000);
*/

        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);


        cancelDialogButton = dialog.findViewById(R.id.cancelNotificationButtonArView);
        cancelDialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                dialog.dismiss();

            }

        });


        Window window = dialog.getWindow();
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.TOP);

        if(!isFinishing())
                dialog.show();

    }

    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer initialisedYouTubePlayer, boolean b) {
        youTubePlayerView.setVisibility(View.VISIBLE);
        youTubePlayerView.bringToFront();
        youTubePlayer = initialisedYouTubePlayer;
        playYoutubeVideo(beaconAttachment);


        youTubePlayer.setPlayerStateChangeListener(new YouTubePlayer.PlayerStateChangeListener() {
            @Override
            public void onLoading() {

            }

            @Override
            public void onLoaded(String s) {

            }

            @Override
            public void onAdStarted() {

            }

            @Override
            public void onVideoStarted() {

            }

            @Override
            public void onVideoEnded() {
                youTubePlayerView.setVisibility(View.INVISIBLE);
                youTubePlayer.release();
                youTubePlayer = null;
            }

            @Override
            public void onError(YouTubePlayer.ErrorReason errorReason) {

            }
        });

    }


    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {

    }

    public void getBeaconInfoData(){

        Log.i(TAG,"Getting Beacon Data Start time"+ c.getTimeInMillis());

        databaseHelper.deleteBeaconInfo();

        beaconLogInfoAftersqldatadd.setAgentID(mAppPreference.getAgentId());
        beaconLogInfoAftersqldatadd.setDistance("0");
        beaconLogInfoAftersqldatadd.setBeaconIdentifier("0");
        Date date = Calendar.getInstance().getTime();
        beaconLogInfoAftersqldatadd.setTimestamp(simpleDateFormat.format(date));

        try {
            Call<MerchandiseBeaconInfoResponse> call2 = apiInterface.getMerchandiseBeaconInfo(mAppPreference.getAgentId());
            call2.enqueue(new Callback<MerchandiseBeaconInfoResponse>() {
                @Override
                public void onResponse(Call<MerchandiseBeaconInfoResponse> call, Response<MerchandiseBeaconInfoResponse> apiResponse) {
                    if(!apiResponse.body().isError()) {
                        // handleResponceFromBeacon(call, apiResponse);
                        merchandiseBeaconInfoResponse = apiResponse.body();
                        beaconInfoList = merchandiseBeaconInfoResponse.getBeaconInfo();

                        mAppPreference.setBeaconLogFlag(merchandiseBeaconInfoResponse.getLogFlag());
                        afterAddingsqldata.append("Adding SQLite Data  ::");

                        for(BeaconModel beaconModel:beaconInfoList){
                            databaseHelper.addBeaconInfo(beaconModel);
                            afterAddingsqldata.append(" For Beacon ID:"+beaconModel.getBeaconId()+ " With Attachment "
                                    + beaconModel.getBeaconAttachment());
                        }
                        beaconLogInfoAftersqldatadd.setLogString(afterAddingsqldata.toString());
                        Log.i(TAG,"Beacon content fetched successfully");

                        if(mAppPreference.getBeaconLogFlag().equalsIgnoreCase("y")){
                            setUserBeaconLogs(beaconLogInfoAftersqldatadd);
                        }

                        Log.i(TAG,"Getting Beacon Data End time"+c.getTimeInMillis());
                    }
                }

                @Override
                public void onFailure(Call<MerchandiseBeaconInfoResponse> call, Throwable t) {
                    Toast.makeText(getApplicationContext(), "No Network Connection", Toast.LENGTH_LONG).show();
                    call.cancel();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void setUserBeaconLogs(BeaconLogInfo beaconLogs){


        //   for(BeaconLogInfo beaconLogInfo:beaconLogInfos){

        try {
            //      Log.i(TAG, "Adding Beacon ID:"+beaconLogInfo.getBeaconIdentifier()+ " with Logs:::::"+ beaconLogInfo.getLogString());
            Call<BeaconLogResponse> call2 = apiInterface.setBeaconlogs(beaconLogs.getBeaconIdentifier(),beaconLogs.getDistance(),beaconLogs.getLogString(),beaconLogs.getAgentID(),beaconLogs.getTimestamp());

            call2.enqueue(new Callback<BeaconLogResponse>() {
                @Override
                public void onResponse(Call<BeaconLogResponse> call, Response<BeaconLogResponse> response) {
                    System.out.println(response.body().toString());
                    BeaconLogResponse res=response.body();
                    if (!res.isError()) {
                       // Toast.makeText(getApplicationContext(), "Beacon Logs added successfully" , Toast.LENGTH_SHORT).show();
                        Log.i(TAG,"Beacon Logs added successfully");

                    } else{
                      //  Toast.makeText(getApplicationContext(), "Beacon logs invalid parameters" , Toast.LENGTH_LONG).show();
                        Log.i(TAG,"Beacon logs invalid parameters");

                    }


                }

                @Override
                public void onFailure(Call<BeaconLogResponse> call, Throwable t) {

                    call.cancel();
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }



        //  }
    }


    private class AsyncTaskRunner extends AsyncTask<String, String, String> {

        private String resp;
        ProgressDialog progressDialog ;

        @Override
        protected String doInBackground(String... params) {
            publishProgress("Sleeping..."); // Calls onProgressUpdate()
            try {
                int time = Integer.parseInt(params[0])*1000;

                Thread.sleep(time);
                resp = "Slept for " + params[0] + " seconds";

                getBeaconInfoData();


            } catch (InterruptedException e) {
                e.printStackTrace();
                resp = e.getMessage();
            } catch (Exception e) {
                e.printStackTrace();
                resp = e.getMessage();
            }
            return resp;
        }


        @Override
        protected void onPostExecute(String result) {
            // execution of result of Long time consuming operation
            progressDialog.dismiss();
            scheduleBeaconTimer(5000);
        //    finalResult.setText(result);
        }


        @Override
        protected void onPreExecute() {
            progressDialog = new ProgressDialog(ArViewActivityNew.this,R.style.MyProgressDialogStyle);
            progressDialog.setTitle("Please Wait");
            progressDialog.setMessage("Fetching Beacon content");
            progressDialog.show();
        /*    progressDialog = ProgressDialog.show(ArViewActivityNew.this,
                    "Please Wait",
                    "Fetching Beacon content");*/

            /*progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorForDialogBackground)));
            progressDialog.getWindow().*/
        }


        @Override
        protected void onProgressUpdate(String... text) {
          //  finalResult.setText(text[0]);

        }
    }

    public void releaseYoutubePlayer(){
        if(youTubePlayer != null && youTubePlayer.isPlaying() ) {
            youTubePlayer.release();
            youTubePlayer =  null;
            youTubePlayerView.setVisibility(View.GONE);
            Log.i(TAG,"YouTube Player Released");
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        releaseYoutubePlayer();
        scheduletimer.cancel();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(
                FrameLayout.LayoutParams.WRAP_CONTENT,
                FrameLayout.LayoutParams.WRAP_CONTENT
        );


        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            Toast.makeText(this, "landscape", Toast.LENGTH_SHORT).show();
            Log.i(TAG,"Landscape");


            params.setMargins(150,200,0,50);
            youTubePlayerView.setLayoutParams(params);

            if(mAppPreference.getBeaconLogFlag().equalsIgnoreCase("y")){
                beaconLogInfoGetsqldata = new BeaconLogInfo();
                beaconLogInfoGetsqldata.setAgentID(mAppPreference.getAgentId());
                beaconLogInfoGetsqldata.setDistance("0");
                beaconLogInfoGetsqldata.setBeaconIdentifier("0");
                Date date = Calendar.getInstance().getTime();
                beaconLogInfoGetsqldata.setTimestamp(simpleDateFormat.format(date));
                afterAddingsqldata =  new StringBuilder();

                afterAddingsqldata.append("Landscape mode:::::::"+ " With Beacon:::"+orientationlogBeaconID+ "  and attachment"
                +orientationlogBeaconAttachhmt);

                beaconLogInfoGetsqldata.setLogString(afterAddingsqldata.toString());
                setUserBeaconLogs(beaconLogInfoGetsqldata);
            }


        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT){
            Toast.makeText(this, "portrait", Toast.LENGTH_SHORT).show();
            Log.i(TAG,"portrait");

            params.setMargins(60,310,40,180);
            youTubePlayerView.setLayoutParams(params);
            if(mAppPreference.getBeaconLogFlag().equalsIgnoreCase("y")){
                beaconLogInfoGetsqldata = new BeaconLogInfo();
                beaconLogInfoGetsqldata.setAgentID(mAppPreference.getAgentId());
                beaconLogInfoGetsqldata.setDistance("0");
                beaconLogInfoGetsqldata.setBeaconIdentifier("0");
                Date date = Calendar.getInstance().getTime();
                beaconLogInfoGetsqldata.setTimestamp(simpleDateFormat.format(date));
                afterAddingsqldata =  new StringBuilder();

                afterAddingsqldata.append("Portrait mode:::::::"+ " With Beacon:::"+orientationlogBeaconID+ "  and attachment"
                        +orientationlogBeaconAttachhmt);

                beaconLogInfoGetsqldata.setLogString(afterAddingsqldata.toString());
                setUserBeaconLogs(beaconLogInfoGetsqldata);
            }
        }
       /* switch(orientation) {
            case Configuration.ORIENTATION_PORTRAIT:
                if(!oAllow) {
                    setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                }
                break;
            case Configuration.ORIENTATION_LANDSCAPE:
                if(!oAllow) {
                    setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                }
                break;
        }*/
    }
}
