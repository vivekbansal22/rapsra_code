package com.example.gns076.rapsra.sql;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.gns076.rapsra.model.BeaconModel;
import com.example.gns076.rapsra.model.MyTeamStatus;
import com.example.gns076.rapsra.model.User;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by .
 */
public class DatabaseHelper extends SQLiteOpenHelper {

    // Database Version
    private static final int DATABASE_VERSION = 2;

    // Database Name
    private static final String DATABASE_NAME = "BeaconManager.db";

    // User table name
    private static final String TABLE_USER = "user";
    private static final String TABLE_TEAM_STATUS = "team_status";
    private static final String TABLE_STATUS = "status";
    private static final String TABLE_BEACON_INFO = "beacon_info";

    // User Table Columns names
    private static final String COLUMN_USER_ID = "user_id";
    private static final String COLUMN_USER_NAME = "user_name";
    private static final String COLUMN_USER_EMAIL = "user_email";
    private static final String COLUMN_USER_PASSWORD = "user_password";
    private static final String COLUMN_USER_STATUS = "user_status";

    // Team Status Table Columns names
    private static final String COLUMN_Q_NO = "qno";
    private static final String COLUMN_ID = "columnId";
    private static final String COLUMN_NAME = "columnName";
    private static final String COLUMN_STATUS = "columnStatus";
    private static final String COLUMN_UPS = "columnUps";
    private static final String COLUMN_CLOSED = "columnClosed";
    private static final String COLUMN_PER = "columnPercentage";

    // Status Table Columns names
    private static final String COLUMN_STATUS_ID = "statusId";
    private static final String COLUMN_STATUS_NAME = "statusName";
    private static final String COLUMN_STATUS_COLOR = "statusColor";

    private static final String COLUMN_BEACON_ID = "beaconId";
    private static final String COLUMN_BEACON_ATTACHMENT = "beaconAttachment";


    private Map<String,String> statusList = new <String,String>HashMap();
    private Map<String,String> dbstatusList = new <String,String>HashMap();

    private List<BeaconModel> dbBeaconList = new ArrayList<BeaconModel>();
    private   List<String> ls;


    // create table sql query
    private String CREATE_USER_TABLE = "CREATE TABLE " + TABLE_USER + "("
            + COLUMN_USER_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + COLUMN_USER_NAME + " TEXT,"
            + COLUMN_USER_EMAIL + " TEXT," + COLUMN_USER_PASSWORD + " TEXT," + COLUMN_USER_STATUS + " TEXT"  + ")";

    // create table sql query
    private String CREATE_STATUS_TABLE = "CREATE TABLE " + TABLE_STATUS + "("
            + COLUMN_STATUS_ID + " TEXT," + COLUMN_STATUS_NAME + " TEXT,"
            + COLUMN_STATUS_COLOR + " TEXT"+")";


    private String CREATE_TEAM_STATUS_TABLE = "CREATE TABLE " + TABLE_TEAM_STATUS + "("
            + COLUMN_Q_NO + " INTEGER PRIMARY KEY AUTOINCREMENT," + COLUMN_ID + " TEXT,"
            + COLUMN_NAME + " TEXT," + COLUMN_STATUS + " TEXT,"
            + COLUMN_UPS + " TEXT," + COLUMN_CLOSED + " TEXT," + COLUMN_PER + " TEXT"  + ")";

    private String CREATE_BEACON_TABLE = "CREATE TABLE " + TABLE_BEACON_INFO + "("
            + COLUMN_BEACON_ID + " TEXT," + COLUMN_BEACON_ATTACHMENT + " TEXT " + ");";

    // drop table sql query
    private String DROP_USER_TABLE = "DROP TABLE IF EXISTS " + TABLE_USER;
    private String DROP_TEAM_STATUS_TABLE = "DROP TABLE IF EXISTS " + TABLE_TEAM_STATUS;
    private String DROP_STATUS_TABLE = "DROP TABLE IF EXISTS " + TABLE_STATUS;
    private String DROP_BEACON_INFO_TABLE = "DROP TABLE IF EXISTS " + TABLE_BEACON_INFO;
    /**
     * Constructor
     *
     * @param context
     */
    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(DROP_USER_TABLE);
        db.execSQL(DROP_STATUS_TABLE);
        db.execSQL(DROP_TEAM_STATUS_TABLE);
        db.execSQL(DROP_BEACON_INFO_TABLE);
        db.execSQL(CREATE_USER_TABLE);
        db.execSQL(CREATE_STATUS_TABLE);
        db.execSQL(CREATE_TEAM_STATUS_TABLE);
        db.execSQL(CREATE_BEACON_TABLE);

        //addStatus(statusList,ls);
    }

    public void createMyTeamTable(SQLiteDatabase db){
        db.execSQL(CREATE_TEAM_STATUS_TABLE);
    }

    public void deleteMyTeamTable(SQLiteDatabase db){
        db.execSQL(CREATE_TEAM_STATUS_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        //Drop User Table if exist
        db.execSQL(DROP_USER_TABLE);
        db.execSQL(DROP_TEAM_STATUS_TABLE);
        db.execSQL(DROP_STATUS_TABLE);
        db.execSQL(DROP_BEACON_INFO_TABLE);

        // Create tables again
        onCreate(db);

    }

    /**
     * This method is to create Beacon record
     *
     */
    public void addBeaconInfo(BeaconModel bm) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMN_BEACON_ID, bm.getBeaconId());
        values.put(COLUMN_BEACON_ATTACHMENT, bm.getBeaconAttachment());

        // Inserting Row
        db.insert(TABLE_BEACON_INFO, null, values);
        db.close();
    }


    /**
     * This method is to create user record
     *
     * @param user
     */
    public void addUser(User user) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMN_USER_NAME, user.getName());
        values.put(COLUMN_USER_EMAIL, user.getEmail());
        //values.put(COLUMN_USER_PASSWORD, user.getPassword());
        //values.put(COLUMN_USER_STATUS, user.getPassword());

        // Inserting Row
        db.insert(TABLE_USER, null, values);
        db.close();
    }

    public void addStatus(String statusid,String statusName,String statusColor) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMN_STATUS_ID, statusid);
        values.put(COLUMN_STATUS_NAME, statusName);
        values.put(COLUMN_STATUS_COLOR, statusColor);

        db.insert(TABLE_STATUS, null, values);
        db.close();
    }


    public  List<BeaconModel>  getBeaconInfo() {
        // array of columns to fetch
        String[] columns = {
                COLUMN_BEACON_ID,
                COLUMN_BEACON_ATTACHMENT

        };

       /* String sortOrder =
                COLUMN_STATUS_ID + " ASC";*/

        SQLiteDatabase db = this.getReadableDatabase();
        List<BeaconModel> dbBeaconList = new ArrayList<>();

        Cursor cursor = db.query(TABLE_BEACON_INFO, //Table to query
                columns,    //columns to return
                null,        //columns for the WHERE clause
                null,        //The values for the WHERE clause
                null,       //group the rows
                null,       //filter by row groups
                ""); //The sort order


        // Traversing through all rows and adding to list
      /*  if (cursor.moveToFirst()) {
            do {
                dbBeaconList.put(cursor.getString(cursor.getColumnIndex(COLUMN_BEACON_ID)),cursor.getString(cursor.getColumnIndex(COLUMN_BEACON_ATTACHMENT)));
            } while (cursor.moveToNext());
        }*/

        if (cursor.moveToFirst()) {
            do {
                BeaconModel beaconModel = new BeaconModel();
                //myTeamStatus.setQno(Integer.parseInt(cursor.getString(cursor.getColumnIndex(COLUMN_Q_NO))));
                beaconModel.setBeaconId(cursor.getString(cursor.getColumnIndex(COLUMN_BEACON_ID)));
                beaconModel.setBeaconAttachment(cursor.getString(cursor.getColumnIndex(COLUMN_BEACON_ATTACHMENT)));

                // Adding Beacon record to list
                dbBeaconList.add(beaconModel);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();

        // return Beacon list
        return dbBeaconList;
    }


    /**
     * This method is to delete Beacon record
     *
     */
    public void deleteBeaconInfo() {
        SQLiteDatabase db = this.getWritableDatabase();
        // delete user record by id
        db.execSQL("delete from "+ TABLE_BEACON_INFO);
        db.close();
    }

    public  Map<String,String>  getAllStatus() {
        // array of columns to fetch
        String[] columns = {
                COLUMN_STATUS_ID,
                COLUMN_STATUS_NAME,
                COLUMN_STATUS_COLOR

        };

        String sortOrder =
                COLUMN_STATUS_ID + " ASC";

        SQLiteDatabase db = this.getReadableDatabase();


        Cursor cursor = db.query(TABLE_STATUS, //Table to query
                columns,    //columns to return
                null,        //columns for the WHERE clause
                null,        //The values for the WHERE clause
                null,       //group the rows
                null,       //filter by row groups
                ""); //The sort order


        // Traversing through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                dbstatusList.put(cursor.getString(cursor.getColumnIndex(COLUMN_STATUS_NAME)),cursor.getString(cursor.getColumnIndex(COLUMN_STATUS_COLOR)));
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();

        // return user list
        return dbstatusList;//"\"Available\",\"Non-Selling Activity\",\"With Client\",\"Training\",\"Meeting\",\"On Break\",\"Off\"";
    }



    /**
     * This method is to create user record
     *
     * @param myTeamStatus
     */
    public void addTeamStatus(MyTeamStatus myTeamStatus) {
        SQLiteDatabase db = this.getWritableDatabase();



        ContentValues values = new ContentValues();
        //values.put(COLUMN_Q_NO, myTeamStatus.getQno());
        values.put(COLUMN_ID, myTeamStatus.getId());
        values.put(COLUMN_NAME, myTeamStatus.getName());
        values.put(COLUMN_STATUS, myTeamStatus.getStatus());
        values.put(COLUMN_UPS, myTeamStatus.getUps());
        values.put(COLUMN_CLOSED, myTeamStatus.getClosed());
        values.put(COLUMN_PER, myTeamStatus.getClosed());


        // Inserting Row
        db.insert(TABLE_TEAM_STATUS, null, values);
        db.close();
    }

    public List<MyTeamStatus> getAllTeamStatus() {
        // array of columns to fetch
        String[] columns = {
                COLUMN_Q_NO,
                COLUMN_ID,
                COLUMN_NAME,
                COLUMN_STATUS,
                COLUMN_UPS,
                COLUMN_CLOSED,
                COLUMN_PER
        };
        // sorting orders
        String sortOrder =
                COLUMN_NAME + " ASC";
        List<MyTeamStatus> myTeamStatusList = new ArrayList<MyTeamStatus>();

        SQLiteDatabase db = this.getReadableDatabase();

        // query the user table
        /**
         * Here query function is used to fetch records from user table this function works like we use sql query.
         * SQL query equivalent to this query function is
         * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
         */
        Cursor cursor = db.query(TABLE_TEAM_STATUS, //Table to query
                columns,    //columns to return
                null,        //columns for the WHERE clause
                null,        //The values for the WHERE clause
                null,       //group the rows
                null,       //filter by row groups
                sortOrder); //The sort order


        // Traversing through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                MyTeamStatus myTeamStatus = new MyTeamStatus();
                //myTeamStatus.setQno(Integer.parseInt(cursor.getString(cursor.getColumnIndex(COLUMN_Q_NO))));
                myTeamStatus.setId(cursor.getString(cursor.getColumnIndex(COLUMN_ID)));
                myTeamStatus.setName(cursor.getString(cursor.getColumnIndex(COLUMN_NAME)));
                myTeamStatus.setStatus(cursor.getString(cursor.getColumnIndex(COLUMN_STATUS)));
                myTeamStatus.setUps(cursor.getString(cursor.getColumnIndex(COLUMN_UPS)));
                myTeamStatus.setClosed(cursor.getString(cursor.getColumnIndex(COLUMN_CLOSED)));
                myTeamStatus.setPercentage(cursor.getString(cursor.getColumnIndex(COLUMN_PER)));
                // Adding user record to list
                myTeamStatusList.add(myTeamStatus);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();

        // return user list
        return myTeamStatusList;
    }

    /**
     * This method is to fetch all user and return the list of user records
     *
     * @return list
     */
    public List<User> getAllUser() {
        // array of columns to fetch
        String[] columns = {
                COLUMN_USER_ID,
                COLUMN_USER_EMAIL,
                COLUMN_USER_NAME,
                COLUMN_USER_PASSWORD,
                COLUMN_USER_STATUS
        };
        // sorting orders
        String sortOrder =
                COLUMN_USER_NAME + " ASC";
        List<User> userList = new ArrayList<User>();

        SQLiteDatabase db = this.getReadableDatabase();

        // query the user table
        /**
         * Here query function is used to fetch records from user table this function works like we use sql query.
         * SQL query equivalent to this query function is
         * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
         */
        Cursor cursor = db.query(TABLE_USER, //Table to query
                columns,    //columns to return
                null,        //columns for the WHERE clause
                null,        //The values for the WHERE clause
                null,       //group the rows
                null,       //filter by row groups
                sortOrder); //The sort order


        // Traversing through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                User user = new User();
                user.setId(cursor.getString(cursor.getColumnIndex(COLUMN_USER_ID)));
                user.setName(cursor.getString(cursor.getColumnIndex(COLUMN_USER_NAME)));
                user.setEmail(cursor.getString(cursor.getColumnIndex(COLUMN_USER_EMAIL)));
                //user.setPassword(cursor.getString(cursor.getColumnIndex(COLUMN_USER_PASSWORD)));
                //user.setStatus(cursor.getString(cursor.getColumnIndex(COLUMN_USER_STATUS)));
                // Adding user record to list
                userList.add(user);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();

        // return user list
        return userList;
    }



    /**
     * This method to update user record
     *
     * @param user
     */
    public void updateUser(User user) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMN_USER_NAME, user.getName());
        values.put(COLUMN_USER_EMAIL, user.getEmail());
        // values.put(COLUMN_USER_PASSWORD, user.getPassword());

        // updating row
        db.update(TABLE_USER, values, COLUMN_USER_ID + " = ?",
                new String[]{String.valueOf(user.getId())});
        db.close();
    }

    /**
     * This method is to delete user record
     *
     * @param user
     */
    public void deleteUser(User user) {
        SQLiteDatabase db = this.getWritableDatabase();
        // delete user record by id
        db.delete(TABLE_USER, COLUMN_USER_ID + " = ?",
                new String[]{String.valueOf(user.getId())});
        db.close();
    }

    /**
     * This method to check user exist or not
     *
     * @param email
     * @return true/false
     */
    public boolean checkUser(String email) {

        // array of columns to fetch
        String[] columns = {
                COLUMN_USER_ID
        };
        SQLiteDatabase db = this.getReadableDatabase();

        // selection criteria
        String selection = COLUMN_USER_EMAIL + " = ?";

        // selection argument
        String[] selectionArgs = {email};

        // query user table with condition
        /**
         * Here query function is used to fetch records from user table this function works like we use sql query.
         * SQL query equivalent to this query function is
         * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
         */
        Cursor cursor = db.query(TABLE_USER, //Table to query
                columns,                    //columns to return
                selection,                  //columns for the WHERE clause
                selectionArgs,              //The values for the WHERE clause
                null,                       //group the rows
                null,                      //filter by row groups
                null);                      //The sort order
        int cursorCount = cursor.getCount();
        cursor.close();
        db.close();

        if (cursorCount > 0) {
            return true;
        }

        return false;
    }

    /**
     * This method to check user exist or not
     *
     * @param email
     * @param password
     * @return true/false
     */
    public boolean checkUser(String email, String password) {

        // array of columns to fetch
        String[] columns = {
                COLUMN_USER_ID
        };
        SQLiteDatabase db = this.getReadableDatabase();
        // selection criteria
        String selection = COLUMN_USER_EMAIL + " = ?" + " AND " + COLUMN_USER_PASSWORD + " = ?";

        // selection arguments
        String[] selectionArgs = {email, password};

        // query user table with conditions
        /**
         * Here query function is used to fetch records from user table this function works like we use sql query.
         * SQL query equivalent to this query function is
         * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
         */
        Cursor cursor = db.query(TABLE_USER, //Table to query
                columns,                    //columns to return
                selection,                  //columns for the WHERE clause
                selectionArgs,              //The values for the WHERE clause
                null,                       //group the rows
                null,                       //filter by row groups
                null);                      //The sort order

        int cursorCount = cursor.getCount();

        cursor.close();
        db.close();
        if (cursorCount > 0) {
            return true;
        }

        return false;
    }


}
