package com.example.gns076.rapsra.model;

import android.os.Parcel;
import android.os.Parcelable;

public class MessageParcelable {

    private String userId;
    private String body;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

/*
    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(userId);
        dest.writeString(body);
    }
*/

   /* public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public MessageParcelable createFromParcel(Parcel in) {
            return new MessageParcelable(in);
        }

        @Override
        public MessageParcelable[] newArray(int size) {
            return new MessageParcelable[size];
        }
    };*/
}