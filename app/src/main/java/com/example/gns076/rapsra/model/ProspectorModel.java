package com.example.gns076.rapsra.model;

import java.util.ArrayList;

public class ProspectorModel {
    private String prospectorId;

    private String prospectorFirstName;

    private String prospectorLastName;

    private String prospectorStreet;

    private String prospectorState;

    private String prospectorZipCode;

    private String prospectorSalesNo;

    private String prospectorPhone;

    private String prospectorEmail;

    public String getProspectorId() {
        return prospectorId;
    }

    public void setProspectorId(String prospectorId) {
        this.prospectorId = prospectorId;
    }

    public String getProspectorMerchandizeString() {
        return prospectorMerchandizeString;
    }

    public void setProspectorMerchandizeString(String prospectorMerchandizeString) {
        this.prospectorMerchandizeString = prospectorMerchandizeString;
    }

    public String getProspectorSubMerchandizeString() {
        return prospectorSubMerchandizeString;
    }

    public void setProspectorSubMerchandizeString(String prospectorSubMerchandizeString) {
        this.prospectorSubMerchandizeString = prospectorSubMerchandizeString;
    }

    private String prospectorCity;

    private String prospectorGender;

    private String prospectorAge;

    private String prospectorVisitReason;

    private String prospectorContactMethod;

    private String prospectorMerchandizeString;

    private String prospectorSubMerchandizeString;

    private String protectionPlanSold;

    public String getProtectionPlanSold() {
        return protectionPlanSold;
    }

    public void setProtectionPlanSold(String protectionPlanSold) {
        this.protectionPlanSold = protectionPlanSold;
    }

    private ArrayList<MerchandizeCategory> prospectorMerchandizeCatList;

    public ArrayList<MerchandizeCategory> getProspectorMerchandizeCatList() {
        return prospectorMerchandizeCatList;
    }

    public void setProspectorMerchandizeCatList(ArrayList<MerchandizeCategory> prospectorMerchandizeCatList) {
        this.prospectorMerchandizeCatList = prospectorMerchandizeCatList;
    }

    public String getProspectorFirstName() {
        return prospectorFirstName;
    }

    public void setProspectorFirstName(String prospectorFirstName) {
        this.prospectorFirstName = prospectorFirstName;
    }

    public String getProspectorLastName() {
        return prospectorLastName;
    }

    public void setProspectorLastName(String prospectorLastName) {
        this.prospectorLastName = prospectorLastName;
    }

    public String getProspectorStreet() {
        return prospectorStreet;
    }

    public void setProspectorStreet(String prospectorStreet) {
        this.prospectorStreet = prospectorStreet;
    }

    public String getProspectorState() {
        return prospectorState;
    }

    public void setProspectorState(String prospectorState) {
        this.prospectorState = prospectorState;
    }

    public String getProspectorZipCode() {
        return prospectorZipCode;
    }

    public void setProspectorZipCode(String prospectorZipCode) {
        this.prospectorZipCode = prospectorZipCode;
    }

    public String getProspectorSalesNo() {
        return prospectorSalesNo;
    }

    public void setProspectorSalesNo(String prospectorSalesNo) {
        this.prospectorSalesNo = prospectorSalesNo;
    }

    public String getProspectorPhone() {
        return prospectorPhone;
    }

    public void setProspectorPhone(String prospectorPhone) {
        this.prospectorPhone = prospectorPhone;
    }

    public String getProspectorEmail() {
        return prospectorEmail;
    }

    public void setProspectorEmail(String prospectorEmail) {
        this.prospectorEmail = prospectorEmail;
    }

    public String getProspectorCity() {
        return prospectorCity;
    }

    public void setProspectorCity(String prospectorCity) {
        this.prospectorCity = prospectorCity;
    }

    public String getProspectorGender() {
        return prospectorGender;
    }

    public void setProspectorGender(String prospectorGender) {
        this.prospectorGender = prospectorGender;
    }

    public String getProspectorAge() {
        return prospectorAge;
    }

    public void setProspectorAge(String prospectorAge) {
        this.prospectorAge = prospectorAge;
    }

    public String getProspectorVisitReason() {
        return prospectorVisitReason;
    }

    public void setProspectorVisitReason(String prospectorVisitReason) {
        this.prospectorVisitReason = prospectorVisitReason;
    }

    public String getProspectorContactMethod() {
        return prospectorContactMethod;
    }

    public void setProspectorContactMethod(String prospectorContactMethod) {
        this.prospectorContactMethod = prospectorContactMethod;
    }
}
