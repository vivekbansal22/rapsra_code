package com.example.gns076.rapsra.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class MerchandizeSubCategory implements Serializable ,Cloneable{

    @SerializedName("id")
    private String merchandizeSubCatId;

    public MerchandizeSubCategory(String merchandizeSubCatId, String merchandize_sub_category_name, boolean isSelected) {
        this.merchandizeSubCatId = merchandizeSubCatId;
        this.isSelected = isSelected;
        this.merchandize_sub_category_name = merchandize_sub_category_name;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        MerchandizeSubCategory clone = null;
        try
        {
            clone = (MerchandizeSubCategory) super.clone();

            //Copy new date object to cloned method
             clone.isSelected = this.isSelected;
             clone.setMerchandize_sub_category_name(this.merchandize_sub_category_name);
             clone.setMerchandizeSubCatId(this.getMerchandizeSubCatId());
        }
        catch (CloneNotSupportedException e)
        {
            throw new RuntimeException(e);
        }
        return clone;
    }
    public boolean isSelected() {
        return isSelected;
    }



    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    private boolean isSelected = false;

    @SerializedName("merchandize_sub_category_name")
    private String merchandize_sub_category_name;

    public String getMerchandizeSubCatId() {
        return merchandizeSubCatId;
    }

    public void setMerchandizeSubCatId(String merchandizeSubCatId) {
        this.merchandizeSubCatId = merchandizeSubCatId;
    }

    public String getMerchandize_sub_category_name() {
        return merchandize_sub_category_name;
    }

    public void setMerchandize_sub_category_name(String merchandize_sub_category_name) {
        this.merchandize_sub_category_name = merchandize_sub_category_name;
    }
}
