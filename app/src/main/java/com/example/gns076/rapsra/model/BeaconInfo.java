package com.example.gns076.rapsra.model;
import com.google.android.gms.nearby.messages.Distance;

public class BeaconInfo {

    public String beaconNamespace;
    public String beaconType;
    public String beaconMessage;
    public Distance beaconDistance;

    public BeaconInfo() {

    }

    public BeaconInfo(String beaconNamespace, String beaconType, String beaconMessage) {
        this.beaconNamespace = beaconNamespace;
        this.beaconType = beaconType;
        this.beaconMessage = beaconMessage;
    }

    public String getBeaconNamespace() {
        return beaconNamespace;
    }

    public void setBeaconNamespace(String beaconNamespace) {
        this.beaconNamespace = beaconNamespace;
    }

    public String getBeaconType() {
        return beaconType;
    }

    public void setBeaconType(String beaconType) {
        this.beaconType = beaconType;
    }

    public String getBeaconMessage() {
        return beaconMessage;
    }

    public void setBeaconMessage(String beaconMessage) {
        this.beaconMessage = beaconMessage;
    }

    public Distance getBeaconDistance() {
        return beaconDistance;
    }

    public void setBeaconDistance(Distance beaconDistance) {
        this.beaconDistance = beaconDistance;
    }
}
