package com.example.gns076.rapsra.activities;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.text.style.RelativeSizeSpan;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.PopupMenu;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;

import com.example.gns076.rapsra.R;
import com.example.gns076.rapsra.model.AppPreference;
import com.example.gns076.rapsra.model.MerchandizeCategoryDetails;
import com.example.gns076.rapsra.model.MerchandizeCateoryApiResponse;
import com.example.gns076.rapsra.model.MerchandizeSubCategory;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProspectActivity extends BaseActivity implements  View.OnClickListener{

    private TextView marqueForSearch;
    //List<MyTeamStatus> mts2;
    public APIInterface apiInterface;

    private AppPreference mAppPreference;
    private Button menuButton;
    private Button searchProspectingButton;
    private Button goBackButton;
    private Button virtualAssist;
    private int mYear, mMonth, mDay;

    private TextView prospectDate;
    private TextView followprospectDate;
    private EditText phoneText;
    private Spinner ageSpinner;
    private Spinner genderSpinner;
    private Spinner actionSpinner;
    private Spinner searchSpinner;
    private Spinner statusSpinner;
    private Spinner financeSpinner;
    private List<String> ageCategories;
    private List<String> genderCategories;
    private  List<String> actionItems;
    private List<String> statusList;
    private List<String> searchByList;
    private List<String> financeList;

    private MerchandizeCateoryApiResponse merchandizeCateoryApiResponse;
    private Spinner merchandiseCategorySpinner;
    private Spinner merchandiseSubCategorySpinner;

    private ArrayList<String> merchandiseCategoriesNameList;
    private ArrayList<String> merchandiseSubCatList;

    private ArrayList <MerchandizeSubCategory> bedroomSubCategoryList;
    private ArrayList<MerchandizeSubCategory> upholsterySubCategoryList;
    private ArrayList<MerchandizeSubCategory> homeOfficeSubCategoryList;
    private ArrayList<MerchandizeSubCategory> ocassionalSubCategoryList;
    private ArrayList<MerchandizeSubCategory> roomPackageSubCategoryList;
    private ArrayList<MerchandizeSubCategory> diningSubCategoryList;
    private ArrayList<MerchandizeSubCategory> matressSubCategoryList;
    private ArrayList<MerchandizeSubCategory> miscellaneousSubCategoryList;

    float phoneOriginalHintSize = (float) 15.77;
    float changedHintSize = (float)11.77;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prospect_new_x);
        mAppPreference = new AppPreference(this);
        apiInterface = APIClient.getClient().create(APIInterface.class);

        initializeObjects();

        merchandiseCategoriesNameList = new ArrayList<String>();
        merchandiseCategoriesNameList.add(" Merchandise");

        merchandiseSubCatList =  new ArrayList<String>();
        merchandiseSubCatList.add("SubCategories");

        getMerchandizeCategoryDataFromDataBase();

        searchProspectingButton.setOnClickListener(this);
        goBackButton.setOnClickListener(this);
        virtualAssist.setOnClickListener(this);
        phoneText.addTextChangedListener(new PhoneNumberFormattingTextWatcher());

        phoneText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                if (charSequence.length() == 0) {
                    // No entered text so will show hint
                    phoneText.setTextSize(TypedValue.COMPLEX_UNIT_SP, phoneOriginalHintSize);
                } else {
                    phoneText.setTextSize(TypedValue.COMPLEX_UNIT_SP, changedHintSize);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        setSpinnerData();

        menuButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Creating the instance of PopupMenu
                PopupMenu popup = new PopupMenu(ProspectActivity.this, menuButton);
                //Inflating the Popup using xml file

                menuButton.setBackgroundResource(R.drawable.exit_menu);

                Menu gamesMenu =popup.getMenu();


                popup.getMenuInflater()
                        .inflate(R.menu.menu_home, gamesMenu);
                gamesMenu.removeItem(R.id.logout);

                gamesMenu.removeItem(R.id.add_edit_so);
                gamesMenu.removeItem(R.id.scheduled_followups);


                for(int i = 0; i < gamesMenu.size(); i++) {
                    MenuItem item = gamesMenu.getItem(i);


                    SpannableString spanString = new SpannableString(gamesMenu.getItem(i).getTitle().toString());
                    int end = spanString.length();

                    //   spanString.setSpan(new RelativeSizeSpan(1.5f), 0, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    spanString.setSpan(new RelativeSizeSpan(0.04f), 0, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    item.setTitle(spanString);

                }


                popup.setOnDismissListener(new PopupMenu.OnDismissListener() {
                    @Override
                    public void onDismiss(PopupMenu popupMenu) {
                        menuButton.setBackgroundResource(R.drawable.menu);
                    }
                });


                //registering popup with OnMenuItemClickListener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {

                        Intent intentButtonClick;
                        switch (item.getItemId()){

                            case R.id.main_page:
                                intentButtonClick = new Intent(getApplicationContext(), SalesRadarActivityNew.class);
                                startActivity(intentButtonClick);
                                break;

                           /* case R.id.add_edit_so:
                                intentButtonClick = new Intent(getApplicationContext(), AddEditSoActivity.class);
                                startActivity(intentButtonClick);
                                break;*/

                            case R.id.add_edit_prospect:
                                intentButtonClick = new Intent(getApplicationContext(), ProspectorActivity.class);
                                intentButtonClick.putExtra("Token",mAppPreference.getToken() );

                                startActivity(intentButtonClick);
                                break;

                            case R.id.rap_gamify:
                                intentButtonClick = new Intent(getApplicationContext(), GamificationActivity.class);
                                startActivity(intentButtonClick);
                                break;
                            case R.id.my_rank:
                                intentButtonClick = new Intent(getApplicationContext(), WrittenActivity.class);
                                startActivity(intentButtonClick);
                                break;
                            case R.id.chat_menu:
                                intentButtonClick = new Intent(getApplicationContext(), ChatActivity.class);
                                startActivity(intentButtonClick);
                                break;

                            case R.id.ar_mode:
                                intentButtonClick = new Intent(getApplicationContext(), ArViewActivityNew.class);
                                startActivity(intentButtonClick);
                                break;

                            case R.id.floor_map_mode:
                                intentButtonClick = new Intent(getApplicationContext(), FloorMapActivity.class);
                                startActivity(intentButtonClick);
                                break;

                            case R.id.fun_games:
                                intentButtonClick = new Intent(getApplicationContext(), FunGames.class);
                                startActivity(intentButtonClick);
                                break;


                        }
                        return true;
                    }
                });

                popup.show(); //showing popup menu


            }
        }); //closi
    }


    public void initializeObjects(){

        String crawler1 = " ";

        mAppPreference = new AppPreference(this);
        crawler1=mAppPreference.getCrawler();

        menuButton = (Button)findViewById(R.id.menu_button_prospect_x);
        virtualAssist = (Button)findViewById(R.id.prospectVirtualAssist);

        marqueForSearch = (TextView)findViewById(R.id.prospect_news_crawler_x);
        marqueForSearch.setText(crawler1);
        marqueForSearch.setSelected(true);

        prospectDate = (TextView)findViewById(R.id.date_prospect_x) ;
        followprospectDate = (TextView)findViewById(R.id.followupdate_prospect_x);
        phoneText = (EditText)findViewById(R.id.phoneno_prospect_x);
        ageSpinner = (Spinner)findViewById(R.id.age_prospect_x);
        genderSpinner = (Spinner)findViewById(R.id.gender_prospect_x);
        actionSpinner = (Spinner)findViewById(R.id.action_prospect_x);
        searchSpinner = (Spinner)findViewById(R.id.searchby_prospect_x) ;
        statusSpinner = (Spinner)findViewById(R.id.status_prospect_x) ;
        financeSpinner = (Spinner)findViewById(R.id.finance_prospect_x);
        searchProspectingButton = (Button)findViewById(R.id.search_button_on_prospect_x);
        goBackButton = (Button)findViewById(R.id.go_bcak_button_prospect_x);
        merchandiseCategorySpinner = (Spinner)findViewById(R.id.merchandise_prospect_x);
        merchandiseSubCategorySpinner = (Spinner)findViewById(R.id.subcategory_prospect_x);


    }
    @Override
    public void onClick(View view)
    {
        Intent intentButtonClick;
        switch (view.getId()) {
            case R.id.search_button_on_prospect_x:
                intentButtonClick = new Intent(getApplicationContext(),SearchActivity.class);
                startActivity(intentButtonClick);
                break;

            case R.id.go_bcak_button_prospect_x:
                intentButtonClick = new Intent(getApplicationContext(),ProspectorActivity.class);
                intentButtonClick.putExtra("Token",mAppPreference.getToken() );

                finish();
                startActivity(intentButtonClick);

                break;


            case R.id.prospectVirtualAssist:
                Intent  intentVirtualAssist= new Intent(getApplicationContext(), VirtualAssistantActivity.class);
                startActivity(intentVirtualAssist);
                finish();
                break;
            // Do something
        }
    }

    public void onClickProspectDate(View v) {

        // Get Current Date
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        if (v.getId()==(R.id.date_prospect_x)) {




            DatePickerDialog datePickerDialog = new DatePickerDialog(this,R.style.CalendarDialogTheme,
                    new DatePickerDialog.OnDateSetListener() {

                        @Override
                        public void onDateSet(DatePicker view, int year,
                                              int monthOfYear, int dayOfMonth) {

                            CharSequence strDate = null;
                            c.set(year,monthOfYear,dayOfMonth);
                            Date chosenDate =  c.getTime();
                            //Date d = new Date(year, monthOfYear, dayOfMonth);
                            SimpleDateFormat dateFormatter = new SimpleDateFormat(
                                    "MM/dd/yy");
                            strDate = dateFormatter.format(chosenDate);
                            prospectDate.setText( strDate);
                            //prospectFollowDate.setText( year+ "-" +  (monthOfYear + 1) + "-" + dayOfMonth );

                        }
                    }, mYear, mMonth, mDay);
            datePickerDialog.show();
            datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
        }
        else if(v.getId()==(R.id.followupdate_prospect_x)){

            DatePickerDialog datePickerDialog = new DatePickerDialog(this,R.style.CalendarDialogTheme,
                    new DatePickerDialog.OnDateSetListener() {

                        @Override
                        public void onDateSet(DatePicker view, int year,
                                              int monthOfYear, int dayOfMonth) {

                            CharSequence strDate = null;
                            c.set(year,monthOfYear,dayOfMonth);
                            Date chosenDate =  c.getTime();
                            //Date d = new Date(year, monthOfYear, dayOfMonth);
                            SimpleDateFormat dateFormatter = new SimpleDateFormat(
                                    "MM/dd/yy");
                            strDate = dateFormatter.format(chosenDate);
                            followprospectDate.setText( strDate);
                            //prospectFollowDate.setText( year+ "-" +  (monthOfYear + 1) + "-" + dayOfMonth );

                        }
                    }, mYear, mMonth, mDay);
            datePickerDialog.show();
            datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());

        }

    }

    public void setSpinnerData(){

        ageCategories = new ArrayList<String>();
        ageCategories.add("Age");
        ageCategories.add("20-30");
        ageCategories.add("30-40");
        ageCategories.add("40-50");
        ageCategories.add("50+");

        ArrayAdapter<String> dataAdapterAge = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, ageCategories)
        { @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent)
        {
            View v = null;

            // If this is the initial dummy entry, make it hidden
            if (position == 0) {
                TextView tv = new TextView(getContext());
                tv.setHeight(0);
                tv.setVisibility(View.GONE);
                v = tv;
            }
            else {
                // Pass convertView as null to prevent reuse of special case views
                v = super.getDropDownView(position, null, parent);
            }

            // Hide scroll bar because it appears sometimes unnecessarily, this does not prevent scrolling
            parent.setVerticalScrollBarEnabled(false);
            return v;
        }


            @RequiresApi(api = Build.VERSION_CODES.O)
            @NonNull
            @Override
            public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
// Typeface myFont = Typeface.createFromAsset(getAssets(), "font/lucidagrande_normal.ttf");
                Typeface myFont = getResources().getFont(R.font.lucidagrande_normal);
                TextView v = (TextView) super.getView(position, convertView, parent);
                v.setTextSize(15.77f);
                v.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                v.setTextColor(getResources().getColor(R.color.colorForProspectSearchText));
                v.setTypeface(myFont);
                return v;
            }
        };
        dataAdapterAge.setDropDownViewResource(R.layout.spinner_item_age_gender_textcolor);
        ageSpinner.setAdapter(dataAdapterAge);


        genderCategories =  new ArrayList<String>();
        genderCategories.add("  Gender");
        genderCategories.add("Male");
        genderCategories.add("Female");

        ArrayAdapter<String> dataAdapterGender = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, genderCategories)
        { @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent)
        {
            View v = null;

            // If this is the initial dummy entry, make it hidden
            if (position == 0) {
                TextView tv = new TextView(getContext());
                tv.setHeight(0);
                tv.setVisibility(View.GONE);
                v = tv;
            }
            else {
                // Pass convertView as null to prevent reuse of special case views
                v = super.getDropDownView(position, null, parent);
            }

            // Hide scroll bar because it appears sometimes unnecessarily, this does not prevent scrolling
            parent.setVerticalScrollBarEnabled(false);
            return v;
        }


            @RequiresApi(api = Build.VERSION_CODES.O)
            @NonNull
            @Override
            public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
// Typeface myFont = Typeface.createFromAsset(getAssets(), "font/lucidagrande_normal.ttf");
                Typeface myFont = getResources().getFont(R.font.lucidagrande_normal);
                TextView v = (TextView) super.getView(position, convertView, parent);
                v.setTextSize(15.77f);
                v.setTextAlignment(View.TEXT_ALIGNMENT_TEXT_START);
                v.setTextColor(getResources().getColor(R.color.colorForProspectSearchText));
                v.setTypeface(myFont);
                return v;
            }
        };
        dataAdapterGender.setDropDownViewResource(R.layout.spinner_item_age_gender_textcolor);
        genderSpinner.setAdapter(dataAdapterGender);

        actionItems = new ArrayList<String>();

        actionItems.add("Action");

        ArrayAdapter<String> dataAdapterAction = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, actionItems)
        { @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent)
        {
            View v = null;

            // If this is the initial dummy entry, make it hidden
            if (position == 0) {
                TextView tv = new TextView(getContext());
                tv.setHeight(0);
                tv.setVisibility(View.GONE);
                v = tv;
            }
            else {
                // Pass convertView as null to prevent reuse of special case views
                v = super.getDropDownView(position, null, parent);
            }

            // Hide scroll bar because it appears sometimes unnecessarily, this does not prevent scrolling
            parent.setVerticalScrollBarEnabled(false);
            return v;
        }


            @RequiresApi(api = Build.VERSION_CODES.O)
            @NonNull
            @Override
            public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
// Typeface myFont = Typeface.createFromAsset(getAssets(), "font/lucidagrande_normal.ttf");
                Typeface myFont = getResources().getFont(R.font.lucidagrande_normal);
                TextView v = (TextView) super.getView(position, convertView, parent);
                v.setTextSize(15.77f);
                v.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                v.setTextColor(getResources().getColor(R.color.colorForProspectSearchText));
                v.setSingleLine(false);
                v.setTypeface(myFont);
                return v;
            }
        };
        dataAdapterAction.setDropDownViewResource(R.layout.spinner_item_age_gender_textcolor);
        actionSpinner.setAdapter(dataAdapterAction);


        searchByList = new ArrayList<String>();

        searchByList.add("Search");


        ArrayAdapter<String> dataAdapterSearch = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, searchByList)
        { @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent)
        {
            View v = null;

            // If this is the initial dummy entry, make it hidden
            if (position == 0) {
                TextView tv = new TextView(getContext());
                tv.setHeight(0);
                tv.setVisibility(View.GONE);
                v = tv;
            }
            else {
                // Pass convertView as null to prevent reuse of special case views
                v = super.getDropDownView(position, null, parent);
            }

            // Hide scroll bar because it appears sometimes unnecessarily, this does not prevent scrolling
            parent.setVerticalScrollBarEnabled(false);
            return v;
        }


            @RequiresApi(api = Build.VERSION_CODES.O)
            @NonNull
            @Override
            public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
// Typeface myFont = Typeface.createFromAsset(getAssets(), "font/lucidagrande_normal.ttf");
                Typeface myFont = getResources().getFont(R.font.lucidagrande_normal);
                TextView v = (TextView) super.getView(position, convertView, parent);
                v.setTextSize(15.77f);
                v.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                v.setTextColor(getResources().getColor(R.color.colorForProspectSearchText));
                v.setSingleLine(false);
                v.setTypeface(myFont);
                return v;
            }
        };
        dataAdapterSearch.setDropDownViewResource(R.layout.spinner_item_age_gender_textcolor);
        searchSpinner.setAdapter(dataAdapterSearch);


        statusList = new ArrayList<String>();

        statusList.add("Status");


        ArrayAdapter<String> dataAdapterStatus = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, statusList)
        { @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent)
        {
            View v = null;

            // If this is the initial dummy entry, make it hidden
            if (position == 0) {
                TextView tv = new TextView(getContext());
                tv.setHeight(0);
                tv.setVisibility(View.GONE);
                v = tv;
            }
            else {
                // Pass convertView as null to prevent reuse of special case views
                v = super.getDropDownView(position, null, parent);
            }

            // Hide scroll bar because it appears sometimes unnecessarily, this does not prevent scrolling
            parent.setVerticalScrollBarEnabled(false);
            return v;
        }


            @RequiresApi(api = Build.VERSION_CODES.O)
            @NonNull
            @Override
            public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
// Typeface myFont = Typeface.createFromAsset(getAssets(), "font/lucidagrande_normal.ttf");
                Typeface myFont = getResources().getFont(R.font.lucidagrande_normal);
                TextView v = (TextView) super.getView(position, convertView, parent);
                v.setTextSize(15.77f);
                v.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                v.setTextColor(getResources().getColor(R.color.colorForProspectSearchText));
                v.setSingleLine(false);
                v.setTypeface(myFont);
                return v;
            }
        };
        dataAdapterStatus.setDropDownViewResource(R.layout.spinner_item_age_gender_textcolor);
        statusSpinner.setAdapter(dataAdapterStatus);


        financeList = new ArrayList<String>();

        financeList.add("Finance");


        ArrayAdapter<String> dataAdapterFinance = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, financeList)
        { @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent)
        {
            View v = null;

            // If this is the initial dummy entry, make it hidden
            if (position == 0) {
                TextView tv = new TextView(getContext());
                tv.setHeight(0);
                tv.setVisibility(View.GONE);
                v = tv;
            }
            else {
                // Pass convertView as null to prevent reuse of special case views
                v = super.getDropDownView(position, null, parent);
            }

            // Hide scroll bar because it appears sometimes unnecessarily, this does not prevent scrolling
            parent.setVerticalScrollBarEnabled(false);
            return v;
        }


            @RequiresApi(api = Build.VERSION_CODES.O)
            @NonNull
            @Override
            public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
// Typeface myFont = Typeface.createFromAsset(getAssets(), "font/lucidagrande_normal.ttf");
                Typeface myFont = getResources().getFont(R.font.lucidagrande_normal);
                TextView v = (TextView) super.getView(position, convertView, parent);
                v.setTextSize(15.77f);
                v.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                v.setTextColor(getResources().getColor(R.color.colorForProspectSearchText));
                v.setSingleLine(false);
                v.setTypeface(myFont);
                return v;
            }
        };
        dataAdapterFinance.setDropDownViewResource(R.layout.spinner_item_age_gender_textcolor);
        financeSpinner.setAdapter(dataAdapterFinance);



        //Merchandise Categories and subcategories spinner


        ArrayAdapter<String> dataAdapterMerchandiseCategory = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, merchandiseCategoriesNameList)
        { @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent)
        {
            View v = null;

            // If this is the initial dummy entry, make it hidden
            if (position == 0) {
                TextView tv = new TextView(getContext());
                tv.setHeight(0);
                tv.setVisibility(View.GONE);
                v = tv;
            }
            else {
                // Pass convertView as null to prevent reuse of special case views
                v = super.getDropDownView(position, null, parent);
            }

            // Hide scroll bar because it appears sometimes unnecessarily, this does not prevent scrolling
            parent.setVerticalScrollBarEnabled(true);
            return v;
        }


            @RequiresApi(api = Build.VERSION_CODES.O)
            @NonNull
            @Override
            public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
// Typeface myFont = Typeface.createFromAsset(getAssets(), "font/lucidagrande_normal.ttf");
                Typeface myFont = getResources().getFont(R.font.lucidagrande_normal);
                TextView v = (TextView) super.getView(position, convertView, parent);
                v.setTextSize(15.77f);
                v.setTextAlignment(View.TEXT_ALIGNMENT_TEXT_START);
                v.setTextColor(getResources().getColor(R.color.colorForProspectSearchText));
                v.setTypeface(myFont);
                return v;
            }
        };
        dataAdapterMerchandiseCategory.setDropDownViewResource(R.layout.spinner_item_merchandise);
        merchandiseCategorySpinner.setAdapter(dataAdapterMerchandiseCategory);
        // merchandiseCategorySpinner.setDropDownHorizontalOffset(30);



        merchandiseCategorySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String selectedItemText = (String) adapterView.getItemAtPosition(i);
                //  Toast.makeText(getApplicationContext(), "Item Selected:"+selectedItemText, Toast.LENGTH_LONG).show();
                setMerchandiseSubCategoryData(selectedItemText);
                //   TextView selectedMerchandiseItem = (TextView)  adapterView.getSelectedView();
                //   selectedMerchandiseItem.setTextColor(getResources().getColor(R.color.colorForRapsraText));
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


    }


    public void setMerchandiseSubCategoryData(String merCategory){

        merchandiseSubCatList =  new ArrayList<String>();
        merchandiseSubCatList.add("Subcategories");
        switch(merCategory) {

            case "Bedroom":
                for (MerchandizeSubCategory mscat : bedroomSubCategoryList) {
                    merchandiseSubCatList.add(mscat.getMerchandize_sub_category_name());
                }

                break;

            case "Upholstery":
                for (MerchandizeSubCategory mscat : upholsterySubCategoryList) {
                    merchandiseSubCatList.add(mscat.getMerchandize_sub_category_name());
                }
                break;

            case "Home Office":
                for (MerchandizeSubCategory mscat : homeOfficeSubCategoryList) {
                    merchandiseSubCatList.add(mscat.getMerchandize_sub_category_name());
                }
                break;

            case "Ocassional":
                for (MerchandizeSubCategory mscat : ocassionalSubCategoryList) {
                    merchandiseSubCatList.add(mscat.getMerchandize_sub_category_name());
                }
                break;

            case "Room Package":
                for (MerchandizeSubCategory mscat : roomPackageSubCategoryList) {
                    merchandiseSubCatList.add(mscat.getMerchandize_sub_category_name());
                }
                break;

            case "Dining":
                for (MerchandizeSubCategory mscat : diningSubCategoryList) {
                    merchandiseSubCatList.add(mscat.getMerchandize_sub_category_name());
                }
                break;

            case "Mattress":
                for (MerchandizeSubCategory mscat : matressSubCategoryList) {
                    merchandiseSubCatList.add(mscat.getMerchandize_sub_category_name());
                }
                break;

            case "Miscellaneous":
                for (MerchandizeSubCategory mscat : miscellaneousSubCategoryList) {
                    merchandiseSubCatList.add(mscat.getMerchandize_sub_category_name());
                }
                break;


        }

        ArrayAdapter<String> dataAdapterMerchandiseSubCategory = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, merchandiseSubCatList)
        { @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent)
        {
            View v = null;

            // If this is the initial dummy entry, make it hidden
            if (position == 0) {
                TextView tv = new TextView(getContext());
                tv.setHeight(0);
                tv.setVisibility(View.GONE);
                v = tv;
            }
            else {
                // Pass convertView as null to prevent reuse of special case views
                v = super.getDropDownView(position, null, parent);
            }

            // Hide scroll bar because it appears sometimes unnecessarily, this does not prevent scrolling
            parent.setVerticalScrollBarEnabled(true);
            return v;
        }


            @RequiresApi(api = Build.VERSION_CODES.O)
            @NonNull
            @Override
            public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
// Typeface myFont = Typeface.createFromAsset(getAssets(), "font/lucidagrande_normal.ttf");
                Typeface myFont = getResources().getFont(R.font.lucidagrande_normal);
                TextView v = (TextView) super.getView(position, convertView, parent);
                v.setTextSize(15.77f);
                v.setTextAlignment(View.TEXT_ALIGNMENT_TEXT_START);
                v.setTextColor(getResources().getColor(R.color.colorForProspectSearchText));
                v.setTypeface(myFont);
                return v;
            }
        };
        dataAdapterMerchandiseSubCategory.setDropDownViewResource(R.layout.spinner_item_merchandise);
        merchandiseSubCategorySpinner.setAdapter(dataAdapterMerchandiseSubCategory);
        merchandiseSubCategorySpinner.setDropDownHorizontalOffset(5);


    }

    public void getMerchandizeCategoryDataFromDataBase(){

        try {
            Call<MerchandizeCateoryApiResponse> call2 = apiInterface.getMerchandizeCategories(mAppPreference.getDealerId());
            call2.enqueue(new Callback<MerchandizeCateoryApiResponse>() {
                @Override
                public void onResponse(Call<MerchandizeCateoryApiResponse> call, Response<MerchandizeCateoryApiResponse> apiResponse) {
                    if(!apiResponse.body().isError()) {
                        // handleResponceFromBeacon(call, apiResponse);
                        merchandizeCateoryApiResponse = apiResponse.body();
                        separateDataFromApiResponse();


                    }
                }

                @Override
                public void onFailure(Call<MerchandizeCateoryApiResponse> call, Throwable t) {

                    //  showDialogForNotification("No Network Connection");

                    //  Toast.makeText(getApplicationContext(), "No Network Connection", Toast.LENGTH_LONG).show();
                    call.cancel();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void separateDataFromApiResponse(){

        if(merchandizeCateoryApiResponse!=null){

            ArrayList<MerchandizeCategoryDetails> merchandizeCategories = merchandizeCateoryApiResponse.getMerchandizeCategories();


            if(merchandizeCategories!=null){
                for(MerchandizeCategoryDetails merchandizeCategoryDetails:merchandizeCategories){
                    String merchandizeCategoryName = merchandizeCategoryDetails.getMerchandizeCategory().getMerchandize_category_name();
                    merchandiseCategoriesNameList.add(merchandizeCategoryName);
                    if(merchandizeCategoryName.equalsIgnoreCase("Bedroom")){
                        bedroomSubCategoryList = merchandizeCategoryDetails.getSubCategories();
                    }
                    if(merchandizeCategoryName.equalsIgnoreCase("Upholstery")){
                        upholsterySubCategoryList  = merchandizeCategoryDetails.getSubCategories();
                    }
                    if(merchandizeCategoryName.equalsIgnoreCase("Home Office")){
                        homeOfficeSubCategoryList = merchandizeCategoryDetails.getSubCategories();
                    }

                    if(merchandizeCategoryName.equalsIgnoreCase("Ocassional")){
                        ocassionalSubCategoryList = merchandizeCategoryDetails.getSubCategories();
                    }
                    if(merchandizeCategoryName.equalsIgnoreCase("Room Package")){
                        roomPackageSubCategoryList = merchandizeCategoryDetails.getSubCategories();
                    }
                    if(merchandizeCategoryName.equalsIgnoreCase("Dining")){
                        diningSubCategoryList = merchandizeCategoryDetails.getSubCategories();
                    }
                    if(merchandizeCategoryName.equalsIgnoreCase("Mattress")){
                        matressSubCategoryList = merchandizeCategoryDetails.getSubCategories();
                    }
                    if(merchandizeCategoryName.equalsIgnoreCase("Miscellaneous")){
                        miscellaneousSubCategoryList = merchandizeCategoryDetails.getSubCategories();
                    }

                }
            }


        }





    }
}
