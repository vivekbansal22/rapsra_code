package com.example.gns076.rapsra.activities;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.Constraints;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.widget.NestedScrollView;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatTextView;

import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.gns076.rapsra.R;
import com.example.gns076.rapsra.helpers.InputValidation;
import com.example.gns076.rapsra.model.AppPreference;
import com.example.gns076.rapsra.model.LoginResponse;
import com.example.gns076.rapsra.model.MyTeamStatus;
import com.example.gns076.rapsra.sql.DatabaseHelper;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private final AppCompatActivity activity = LoginActivity.this;

    private NestedScrollView nestedScrollView;
    /*  private TextInputLayout textInputLayoutEmail;
      private TextInputLayout textInputLayoutPassword;*/
    private EditText textInputEditTextEmail;
    private EditText textInputEditTextPassword;
    private Button appCompatButtonLogin;
    /*private LinearLayout layoutUserName;
    private LinearLayout layoutPassword;*/
    //  private AppCompatTextView textViewLinkRegister;
    private InputValidation inputValidation;
    private DatabaseHelper databaseHelper;
    private Map<String,String> statusList = new <String,String>HashMap();
    private   List<String> ls;

    private APIInterface apiInterface;
    private FirebaseDatabase fdinstance;
    private AppPreference mAppPreference;

  //  private BeaconScanningApp scanningApp;

    boolean doubleBackToExitPressedOnce = false;
    private TextView forgotPasswordLink;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       // setContentView(R.layout.activity_login_new1);
        setContentView(R.layout.activity_login_x);
    //    scanningApp = new BeaconScanningApp();




        initViews();
        initListeners();
        initObjects();

        if(!(databaseHelper.getAllTeamStatus().size()>0)) {
            List<MyTeamStatus> mtsl = new ArrayList<>();
            MyTeamStatus myTeamStatus = new MyTeamStatus();

            myTeamStatus.setId("JP");
            myTeamStatus.setName("JIM P");
            myTeamStatus.setStatus("Active");
            myTeamStatus.setUps("10");
            myTeamStatus.setClosed("3");
            myTeamStatus.setPercentage("30%");
            mtsl.add(myTeamStatus);

            MyTeamStatus myTeamStatus1 = new MyTeamStatus();

            myTeamStatus1.setId("JW");
            myTeamStatus1.setName("JIM W");
            myTeamStatus1.setStatus("Active");
            myTeamStatus1.setUps("3");
            myTeamStatus1.setClosed("2");
            myTeamStatus1.setPercentage("66%");
            mtsl.add(myTeamStatus1);

            MyTeamStatus myTeamStatus2 = new MyTeamStatus();

            myTeamStatus2.setId("DK");
            myTeamStatus2.setName("DON K");
            myTeamStatus2.setStatus("Break");
            myTeamStatus2.setUps("12");
            myTeamStatus2.setClosed("3");
            myTeamStatus2.setPercentage("25%");
            mtsl.add(myTeamStatus2);

            MyTeamStatus myTeamStatus3 = new MyTeamStatus();

            myTeamStatus3.setId("LK");
            myTeamStatus3.setName("LINA K");
            myTeamStatus3.setStatus("off");
            myTeamStatus3.setUps("5");
            myTeamStatus3.setClosed("0");
            myTeamStatus3.setPercentage("0%");
            mtsl.add(myTeamStatus3);

            MyTeamStatus myTeamStatus4 = new MyTeamStatus();

            myTeamStatus4.setId("IS");
            myTeamStatus4.setName("IRIS S");
            myTeamStatus4.setStatus("off");
            myTeamStatus4.setUps("2");
            myTeamStatus4.setClosed("1");
            myTeamStatus4.setPercentage("50%");
            mtsl.add(myTeamStatus4);

            MyTeamStatus myTeamStatus5 = new MyTeamStatus();

            myTeamStatus5.setId("JS");
            myTeamStatus5.setName("JERRY S");
            myTeamStatus5.setStatus("Training");
            myTeamStatus5.setUps("0");
            myTeamStatus5.setClosed("0");
            myTeamStatus5.setPercentage("0%");
            mtsl.add(myTeamStatus5);


            for (MyTeamStatus item : mtsl) {
                databaseHelper.addTeamStatus(item);
            }
        }

        statusList.put(getResources().getString(R.string.available),getResources().getString(R.string.available_status_value));
        statusList.put(getResources().getString(R.string.with_new_client),getResources().getString(R.string.with_new_client_value));
        statusList.put(getResources().getString(R.string.with_existing_client),getResources().getString(R.string.with_existing_client_value));
        statusList.put(getResources().getString(R.string.non_selling_activity),getResources().getString(R.string.non_selling_activity_value));
        statusList.put(getResources().getString(R.string.training),getResources().getString(R.string.training_value));
        statusList.put(getResources().getString(R.string.meeting),getResources().getString(R.string.meeting_value));
        statusList.put(getResources().getString(R.string.on_break),getResources().getString(R.string.on_break_value));
        statusList.put(getResources().getString(R.string.off),getResources().getString(R.string.off_value));

        ls= new ArrayList<>(statusList.keySet());
        if(!(databaseHelper.getAllStatus().size()>0)){
            for (int i = 0; i < ls.size(); i++) {
                databaseHelper.addStatus(String.valueOf(i), ls.get(i), statusList.get(ls.get(i)));
            }
        }


    }



    /**
     * This method is to initialize views
     */
    private void initViews() {



        textInputEditTextEmail = (EditText) findViewById(R.id.userName);
       // textInputEditTextEmail.setSelection(1);
        textInputEditTextPassword = (EditText) findViewById(R.id.userPassword);
    //    textInputEditTextPassword.setSelection(1);
        appCompatButtonLogin = (Button) findViewById(R.id.userSignInButton);
        forgotPasswordLink = (TextView)findViewById(R.id.forgor_text_lpo);

   }

    /**
     * This method is to initialize listeners
     */
    private void initListeners() {
        appCompatButtonLogin.setOnClickListener(this);
        //   textViewLinkRegister.setOnClickListener(this);
        forgotPasswordLink.setOnClickListener(this);
    }

    /**
     * This method is to initialize objects to be used
     */
    private void initObjects() {
        databaseHelper = new DatabaseHelper(activity);
        inputValidation = new InputValidation(activity);

        if(fdinstance==null){
            fdinstance = FirebaseDatabase.getInstance();
            fdinstance.getReference().keepSynced(true);
        }

    }

    /**
     * This implemented method is to listen the click on view
     *
     * @param v
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
           case R.id.userSignInButton:
          //  case R.id.signIn:
                verifyFromSQLite();
                break;

            case R.id.forgor_text_lpo:
                // Navigate to RegisterActivity
                showDialogForForgotPassword();
                break;
        }
    }



    /**
     * This method is to validate the input text fields and verify login credentials from
     */
    public void verifyFromSQLite() {
        if (!inputValidation.isInputEditTextFilled(textInputEditTextEmail, getString(R.string.error_message_email))) {
            Toast toast =  Toast.makeText(getApplicationContext(), "Empty field" , Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER |Gravity.CENTER_HORIZONTAL, 0, 0);
            toast.show();
            return;
        }
        if (!inputValidation.isInputEditTextEmail(textInputEditTextEmail, getString(R.string.error_message_email))) {
            Toast toast = Toast.makeText(getApplicationContext(), "Invalid Email" , Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER |Gravity.CENTER_HORIZONTAL, 0, 0);
            toast.show();
            return;
        }
        if (!inputValidation.isInputEditTextFilled(textInputEditTextPassword, getString(R.string.error_message_email))) {
            Toast toast = Toast.makeText(getApplicationContext(), "Empty field" , Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER |Gravity.CENTER_HORIZONTAL, 0, 0);
            toast.show();
            return;
        }

        apiInterface = APIClient.getClient().create(APIInterface.class);

        try {
            Call<LoginResponse> call1 = apiInterface.checkUser(textInputEditTextEmail.getText().toString().trim()
                    , textInputEditTextPassword.getText().toString().trim());


            call1.enqueue(new Callback<LoginResponse>() {
                @Override
                public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                 //   System.out.println(response.body().toString());
                    LoginResponse res=response.body();

                 //   System.out.println("message"+res.getMessage());

                    if (!res.isError()) {

                        mAppPreference = new AppPreference(LoginActivity.this);
                        Intent accountsIntent = new Intent(activity, SalesRadarActivityNew.class);
                        mAppPreference.setToken(res.getUser().getToken());
                        mAppPreference.setAgentId(res.getUser().getId());
                        mAppPreference.setStoreId(res.getUser().getStore_id());
                        mAppPreference.setDealerId(res.getUser().getDealer_id());
                        mAppPreference.setEmail(res.getUser().getEmail());
                        mAppPreference.setUser(res.getUser().getName());
                        mAppPreference.setUserStatus("Off");
                        mAppPreference.setRole(res.getUser().getUser_role());
                        emptyInputEditText();
                        startActivity(accountsIntent);


                    } else {
                        Toast toast = Toast.makeText(getApplicationContext(), res.getMessage() , Toast.LENGTH_LONG);
                        toast.setGravity(Gravity.CENTER |Gravity.CENTER_HORIZONTAL, 0, 0);
                        toast.show();
                    }
                }

                @Override
                public void onFailure(Call<LoginResponse> call, Throwable t) {

                    call.cancel();
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }
    }


    /**
     * This method is to empty all input edit text
     */
    private void emptyInputEditText() {
        textInputEditTextEmail.setText(null);
        textInputEditTextPassword.setText(null);
    }
    /**
     * This method is used on pressing back button
     */
    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();


            return;

        }

        this.doubleBackToExitPressedOnce = true;
        Toast toast = Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER |Gravity.CENTER_HORIZONTAL, 0, 0);
        toast.show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);
    }

    public void showDialogForForgotPassword(){


        TextView cancelDialogButton;

        final Dialog dialog = new Dialog(LoginActivity.this);

        dialog.setCancelable(false);
        dialog.setContentView(R.layout.forgot_password_popup);

       // dialog.getWindow().setLayout(Constraints.LayoutParams.FILL_PARENT, Toolbar.LayoutParams.FILL_PARENT);
    //    dialog.getWindow().setGravity(Gravity.CENTER);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);


        cancelDialogButton = dialog.findViewById(R.id.cancelForgotPasswordPopup);
        cancelDialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                dialog.dismiss();

            }

        });


        Window window = dialog.getWindow();
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.TOP);
        dialog.show();

    }

}
