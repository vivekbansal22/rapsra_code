package com.example.gns076.rapsra.adapters.com.example.gns076.rapsra.adapters.spinner;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.AutoCompleteTextView;

import java.util.Calendar;


public class MyAutoCompleteTextView extends AutoCompleteTextView {

    private static final int MAX_CLICK_DURATION = 200;
    private long startClickTime;

    public MyAutoCompleteTextView(Context context) {
        super(context);
    }

    public MyAutoCompleteTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MyAutoCompleteTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public MyAutoCompleteTextView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }



    public void performFiltering(CharSequence text){
        super.performFiltering(text, 0);
    }


    @Override
    public boolean enoughToFilter() {
        if (!isEnabled() || getAdapter() == null){
            return false;
        }
        if(getText().length() == 0){
            return true;
        }else{
            return super.enoughToFilter();
        }
    }


    @Override
    public boolean onTouchEvent(MotionEvent event) {

        if (!isEnabled() || getAdapter() == null){
            return false;
        }

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN: {
                startClickTime = Calendar.getInstance().getTimeInMillis();
                break;
            }
            case MotionEvent.ACTION_UP: {
                long clickDuration = Calendar.getInstance().getTimeInMillis() - startClickTime;
                if (clickDuration < MAX_CLICK_DURATION) {
                     if(!isPopupShowing()){
                         performFiltering(getText());
                         showDropDown();
                     }
                }
                break;
            }
        }
        return super.onTouchEvent(event);
    }


}
