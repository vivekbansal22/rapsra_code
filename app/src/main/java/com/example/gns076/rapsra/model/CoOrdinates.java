package com.example.gns076.rapsra.model;

public class CoOrdinates {

   private  double X;
    private double Y;

    public double getX() {
        return X;
    }

    public CoOrdinates(double x, double y) {
        X = x;
        Y = y;
    }
    public CoOrdinates() {

    }

    public void setX(double x) {
        X = x;
    }

    public double getY() {
        return Y;
    }

    public void setY(double y) {
        Y = y;
    }
}
