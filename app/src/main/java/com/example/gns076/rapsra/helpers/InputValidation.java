package com.example.gns076.rapsra.helpers;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

/**
 * Created by .
 */
public class InputValidation {
    private Context context;

    /**
     * constructor
     *
     * @param context
     */
    public InputValidation(Context context) {
        this.context = context;
    }

    /**
     * method to check InputEditText filled .
     *
     * @param textInputEditText

     * @param message
     * @return
     */
    public boolean isInputEditTextFilled(EditText textInputEditText, String message) {
        String value = textInputEditText.getText().toString().trim();
        //System.out.println("hello1"+value);
        if (value.isEmpty()) {
            //System.out.println("hello2"+value);
            //textInputLayout.setError(message);
            //System.out.println("hello3"+value);
            hideKeyboardFrom(textInputEditText);
            return false;
        } else {
            //textInputLayout.setErrorEnabled(false);
        }

        return true;
    }


    /**
     * method to check InputEditText has valid email .
     *
     * @param textInputEditText

     * @param message
     * @return
     */
    public boolean isInputEditTextEmail(EditText textInputEditText,String message) {
        String value = textInputEditText.getText().toString().trim();
        if (value.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(value).matches()) {
            //textInputLayout.setError(message);
            hideKeyboardFrom(textInputEditText);
            return false;
        } else {
            //textInputLayout.setErrorEnabled(false);
        }
        return true;
    }

    public boolean isInputEditTextMatches(EditText textInputEditText1, EditText textInputEditText2, String message) {
        String value1 = textInputEditText1.getText().toString().trim();
        String value2 = textInputEditText2.getText().toString().trim();
        if (!value1.contentEquals(value2)) {
            //textInputLayout.setError(message);
            hideKeyboardFrom(textInputEditText2);
            return false;
        } else {
            //textInputLayout.setErrorEnabled(false);
        }
        return true;
    }

    /**
     * method to Hide keyboard
     *
     * @param view
     */
    private void hideKeyboardFrom(View view) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    public boolean isValidPhone(CharSequence phone) {
        /*if (TextUtils.isEmpty(phone)) {
            return false;
        } else {
            return android.util.Patterns.PHONE.matcher(phone).matches();
        }*/
        if (!TextUtils.isEmpty(phone)){
            return android.util.Patterns.PHONE.matcher(phone).matches();
        }
        return true;

    }

    public boolean isValidEmail(Context c, CharSequence email) {
       /* if (TextUtils.isEmpty(email)) {
            return false;
        }else{
         return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
         }*/

        if (!TextUtils.isEmpty(email)) {
            return Patterns.EMAIL_ADDRESS.matcher(email).matches();
        }
        return true;
    }

    public boolean isAlphaNumeric(String value){
        if (value.matches("[A-Za-z0-9]+")){
            return true;
        }
        return false;
    }
}
