package com.example.gns076.rapsra.model;

import com.google.gson.annotations.SerializedName;

public class ProspectsFollowUpInfoModel implements java.io.Serializable{
    @SerializedName("id")
    public String id;

    @SerializedName("date_notes")
    public String date_notes;
    @SerializedName("FollowDate")
    public String FollowDate;

    @SerializedName("updated_at")
    public String updatedAt;

    public ProspectsFollowUpInfoModel(String ref_Id, String date_notes, String followDate,String updatedAt) {
        this.id = ref_Id;
        this.date_notes = date_notes;
        this.FollowDate = followDate;
        this.updatedAt = updatedAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getRef_Id() {
        return id;
    }

    public void setRef_Id(String ref_Id) {
        this.id = ref_Id;
    }

    public String getDate_notes() {
        return date_notes;
    }

    public void setDate_notes(String date_notes) {
        this.date_notes = date_notes;
    }

    public String getFollowDate() {
        return FollowDate;
    }

    public void setFollowDate(String followDate) {
        this.FollowDate = followDate;
    }


}
