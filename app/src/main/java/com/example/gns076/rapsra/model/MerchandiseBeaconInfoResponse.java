package com.example.gns076.rapsra.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class MerchandiseBeaconInfoResponse {

    @SerializedName("error")
    public boolean error;

    @SerializedName("beacon_list")
    public ArrayList<BeaconModel> beaconInfo = new ArrayList<>();

    @SerializedName("message")
    public String message;

    @SerializedName("log_flag")
    public String logFlag;


    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public ArrayList<BeaconModel> getBeaconInfo() {
        return beaconInfo;
    }

    public void setBeaconInfo(ArrayList<BeaconModel> beaconInfo) {
        this.beaconInfo = beaconInfo;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getLogFlag() {
        return logFlag;
    }

    public void setLogFlag(String logFlag) {
        this.logFlag = logFlag;
    }
}
