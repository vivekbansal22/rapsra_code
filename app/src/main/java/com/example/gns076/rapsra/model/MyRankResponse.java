package com.example.gns076.rapsra.model;

import com.google.gson.annotations.SerializedName;

public class MyRankResponse {

    @SerializedName("error")
    private boolean error;


    @SerializedName("message")
    private String message;

    @SerializedName("data")
    private Data data;


    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

}
