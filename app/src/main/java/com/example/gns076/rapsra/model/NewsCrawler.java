package com.example.gns076.rapsra.model;

import com.google.gson.annotations.SerializedName;

public class NewsCrawler {

    @SerializedName("error")
    public boolean error;

    @SerializedName("crawler")
    public String[] crawler;

    @SerializedName("message")
    public String message;

    public String[] getCrawler() {
        return crawler;
    }

    public void setCrawler(String[] crawler) {
        this.crawler = crawler;
    }







    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


}
