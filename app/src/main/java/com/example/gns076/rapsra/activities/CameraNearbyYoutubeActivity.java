package com.example.gns076.rapsra.activities;

import android.Manifest;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.os.Bundle;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.gns076.rapsra.R;
import com.example.gns076.rapsra.model.AppPreference;
import com.example.gns076.rapsra.model.BeaconInfo;
import com.example.gns076.rapsra.model.BeaconInfoResponse;
import com.example.gns076.rapsra.model.BeaconModel;
import com.example.gns076.rapsra.model.BeaconModelFloorPlan;
import com.google.android.gms.nearby.messages.MessageListener;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CameraNearbyYoutubeActivity extends YouTubeBaseActivity implements SurfaceHolder.Callback,YouTubePlayer.OnInitializedListener {

    private static final String TAG = CameraNearbyYoutubeActivity.class.getSimpleName();
    Camera camera;
    SurfaceView surfaceView;
    SurfaceView surfaceViewYoutube;
    SurfaceHolder surfaceHolder;
    SurfaceHolder surfaceHolderyoutube;
    private AppPreference mAppPreference;
    private BeaconInfoResponse beaconInfoResponse;
    private String role;
    public APIInterface apiInterface;
    boolean previewing = false;
    LayoutInflater controlInflater = null;
    private List<BeaconInfo> beaconList = new ArrayList<>();

    Timer scheduletimer = new Timer();

    private static final int REQUEST_GET_ACCOUNT = 112;
    private static final int PERMISSION_REQUEST_CODE = 200;
    private MessageListener mMessageListener;
    //private String strForVideoPlaying= "";
    private String beaconAttachment = "";
    private String messageDisplayed;



    YouTubePlayer.OnInitializedListener onInitializedListener;

    YouTubePlayerView youTubePlayerView;
    YouTubePlayer youTubePlayer = null;

    TextView displayMessageRecievedTextView ;

    private List<BeaconModel> beaconInfoList = new ArrayList<BeaconModel>();
    private SharedPreferences sharedPreferences;
    List<BeaconModelFloorPlan> beaconDistanceList =  new ArrayList<>();
    Gson gson = new Gson();
    private Map<String,String> beaconAttachmentMap = new HashMap<String,String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.i(TAG,"inside onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera_nearby_youtube);

        apiInterface = APIClient.getClient().create(APIInterface.class);
        mAppPreference = new AppPreference(this);
        role =  mAppPreference.getRole();
        surfaceView = (SurfaceView) findViewById(R.id.camerapreview1);
        surfaceHolder = surfaceView.getHolder();
        surfaceHolder.addCallback(this);
        controlInflater = LayoutInflater.from(getBaseContext());
        final View  viewControl = controlInflater.inflate(R.layout.control, null);

        youTubePlayerView = (YouTubePlayerView)viewControl.findViewById(R.id.youtube2);
        youTubePlayerView.setVisibility(View.INVISIBLE);
        final ViewGroup.LayoutParams layoutParamsControl
                = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);

        // for Displaying text
        displayMessageRecievedTextView = (TextView)viewControl.findViewById(R.id.textViewMessageForBeacon) ;
        displayMessageRecievedTextView.setVisibility(View.INVISIBLE);

        this.addContentView(viewControl, layoutParamsControl);

     /*   surfaceViewYoutube = (SurfaceView) findViewById(R.id.youtubepreview1);
        surfaceHolderyoutube =  surfaceViewYoutube.getHolder();*/
        surfaceView.setZOrderMediaOverlay(false);
        surfaceView.setZOrderOnTop(false);
        onInitializedListener = this;

        sharedPreferences =  getSharedPreferences("rapsra_pref_beacon",MODE_PRIVATE);

       /* mMessageListener = new MessageListener() {
            @Override
            public void onFound(Message message) {
                Log.i(TAG, "Message found: " + message);
                Log.i(TAG, "Message string: " + new String(message.getContent()));
                Log.i(TAG, "Message namespaced type: " + message.getNamespace() +
                        "/" + message.getType());
                boolean found = false;
                if(!beaconList.isEmpty()) {
                    for (BeaconInfo beaconInfo : beaconList) {
                        if(beaconInfo.beaconNamespace.equals(message.getNamespace())
                                && beaconInfo.beaconType.equals(message.getType()))
                        {
                            // skip
                            found = true;
                            break;
                        }
                    }
                }
                if(!found) {
                    BeaconInfo beaconInfo = new BeaconInfo("OnFound Message namespace : " + message.getNamespace(),
                            "OnFound Message Type : " + message.getType(),
                            "OnFound Beacon Message:" + message.getContent());
                    beaconList.add(beaconInfo);
                }
                String onlyId = "";
                try {
                    Call<BeaconInfoResponse> call2 = apiInterface.getBeaconInfo(message.getType(), role);
                    call2.enqueue(new Callback<BeaconInfoResponse>() {
                        @Override
                        public void onResponse(Call<BeaconInfoResponse> call, Response<BeaconInfoResponse> apiResponse) {
                            if(!apiResponse.body().isError())
                                handleResponceFromBeacon(call,apiResponse);
                        }

                        @Override
                        public void onFailure(Call<BeaconInfoResponse> call, Throwable t) {
                            Toast.makeText(getApplicationContext(), "No Network Connection", Toast.LENGTH_LONG).show();
                            call.cancel();
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }

//                Log.i(TAG, "Beacon Attachment:: " + beaconAttachment);
//                onlyId = beaconAttachment.replace("https://www.youtube.com/watch?v=", "");
//                Log.i(TAG, "Youtube Id:: " + onlyId);
//                strForVideoPlaying = onlyId;

            }
            //beacon listener

            @Override
            public void onBleSignalChanged(final Message message, final BleSignal bleSignal) {
                Log.i(TAG, "Message: " + message + " has new BLE signal information: " + bleSignal);

            }

            @Override
            public void onDistanceChanged(final Message message, final Distance distance) {
                Log.i(TAG, "Distance changed, message: " + message + ", new distance: " + distance);
                if(!beaconList.isEmpty()){
                    for (BeaconInfo beaconInfo : beaconList) {
                        if(beaconInfo.beaconNamespace.equals(message.getNamespace())
                                && beaconInfo.beaconType.equals(message.getType()))
                        {
                            beaconInfo.setBeaconDistance(distance);
                            break;
                        }
                    }

                }

                // decide the logic for displaying the information.
                // find the nearest beacon in the list and then display that information
                // for displaying we need to check what is actually getting displayed right now
                // e.g. if the youtube player is getting displayed then we need to continue with the same in case the same beacon is nearby
                // if the youtube player is getting displayed and the beacon with text message is nearest then we need to hide the video player
                // and display the text messsage
                // similarly if the text message is displayed and it is now farther then we need to display other
                if (distance.getMeters() > 20) {
                    if (messageDisplayed.equals("youtube")) {
                        if (youTubePlayer != null) {
                            if (youTubePlayer.isPlaying()) youTubePlayer.pause();
                            youTubePlayerView.setVisibility(View.INVISIBLE);


                        }
                    } else {
                        displayMessageRecievedTextView.setVisibility(View.INVISIBLE);

                    }
                }
            }

            @Override
            public void onLost(Message message) {
                Log.d(TAG, "Lost sight of message: " + new String(message.getContent()));
            }

        };*/


        getBeaconScanningPermission();

        surfaceView.setVisibility(View.VISIBLE);
        scheduletimer.schedule(new TimerTask() {

            @Override
            public void run() {
                //Do something after 100ms
                timedAction();

            }
        },0, 1000);
    }


    // Commented code for timer
    private void timedAction(){
        this.runOnUiThread(timer);
    }

    private Runnable timer =new Runnable() {
        @Override
        public void run() {

            String json = sharedPreferences.getString("BeaconInfoList", "");

            Type type =  new TypeToken<ArrayList<BeaconModelFloorPlan>>(){}.getType();

            beaconDistanceList =  gson.fromJson(json,type);

            Collections.sort(beaconDistanceList);

            if(youTubePlayer.isPlaying()){

            }




            }

    };



    public void getBeaconScanningPermission(){
        int permissionCheck = ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION);
        int permissionCheck1 = ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION);
        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
                Toast.makeText(this, "The permission to get BLE location data is required", Toast.LENGTH_SHORT).show();
            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
            }
        } else {
            Toast.makeText(this, "Location permissions already granted", Toast.LENGTH_SHORT).show();
        }


        if (permissionCheck1 != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_COARSE_LOCATION)) {
                Toast.makeText(this, "The permission to get BLE location data is required", Toast.LENGTH_SHORT).show();
            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, 1);
            }
        } else {
            Toast.makeText(this, "Location permissions already granted", Toast.LENGTH_SHORT).show();
        }
    }



    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width,
                               int height) {
// TODO Auto-generated method stub

        if(previewing){
            camera.stopPreview();
            previewing = false;
        }

        if (camera != null){
            try {
                camera.setPreviewDisplay(surfaceHolder);
            } catch (IOException e) {
                e.printStackTrace();
            }
            camera.startPreview();
            previewing = true;
            // surfaceView.setZOrderMediaOverlay(false);
            //surfaceView.setZOrderOnTop(true);
        }
        surfaceView.setZOrderMediaOverlay(false);
        Log.i(TAG,"inside surface changed");
        surfaceView.setZOrderOnTop(false);
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
// TODO Auto-generated method stub
        camera = Camera.open();
        /*strForVideoPlaying = "dpSGbWxjQNw";
            onInitializedListener = new YouTubePlayer.OnInitializedListener() {
                @Override
                public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {

                    youTubePlayer.loadVideo(strForVideoPlaying);
                   // camera = Camera.open();


                }

                @Override
                public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {

                }
            };
            */
//        surfaceView.setZOrderMediaOverlay(true);
        Log.i(TAG,"inside surface created");
        //  surfaceView.setZOrderOnTop(false);

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
// TODO Auto-generated method stub
        if(camera!=null) {
            camera.stopPreview();
            camera.release();
            camera = null;
            previewing = false;
/*
            youTubePlayerView.initialize(PlayerConfig.API_KEY,null);
*/
        }
        Log.i(TAG,"inside surface destroyed");
    }

  /*  @Override
    protected void onStart() {
        super.onStart();
        SubscribeOptions options = new SubscribeOptions.Builder() .setStrategy(Strategy.BLE_ONLY)
                .setFilter(
                        new MessageFilter.Builder()
                                .includeNamespacedType("gnsbeacon-1", "")
                                .build()
                )
                .build();
        //beacontest-222719
        //gnsbeacon-1
        Nearby.getMessagesClient(this).subscribe(mMessageListener,options);

    }

    @Override
    protected void onStop() {
        Nearby.getMessagesClient(this).unsubscribe(mMessageListener);
        super.onStop();
    }
*/
    public void getBeaconDetails(String id, String role){

        Log.i(TAG, "Calling getBeaconDetails::");
        Log.i(TAG, "Beacon ID:"+id);
        Log.i(TAG, "Beacon role:"+role);
        final String[] beaconAttachment = {""};
        try {

            Call<BeaconInfoResponse> call2 = apiInterface.getBeaconInfo(id,role);
            call2.enqueue(new Callback<BeaconInfoResponse>() {
                @Override
                public void onResponse(Call<BeaconInfoResponse> call, Response<BeaconInfoResponse> apiResponse) {
                    //System.out.println(response.body().toString());
                    beaconInfoResponse = apiResponse.body();
                    if(!beaconInfoResponse.isError()) {
                        beaconInfoList = beaconInfoResponse.getBeaconInfo();

                    }

                }

                @Override
                public void onFailure(Call<BeaconInfoResponse> call, Throwable t) {
                    Toast.makeText(getApplicationContext(), "No Network Connection", Toast.LENGTH_LONG).show();
                    call.cancel();
                }
            });


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void playYoutubeVideo(String youTubeLink){
        if(youTubePlayer != null) {
            Log.i(TAG, "Beacon Attachment:: " + youTubeLink);
            String onlyId = youTubeLink.replace("https://www.youtube.com/watch?v=", "");
            Log.i(TAG, "Youtube Id:: " + onlyId);
            //strForVideoPlaying = onlyId;
            youTubePlayerView.setVisibility(View.VISIBLE);
            youTubePlayerView.bringToFront();

            Log.i(TAG, "id for youtube: " + onlyId);
            youTubePlayer.loadVideo(onlyId);
        }

    }

    public void handleResponceFromBeacon(Call<BeaconInfoResponse> call, Response<BeaconInfoResponse> apiResponse)
    {
        beaconInfoResponse = apiResponse.body();
        beaconInfoList = beaconInfoResponse.getBeaconInfo();
        String beaconAttachmentData = "";
        for (BeaconModel beacon : beaconInfoList) {
            beaconAttachmentData = beacon.getBeaconAttachment();
        }
        if(beaconAttachmentData.contains("youtube")) {
            messageDisplayed = "youtube";
            if(youTubePlayer == null) {
                youTubePlayerView.initialize(PlayerConfig.API_KEY, onInitializedListener);
                beaconAttachment = beaconAttachmentData;
            }
            else
            if(youTubePlayer != null && !youTubePlayer.isPlaying()) {


                displayMessageRecievedTextView.setVisibility(View.INVISIBLE);
                youTubePlayerView.setVisibility(View.VISIBLE);

                playYoutubeVideo(beaconAttachmentData);
                beaconAttachment = beaconAttachmentData;
            }
            else if(youTubePlayer != null && youTubePlayer.isPlaying()){
                if(!beaconAttachment.equals(beaconAttachmentData)){
                    playYoutubeVideo(beaconAttachmentData);
                    beaconAttachment = beaconAttachmentData;
                }
            }
        }
        else{
            youTubePlayerView.setVisibility(View.INVISIBLE);
            displayMessageRecievedTextView.setVisibility(View.VISIBLE);
            displayMessageRecievedTextView.setText(beaconAttachmentData);
            messageDisplayed = "text";
            Toast.makeText(getApplicationContext(),beaconAttachmentData, Toast.LENGTH_SHORT).show();
//                openDocument(beaconAttachmentData);
        }

    }

    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer initialisedYouTubePlayer, boolean b) {
        youTubePlayerView.setVisibility(View.VISIBLE);
        youTubePlayerView.bringToFront();
        youTubePlayer = initialisedYouTubePlayer;
        playYoutubeVideo(beaconAttachment);

//                        Log.i(TAG, "strForVideoPlaying: " + strForVideoPlaying);
//                        youTubePlayer.loadVideo(strForVideoPlaying);
//                        while (youTubePlayer.isPlaying()) {
//                            System.out.println("playing video");
//                        }

        youTubePlayer.setPlayerStateChangeListener(new YouTubePlayer.PlayerStateChangeListener() {
            @Override
            public void onLoading() {

            }

            @Override
            public void onLoaded(String s) {

            }

            @Override
            public void onAdStarted() {

            }

            @Override
            public void onVideoStarted() {

            }

            @Override
            public void onVideoEnded() {
                youTubePlayerView.setVisibility(View.INVISIBLE);
                youTubePlayer.release();
                // youTubePlayer = null;
            }

            @Override
            public void onError(YouTubePlayer.ErrorReason errorReason) {

            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    /*    BeaconScanningApp scanningApp;
        scanningApp = (BeaconScanningApp)getApplicationContext();
        scanningApp.terminateScan();*/
    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {

    }

//    public void openDocument(String name) {
//        Intent intent = new Intent(android.content.Intent.ACTION_VIEW);
//        File file = new File(name);
//        String extension = android.webkit.MimeTypeMap.getFileExtensionFromUrl(Uri.fromFile(file).toString());
//        String mimetype = android.webkit.MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
//        if (extension.equalsIgnoreCase("") || mimetype == null) {
//            // if there is no extension or there is no definite mimetype, still try to open the file
//            intent.setDataAndType(Uri.fromFile(file), "text/*");
//        } else {
//            intent.setDataAndType(Uri.fromFile(file), mimetype);
//        }
//        // custom message for the intent
//        startActivity(Intent.createChooser(intent, "Choose an Application:"));
//    }
}
