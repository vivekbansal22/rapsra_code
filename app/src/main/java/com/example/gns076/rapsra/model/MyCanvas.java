package com.example.gns076.rapsra.model;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.graphics.Path;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.example.gns076.rapsra.R;

import java.util.ArrayList;
import java.util.List;


public class MyCanvas extends FrameLayout {

    //     new code for commit
    private AppPreference mAppPreference;
    private Canvas myCanvas;
    private  WindowManager manager;
    private Context canvasContext;
    // public static boolean flag = false;

    private CoOrdinates userCoOrdinates = null;
    private List<CoOrdinates> beaconList;
    private boolean userCoord;
    private CoOrdinates previousUserCoordinate;
    private Paint mBackgroundPaint;

    public CoOrdinates getPreviousUserCoordinate() {
        return previousUserCoordinate;
    }

    public void setPreviousUserCoordinate(CoOrdinates previousUserCoordinate) {

        this.previousUserCoordinate = previousUserCoordinate;
    }

    public boolean isUserCoord() {
        return userCoord;
    }

    public void setUserCoord(boolean userCoord) {
        this.userCoord = userCoord;
    }

    public void setUserCoOrdinates(CoOrdinates userCoOrdinates) {
        this.userCoOrdinates = userCoOrdinates;
    }


    public void setBeaconList(List<CoOrdinates>beaconList) {
        this.beaconList = beaconList;
    }


    public MyCanvas(Context context, WindowManager manager) {
        super(context);

        this.manager = manager;
        //    mAppPreference = new AppPreference(context);
        //  mAppPreference = new AppPreference(this);
        canvasContext = context;
        init();
        // TODO Auto-generated constructor stub
    }

    // new code transparent
    private void init() {
        setWillNotDraw(false);
        setLayerType(LAYER_TYPE_HARDWARE, null);

        mBackgroundPaint = new Paint();
        mBackgroundPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
    }


    @Override
    protected void onDraw(Canvas canvas) {
        // TODO Auto-generated method stub
        super.onDraw(canvas);

        //     myCanvas = canvas;
        if(userCoord == false) {
          /*  BitmapFactory.Options options = new BitmapFactory.Options();
            options.inScaled = true;
            Bitmap backgroundImage = BitmapFactory.decodeResource(getResources(), R.drawable.officefloormap, options);
*/
            //     canvas.drawBitmap(backgroundImage, 0, 0, null);

            //  Own code

       /* int height = (Resources.getSystem().getDisplayMetrics().heightPixels);// + ((Resources.getSystem().getDisplayMetrics().heightPixels * 40) / 100)); // to fit screen  - 51  - getResources().getDimensionPixelSize(getResources().getIdentifier("navigation_bar_height", "dimen", "android")));
        int width = (Resources.getSystem().getDisplayMetrics().widthPixels) ;//((Resources.getSystem().getDisplayMetrics().widthPixels * 40) / 100));

        ImageView conference =   findViewById(R.id.canvasImage1);

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(width, height);
        conference.setLayoutParams(params);

        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inScaled=false;
      //  Canvas canvas = new Canvas(bitmap);
        Drawable d = getResources().getDrawable(R.drawable.officefloormap, null);
        d.setBounds(0, 0, width, height);
        d.draw(canvas);*/  // prathmesh code


            Paint paintForBeacons = new Paint();
            paintForBeacons.setColor(Color.BLUE);

            for (int beaconListLoop = 0; beaconListLoop < beaconList.size(); beaconListLoop++) {
                canvas.drawCircle((float) beaconList.get(beaconListLoop).getX(), (float) beaconList.get(beaconListLoop).getY(), 15, paintForBeacons);

            }
        }

    /*    for(int i=0;i<10;i++){
            canvas.drawCircle(i*10,i*10,20,paintForUser1 );

        }
 */


/*

        canvas.drawCircle((float)0,(float)0,20,paintForUser2 );
        canvas.drawCircle((float)865,(float)0,20,paintForUser2 );
        canvas.drawCircle((float)0,(float)1510,20,paintForUser2 );
        canvas.drawCircle((float)865,(float)1510,20,paintForUser2 );
*/


        //  if(userCoOrdinates!=null)
        if(userCoord == true){

            Paint paintForUser = new Paint();
            paintForUser.setColor(Color.LTGRAY);
            paintForUser.setAlpha(90);

            Paint paintForUser1 = new Paint();
            paintForUser1.setColor(Color.RED);

            //tried code
           /* if (previousUserCoordinate!=null) {

                Double xCoord = previousUserCoordinate.getX();
                Double yCoord = previousUserCoordinate.getY();


                while (xCoord <= userCoOrdinates.getX() || yCoord <= userCoOrdinates.getY()) {

                    canvas.drawCircle(xCoord.floatValue(), yCoord.floatValue(), 100, mBackgroundPaint);
                    canvas.drawCircle(xCoord.floatValue(), yCoord.floatValue(), 20, mBackgroundPaint);


                    canvas.drawCircle(xCoord.floatValue(), yCoord.floatValue(), 100, paintForUser);
                    canvas.drawCircle(xCoord.floatValue(), yCoord.floatValue(), 20, paintForUser1);

                    if (xCoord <= userCoOrdinates.getX()) {
                        xCoord++;
                    }
                    if (yCoord <= userCoOrdinates.getY()) {
                        yCoord++;
                    }
                }

            }*/
            //tried code



            //older code
         /*   if (previousUserCoordinate!=null) {
                canvas.drawCircle((float) previousUserCoordinate.getX(), (float) previousUserCoordinate.getY(), 100, mBackgroundPaint);
                canvas.drawCircle( (float) previousUserCoordinate.getX(),(float)previousUserCoordinate.getY(),20,mBackgroundPaint);
            }*/


            canvas.drawCircle((float) userCoOrdinates.getX(), (float) userCoOrdinates.getY(), 100, paintForUser);
            canvas.drawCircle((float) userCoOrdinates.getX(), (float) userCoOrdinates.getY(), 20, paintForUser1);

               /* canvas.drawCircle((float)199.27,(float)383.93,20,paintForUser1);
                canvas.drawCircle((float)82.17,(float)700.77,20,paintForUser1);*/

//older code
        }
       /* Path path = new Path();
        Paint paint = new Paint();




        canvas.drawPath(path, paint);
*/


        // if(flag == true)
        //  canvas.drawCircle((float) coOrdinatesArrayList.get(coOrdinatesArrayList.size()-1).getX(),(float) coOrdinatesArrayList.get(coOrdinatesArrayList.size()-1).getY(),20,pText);

    }
}
