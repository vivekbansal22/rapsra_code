package com.example.gns076.rapsra.activities;

import android.content.Intent;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.RelativeSizeSpan;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.gns076.rapsra.R;
import com.example.gns076.rapsra.model.AppPreference;

public class GamificationActivity extends AppCompatActivity implements View.OnClickListener {

    private AppPreference mAppPreference;
    private String token;

    private Button menuButton;
    private String newsCrawler;
    private TextView marqueForSearch;
    private Button virtualAssist;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gamification);

        mAppPreference = new AppPreference(this);
        token=mAppPreference.getToken();
        newsCrawler  =  mAppPreference.getCrawler();

        marqueForSearch = (TextView)findViewById(R.id.gamifiationnewscrawler);
        virtualAssist = (Button)findViewById(R.id.gamificationVirtualAssist);
        marqueForSearch.setText(newsCrawler);
        marqueForSearch.setSelected(true);

        menuButton=  (Button)findViewById(R.id.gamificationMenu);

        virtualAssist.setOnClickListener(this);

        menuButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Creating the instance of PopupMenu
                PopupMenu popup = new PopupMenu(GamificationActivity.this, menuButton);
                //Inflating the Popup using xml file

                menuButton.setBackgroundResource(R.drawable.exit_menu);

                Menu gamificationMenu =popup.getMenu();


                popup.getMenuInflater()
                        .inflate(R.menu.menu_home, gamificationMenu);
                gamificationMenu.removeItem(R.id.logout);

                gamificationMenu.removeItem(R.id.add_edit_so);
                gamificationMenu.removeItem(R.id.scheduled_followups);
                gamificationMenu.removeItem(R.id.rap_gamify);


                for(int i = 0; i < gamificationMenu.size(); i++) {
                    MenuItem item = gamificationMenu.getItem(i);


                    SpannableString spanString = new SpannableString(gamificationMenu.getItem(i).getTitle().toString());
                    int end = spanString.length();

                    //   spanString.setSpan(new RelativeSizeSpan(1.5f), 0, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    spanString.setSpan(new RelativeSizeSpan(0.04f), 0, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    item.setTitle(spanString);

                }


                popup.setOnDismissListener(new PopupMenu.OnDismissListener() {
                    @Override
                    public void onDismiss(PopupMenu popupMenu) {
                        menuButton.setBackgroundResource(R.drawable.menu);
                    }
                });


                //registering popup with OnMenuItemClickListener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {

                        Intent intentButtonClick;
                        switch (item.getItemId()){

                            case R.id.main_page:
                                intentButtonClick = new Intent(getApplicationContext(), SalesRadarActivityNew.class);
                                startActivity(intentButtonClick);
                                break;

                           /* case R.id.add_edit_so:
                                intentButtonClick = new Intent(getApplicationContext(), AddEditSoActivity.class);
                                startActivity(intentButtonClick);
                                break;*/

                            case R.id.add_edit_prospect:
                                intentButtonClick = new Intent(getApplicationContext(), ProspectorActivity.class);
                                intentButtonClick.putExtra("Token",mAppPreference.getToken() );

                                startActivity(intentButtonClick);
                                break;

                            case R.id.rap_gamify:
                                intentButtonClick = new Intent(getApplicationContext(), RapGamifyActivity.class);
                                startActivity(intentButtonClick);
                                break;
                            case R.id.my_rank:
                                intentButtonClick = new Intent(getApplicationContext(), WrittenActivity.class);
                                startActivity(intentButtonClick);
                                break;
                            case R.id.chat_menu:
                                intentButtonClick = new Intent(getApplicationContext(), ChatActivity.class);
                                startActivity(intentButtonClick);
                                break;

                            case R.id.ar_mode:
                                intentButtonClick = new Intent(getApplicationContext(), ArViewActivityNew.class);
                                startActivity(intentButtonClick);
                                break;

                            case R.id.floor_map_mode:
                                intentButtonClick = new Intent(getApplicationContext(), FloorActivity.class);
                                startActivity(intentButtonClick);
                                break;

                            case R.id.fun_games:
                                intentButtonClick = new Intent(getApplicationContext(), FunGames.class);
                                startActivity(intentButtonClick);
                                break;


                        }
                        return true;
                    }
                });

                popup.show(); //showing popup menu


            }
        }); //closi
    }

    @Override
    public void onClick(View view) {

        {
            Intent intentButtonClick;
            switch (view.getId()) {


                case R.id.gamificationVirtualAssist:
                    intentButtonClick = new Intent(getApplicationContext(), VirtualAssistantActivity.class);
                    startActivity(intentButtonClick);
                    finish();
                    break;
                // Do something
            }
        }
    }
}
