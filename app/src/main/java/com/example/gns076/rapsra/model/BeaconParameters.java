package com.example.gns076.rapsra.model;

public class BeaconParameters {

    public String beaconDistance;
    public String beaconRssi;

    public BeaconParameters(String beaconDistance, String beaconRssi) {
        this.beaconDistance = beaconDistance;
        this.beaconRssi = beaconRssi;
    }

    public String getBeaconDistance() {
        return beaconDistance;
    }

    public void setBeaconDistance(String beaconDistance) {
        this.beaconDistance = beaconDistance;
    }

    public String getBeaconRssi() {
        return beaconRssi;
    }

    public void setBeaconRssi(String beaconRssi) {
        this.beaconRssi = beaconRssi;
    }
}
