package com.example.gns076.rapsra.activities;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.text.style.RelativeSizeSpan;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.PopupWindow;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.gns076.rapsra.R;
import com.example.gns076.rapsra.adapters.CustomAdapter;
import com.example.gns076.rapsra.adapters.com.example.gns076.rapsra.adapters.spinner.SearchableSpinner;
import com.example.gns076.rapsra.helpers.InputValidation;
import com.example.gns076.rapsra.model.AppPreference;
import com.example.gns076.rapsra.model.MerchandizeCategoryDetails;
import com.example.gns076.rapsra.model.MerchandizeCateoryApiResponse;
import com.example.gns076.rapsra.model.MerchandizeSubCategory;
import com.example.gns076.rapsra.model.ProspectorApiResponse;
import com.example.gns076.rapsra.model.ProspectorModel;
import com.example.gns076.rapsra.model.ProspectorUpdateApiResponse;
import com.example.gns076.rapsra.model.ProspectorUpdateModel;
import com.example.gns076.rapsra.model.SpinnerStateApiResponse;
import com.example.gns076.rapsra.model.StateModel;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProspectorActivity extends BaseActivity implements View.OnClickListener {
    SearchableSpinner stateSpinner;
    Spinner genderSpinner;
    Spinner ageSpinner;
    private ListView dialogBoxListView;
    private MerchandizeCateoryApiResponse merchandizeCateoryApiResponse;
    private ProspectorModel prospectorModelForApi;
    private InputValidation inputValidation;

    private EditText firstNameText;
    private EditText lastNameText;
    private EditText streetText;
    private EditText salesNoText ;
    private EditText emailText;
    private EditText  phoneText;
    private EditText cityText;
    private EditText zipCodeText;

    private Button menuButton;
    private Button upHolsteryButton;
    private Button bedroomButton;
    private Button homeOfficeButton;
    private Button ocassionalButton;
    private Button roomPackageButton;
    private Button diningButton;
    private Button matressButton;
    private Button miscellaneousButton;
    private Button prospectorPlanSoldButton;
    private Button saveButton;
    private  Button searchButton;

    private Button taskButton;
    private Button basketButton;
    private Button prospectorNewButton;
    private Button prospectVirtualAssist;
    private Button genderButton;
    private Button ageButton;
    private Button prospectingSoldButton;



    private ImageView reasonForVisitfinanceImage;
    private ImageView reasonForVisitinternetImage;
    private ImageView reasonForVisitsaleImage;

    private ImageView contactMethodMessage;
    private ImageView contactMethodMail;
    private ImageView contactMethodCall;

    private TextView newsCrawler;

    private Boolean isUpholestrySelected = false;
    private Boolean isBedroomSelected = false;
    private Boolean isHomeOfficeSelected = false;
    private Boolean isOcassionalSelected = false;
    private Boolean isRoomPackageSelected = false;
    private Boolean isDiningSelected = false;
    private  Boolean isMattressSelected = false;
    private Boolean isMiscellaneousSelected = false;
    private Boolean isProspectorFromTask = false;
    private Boolean isProspectorUpdateDataSaved = false;

    private Boolean reasonForVistFinance =  false;
    private Boolean reasonForVistInternet =  false;
    private Boolean reasonForVistSale =  false;

    private Boolean contactmethodMessage = false;
    private Boolean contactmethodMail = false;
    private Boolean contactmethodCall =  false;

    private Boolean taskButtonEnabled;

    private AppPreference mAppPreference;
    private String prospectorIdFromTaskActivity;

    private ProspectorUpdateModel prospectorUpdateModel ;



    private boolean prospectPlanSold = false;
    //  public Button newButton;

    //  public Button searchButton;


    private ArrayList <MerchandizeSubCategory> bedroomSubCategoryPopUpModelForPopUp;
    private ArrayList<MerchandizeSubCategory> upholsterySubCategoryPopUpModelForPopUp;
    private ArrayList<MerchandizeSubCategory> homeOfficeSubCategoryPopUpModelForPopUp;
    private ArrayList<MerchandizeSubCategory> ocassionalSubCategoryPopUpModelForPopUp;
    private ArrayList<MerchandizeSubCategory> roomPackageSubCategoryPopUpModelForPopUp;
    private ArrayList<MerchandizeSubCategory> diningSubCategoryPopUpModelForPopUp;
    private ArrayList<MerchandizeSubCategory> matressSubCategoryPopUpModelForPopUp;
    private ArrayList<MerchandizeSubCategory> miscellaneousSubCategoryPopUpModelForPopUp;

    private ArrayList<StateModel> stateModelArrayList;

    private ArrayList<String> stateList;
    private List<String> genderCategories;
    private List<String> ageCategories;

    String genderContents[];
    PopupWindow popupWindowGender;





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prospector_x);

        setObjects();
        getStateSpinnerData();
        setListeners();
        //getProspectorUpdateData();


        menuButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Creating the instance of PopupMenu
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);


                PopupMenu popup = new PopupMenu(ProspectorActivity.this, menuButton);
                //Inflating the Popup using xml file

                menuButton.setBackgroundResource(R.drawable.exit_menu);

                Menu gamesMenu =popup.getMenu();


                popup.getMenuInflater()
                        .inflate(R.menu.menu_home, gamesMenu);
                gamesMenu.removeItem(R.id.logout);
                gamesMenu.removeItem(R.id.add_edit_prospect);
                gamesMenu.removeItem(R.id.add_edit_so);
                gamesMenu.removeItem(R.id.scheduled_followups);


                for(int i = 0; i < gamesMenu.size(); i++) {
                    MenuItem item = gamesMenu.getItem(i);


                    SpannableString spanString = new SpannableString(gamesMenu.getItem(i).getTitle().toString());
                    int end = spanString.length();

                    //   spanString.setSpan(new RelativeSizeSpan(1.5f), 0, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    spanString.setSpan(new RelativeSizeSpan(0.04f), 0, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    item.setTitle(spanString);

                }


                popup.setOnDismissListener(new PopupMenu.OnDismissListener() {
                    @Override
                    public void onDismiss(PopupMenu popupMenu) {
                        menuButton.setBackgroundResource(R.drawable.menu);
                    }
                });


                //registering popup with OnMenuItemClickListener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {

                        Intent intentButtonClick;
                        switch (item.getItemId()){

                            case R.id.main_page:
                                intentButtonClick = new Intent(getApplicationContext(), SalesRadarActivityNew.class);
                                startActivity(intentButtonClick);
                                break;

                           /* case R.id.add_edit_so:
                                intentButtonClick = new Intent(getApplicationContext(), AddEditSoActivity.class);
                                startActivity(intentButtonClick);
                                break;*/

                            case R.id.add_edit_prospect:
                                intentButtonClick = new Intent(getApplicationContext(), ProspectorActivity.class);
                                intentButtonClick.putExtra("Token",mAppPreference.getToken() );
                                startActivity(intentButtonClick);
                                break;

                            case R.id.rap_gamify:
                                intentButtonClick = new Intent(getApplicationContext(), GamificationActivity.class);
                                startActivity(intentButtonClick);
                                break;
                            case R.id.my_rank:
                                intentButtonClick = new Intent(getApplicationContext(), WrittenActivity.class);
                                startActivity(intentButtonClick);
                                break;
                            case R.id.chat_menu:
                                intentButtonClick = new Intent(getApplicationContext(), ChatActivity.class);
                                startActivity(intentButtonClick);
                                break;

                            case R.id.ar_mode:
                                intentButtonClick = new Intent(getApplicationContext(), ArViewActivityNew.class);
                                startActivity(intentButtonClick);
                                break;

                            case R.id.floor_map_mode:
                                intentButtonClick = new Intent(getApplicationContext(), FloorActivity.class);
                                startActivity(intentButtonClick);
                                break;

                            case R.id.fun_games:
                                intentButtonClick = new Intent(getApplicationContext(), FunGames.class);
                                startActivity(intentButtonClick);
                                break;




                        }
                        return true;
                    }
                });

                popup.show(); //showing popup menu


            }
        }); //closi

        //      setDataForSpinner();
        //  taskButton.setEnabled(true);

        prospectorIdFromTaskActivity =  (String) getIntent().getExtras().get("prospect_id");

        if(prospectorIdFromTaskActivity!=null && !prospectorIdFromTaskActivity.toString().trim().isEmpty() ){
            isProspectorFromTask = true;
            //taskButton.setEnabled(true);
            taskButtonEnabled = true;
            firstNameText.setEnabled(false);
            lastNameText.setEnabled(false);

            salesNoText.clearFocus();
            firstNameText.clearFocus();
            lastNameText.clearFocus();
            phoneText.clearFocus();
            emailText.clearFocus();
            streetText.clearFocus();
            cityText.clearFocus();
            zipCodeText.clearFocus();
        }
      /*  else{

        }*/
        getMerchandizeCategoryDataFromDataBase();


    }
    // method to separte data for merchandize data
    public void separateDataFromApiResponse(){

        if(merchandizeCateoryApiResponse!=null){

            ArrayList<MerchandizeCategoryDetails> merchandizeCategories = merchandizeCateoryApiResponse.getMerchandizeCategories();

            if(merchandizeCategories!=null){
                for(MerchandizeCategoryDetails merchandizeCategoryDetails:merchandizeCategories){
                    String merchandizeCategoryName = merchandizeCategoryDetails.getMerchandizeCategory().getMerchandize_category_name();

                    if(merchandizeCategoryName.equalsIgnoreCase("Bedroom")){
                        bedroomSubCategoryPopUpModelForPopUp = merchandizeCategoryDetails.getSubCategories();
                    }
                    if(merchandizeCategoryName.equalsIgnoreCase("Upholstery")){
                        upholsterySubCategoryPopUpModelForPopUp  = merchandizeCategoryDetails.getSubCategories();
                    }
                    if(merchandizeCategoryName.equalsIgnoreCase("Home Office")){
                        homeOfficeSubCategoryPopUpModelForPopUp = merchandizeCategoryDetails.getSubCategories();
                    }

                    if(merchandizeCategoryName.equalsIgnoreCase("Ocassional")){
                        ocassionalSubCategoryPopUpModelForPopUp = merchandizeCategoryDetails.getSubCategories();
                    }
                    if(merchandizeCategoryName.equalsIgnoreCase("Room Package")){
                        roomPackageSubCategoryPopUpModelForPopUp = merchandizeCategoryDetails.getSubCategories();
                    }
                    if(merchandizeCategoryName.equalsIgnoreCase("Dining")){
                        diningSubCategoryPopUpModelForPopUp = merchandizeCategoryDetails.getSubCategories();
                    }
                    if(merchandizeCategoryName.equalsIgnoreCase("Mattress")){
                        matressSubCategoryPopUpModelForPopUp = merchandizeCategoryDetails.getSubCategories();
                    }
                    if(merchandizeCategoryName.equalsIgnoreCase("Miscellaneous")){
                        miscellaneousSubCategoryPopUpModelForPopUp = merchandizeCategoryDetails.getSubCategories();
                    }

                }
            }


        }





    }
    public void setObjects(){
        stateSpinner = (SearchableSpinner) findViewById(R.id.prospectorStateSpinner_x);
        genderSpinner = (Spinner) findViewById(R.id.gender_x);
        ageSpinner  = (Spinner)findViewById(R.id.age_x);

        firstNameText =(EditText)findViewById(R.id.firstNameProspectorPage);
        lastNameText =(EditText)findViewById(R.id.lastNameProspectorPage);
        streetText =(EditText)findViewById(R.id.streetProspectorPage);


        salesNoText =(EditText)findViewById(R.id.salesOrderNumberProspectorPage);
        emailText = (EditText)findViewById(R.id.emailProspectorPage);
        phoneText = (EditText)findViewById(R.id.phoneNumberProspectorPage);
        cityText =(EditText)findViewById(R.id.cityProspectorPage);
        zipCodeText = (EditText)findViewById(R.id.zipCodeProspectorPage);




        upHolsteryButton =  (Button) findViewById(R.id.upholstery);
        bedroomButton =  (Button) findViewById(R.id.bedroom);
        homeOfficeButton = (Button) findViewById(R.id.home_office);
        ocassionalButton = (Button) findViewById(R.id.ocassionalButton);
        roomPackageButton = (Button) findViewById(R.id.room_package);
        diningButton = (Button) findViewById(R.id.dining);
        matressButton = (Button) findViewById(R.id.mattress);
        miscellaneousButton = (Button) findViewById(R.id.miscellanous);
        prospectorPlanSoldButton = (Button) findViewById(R.id.protection_plan);
        saveButton = (Button) findViewById(R.id.prospectorSaveButton);
        menuButton = findViewById(R.id.prospectorMenuButton);
        taskButton = findViewById(R.id.taskButton);

        basketButton = (Button)findViewById(R.id.basket_button);
        prospectingSoldButton = (Button)findViewById(R.id.prospecting_list_button_x);
        searchButton = (Button)findViewById(R.id.search_button) ;
        prospectorNewButton = (Button)findViewById(R.id.new_button);
        prospectVirtualAssist =  (Button)findViewById(R.id.prospectorVirtualAssist);
/*        genderButton = (Button)findViewById(R.id.genderButton);
        ageButton  = (Button)findViewById(R.id.ageButton);*/

/*        reasonForVisitSale = (CheckBox)findViewById(R.id.reasonForVisitSale) ;
         contactMethodCall =  (CheckBox)findViewById(R.id.contactMethodCall);
         contactMethodMail = (CheckBox)findViewById(R.id.contactMethodMail);
         contactMethodMessage = (CheckBox)findViewById(R.id.contactMethodMessage);
         reasonForVisitMail =  (CheckBox)findViewById(R.id.reasonForVisitMail);
         reasonForVisitMoney =  (CheckBox)findViewById(R.id.reasonForVisitMoney);*/

        reasonForVisitfinanceImage = (ImageView)findViewById(R.id.finance_logo);
        reasonForVisitinternetImage = (ImageView)findViewById(R.id.internet_logo3);
        reasonForVisitsaleImage =  (ImageView)findViewById(R.id.sales_logo);

        contactMethodMessage = (ImageView)findViewById(R.id.mail_logo);
        contactMethodMail = (ImageView)findViewById(R.id.email_logo);
        contactMethodCall = (ImageView)findViewById(R.id.phone_logo);

        bedroomSubCategoryPopUpModelForPopUp =  new ArrayList<MerchandizeSubCategory>();
        upholsterySubCategoryPopUpModelForPopUp  =  new ArrayList<MerchandizeSubCategory>();
        homeOfficeSubCategoryPopUpModelForPopUp =  new ArrayList<MerchandizeSubCategory>();
        ocassionalSubCategoryPopUpModelForPopUp  =  new ArrayList<MerchandizeSubCategory>();
        roomPackageSubCategoryPopUpModelForPopUp  =  new ArrayList<MerchandizeSubCategory>();
        diningSubCategoryPopUpModelForPopUp =  new ArrayList<MerchandizeSubCategory>();
        matressSubCategoryPopUpModelForPopUp =  new ArrayList<MerchandizeSubCategory>();
        miscellaneousSubCategoryPopUpModelForPopUp  =  new ArrayList<MerchandizeSubCategory>();

        stateModelArrayList = new ArrayList<StateModel>();
        prospectorModelForApi =  new ProspectorModel();
        mAppPreference = new AppPreference(this);
        newsCrawler = findViewById(R.id.prospectorNewscrawler);
        newsCrawler.setText(mAppPreference.getCrawler());
        newsCrawler.setSelected(true);
        inputValidation = new InputValidation(this);
        salesNoText.requestFocus();
        taskButtonEnabled =  false;
    }



    public void setListeners(){
        upHolsteryButton.setOnClickListener(this);
        bedroomButton.setOnClickListener(this);
        homeOfficeButton.setOnClickListener(this);
        ocassionalButton.setOnClickListener(this);
        roomPackageButton.setOnClickListener(this);
        diningButton.setOnClickListener(this);
        matressButton.setOnClickListener(this);
        miscellaneousButton.setOnClickListener(this);
        prospectorPlanSoldButton.setOnClickListener(this);
        saveButton.setOnClickListener(this);
        taskButton.setOnClickListener(this);
        basketButton.setOnClickListener(this);
        prospectorNewButton.setOnClickListener(this);
        searchButton.setOnClickListener(this);
        prospectingSoldButton.setOnClickListener(this);
        prospectVirtualAssist.setOnClickListener(this);

        phoneText.addTextChangedListener(new PhoneNumberFormattingTextWatcher());


        salesNoText.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                salesNoText.setHint("");
                return false;
            }
        });

        salesNoText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    salesNoText.setHint("Sales No: ");
                }
            }
        });

        firstNameText.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                firstNameText.setHint("");
                return false;
            }
        });
        firstNameText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    firstNameText.setHint("First Name:");
                }
            }
        });

        lastNameText.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                lastNameText.setHint("");
                return false;
            }
        });

        lastNameText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    lastNameText.setHint("Last Name: ");
                }
            }
        });

        phoneText.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                phoneText.setHint("");
                return false;
            }
        });

        phoneText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    phoneText.setHint("Phone: ");
                }
            }
        });


        emailText.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                emailText.setHint("");
                return false;
            }
        });

        emailText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    emailText.setHint("Email: ");
                }
            }
        });


        streetText.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                streetText.setHint("");
                return false;
            }
        });

        streetText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    streetText.setHint("Street: ");
                }
            }
        });



        cityText.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                cityText.setHint("");
                return false;
            }
        });

        cityText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    cityText.setHint("City: ");
                }
            }
        });

        zipCodeText.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                zipCodeText.setHint("");
                return false;
            }
        });

        zipCodeText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    zipCodeText.setHint("Zip Code: ");
                }
            }
        });




       /* stateSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            @Override
            public void onItemSelected(AdapterView adapter, View v, int i, long lng) {

                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

            }
            @Override
            public void onNothingSelected(AdapterView<?> parentView)
            {

            }
        });*/
      /*  genderSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            @Override
            public void onItemSelected(AdapterView adapter, View v, int i, long lng) {

                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

            }
            @Override
            public void onNothingSelected(AdapterView<?> parentView)
            {

            }
        });

        ageSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            @Override
            public void onItemSelected(AdapterView adapter, View v, int i, long lng) {

                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

            }
            @Override
            public void onNothingSelected(AdapterView<?> parentView)
            {

            }
        });*/

    /*    genderSpinner.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                InputMethodManager imm=(InputMethodManager)getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(zipCodeText.getWindowToken(), 0);
                return false;
            }
        }) ;
        ageSpinner.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                InputMethodManager imm=(InputMethodManager)getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(zipCodeText.getWindowToken(), 0);
                return false;
            }
        }) ;
*/
        genderCategories = new ArrayList<String>();
        //  genderCategories.add("Gender:");
        genderCategories.add("Male");
        genderCategories.add("Female");
        genderCategories.add("Other");

        genderContents =  new String[genderCategories.size()];
        genderCategories.toArray(genderContents);

        popupWindowGender =  popupWindowGender();

        final int[] loc_int = new int[2];

       /* genderButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                view.getLocationOnScreen(loc_int);

                Rect location = new Rect();
                location.left = loc_int[0];
                location.top = loc_int[1];
                location.right = location.left + view.getWidth();
                location.bottom = location.top + view.getHeight();


                popupWindowGender.showAsDropDown(view,-150, 20);

                //   popupWindowGender.showAtLocation(view,Gravity.TOP|Gravity.LEFT, location.left, location.bottom);
                //          popupWindowGender.setBackgroundDrawable(new ColorDrawable(
                //                 android.graphics.Color.TRANSPARENT));


                //     popupWindowGender.showAtLocation(view, view.getWidth() - popupWindowGender.getWidth(), 0);


               *//* PopupMenu popup = new PopupMenu(ProspectorActivity.this, genderButton);
                //Inflating the Popup using xml file
                popup.getMenuInflater()
                        .inflate(R.menu.popup_prospectgender, popup.getMenu());


                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {
                        Toast.makeText(
                                ProspectorActivity.this,
                                "You Clicked : " + item.getTitle(),
                                Toast.LENGTH_SHORT
                        ).show();
                        return true;
                    }
                });

                popup.show(); //showing popup menu*//*


            }
        });*/
        firstNameText.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {

                if(firstNameText.getText().toString()!=null && !firstNameText.getText().toString().isEmpty()) {
                    if ((s.charAt(0) >= 65 && s.charAt(0) <= 90)
                            || (s.charAt(0) >= 97 && s.charAt(0) <= 122)) {

                    }


                    // CHECKING FOR DIGITS
                    else if (s.charAt(0) >= 48 && s.charAt(0) <= 57) {
                        firstNameText.setText("");
                        showDialogForNotification("Numeric not allowed at first name start");

                    }
                    // OTHERWISE SPECIAL CHARACTER


                    Pattern p = Pattern.compile("[^a-z0-9 ]", Pattern.CASE_INSENSITIVE);
                    Matcher m = p.matcher(firstNameText.getText().toString());
                    boolean b = m.find();

                    if (b) {
                        String text = firstNameText.getText().toString();
                        if(text.length()==1) {
                            firstNameText.setText("");
                        }
                        else{
                            firstNameText.setText(text.substring(0, text.length() - 1));
                        }

                        showDialogForNotification("Special Characters not allowed at First Name ");
                    }




                }


            }
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {    }
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }
        });

        lastNameText.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {

                if(lastNameText.getText().toString()!=null && !lastNameText.getText().toString().isEmpty()) {
                    if ((s.charAt(0) >= 65 && s.charAt(0) <= 90)
                            || (s.charAt(0) >= 97 && s.charAt(0) <= 122)) {

                    }


                    // CHECKING FOR DIGITS
                    else if (s.charAt(0) >= 48 && s.charAt(0) <= 57) {
                        lastNameText.setText("");


                        showDialogForNotification("Numeric not allowed at Last name start");

                    }


                    // OTHERWISE SPECIAL CHARACTER


                    Pattern p = Pattern.compile("[^a-z0-9 ]", Pattern.CASE_INSENSITIVE);
                    Matcher m = p.matcher(lastNameText.getText().toString());
                    boolean b = m.find();

                    if (b) {
                        String text = lastNameText.getText().toString();
                        if(text.length()==1) {
                            lastNameText.setText("");
                        }
                        else{
                            lastNameText.setText(text.substring(0, text.length() - 1));
                        }

                        showDialogForNotification("Special Characters not allowed in Last Name");
                    }
                }


            }
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {    }
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }
        });

        cityText.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {

                if(cityText.getText().toString()!=null && !cityText.getText().toString().isEmpty()) {
                    if ((s.charAt(0) >= 65 && s.charAt(0) <= 90)
                            || (s.charAt(0) >= 97 && s.charAt(0) <= 122)) {

                    }


                    // CHECKING FOR DIGITS
                    else if (s.charAt(0) >= 48 && s.charAt(0) <= 57) {
                        cityText.setText("");

                        showDialogForNotification("Numeric not allowed at City start");

                    }

                    // OTHERWISE SPECIAL CHARACTER


                    Pattern p = Pattern.compile("[^a-z0-9 ]", Pattern.CASE_INSENSITIVE);
                    Matcher m = p.matcher(cityText.getText().toString());
                    boolean b = m.find();

                    if (b) {
                        String text = cityText.getText().toString();
                        if(text.length()==1) {
                            cityText.setText("");
                        }
                        else{
                            cityText.setText(text.substring(0, text.length() - 1));
                        }

                        showDialogForNotification("Special Characters not allowed in City");
                    }

                }


            }
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {    }
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }
        });


        salesNoText.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {

                Pattern p = Pattern.compile("[^a-z0-9 ]", Pattern.CASE_INSENSITIVE);
                Matcher m = p.matcher(salesNoText.getText().toString());
                boolean b = m.find();

                if (b) {
                    String text = salesNoText.getText().toString();
                    if(text.length()==1) {
                        salesNoText.setText("");
                    }
                    else{
                        salesNoText.setText(text.substring(0, text.length() - 1));
                    }

                    showDialogForNotification("Special Characters not allowed in Sales No");
                }

            }
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {    }
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }
        });
/*
        emailText.addTextChangedListener(new TextWatcher() {
           public void afterTextChanged(Editable s) {

               if(!inputValidation.isValidEmail(getApplicationContext(),emailText.getText().toString().trim())){

                   Toast toast = Toast.makeText(getApplicationContext(), "Please enter valid Email",  Toast.LENGTH_LONG);
                   View view = toast.getView();

                   view.getBackground().setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_IN);

                   TextView text = view.findViewById(android.R.id.message);
                   text.setTextColor(Color.RED);

                   toast.show();
               }
           }
           public void beforeTextChanged(CharSequence s, int start, int count, int after) {    }
           public void onTextChanged(CharSequence s, int start, int before, int count) {
           }
       });*/

        reasonForVisitfinanceImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!reasonForVistFinance) {
                    reasonForVisitfinanceImage.setImageResource(R.drawable.finance_selected);
                    reasonForVistFinance = true;
                }

                if(reasonForVistInternet){
                    reasonForVisitinternetImage.setImageResource(R.drawable.internet_logo);
                    reasonForVistInternet =  false;
                }

                if(reasonForVistSale){
                    reasonForVisitsaleImage.setImageResource(R.drawable.sales_logo);
                    reasonForVistSale = false;
                }

            }
        });



        reasonForVisitinternetImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!reasonForVistInternet) {
                    reasonForVisitinternetImage.setImageResource(R.drawable.internet_selected);

                    reasonForVistInternet = true;
                }

                if(reasonForVistFinance){
                    reasonForVisitfinanceImage.setImageResource(R.drawable.finance_logo);
                    reasonForVistFinance =  false;
                }

                if(reasonForVistSale){
                    reasonForVisitsaleImage.setImageResource(R.drawable.sales_logo);
                    reasonForVistSale = false;
                }

            }
        });

        reasonForVisitsaleImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(!reasonForVistSale){
                    reasonForVisitsaleImage.setImageResource(R.drawable.sale_selected);
                    reasonForVistSale = true;
                }
                if(reasonForVistInternet) {
                    reasonForVisitinternetImage.setImageResource(R.drawable.internet_logo);
                    reasonForVistInternet = false;
                }

                if(reasonForVistFinance){
                    reasonForVisitfinanceImage.setImageResource(R.drawable.finance_logo);
                    reasonForVistFinance =  false;
                }


            }
        });



        contactMethodMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!contactmethodMessage) {
                    contactMethodMessage.setImageResource(R.drawable.mail_icon_orange);
                    contactmethodMessage = true;
                }

                if(contactmethodMail){
                    contactMethodMail.setImageResource(R.drawable.email_logo);
                    contactmethodMail =  false;
                }

                if(contactmethodCall){
                    contactMethodCall.setImageResource(R.drawable.phone_logo);
                    contactmethodCall = false;
                }

            }
        });




        contactMethodMail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!contactmethodMail) {
                    contactMethodMail.setImageResource(R.drawable.email_selected);
                    contactmethodMail = true;
                }

                if(contactmethodMessage){
                    contactMethodMessage.setImageResource(R.drawable.mail_con_white);
                    contactmethodMessage =  false;
                }

                if(contactmethodCall){
                    contactMethodCall.setImageResource(R.drawable.phone_logo);
                    contactmethodCall = false;
                }
            }
        });

        contactMethodCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(!contactmethodCall){
                    contactMethodCall.setImageResource(R.drawable.phone_selected);
                    contactmethodCall = true;
                }
                if(contactmethodMessage){
                    contactMethodMessage.setImageResource(R.drawable.mail_con_white);
                    contactmethodMessage =  false;
                }

                if(contactmethodMail){
                    contactMethodMail.setImageResource(R.drawable.email_logo);
                    contactmethodMail =  false;
                }



            }
        });



// commented because checkboxes removed

       /* contactMethodCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(((CompoundButton) view).isChecked()){
                    contactMethodMail.setChecked(false);
                    contactMethodMessage.setChecked(false);


                    InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

                } else {
                    System.out.println("Un-Checked");
                }
            }
        });
        contactMethodMail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(((CompoundButton) view).isChecked()){
                    contactMethodMessage.setChecked(false);
                    contactMethodCall.setChecked(false);
                    InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                } else {

                }
            }
        });
        contactMethodMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(((CompoundButton) view).isChecked()){
                    contactMethodCall.setChecked(false);
                    contactMethodMail.setChecked(false);
                    InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                } else {

                }
            }
        });
        reasonForVisitSale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(((CompoundButton) view).isChecked()){
                     reasonForVisitMail.setChecked(false);
                     reasonForVisitMoney.setChecked(false);
                    InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                } else {

                }
            }
        });
        reasonForVisitMail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(((CompoundButton) view).isChecked()){
                    reasonForVisitMoney.setChecked(false);
                    reasonForVisitSale.setChecked(false);
                    InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                } else {

                }
            }
        });
        reasonForVisitMoney.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(((CompoundButton) view).isChecked()){
                    reasonForVisitSale.setChecked(false);
                    reasonForVisitMail.setChecked(false);
                    InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                } else{
                }
            }
        });*/
    }


    // method to show dialog
    public void showDialogForNotification(String msgNotification){
        TextView cancelDialogButton;
        final Dialog dialog = new Dialog(ProspectorActivity.this);
        TextView textViewMessage;
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.notification_popup_new);
        // dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        textViewMessage  = (TextView)dialog.findViewById(R.id.notificationMessage);
        textViewMessage.setText(msgNotification);
        /*dialog.show();

        final Timer t = new Timer();
        t.schedule(new TimerTask() {
            public void run() {
                dialog.dismiss(); // when the task active then close the dialog
                t.cancel(); // also just top the timer thread, otherwise, you may receive a crash report
            }
        }, 1000);
*/

        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);


        cancelDialogButton = dialog.findViewById(R.id.cancelNotificationButton);
        cancelDialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                dialog.dismiss();

            }

        });


        Window window = dialog.getWindow();
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.TOP);
        dialog.show();

    }
    // method to show pop up window for gender
    public PopupWindow popupWindowGender() {

        // initialize a pop up window type
        PopupWindow popupWindow = new PopupWindow(this);

        // the drop down list is a list view
        ListView listViewgender = new ListView(this);

        // set our adapter and pass our pop up window contents
        listViewgender.setDividerHeight(10);
        listViewgender.setDivider(this.getResources().getDrawable(R.color.transprentColor));

       /* listViewgender.setBackground(new ColorDrawable(
                android.graphics.Color.TRANSPARENT));*/

        //  listViewgender.setBackgroundResource();
        listViewgender.setAdapter(genderAdapter(genderContents));

        // set the item click listener
        //     listViewgender.setOnItemClickListener(new DogsDropdownOnItemClickListener());

        // some other visual settings
        popupWindow.setBackgroundDrawable(null);
        popupWindow.setFocusable(true);
        popupWindow.setWidth(200);
        popupWindow.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);


        // set the list view as pop up window content

        popupWindow.setContentView(listViewgender);

        return popupWindow;
    }

    private ArrayAdapter<String> genderAdapter(String genderArray[]) {

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, genderArray) {

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {

                // setting the ID and text for every items in the list
                String item = getItem(position);
                String[] itemArr = item.split("::");
                String text = itemArr[0];
                String id = itemArr[0];

                // visual settings for the list item
                TextView listItem = new TextView(ProspectorActivity.this);

                listItem.setText(text);
                listItem.setTag(id);
                listItem.setTextSize((float) 19.53);
                listItem.setBackgroundResource(R.drawable.dropdown_image);
                // listItem.setPadding(10, 10, 10, 10);
                listItem.setTextColor(getResources().getColor(R.color.colorForSpinnerItem));

                return listItem;
            }
        };

        return adapter;
    }


    //  method to add data to spinner
    public void setDataForSpinner(ArrayList<String> stateListFromApi){
        // Spinner Drop down elements
        List<String> stateCategories = new ArrayList<String>();
        stateCategories = stateListFromApi;
        if(stateCategories==null || stateCategories.isEmpty()) {

            stateCategories.add("CA");
            stateCategories.add("NY");
            stateCategories.add("TX");
            stateCategories.add("NJ");
            stateCategories.add("NC");
            stateCategories.add("SC");
            stateList = new ArrayList<String>(stateCategories);

        }
        else{
            stateList = new ArrayList<String>(stateCategories);
            // stateCategories =stateList;

        }

        // Creating adapter for spinner
        //     ArrayAdapter<String> dataAdapterState = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, stateCategories);


        //   dataAdapterState.setDropDownViewResource(R.layout.spinner_item_custom);
        //  stateSpinner.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, stateCategories));
        stateSpinner.init(stateCategories, SearchableSpinner.filterMode_Prefix);

        //  stateSpinner.getAutoCompleteTextView().setBackgroundResource(R.drawable.white_drop_down);


        genderCategories = new ArrayList<String>();
        genderCategories.add("Gender:");
        genderCategories.add("Male");
        genderCategories.add("Female");

        // genderCategories.add("Other");

        ArrayAdapter<String> dataAdapterGender = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, genderCategories){ @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent)
        {
            View v = null;

            // If this is the initial dummy entry, make it hidden
            if (position == 0) {
                TextView tv = new TextView(getContext());
                tv.setHeight(0);
                tv.setVisibility(View.GONE);
                v = tv;
            }
            else {
                // Pass convertView as null to prevent reuse of special case views
                v = super.getDropDownView(position, null, parent);
            }

            // Hide scroll bar because it appears sometimes unnecessarily, this does not prevent scrolling
            parent.setVerticalScrollBarEnabled(false);
            return v;
        }



            @RequiresApi(api = Build.VERSION_CODES.O)
            @NonNull
            @Override
            public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {


                //   getResources().getFont(R.font.indie_flower);
                Typeface myFont = getResources().getFont(R.font.lucidagrande_normal);
                //  Typeface myFont = Typeface.createFromAsset(getAssets(), "fonts/lucidagrande_normal.ttf");

                TextView v = (TextView) super.getView(position, convertView, parent);
                v.setTextSize(19.53f);
                v.setTypeface(myFont);
                v.setTextColor(getResources().getColor(R.color.colorForProspectorInputText));
                return v;
            }
        };

        dataAdapterGender.setDropDownViewResource(R.layout.spinner_item_age_gender);


        genderSpinner.setAdapter(dataAdapterGender);
        //   genderSpinner.init(genderCategories,"");
        //  genderSpinner.setHint("Gender:");

        ageCategories = new ArrayList<String>();
        ageCategories.add("Age:");
        ageCategories.add("20-30");
        ageCategories.add("30-40");
        ageCategories.add("40-50");
        ageCategories.add("50+");

        ArrayAdapter<String> dataAdapterAge = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, ageCategories)
        { @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent)
        {
            View v = null;

            // If this is the initial dummy entry, make it hidden
            if (position == 0) {
                TextView tv = new TextView(getContext());
                tv.setHeight(0);
                tv.setVisibility(View.GONE);
                v = tv;
            }
            else {
                // Pass convertView as null to prevent reuse of special case views
                v = super.getDropDownView(position, null, parent);
            }

            // Hide scroll bar because it appears sometimes unnecessarily, this does not prevent scrolling
            parent.setVerticalScrollBarEnabled(false);
            return v;
        }

            @RequiresApi(api = Build.VERSION_CODES.O)
            @NonNull
            @Override
            public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                //  Typeface myFont = Typeface.createFromAsset(getAssets(), "font/lucidagrande_normal.ttf");
                Typeface myFont = getResources().getFont(R.font.lucidagrande_normal);
                TextView v = (TextView) super.getView(position, convertView, parent);
                v.setTextSize(19.53f);
                v.setTextColor(getResources().getColor(R.color.colorForProspectorInputText));
                v.setTypeface(myFont);
                return v;
            }
        };
        dataAdapterAge.setDropDownViewResource(R.layout.spinner_item_age);

        ageSpinner.setAdapter(dataAdapterAge);
        // ageSpinner.setDropDownHorizontalOffset(-50);
        /*  ageSpinner.init(ageCategories, SearchableSpinner.filterMode_Prefix);
          ageSpinner.setHint("Age:");*/

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.upholstery:
                showDialogForProspector(upholsterySubCategoryPopUpModelForPopUp, R.id.upholstery,R.drawable.upholstery_unselected, R.drawable.upholstery_selected,"Upholstery");

                break;

            case R.id.bedroom:

                showDialogForProspector(bedroomSubCategoryPopUpModelForPopUp, R.id.bedroom, R.drawable.bedroom_unselected,R.drawable.bedroom_selected_,"Bedroom");

                break;

            case R.id.home_office:

                showDialogForProspector(homeOfficeSubCategoryPopUpModelForPopUp, R.id.home_office, R.drawable.home_office_unselected,R.drawable.home_office_selected,"Home Office");

                break;

            case R.id.ocassionalButton:

                showDialogForProspector(ocassionalSubCategoryPopUpModelForPopUp, R.id.ocassionalButton,R.drawable.ocassional_unselected ,R.drawable.ocassional,"Ocassional");

                break;

            case R.id.room_package:

                showDialogForProspector(roomPackageSubCategoryPopUpModelForPopUp, R.id.room_package,R.drawable.room_package_unselected, R.drawable.room_package_selected,"Room Package");

                break;

            case R.id.dining:

                showDialogForProspector(diningSubCategoryPopUpModelForPopUp, R.id.dining,R.drawable.dining_unselected, R.drawable.dining_selected,"Dining");

                break;

            case R.id.mattress:


                showDialogForProspector(matressSubCategoryPopUpModelForPopUp, R.id.mattress,R.drawable.mattress_unselected ,R.drawable.matress_selected,"Mattress");

                break;

            case R.id.miscellanous:

                showDialogForProspector(miscellaneousSubCategoryPopUpModelForPopUp, R.id.miscellanous, R.drawable.miscellaneous_unselected,R.drawable.miscellaneous_selected,"Miscellaneous");

                break;

            case R.id.protection_plan:

                if(prospectPlanSold == false){
                    activateDictivateProspectSoldButton();
                }
                else{
                    prospectPlanSold = false;
                    prospectorPlanSoldButton.setBackgroundResource(R.drawable.protection_plan_unselected);
                }

                break;

            case R.id.prospectorSaveButton:
                if(validateProspectorForm()){
                    gatherProspectorData();

                    if(isProspectorFromTask){
                        setProspectorMerchandiseUpdateDataToServer();
                    }
                    else{
                        setProspectorMerchandiseDataToServer();
                    }

                }

                break;

            case R.id.taskButton:
                if(taskButtonEnabled) {

                    if (isProspectorFromTask) {


                        if (isProspectorUpdateDataSaved) {
                            Intent intentTask = new Intent(getApplicationContext(), TaskActivity.class);
                            intentTask.putExtra("prospect_id", prospectorIdFromTaskActivity);
                            intentTask.putExtra("prospector_firstname", firstNameText.getText().toString());
                            intentTask.putExtra("prospector_lastname", lastNameText.getText().toString());
                            intentTask.putExtra("prospector_phone", phoneText.getText().toString());
                            startActivity(intentTask);
                            finish();
                        } else {

                   /* AlertDialog.Builder builder = new AlertDialog.Builder(ProspectorActivity.this);

                    builder.setMessage("Do You want to save Updated Info!!");

                    //Button One : Yes
                    builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            if(validateProspectorForm()){
                            saveProspectorUpdatedData();
                            Intent intentTask = new Intent(getApplicationContext(), TaskActivity.class);
                            intentTask.putExtra("prospect_id", prospectorIdFromTaskActivity);
                            intentTask.putExtra("prospector_firstname", firstNameText.getText().toString());
                            intentTask.putExtra("prospector_lastname", lastNameText.getText().toString());
                            intentTask.putExtra("prospector_phone", phoneText.getText().toString());
                            dialog.dismiss();
                            startActivity(intentTask);
                            finish();
                            }
                             else{

                                 dialog.cancel();
                            }

                        }
                    });

                    //Button Two : No
                    builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Intent intentTask = new Intent(getApplicationContext(), TaskActivity.class);
                            intentTask.putExtra("prospect_id", prospectorIdFromTaskActivity);
                            intentTask.putExtra("prospector_firstname", prospectorUpdateModel.getFirstName());
                            intentTask.putExtra("prospector_lastname", prospectorUpdateModel.getLastName());
                            intentTask.putExtra("prospector_phone", prospectorUpdateModel.getPhoneNumber());
                            dialog.dismiss();
                            startActivity(intentTask);
                            finish();
                        }
                    });
                    //Button Three : Neutral
                    builder.setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            dialog.cancel();
                        }
                    });
                    AlertDialog diag = builder.create();
                    diag.show();
                 }*/

                            taskBasketButtonNotification();

                        }
                    }
                    // isProspectorFromTask
                    else {

                        Intent intentTask = new Intent(getApplicationContext(), TaskActivity.class);
                        intentTask.putExtra("prospect_id", prospectorModelForApi.getProspectorId());
                        intentTask.putExtra("prospector_firstname", firstNameText.getText().toString());
                        intentTask.putExtra("prospector_lastname", lastNameText.getText().toString());
                        intentTask.putExtra("prospector_phone", phoneText.getText().toString());
                        startActivity(intentTask);
                        finish();

                    }
                }else{

                    showDialogForNotification("Please save prospector details before navigating to Task and Basket.");

                }
                break;

            case R.id.basket_button:

                if(!taskButtonEnabled){
                    showDialogForNotification("Please save prospector details before navigating to Task and Basket.");

                }else {

                    Intent intentBasket = new Intent(getApplicationContext(), WishListActivity.class);
                    intentBasket.putExtra("prospect_id", prospectorModelForApi.getProspectorId());
                    intentBasket.putExtra("prospector_firstname", firstNameText.getText().toString());
                    intentBasket.putExtra("prospector_lastname", lastNameText.getText().toString());
                    intentBasket.putExtra("prospector_phone", phoneText.getText().toString());
                    startActivity(intentBasket);
                    finish();
                }
                break;

            case R.id.search_button:
                Intent  intentSearch= new Intent(getApplicationContext(), SearchActivity.class);
                startActivity(intentSearch);
                finish();
                break;

            case R.id.new_button:
                final Dialog dialog = new Dialog(ProspectorActivity.this);

                dialog.setCancelable(false);
                dialog.setContentView(R.layout.prospector_new_dialog);
                dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                TextView continueNewButton;
                TextView cancelNewButton;

                continueNewButton  = (TextView)dialog.findViewById(R.id.continueDialogNewButton);
                continueNewButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        resetProspectorPage();
                        dialog.dismiss();

                    }

                });

                cancelNewButton = (TextView)dialog.findViewById(R.id.cancelNewProspectorDialog);

                cancelNewButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                Window window = dialog.getWindow();
                window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                window.setGravity(Gravity.TOP);
                dialog.show();
                break;
            case R.id.prospectorVirtualAssist:
                Intent  intentVirtualAssist= new Intent(getApplicationContext(), VirtualAssistantActivity.class);
                startActivity(intentVirtualAssist);
                finish();
                break;

            case R.id.prospecting_list_button_x:

                Intent intentButtonClick = new Intent(getApplicationContext(), ProspectActivity.class);
                startActivity(intentButtonClick);
                finish();
                break;

        }//switch case
    }

    public void taskBasketButtonNotification(){
        Button yesDialogButton;
        Button noDialogButton;
        Button cancelDialogButton;

        final Dialog dialog = new Dialog(ProspectorActivity.this);

        dialog.setCancelable(false);
        dialog.setContentView(R.layout.prospector_task_button_dialog);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

        yesDialogButton = dialog.findViewById(R.id.yesDialog);
        yesDialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validateProspectorForm()) {
                    saveProspectorUpdatedData();
                    Intent intentTask = new Intent(getApplicationContext(), TaskActivity.class);
                    intentTask.putExtra("prospect_id", prospectorIdFromTaskActivity);
                    intentTask.putExtra("prospector_firstname", firstNameText.getText().toString());
                    intentTask.putExtra("prospector_lastname", lastNameText.getText().toString());
                    intentTask.putExtra("prospector_phone", phoneText.getText().toString());
                    dialog.dismiss();
                    startActivity(intentTask);
                    finish();
                } else {

                    dialog.cancel();
                }

            }
        });


        noDialogButton = dialog.findViewById(R.id.noDialog);
        noDialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentTask = new Intent(getApplicationContext(), TaskActivity.class);
                intentTask.putExtra("prospect_id", prospectorIdFromTaskActivity);
                intentTask.putExtra("prospector_firstname", prospectorUpdateModel.getFirstName());
                intentTask.putExtra("prospector_lastname", prospectorUpdateModel.getLastName());
                intentTask.putExtra("prospector_phone", prospectorUpdateModel.getPhoneNumber());
                dialog.dismiss();
                startActivity(intentTask);
                finish();

            }
        });

        cancelDialogButton = dialog.findViewById(R.id.cancelDialog);
        cancelDialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                dialog.dismiss();

            }

        });
        dialog.show();
    }

    public void resetProspectorPage(){
        /*Intent refresh = new Intent(this, ProspectorActivity.class);
        startActivity(refresh);
        this.finish();*/
        //  recreate();

        Intent intent = getIntent();

        intent.putExtra("prospect_id","");
        finish();
        startActivity(intent);
    }
    public Boolean checkIfMerchandizeIsSelected(){

        if(((isUpholestrySelected==true)|| (isBedroomSelected == true)|| (isHomeOfficeSelected == true)|| (isOcassionalSelected == true)||
                (isRoomPackageSelected == true)|| (isDiningSelected == true)|| (isMattressSelected == true) || (isMiscellaneousSelected == true))){
            return true;
        }
        return  false;
    }

    // method to save data to database
    public void saveProspectorUpdatedData(){

        gatherProspectorData();
        prospectorModelForApi.setProspectorId(prospectorIdFromTaskActivity);
        try {

            Call<ProspectorApiResponse> call2 = apiInterface.setProspectsData(prospectorIdFromTaskActivity,mAppPreference.getStoreId().toString(),mAppPreference.getDealerId().toString(),
                    mAppPreference.getAgentId(),prospectorModelForApi.getProspectorFirstName().toString(),prospectorModelForApi.getProspectorLastName(),prospectorModelForApi.getProspectorPhone(),
                    prospectorModelForApi.getProspectorEmail(),prospectorModelForApi.getProspectorStreet(),prospectorModelForApi.getProspectorCity(),prospectorModelForApi.getProspectorState(),
                    prospectorModelForApi.getProspectorZipCode(),prospectorModelForApi.getProspectorAge(),prospectorModelForApi.getProspectorGender(),prospectorModelForApi.getProspectorVisitReason(),
                    prospectorModelForApi.getProspectorContactMethod(),prospectorModelForApi.getProspectorMerchandizeString(),prospectorModelForApi.getProspectorSubMerchandizeString(),

                    prospectorModelForApi.getProtectionPlanSold(),prospectorModelForApi.getProspectorSalesNo());

            call2.enqueue(new Callback<ProspectorApiResponse>() {
                @Override
                public void onResponse(Call<ProspectorApiResponse> call, Response<ProspectorApiResponse> response) {
                    System.out.println(response.body().toString());
                    ProspectorApiResponse res=response.body();
                    if (!res.isError()) {

                        prospectorModelForApi.setProspectorId(res.getProspectId());
                        // taskButton.setEnabled(true);
                        taskButtonEnabled =  true;



                    } else{
                        //  Toast.makeText(getApplicationContext(), "invalid parameters" , Toast.LENGTH_LONG).show();
                        showDialogForNotification("invalid parameters");
                    }


                }

                @Override
                public void onFailure(Call<ProspectorApiResponse> call, Throwable t) {

                    showDialogForNotification("Save Failed.Please check Net connection");

                    call.cancel();
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    // method to set flags for selected merchandize
    public void setIfMerchandizeIsSelected(){

        if(upholsterySubCategoryPopUpModelForPopUp!=null) {
            isUpholestrySelected = checkIfMerchandizeIsSelected(upholsterySubCategoryPopUpModelForPopUp);

        }

        if(bedroomSubCategoryPopUpModelForPopUp!=null) {
            isBedroomSelected = checkIfMerchandizeIsSelected(bedroomSubCategoryPopUpModelForPopUp);
        }

        if(homeOfficeSubCategoryPopUpModelForPopUp!=null) {
            isHomeOfficeSelected = checkIfMerchandizeIsSelected(homeOfficeSubCategoryPopUpModelForPopUp);
        }

        if(ocassionalSubCategoryPopUpModelForPopUp!=null) {
            isOcassionalSelected = checkIfMerchandizeIsSelected(ocassionalSubCategoryPopUpModelForPopUp);
        }

        if(roomPackageSubCategoryPopUpModelForPopUp!=null) {
            isRoomPackageSelected = checkIfMerchandizeIsSelected(roomPackageSubCategoryPopUpModelForPopUp);
        }

        if(diningSubCategoryPopUpModelForPopUp!=null) {
            isDiningSelected = checkIfMerchandizeIsSelected(diningSubCategoryPopUpModelForPopUp);
        }

        if(matressSubCategoryPopUpModelForPopUp!=null) {
            isMattressSelected = checkIfMerchandizeIsSelected(matressSubCategoryPopUpModelForPopUp);
        }

        if(miscellaneousSubCategoryPopUpModelForPopUp!=null) {
            isMiscellaneousSelected = checkIfMerchandizeIsSelected(miscellaneousSubCategoryPopUpModelForPopUp);
        }
    }

    public Boolean checkIfMerchandizeIsSelected(ArrayList<MerchandizeSubCategory> subCategoryModels){
        Boolean isSelected = false;

        for(int i=0;i<subCategoryModels.size();i++){
            if(subCategoryModels.get(i).isSelected()){
                isSelected = true;


            }
        }
        return isSelected;
    }

    // method to validate form on click of Save Button
    public boolean validateProspectorForm(){
        Boolean isFormValid = true;

     /*   if(firstNameText.getText().toString().trim().isEmpty()){


            Toast toast = Toast.makeText(getApplicationContext(), "First Name is empty",  Toast.LENGTH_LONG);
            View view = toast.getView();

            view.getBackground().setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_IN);

            TextView text = view.findViewById(android.R.id.message);
            text.setTextColor(Color.RED);

            toast.show();
            return false;

        }*/

        if(!firstNameText.getText().toString().trim().isEmpty()) {
            if (!inputValidation.isAlphaNumeric(firstNameText.getText().toString())) {

                char last = firstNameText.getText().charAt(firstNameText.getText().length() - 1);

                if (last == ' ') {

                    showDialogForNotification("Please Enter Valid FirstName.Space not allowed at end");
                    return false;
                }
                showDialogForNotification("Please Enter Valid FirstName");
                return false;
            }
        }

        if(lastNameText.getText().toString().trim().isEmpty()) {

            showDialogForNotification("Last Name is empty");
            return false;
        }
        if (!inputValidation.isAlphaNumeric(lastNameText.getText().toString())) {

            char lastCharacter = lastNameText.getText().charAt(lastNameText.getText().length() - 1);
            if(lastCharacter == ' '){
                showDialogForNotification("Please Enter Valid LastName.Space not allowed at end");
                return false;
            }

            showDialogForNotification("Please enter valid Last Name");
            return false;


        }


       /* if(cityText.getText().toString().trim().isEmpty()){

            Toast toast = Toast.makeText(getApplicationContext(), "City is empty",  Toast.LENGTH_LONG);
            View view = toast.getView();

            view.getBackground().setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_IN);

            TextView text = view.findViewById(android.R.id.message);
            text.setTextColor(Color.RED);

            toast.show();
            return false;


        }*/

        if(!cityText.getText().toString().trim().isEmpty()) {
            if (!inputValidation.isAlphaNumeric(cityText.getText().toString())) {


                char lastCharacter = cityText.getText().charAt(cityText.getText().length() - 1);
                if (lastCharacter == ' ') {

                    showDialogForNotification("Please Enter Valid City.Space not allowed at end");
                    return false;
                }

                showDialogForNotification("Please Enter Valid City");
                return false;
            }
        }


       /* if(phoneText.getText().toString().isEmpty()){
           Toast toast = Toast.makeText(getApplicationContext(), "Phone No is empty",  Toast.LENGTH_LONG);
            View view = toast.getView();

            view.getBackground().setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_IN);

            TextView text = view.findViewById(android.R.id.message);
            text.setTextColor(Color.RED);

            toast.show();
            return false;

        }*/

        if(!phoneText.getText().toString().isEmpty()) {
            if (!inputValidation.isValidPhone(phoneText.getText().toString())) {
                char lastCharacter = phoneText.getText().charAt(phoneText.getText().length() - 1);
                if (lastCharacter == ' ') {

                    showDialogForNotification("Please Enter Valid Phone.Space not allowed at end");
                    return false;
                }


                showDialogForNotification("Enter Valid Phone no ");
                return false;

            }

        }
        if(!phoneText.getText().toString().isEmpty()) {
            if (phoneText.getText().toString().length() < 14) {

                showDialogForNotification("Please Enter 10 Digit Phone No ");
                return false;
            }
        }
       /* if(emailText.getText().toString().trim().isEmpty()){
            Toast toast = Toast.makeText(getApplicationContext(), "Email is empty",  Toast.LENGTH_LONG);
            View view = toast.getView();

            view.getBackground().setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_IN);

            TextView text = view.findViewById(android.R.id.message);
            text.setTextColor(Color.RED);

            toast.show();
            return false;
        }*/

        if(!emailText.getText().toString().trim().isEmpty()){


            if(!inputValidation.isValidEmail(getApplicationContext(),emailText.getText().toString())){

                char lastCharacter = emailText.getText().charAt(emailText.getText().length() - 1);
                if(lastCharacter == ' '){


                    showDialogForNotification("Please Enter Valid Email.Space not allowed at end");
                    return false;
                }

                showDialogForNotification("Please enter valid Email");
                return false;
            }
        }
        /*if(salesNoText.getText().toString().trim().isEmpty()){


            Toast toast = Toast.makeText(getApplicationContext(), "Sales No is empty",  Toast.LENGTH_LONG);
            View view = toast.getView();

            view.getBackground().setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_IN);

            TextView text = view.findViewById(android.R.id.message);
            text.setTextColor(Color.RED);

            toast.show();
            return false;

          *//*  Toast.makeText(getApplicationContext(), "Sales No is empty", Toast.LENGTH_LONG).show();
            return false;*//*
        }*/

/*
        if(stateSpinner.getSelectedItem().toString().trim().isEmpty()){


            Toast toast = Toast.makeText(getApplicationContext(), "State is empty.Please Select State From List",  Toast.LENGTH_LONG);
            View view = toast.getView();

            view.getBackground().setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_IN);

            TextView text = view.findViewById(android.R.id.message);
            text.setTextColor(Color.RED);

            toast.show();
            return false;

          */
/*  Toast.makeText(getApplicationContext(), "State is empty", Toast.LENGTH_LONG).show();
            return false;*//*

        }
        boolean isStatePresent=false;
        for(String state:stateList){
            if(stateSpinner.getSelectedItem().toString().trim().equalsIgnoreCase(state)){
                isStatePresent = true;
                break;
            }
        }
        if(!isStatePresent){
            Toast toast = Toast.makeText(getApplicationContext(), "Invalid State.Please Select State From List",  Toast.LENGTH_LONG);
            View view = toast.getView();

            view.getBackground().setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_IN);

            TextView text = view.findViewById(android.R.id.message);
            text.setTextColor(Color.RED);

            toast.show();
            return false;
        }
*/



       /* if(ageSpinner.getSelectedItem().toString().equalsIgnoreCase("Age:")){


            Toast toast = Toast.makeText(getApplicationContext(), "Age is empty",  Toast.LENGTH_LONG);
            View view = toast.getView();

            view.getBackground().setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_IN);

            TextView text = view.findViewById(android.R.id.message);
            text.setTextColor(Color.RED);

            toast.show();
            return false;


            *//* Toast.makeText(getApplicationContext(), "Age is empty", Toast.LENGTH_LONG).show();
            return false;*//*
        }


        if(genderSpinner.getSelectedItem().toString().equalsIgnoreCase("Gender:")){


            Toast toast = Toast.makeText(getApplicationContext(), "Gender is empty",  Toast.LENGTH_LONG);
            View view = toast.getView();

            view.getBackground().setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_IN);

            TextView text = view.findViewById(android.R.id.message);
            text.setTextColor(Color.RED);

            toast.show();
            return false;

           *//* Toast.makeText(getApplicationContext(), "Gender is empty", Toast.LENGTH_LONG).show();
            return false;*//*
        }*/

       /*
        // commented for further improvement
        if(getMerchandizeCategoryString()==null){

            Toast toast = Toast.makeText(getApplicationContext(), " Merchandize Category Not Selected",  Toast.LENGTH_LONG);
            View view = toast.getView();

            view.getBackground().setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_IN);

            TextView text = view.findViewById(android.R.id.message);
            text.setTextColor(Color.RED);

            toast.show();
            return false;
        }
*/
       /* if((reasonForVisitMoney.isChecked()==false)&& (reasonForVisitMail.isChecked()==false) && (reasonForVisitSale.isChecked()==false)){


            Toast toast = Toast.makeText(getApplicationContext(), " Reason For Visit Not Selected",  Toast.LENGTH_LONG);
            View view = toast.getView();

            view.getBackground().setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_IN);

            TextView text = view.findViewById(android.R.id.message);
            text.setTextColor(Color.RED);

            toast.show();
            return false;


            *//* Toast.makeText(getApplicationContext(), "Reason For Visit Not Selected", Toast.LENGTH_LONG).show();
            return false;*//*
        }

        if((contactMethodMessage.isChecked()==false)&& (contactMethodMail.isChecked()==false) && (contactMethodCall.isChecked()==false)){

            Toast toast = Toast.makeText(getApplicationContext(), "Contact Method Not Selected",  Toast.LENGTH_LONG);
            View view = toast.getView();

            view.getBackground().setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_IN);

            TextView text = view.findViewById(android.R.id.message);
            text.setTextColor(Color.RED);

            toast.show();
            return false;

            *//* Toast.makeText(getApplicationContext(), "Contact Method Not Selected", Toast.LENGTH_LONG).show();
            return false;*//*
        }
*/
        //streetText
        //zipCodeText

        return  isFormValid;
    }

    // method to gather all data after click on Save button
    public void gatherProspectorData(){
        setIfMerchandizeIsSelected();
        //  removeMerchadizeFromSubCategories();

        prospectorModelForApi.setProspectorFirstName(firstNameText.getText().toString());
        prospectorModelForApi.setProspectorLastName(lastNameText.getText().toString());
        prospectorModelForApi.setProspectorStreet(streetText.getText().toString());

        prospectorModelForApi.setProspectorZipCode(zipCodeText.getText().toString());
        prospectorModelForApi.setProspectorSalesNo(salesNoText.getText().toString());
        prospectorModelForApi.setProspectorPhone(phoneText.getText().toString());
        prospectorModelForApi.setProspectorEmail(emailText.getText().toString());
        prospectorModelForApi.setProspectorCity(cityText.getText().toString());
        prospectorModelForApi.setProspectorGender(genderSpinner.getSelectedItem().toString());
        prospectorModelForApi.setProspectorAge(ageSpinner.getSelectedItem().toString());
        prospectorModelForApi.setProspectorState(getStateId(stateSpinner.getSelectedItem().toString()));

        String reasonForVisit = null;
        reasonForVisit =  getReasonForVisit();

        String contactMethod = null;
        contactMethod = getContactMethod();

        prospectorModelForApi.setProspectorVisitReason(reasonForVisit);
        prospectorModelForApi.setProspectorContactMethod(contactMethod);


        String merchandizeCategoryString = getMerchandizeCategoryString();


        String merchandizeSubCategoryString = "";

        /*Scenario for Merchandie category not selected*/
        if(merchandizeCategoryString!=null){
            merchandizeSubCategoryString = removeCommas(getMerchandizeSubCategoryString());
        }
        else if(merchandizeCategoryString==null){
            merchandizeCategoryString="";
        }
        /*Scenario for Merchandie category not selected*/

        //   merchandizeSubCategoryString = removeCommas(getMerchandizeSubCategoryString());

        prospectorModelForApi.setProspectorMerchandizeString(merchandizeCategoryString);
        prospectorModelForApi.setProspectorSubMerchandizeString(merchandizeSubCategoryString);

        if(prospectPlanSold == true){
            prospectorModelForApi.setProtectionPlanSold("Y");
        }
        else{
            prospectorModelForApi.setProtectionPlanSold("N");
        }

    }

    public String getStateId(String state){
        String stateId ="";
        for(StateModel stateModel :stateModelArrayList){
            if(stateModel.getStateShortName().equalsIgnoreCase(state)){
                stateId = stateModel.getStateId();
            }
        }
        return stateId;
    }

    public void  removeMerchadizeFromSubCategories() {
        if (upholsterySubCategoryPopUpModelForPopUp != null && upholsterySubCategoryPopUpModelForPopUp.size() >= 1) {
            upholsterySubCategoryPopUpModelForPopUp.remove(0);
        }

        if (bedroomSubCategoryPopUpModelForPopUp != null && bedroomSubCategoryPopUpModelForPopUp.size() >= 1) {
            bedroomSubCategoryPopUpModelForPopUp.remove(0);
        }
        if (homeOfficeSubCategoryPopUpModelForPopUp != null && homeOfficeSubCategoryPopUpModelForPopUp.size() >= 1) {
            homeOfficeSubCategoryPopUpModelForPopUp.remove(0);
        }
        if (ocassionalSubCategoryPopUpModelForPopUp != null && ocassionalSubCategoryPopUpModelForPopUp.size() >= 1){
            ocassionalSubCategoryPopUpModelForPopUp.remove(0);
        }
        if(roomPackageSubCategoryPopUpModelForPopUp!=null && roomPackageSubCategoryPopUpModelForPopUp.size() >= 1)
        {
            roomPackageSubCategoryPopUpModelForPopUp.remove(0);
        }
        if(diningSubCategoryPopUpModelForPopUp!=null && diningSubCategoryPopUpModelForPopUp.size() >= 1) {
            diningSubCategoryPopUpModelForPopUp.remove(0);
        }

        if(matressSubCategoryPopUpModelForPopUp!=null && matressSubCategoryPopUpModelForPopUp.size() >= 1) {
            matressSubCategoryPopUpModelForPopUp.remove(0);
        }
        if(miscellaneousSubCategoryPopUpModelForPopUp!=null && miscellaneousSubCategoryPopUpModelForPopUp.size() >=1 ) {
            miscellaneousSubCategoryPopUpModelForPopUp.remove(0);
        }
    }
    // method to get selected sub merchandize category string
    public String getMerchandizeSubCategoryString(){
        StringBuilder merchandizeSubCategoryString = new StringBuilder();

        if(isUpholestrySelected) {
            if (upholsterySubCategoryPopUpModelForPopUp != null) {

                String stringToAppend = getSubString(upholsterySubCategoryPopUpModelForPopUp);
                if (!stringToAppend.isEmpty() && !(stringToAppend == null)) {

                    merchandizeSubCategoryString = merchandizeSubCategoryString.append(stringToAppend.toString());
                }
            }
        }
        if(isBedroomSelected) {
            if (bedroomSubCategoryPopUpModelForPopUp != null) {
                String stringToAppend = getSubString(bedroomSubCategoryPopUpModelForPopUp);
                if (!stringToAppend.isEmpty() && !(stringToAppend == null)) {
                    merchandizeSubCategoryString = merchandizeSubCategoryString.append("," + stringToAppend.toString());
                }
            }
        }
        if(isHomeOfficeSelected) {
            if (homeOfficeSubCategoryPopUpModelForPopUp != null) {
                String stringToAppend = getSubString(homeOfficeSubCategoryPopUpModelForPopUp);
                if (!stringToAppend.isEmpty() && !(stringToAppend == null)) {
                    merchandizeSubCategoryString = merchandizeSubCategoryString.append("," + stringToAppend.toString());
                }
            }
        }

        if(isOcassionalSelected) {
            if (ocassionalSubCategoryPopUpModelForPopUp != null) {
                String stringToAppend = getSubString(ocassionalSubCategoryPopUpModelForPopUp);
                if (!stringToAppend.isEmpty() && !(stringToAppend == null)) {
                    merchandizeSubCategoryString = merchandizeSubCategoryString.append("," + stringToAppend.toString());
                }
            }
        }

        if(isRoomPackageSelected) {
            if (roomPackageSubCategoryPopUpModelForPopUp != null) {

                String stringToAppend = getSubString(roomPackageSubCategoryPopUpModelForPopUp);
                if (!stringToAppend.isEmpty() && !(stringToAppend == null)) {
                    merchandizeSubCategoryString = merchandizeSubCategoryString.append("," + stringToAppend.toString());
                }
            }
        }

        if(isDiningSelected) {
            if (diningSubCategoryPopUpModelForPopUp != null) {
                String stringToAppend = getSubString(diningSubCategoryPopUpModelForPopUp);
                if (!stringToAppend.isEmpty() && !(stringToAppend == null)) {
                    merchandizeSubCategoryString = merchandizeSubCategoryString.append("," + stringToAppend.toString());
                }
            }
        }
        if(isMattressSelected) {
            if (matressSubCategoryPopUpModelForPopUp != null) {

                String stringToAppend = getSubString(matressSubCategoryPopUpModelForPopUp);
                if (!stringToAppend.isEmpty() && !(stringToAppend == null)) {
                    merchandizeSubCategoryString = merchandizeSubCategoryString.append("," + stringToAppend.toString());
                }
            }
        }
        if(isMiscellaneousSelected) {
            if (miscellaneousSubCategoryPopUpModelForPopUp != null) {


                String stringToAppend = getSubString(miscellaneousSubCategoryPopUpModelForPopUp);
                if (!stringToAppend.isEmpty() && !(stringToAppend == null)) {
                    merchandizeSubCategoryString = merchandizeSubCategoryString.append("," + stringToAppend.toString());
                }
            }
        }


        return merchandizeSubCategoryString.toString();
    }

    // method to get id's of selected sub merchandize category string
    public String getSubString(ArrayList<MerchandizeSubCategory> subCategoryModels){
        StringBuilder subCategoryString = new StringBuilder();
        ArrayList<Integer>arrayListIds =  new ArrayList<Integer>();
        for(int i=0;i<subCategoryModels.size();i++){
            if(subCategoryModels.get(i).isSelected()){

                arrayListIds.add(Integer.parseInt(subCategoryModels.get(i).getMerchandizeSubCatId().trim()));
                /*
                if(i == subCategoryModels.size()-1){
                    subCategoryString.append(subCategoryModels.get(i).getMerchandizeSubCatId().trim());
                }
                else if(i==0){
                    subCategoryString.append(subCategoryModels.get(i).getMerchandizeSubCatId().trim());
                }
                else{
                    if(subCategoryString.toString().isEmpty()){
                        subCategoryString.append(subCategoryModels.get(i).getMerchandizeSubCatId().trim());
                    }
                    else {
                        subCategoryString.append("," + subCategoryModels.get(i).getMerchandizeSubCatId().trim());
                    }
                    }
                    */
            }

        }
        if(arrayListIds.size()!=0) {
            for (int id : arrayListIds) {
                subCategoryString.append(String.valueOf(id)+",");
            }
        }
        String stringToReturn =  subCategoryString.toString();
        if( !stringToReturn.isEmpty() && stringToReturn!=null) {
            String str = stringToReturn;
            if (!str.isEmpty() && str != null) {
                stringToReturn = removeCommas(str);
            }
        }
        return stringToReturn;
    }
    public String removeCommas(String str){
        String withoutCommas = str ;
        if(str!=null && !str.isEmpty()) {
            if (str.charAt(0) == ',') {
                withoutCommas = withoutCommas.substring(1);
            }
            if (str.charAt(str.length() - 1) == ',') {
                withoutCommas = withoutCommas.substring(0, withoutCommas.length() - 1);
            }
        }
        else
            return "";

        return withoutCommas;
    }

    public String getMerchandizeCategoryString(){
        StringBuilder merchandizecategoryString = null;

        if(isUpholestrySelected){
            merchandizecategoryString  = new StringBuilder(getMerchanCategoryId("Upholstery"));
        }

        if(isBedroomSelected){
            if(merchandizecategoryString==null){
                merchandizecategoryString =  new StringBuilder(getMerchanCategoryId("Bedroom"));
            }
            else{
                merchandizecategoryString =  merchandizecategoryString.append(","+getMerchanCategoryId("Bedroom"));
            }
        }

        if(isHomeOfficeSelected){
            if(merchandizecategoryString==null){
                merchandizecategoryString =  new StringBuilder(getMerchanCategoryId("Home Office"));
            }
            else {
                merchandizecategoryString = merchandizecategoryString.append("," + getMerchanCategoryId("Home Office"));
            }
        }

        if(isOcassionalSelected){
            if(merchandizecategoryString==null){
                merchandizecategoryString =  new StringBuilder(getMerchanCategoryId("Ocassional"));
            }
            else {

                merchandizecategoryString = merchandizecategoryString.append("," + getMerchanCategoryId("Ocassional"));
            }

        }
        if(isRoomPackageSelected){

            if(merchandizecategoryString==null){
                merchandizecategoryString =  new StringBuilder(getMerchanCategoryId("Room Package"));
            }
            else {
                merchandizecategoryString = merchandizecategoryString.append("," + getMerchanCategoryId("Room Package"));
            }
        }
        if(isDiningSelected){

            if(merchandizecategoryString==null){
                merchandizecategoryString =  new StringBuilder(getMerchanCategoryId("Dining"));
            }
            else {

                merchandizecategoryString = merchandizecategoryString.append("," + getMerchanCategoryId("Dining"));

            }
        }

        if(isMattressSelected){

            if(merchandizecategoryString==null){
                merchandizecategoryString =  new StringBuilder(getMerchanCategoryId("Mattress"));
            }
            else {
                merchandizecategoryString = merchandizecategoryString.append("," + getMerchanCategoryId("Mattress"));
            }

        }
        if(isMiscellaneousSelected){
            if(merchandizecategoryString==null){
                merchandizecategoryString =  new StringBuilder(getMerchanCategoryId("Miscellaneous"));
            }
            else {
                merchandizecategoryString = merchandizecategoryString.append("," + getMerchanCategoryId("Miscellaneous"));
            }
        }

        if(merchandizecategoryString==null){
            return null;
        }
        return merchandizecategoryString.toString();
    }

    public String getMerchanCategoryId(String merchantCategory){
        String merchantCategoryId = null;

        if(merchandizeCateoryApiResponse!=null) {

            ArrayList<MerchandizeCategoryDetails> merchandizeCategories = merchandizeCateoryApiResponse.getMerchandizeCategories();

            if (merchandizeCategories != null) {
                for (MerchandizeCategoryDetails merchandizeCategoryDetails : merchandizeCategories) {
                    String merchandizeCategoryName = merchandizeCategoryDetails.getMerchandizeCategory().getMerchandize_category_name();

                    if (merchandizeCategoryName.equalsIgnoreCase(merchantCategory)) {
                        merchantCategoryId = merchandizeCategoryDetails.getMerchandizeCategory().getMerchanizeId().trim();
                    }
                    if (merchandizeCategoryName.equalsIgnoreCase(merchantCategory)) {
                        merchantCategoryId = merchandizeCategoryDetails.getMerchandizeCategory().getMerchanizeId().trim();
                    }
                    if (merchandizeCategoryName.equalsIgnoreCase(merchantCategory)) {
                        merchantCategoryId = merchandizeCategoryDetails.getMerchandizeCategory().getMerchanizeId().trim();
                    }
                    if (merchandizeCategoryName.equalsIgnoreCase(merchantCategory)) {
                        merchantCategoryId = merchandizeCategoryDetails.getMerchandizeCategory().getMerchanizeId().trim();
                    }
                    if (merchandizeCategoryName.equalsIgnoreCase(merchantCategory)) {
                        merchantCategoryId = merchandizeCategoryDetails.getMerchandizeCategory().getMerchanizeId().trim();
                    }
                    if (merchandizeCategoryName.equalsIgnoreCase(merchantCategory)) {
                        merchantCategoryId = merchandizeCategoryDetails.getMerchandizeCategory().getMerchanizeId().trim();
                    }
                    if (merchandizeCategoryName.equalsIgnoreCase(merchantCategory)) {
                        merchantCategoryId = merchandizeCategoryDetails.getMerchandizeCategory().getMerchanizeId().trim();
                    }
                    if (merchandizeCategoryName.equalsIgnoreCase(merchantCategory)) {
                        merchantCategoryId = merchandizeCategoryDetails.getMerchandizeCategory().getMerchanizeId().trim();
                    }

                }
            }

        }


        return  merchantCategoryId;
    }
    public String getContactMethod(){
        String contactMethod = null;
        if(contactmethodMessage == true){
            contactMethod = "Message";
        }
        if(contactmethodMail == true){
            contactMethod = "Mail";
        }
        if (contactmethodCall == true){
            contactMethod = "Call";
        }
        if(contactMethod == null){
            contactMethod = "";
        }
        return  contactMethod;
    }






    public void setContactMethod(String contactMethod){

        if(contactMethod.toString().equalsIgnoreCase("Message")){
            contactMethodMessage.setImageResource(R.drawable.mail_selected);
            contactmethodMessage = true;


        }
        else if(contactMethod.toString().equalsIgnoreCase("Mail")){

            contactMethodMail.setImageResource(R.drawable.email_selected);
            contactmethodMail = true;
        }

        else if(contactMethod.toString().equalsIgnoreCase("Call")){
            contactMethodCall.setImageResource(R.drawable.phone_selected);
            contactmethodCall = true;

        }

    }

    // method to get reason for visit
    public String getReasonForVisit(){
        String reasonForVisit = null;
        if(reasonForVistSale == true){
            reasonForVisit = "Sale";
        }
        if(reasonForVistFinance == true){
            reasonForVisit = "Finance";

        }
        if (reasonForVistInternet == true){
            reasonForVisit = "Internet";
        }
        if(reasonForVisit == null){
            reasonForVisit = "";
        }

        return reasonForVisit;
    }


    public void setReasonForVisit(String reasonForVisit){

        if(reasonForVisit.toString().equalsIgnoreCase("Sale")){
            reasonForVistSale = true;
            reasonForVisitsaleImage.setImageResource(R.drawable.sale_selected);
        }
        else  if(reasonForVisit.toString().equalsIgnoreCase("Finance")){
            reasonForVistFinance = true;
            reasonForVisitfinanceImage.setImageResource(R.drawable.finance_selected);
        }

        else  if(reasonForVisit.toString().equalsIgnoreCase("Internet")){
            reasonForVistInternet = true;
            reasonForVisitinternetImage.setImageResource(R.drawable.internet_selected);
        }

    }







    public void showDialogForProspector(ArrayList<MerchandizeSubCategory> subCategoryPopUpModels, final int buttonId, final int originalButtonBackGround,final int changedButtonBackground,final String subCategoryName){

        final ArrayList<MerchandizeSubCategory> subCategoryPopUpModelsForDialog = new ArrayList<MerchandizeSubCategory>();

        Iterator<MerchandizeSubCategory> iterator = subCategoryPopUpModels.iterator();
        while(iterator.hasNext()){
            try {
                subCategoryPopUpModelsForDialog.add((MerchandizeSubCategory) iterator.next().clone());
            }
            catch (Exception ex){
                ex.printStackTrace();
            }

        }
        CustomAdapter adapterForPopUp  = new CustomAdapter(this, subCategoryPopUpModelsForDialog);
        ImageButton saveDialogButton;
        ImageButton returnDialogButton;
        final Dialog dialog = new Dialog(ProspectorActivity.this);

        dialog.setCancelable(false);
        dialog.setContentView(R.layout.activity_dialog);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

        saveDialogButton  = (ImageButton)dialog.findViewById(R.id.saveDialog);
        saveDialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Boolean isButtonSelected = false;
                //  getMerchandizeCategoryData();
                for(MerchandizeSubCategory merchandizeSubCategory :subCategoryPopUpModelsForDialog){
                    if(merchandizeSubCategory.isSelected()){
                        Button button = (Button) findViewById(buttonId);
                        button.setBackgroundResource(changedButtonBackground);

                        isButtonSelected = true;


                        break;
                    }
                }
                if(isButtonSelected == false){
                    Button button = (Button) findViewById(buttonId);
                    button.setBackgroundResource(originalButtonBackGround);
                    changeSubCategory(subCategoryName , subCategoryPopUpModelsForDialog);
                    setIfMerchandizeIsSelected();
                    dicactivateProspectSoldButton();
                }
                else{

                    changeSubCategory(subCategoryName , subCategoryPopUpModelsForDialog);
                    setIfMerchandizeIsSelected();
                }
                //  activateDictivateProspectSoldButton();
                dialog.dismiss();

            }

        });

        returnDialogButton = (ImageButton)dialog.findViewById(R.id.returnDialog);

        returnDialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                dialog.dismiss();

            }

        });
        dialogBoxListView = (ListView) dialog.findViewById(R.id.lv);
        dialogBoxListView.setAdapter(adapterForPopUp);

        dialog.show();

    }

    // method to set flags for Prospect sold button
    public void dicactivateProspectSoldButton(){
        if(!checkIfMerchandizeIsSelected() && prospectPlanSold==true){
            prospectorPlanSoldButton.setBackgroundResource(R.drawable.protection_plan_unselected);
            prospectPlanSold = false;
        }
    }
    // method to set flags for Prospect sold button
    public void activateDictivateProspectSoldButton(){
        //setIfMerchandizeIsSelected();
        String salesNo = salesNoText.getText().toString();
        if(checkIfMerchandizeIsSelected() && (!salesNo.trim().isEmpty()) && !salesNo.equalsIgnoreCase("")){
            prospectorPlanSoldButton.setBackgroundResource(R.drawable.protection_plan_sold_selected);
            prospectPlanSold = true;

        }
        //   else if((isUpholestrySelected==false) && (isBedroomSelected == false) && (isHomeOfficeSelected == false)&& (isOcassionalSelected == false)&&
        //          (isRoomPackageSelected == false)&& (isDiningSelected == false)&& (isMattressSelected == false) && (isMiscellaneousSelected==false)){
        else{
           /*  Toast toast = Toast.makeText(getApplicationContext(), "Please Add Sales No And Select Merchandize Category to sell protection plan", Toast.LENGTH_LONG);
            View view = toast.getView();
            view.getBackground().setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_IN);
            TextView text = view.findViewById(android.R.id.message);
            text.setTextColor(Color.RED);
            toast.show();*/

            showDialogForNotification("Please add Sales No. and select Merchandise to add a Protection Plan");

        }
    }
    public void changeSubCategory(String subCategoryName,ArrayList<MerchandizeSubCategory> subCategoryModels ){

        switch(subCategoryName.toString())
        {
            case "Upholstery":
                upholsterySubCategoryPopUpModelForPopUp = new ArrayList<MerchandizeSubCategory>();

                Iterator<MerchandizeSubCategory> iterator = subCategoryModels.iterator();
                while(iterator.hasNext()){
                    try {
                        upholsterySubCategoryPopUpModelForPopUp.add((MerchandizeSubCategory) iterator.next().clone());
                    }
                    catch (Exception ex){
                        ex.printStackTrace();
                    }

                }
                break;

            case "Bedroom":
                bedroomSubCategoryPopUpModelForPopUp = new ArrayList<MerchandizeSubCategory>();
                Iterator<MerchandizeSubCategory> iterator1 = subCategoryModels.iterator();
                while(iterator1.hasNext()){
                    try {
                        bedroomSubCategoryPopUpModelForPopUp.add((MerchandizeSubCategory) iterator1.next().clone());
                    }
                    catch (Exception ex){
                        ex.printStackTrace();
                    }

                }
                break;

            case "Home Office":
                homeOfficeSubCategoryPopUpModelForPopUp =  new ArrayList<MerchandizeSubCategory>();

                Iterator<MerchandizeSubCategory> iterator2 = subCategoryModels.iterator();
                while(iterator2.hasNext()){
                    try {
                        homeOfficeSubCategoryPopUpModelForPopUp.add((MerchandizeSubCategory) iterator2.next().clone());
                    }
                    catch (Exception ex){
                        ex.printStackTrace();
                    }

                }
                break;

            case "Ocassional":
                ocassionalSubCategoryPopUpModelForPopUp = new ArrayList<MerchandizeSubCategory>();

                Iterator<MerchandizeSubCategory> iterator3 = subCategoryModels.iterator();
                while(iterator3.hasNext()){
                    try {
                        ocassionalSubCategoryPopUpModelForPopUp.add((MerchandizeSubCategory) iterator3.next().clone());
                    }
                    catch (Exception ex){
                        ex.printStackTrace();
                    }

                }

                break;

            case "Room Package":
                roomPackageSubCategoryPopUpModelForPopUp = new ArrayList<MerchandizeSubCategory>();
                Iterator<MerchandizeSubCategory> iterator4 = subCategoryModels.iterator();
                while(iterator4.hasNext()){
                    try {
                        roomPackageSubCategoryPopUpModelForPopUp.add((MerchandizeSubCategory) iterator4.next().clone());
                    }
                    catch (Exception ex){
                        ex.printStackTrace();
                    }

                }



                break;

            case "Dining":
                diningSubCategoryPopUpModelForPopUp = new ArrayList<MerchandizeSubCategory>();

                Iterator<MerchandizeSubCategory> iterator5 = subCategoryModels.iterator();
                while(iterator5.hasNext()){
                    try {
                        diningSubCategoryPopUpModelForPopUp.add((MerchandizeSubCategory) iterator5.next().clone());
                    }
                    catch (Exception ex){
                        ex.printStackTrace();
                    }

                }
                break;

            case "Mattress":
                matressSubCategoryPopUpModelForPopUp = new ArrayList<MerchandizeSubCategory>();

                Iterator<MerchandizeSubCategory> iterator6 = subCategoryModels.iterator();
                while(iterator6.hasNext()){
                    try {
                        matressSubCategoryPopUpModelForPopUp.add((MerchandizeSubCategory) iterator6.next().clone());
                    }
                    catch (Exception ex){
                        ex.printStackTrace();
                    }

                }


                break;
            case "Miscellaneous":
                miscellaneousSubCategoryPopUpModelForPopUp = new ArrayList<MerchandizeSubCategory>();

                Iterator<MerchandizeSubCategory> iterator7 = subCategoryModels.iterator();
                while(iterator7.hasNext()){
                    try {
                        miscellaneousSubCategoryPopUpModelForPopUp.add((MerchandizeSubCategory) iterator7.next().clone());
                    }
                    catch (Exception ex){
                        ex.printStackTrace();
                    }

                }
                break;
        }//switch




    }
    public void setDataForUpdate(){

        firstNameText.setText(prospectorUpdateModel.getFirstName());
        lastNameText.setText(prospectorUpdateModel.getLastName());
        salesNoText.setText(prospectorUpdateModel.getSalesOrder());
        phoneText.setText(prospectorUpdateModel.getPhoneNumber());
        emailText.setText(prospectorUpdateModel.getEmailId());
        streetText.setText(prospectorUpdateModel.getStreet());
        cityText.setText(prospectorUpdateModel.getCity());
        zipCodeText.setText(prospectorUpdateModel.getZipCode());


        for(int i=0 ; i < stateList.size();i++){

            if(!prospectorUpdateModel.getState().toString().trim().isEmpty()){
                int no = Integer.parseInt(prospectorUpdateModel.getState().toString());
                stateSpinner.setSelection(stateList.get(no-1));
            }
            /*if(stateList.get(i).equalsIgnoreCase(prospectorUpdateModel.getState())){
                stateSpinner.setSelection(prospectorUpdateModel.getState());
                break;
            }*/
        }
        for(int i=0; i < ageCategories.size();i++){

            if(ageCategories.get(i).equalsIgnoreCase(prospectorUpdateModel.getAge())){
                ageSpinner.setSelection(i);
                break;
            }

        }

        for(int i=0; i < genderCategories.size();i++){
            if(genderCategories.get(i).equalsIgnoreCase(prospectorUpdateModel.getGender())){
                genderSpinner.setSelection(i);
                break;
            }

        }

        setReasonForVisit(prospectorUpdateModel.getReasonForVisit());
        setContactMethod(prospectorUpdateModel.getContactMethod());

        setMerchantCategory();

        if(prospectorUpdateModel.getIsProspectorPlanSold().equalsIgnoreCase("Y")) {
            prospectPlanSold = true;
            prospectorPlanSoldButton.setBackgroundResource(R.drawable.protection_plan_sold_selected);

        }
        else{
            prospectPlanSold = false;
            prospectorPlanSoldButton.setBackgroundResource(R.drawable.protection_plan_unselected);
        }

    }
    // method to set Merchant Category for updation of data.
    public void setMerchantCategory(){
        if(prospectorUpdateModel.getMerchantCategoryIdString()!=null && !(prospectorUpdateModel.getMerchantCategoryIdString().isEmpty())){
            String[] values = prospectorUpdateModel.getSubMerchantCategoryIdString().split(",");
            ArrayList<MerchandizeCategoryDetails> merchandizeCategories = merchandizeCateoryApiResponse.getMerchandizeCategories();

            for(String str:values) {
                for (MerchandizeCategoryDetails merchandizeCategoryDetail : merchandizeCategories) {
                    ArrayList<MerchandizeSubCategory> merchandizeSubCategories =   merchandizeCategoryDetail.getSubCategories();
                    for(MerchandizeSubCategory merchandizeSubCategory:merchandizeSubCategories){

                        if(merchandizeSubCategory.getMerchandizeSubCatId().equalsIgnoreCase(str)){
                            setMerchandizeCategorySelected(merchandizeCategoryDetail.getMerchandizeCategory().getMerchandize_category_name(),str);
                        }
                    }

                }//
            }
        }

    }

    // method to set data
    public void setMerchandizeCategorySelected(String merchantCategoryName,String subMerchandizeCategoryId){

        switch(merchantCategoryName)
        {
            case "Upholstery":
                for(MerchandizeSubCategory  merchandizeSubCategory :upholsterySubCategoryPopUpModelForPopUp){
                    if(merchandizeSubCategory.getMerchandizeSubCatId().equalsIgnoreCase(subMerchandizeCategoryId)){
                        merchandizeSubCategory.setSelected(true);
                        upHolsteryButton.setBackgroundResource(R.drawable.upholstery_selected);
                        setIfMerchandizeIsSelected();
                        // activateDictivateProspectSoldButton();
                        break;
                    }
                }
                break;

            case "Bedroom":
                for(MerchandizeSubCategory  merchandizeSubCategory :bedroomSubCategoryPopUpModelForPopUp){
                    if(merchandizeSubCategory.getMerchandizeSubCatId().equalsIgnoreCase(subMerchandizeCategoryId)){
                        merchandizeSubCategory.setSelected(true);
                        bedroomButton.setBackgroundResource(R.drawable.bedroom_selected_);
                        setIfMerchandizeIsSelected();
                        // activateDictivateProspectSoldButton();
                        break;

                    }
                }
                break;

            case "Home Office":
                for(MerchandizeSubCategory  merchandizeSubCategory :homeOfficeSubCategoryPopUpModelForPopUp){
                    if(merchandizeSubCategory.getMerchandizeSubCatId().equalsIgnoreCase(subMerchandizeCategoryId)){
                        merchandizeSubCategory.setSelected(true);
                        homeOfficeButton.setBackgroundResource(R.drawable.home_office_selected);
                        setIfMerchandizeIsSelected();
                        // activateDictivateProspectSoldButton();
                        break;
                    }
                }
                break;

            case "Ocassional":
                for(MerchandizeSubCategory  merchandizeSubCategory :ocassionalSubCategoryPopUpModelForPopUp){
                    if(merchandizeSubCategory.getMerchandizeSubCatId().equalsIgnoreCase(subMerchandizeCategoryId)){
                        merchandizeSubCategory.setSelected(true);
                        ocassionalButton.setBackgroundResource(R.drawable.ocassional);
                        setIfMerchandizeIsSelected();
                        // activateDictivateProspectSoldButton();
                        break;
                    }
                }

                break;

            case "Room Package":
                for(MerchandizeSubCategory  merchandizeSubCategory :roomPackageSubCategoryPopUpModelForPopUp){
                    if(merchandizeSubCategory.getMerchandizeSubCatId().equalsIgnoreCase(subMerchandizeCategoryId)){
                        merchandizeSubCategory.setSelected(true);
                        roomPackageButton.setBackgroundResource(R.drawable.room_package_selected);
                        setIfMerchandizeIsSelected();
                        // activateDictivateProspectSoldButton();
                        break;
                    }
                }

                break;

            case "Dining":
                for(MerchandizeSubCategory  merchandizeSubCategory :diningSubCategoryPopUpModelForPopUp){
                    if(merchandizeSubCategory.getMerchandizeSubCatId().equalsIgnoreCase(subMerchandizeCategoryId)){
                        merchandizeSubCategory.setSelected(true);
                        diningButton.setBackgroundResource(R.drawable.dining_selected);
                        setIfMerchandizeIsSelected();
                        // activateDictivateProspectSoldButton();
                        break;
                    }
                }

                break;

            case "Mattress":
                for(MerchandizeSubCategory  merchandizeSubCategory :matressSubCategoryPopUpModelForPopUp){
                    if(merchandizeSubCategory.getMerchandizeSubCatId().equalsIgnoreCase(subMerchandizeCategoryId)){
                        merchandizeSubCategory.setSelected(true);
                        matressButton.setBackgroundResource(R.drawable.matress_selected);
                        setIfMerchandizeIsSelected();
                        // activateDictivateProspectSoldButton();
                        break;
                    }
                }

                break;
            case "Miscellaneous":
                for(MerchandizeSubCategory  merchandizeSubCategory :miscellaneousSubCategoryPopUpModelForPopUp){
                    if(merchandizeSubCategory.getMerchandizeSubCatId().equalsIgnoreCase(subMerchandizeCategoryId)){
                        merchandizeSubCategory.setSelected(true);
                        miscellaneousButton.setBackgroundResource(R.drawable.miscellaneous_selected);
                        setIfMerchandizeIsSelected();
                        //  activateDictivateProspectSoldButton();

                        break;
                    }
                }

                break;
        }//switch

    }

    // method to get data from database for given updation.
    public void getMerchandizeCategoryDataFromDataBase(){

        try {
            Call<MerchandizeCateoryApiResponse> call2 = apiInterface.getMerchandizeCategories(mAppPreference.getDealerId());
            call2.enqueue(new Callback<MerchandizeCateoryApiResponse>() {
                @Override
                public void onResponse(Call<MerchandizeCateoryApiResponse> call, Response<MerchandizeCateoryApiResponse> apiResponse) {
                    if(!apiResponse.body().isError()) {
                        // handleResponceFromBeacon(call, apiResponse);
                        merchandizeCateoryApiResponse = apiResponse.body();
                        separateDataFromApiResponse();
                        if(isProspectorFromTask){
                            getProspectorUpdateData();
                        }

                    }
                }

                @Override
                public void onFailure(Call<MerchandizeCateoryApiResponse> call, Throwable t) {

                    showDialogForNotification("No Network Connection");

                    //  Toast.makeText(getApplicationContext(), "No Network Connection", Toast.LENGTH_LONG).show();
                    call.cancel();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    public ArrayList<String>getStateSpinnerData(){
        final ArrayList<String> stateList =  new ArrayList<String>();
        try {
            Call<SpinnerStateApiResponse> call2 = apiInterface.getSpinnerForState(mAppPreference.getStoreId());
            call2.enqueue(new Callback<SpinnerStateApiResponse>() {
                @Override
                public void onResponse(Call<SpinnerStateApiResponse> call, Response<SpinnerStateApiResponse> apiResponse) {
                    if(!apiResponse.body().isError()) {
                        ArrayList<String> stringArrayList = new ArrayList<String>();
                        // handleResponceFromBeacon(call, apiResponse);
                        stateModelArrayList = apiResponse.body().getStateSpinnerList();


                        for(StateModel  stateModel:stateModelArrayList){
                            //  stateList.add(stateModel.getStateShortName());
                            stringArrayList.add(stateModel.getStateShortName());

                        }
                        setDataForSpinner(stringArrayList);
                        // System.out.println(""+merchandizeCateoryApiResponse);
                    }
                    else
                    {

                        showDialogForNotification("No Network Connection");
                    }
                }

                @Override
                public void onFailure(Call<SpinnerStateApiResponse> call, Throwable t) {

                    showDialogForNotification("No Network Connection");

                    //   Toast.makeText(getApplicationContext(), "No Network Connection", Toast.LENGTH_LONG).show();
                    call.cancel();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
        return stateList;
    }



    // get data for update
    public void getProspectorUpdateData(){
        try {
            Call<ProspectorUpdateApiResponse> call2 = apiInterface.getProspectorData(prospectorIdFromTaskActivity);
            call2.enqueue(new Callback<ProspectorUpdateApiResponse>() {
                @Override
                public void onResponse(Call<ProspectorUpdateApiResponse> call, Response<ProspectorUpdateApiResponse> apiResponse) {
                    if(!apiResponse.body().isError()) {
                        ProspectorUpdateModel prospectorUpdateModelFromApi[] = apiResponse.body().getProspectorUpdateModel();
                        prospectorUpdateModel = new ProspectorUpdateModel();
                        prospectorUpdateModel = prospectorUpdateModelFromApi[0];
                        setDataForUpdate();

                    }
                    else
                    {
                        // System.out.println(apiResponse.body().getMessage());
                    }
                }

                @Override
                public void onFailure(Call<ProspectorUpdateApiResponse> call, Throwable t) {


                    //   Toast.makeText(getApplicationContext(), "No Network Connection", Toast.LENGTH_LONG).show();
                    call.cancel();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    public void showDialogForSaveProspector(){

        final Dialog dialog = new Dialog(ProspectorActivity.this);
        ImageView okButton ;
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.save_task_dialog);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

        okButton = dialog.findViewById(R.id.ok_button);
        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

            }

        });
        dialog.show();
    }


    public void setProspectorMerchandiseDataToServer(){
        try {

            Call<ProspectorApiResponse> call2 = apiInterface.setProspectsData("0",mAppPreference.getStoreId().toString(),mAppPreference.getDealerId().toString(),
                    mAppPreference.getAgentId(),prospectorModelForApi.getProspectorFirstName().toString(),prospectorModelForApi.getProspectorLastName(),prospectorModelForApi.getProspectorPhone(),
                    prospectorModelForApi.getProspectorEmail(),prospectorModelForApi.getProspectorStreet(),prospectorModelForApi.getProspectorCity(),prospectorModelForApi.getProspectorState(),
                    prospectorModelForApi.getProspectorZipCode(),
                    prospectorModelForApi.getProspectorAge(),
                    prospectorModelForApi.getProspectorGender(),
                    prospectorModelForApi.getProspectorVisitReason(),
                    prospectorModelForApi.getProspectorContactMethod(),
                    prospectorModelForApi.getProspectorMerchandizeString(),prospectorModelForApi.getProspectorSubMerchandizeString(),
                    prospectorModelForApi.getProtectionPlanSold(),prospectorModelForApi.getProspectorSalesNo());

            call2.enqueue(new Callback<ProspectorApiResponse>() {
                @Override
                public void onResponse(Call<ProspectorApiResponse> call, Response<ProspectorApiResponse> response) {
                    //  System.out.println(response.body().toString());
                    ProspectorApiResponse res=response.body();
                    if (!res.isError()) {

                        prospectorModelForApi.setProspectorId(res.getProspectId());
                        //   taskButton.setEnabled(true);
                        taskButtonEnabled =  true;
                        showDialogForSaveProspector();

                    } else{
                        //  Toast.makeText(getApplicationContext(), "invalid parameters" , Toast.LENGTH_LONG).show();

                        showDialogForNotification("invalid parameters");

                    }


                }

                @Override
                public void onFailure(Call<ProspectorApiResponse> call, Throwable t) {
                   /* Toast toast = Toast.makeText(getApplicationContext(), "Save Failed.Please check Net connection", Toast.LENGTH_LONG);
                    View view = toast.getView();
                    view.getBackground().setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_IN);
                    TextView text = view.findViewById(android.R.id.message);
                    text.setTextColor(Color.RED);
                    toast.show();*/

                    showDialogForNotification("Save Failed.Please check Net connection");

                    //  Toast.makeText(getApplicationContext(), "Saved Failed.Please check Net connection" , Toast.LENGTH_LONG).show();
                    call.cancel();
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void setProspectorMerchandiseUpdateDataToServer(){
        try {

            Call<ProspectorApiResponse> call2 = apiInterface.setProspectsData(prospectorIdFromTaskActivity,mAppPreference.getStoreId().toString(),mAppPreference.getDealerId().toString(),
                    mAppPreference.getAgentId(),prospectorModelForApi.getProspectorFirstName().toString(),prospectorModelForApi.getProspectorLastName(),prospectorModelForApi.getProspectorPhone(),
                    prospectorModelForApi.getProspectorEmail(),prospectorModelForApi.getProspectorStreet(),prospectorModelForApi.getProspectorCity(),prospectorModelForApi.getProspectorState(),
                    prospectorModelForApi.getProspectorZipCode(),prospectorModelForApi.getProspectorAge(),prospectorModelForApi.getProspectorGender(),prospectorModelForApi.getProspectorVisitReason(),
                    prospectorModelForApi.getProspectorContactMethod(),prospectorModelForApi.getProspectorMerchandizeString(),prospectorModelForApi.getProspectorSubMerchandizeString(),
                    prospectorModelForApi.getProtectionPlanSold(),prospectorModelForApi.getProspectorSalesNo());

            call2.enqueue(new Callback<ProspectorApiResponse>() {
                @Override
                public void onResponse(Call<ProspectorApiResponse> call, Response<ProspectorApiResponse> response) {
                    System.out.println(response.body().toString());
                    ProspectorApiResponse res=response.body();
                    if (!res.isError()) {

                        prospectorModelForApi.setProspectorId(res.getProspectId());
                        //taskButton.setEnabled(true);
                        taskButtonEnabled = true;
                        showDialogForSaveProspector();
                        isProspectorUpdateDataSaved = true;

                    } else{
                        // Toast.makeText(getApplicationContext(), "invalid parameters" , Toast.LENGTH_LONG).show();

                        showDialogForNotification("invalid parameters" );
                    }


                }

                @Override
                public void onFailure(Call<ProspectorApiResponse> call, Throwable t) {
                  /* Toast toast = Toast.makeText(getApplicationContext(), "Save Failed.Please check Net connection", Toast.LENGTH_LONG);
                   View view = toast.getView();
                   view.getBackground().setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_IN);
                   TextView text = view.findViewById(android.R.id.message);
                   text.setTextColor(Color.RED);
                   toast.show();*/

                    showDialogForNotification("Save Failed.Please check Net connection");

                    //  Toast.makeText(getApplicationContext(), "Saved Failed.Please check Net connection" , Toast.LENGTH_LONG).show();
                    call.cancel();
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
