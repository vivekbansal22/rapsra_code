package com.example.gns076.rapsra.activities;

import android.content.Intent;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.RelativeSizeSpan;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.example.gns076.rapsra.R;
import com.example.gns076.rapsra.model.AppPreference;

public class WrittenActivity extends BaseActivity implements View.OnClickListener {

    private TextView marque1;
    //List<MyTeamStatus> mts2;
    private APIInterface apiInterface;
    private String crawler1;
    private AppPreference mAppPreference;
    public Button menuButton;
    public Button virtualAssist;
    private Button todayPerformanceButton;
    private Button weekPerformanceButton;
    private Button monthPerformanceButton;
    private Button yearPerformanceButton;

    private Button deliveredButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_written_x);
        initializeObjects();

        setListeners();

        menuButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Creating the instance of PopupMenu
                PopupMenu popup = new PopupMenu(WrittenActivity.this, menuButton);
                //Inflating the Popup using xml file

                menuButton.setBackgroundResource(R.drawable.exit_menu);

                Menu gamesMenu =popup.getMenu();


                popup.getMenuInflater()
                        .inflate(R.menu.menu_home, gamesMenu);
                gamesMenu.removeItem(R.id.logout);
                gamesMenu.removeItem(R.id.my_rank);
                gamesMenu.removeItem(R.id.add_edit_so);
                gamesMenu.removeItem(R.id.scheduled_followups);


                for(int i = 0; i < gamesMenu.size(); i++) {
                    MenuItem item = gamesMenu.getItem(i);


                    SpannableString spanString = new SpannableString(gamesMenu.getItem(i).getTitle().toString());
                    int end = spanString.length();

                    //   spanString.setSpan(new RelativeSizeSpan(1.5f), 0, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    spanString.setSpan(new RelativeSizeSpan(0.04f), 0, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    item.setTitle(spanString);

                }


                popup.setOnDismissListener(new PopupMenu.OnDismissListener() {
                    @Override
                    public void onDismiss(PopupMenu popupMenu) {
                        menuButton.setBackgroundResource(R.drawable.menu);
                    }
                });


                //registering popup with OnMenuItemClickListener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {

                        Intent intentButtonClick;
                        switch (item.getItemId()){

                            case R.id.main_page:
                                intentButtonClick = new Intent(getApplicationContext(), SalesRadarActivityNew.class);
                                startActivity(intentButtonClick);
                                break;

                          /*  case R.id.add_edit_so:
                                intentButtonClick = new Intent(getApplicationContext(), AddEditSoActivity.class);
                                startActivity(intentButtonClick);
                                break;*/

                            case R.id.add_edit_prospect:
                                intentButtonClick = new Intent(getApplicationContext(), ProspectorActivity.class);
                                intentButtonClick.putExtra("Token",mAppPreference.getToken() );
                                startActivity(intentButtonClick);
                                break;

                            case R.id.rap_gamify:
                                intentButtonClick = new Intent(getApplicationContext(), GamificationActivity.class);
                                startActivity(intentButtonClick);
                                break;
                            case R.id.my_rank:
                                intentButtonClick = new Intent(getApplicationContext(), WrittenActivity.class);
                                startActivity(intentButtonClick);
                                break;
                            case R.id.chat_menu:
                                intentButtonClick = new Intent(getApplicationContext(), ChatActivity.class);
                                startActivity(intentButtonClick);
                                break;

                            case R.id.ar_mode:
                                intentButtonClick = new Intent(getApplicationContext(), ArViewActivity.class);
                                startActivity(intentButtonClick);
                                break;

                            case R.id.floor_map_mode:
                                intentButtonClick = new Intent(getApplicationContext(), FloorActivity.class);
                                startActivity(intentButtonClick);
                                break;

                            case R.id.fun_games:
                                intentButtonClick = new Intent(getApplicationContext(), FunGames.class);
                                startActivity(intentButtonClick);
                                break;


                        }
                        return true;
                    }
                });

                popup.show(); //showing popup menu


            }
        }); //closi
    }

    // method to   initializeObjects(){
    public void initializeObjects(){

        mAppPreference = new AppPreference(this);
        crawler1=mAppPreference.getCrawler();

        marque1 = (TextView) this.findViewById(R.id.my_performance_news_crawler_x);
        marque1.setText(crawler1);
        marque1.setSelected(true);

        menuButton = (Button)findViewById(R.id.menu_button_performance_x);
        virtualAssist = (Button)findViewById(R.id.performanceVirtualAssitant_x);

        todayPerformanceButton = (Button)findViewById(R.id.today_selected2_x);
        weekPerformanceButton = (Button)findViewById(R.id.week_selected3_x);
        monthPerformanceButton = (Button)findViewById(R.id.month_selected4_x);
        yearPerformanceButton = (Button)findViewById(R.id.year_selected_x);

        deliveredButton = (Button)findViewById(R.id.unhighlighted_delivered_tab_x);

    }

    // method to set listeners
    public void setListeners(){

        todayPerformanceButton.setOnClickListener(this);
        weekPerformanceButton.setOnClickListener(this);
        monthPerformanceButton.setOnClickListener(this);
        yearPerformanceButton.setOnClickListener(this);

        deliveredButton.setOnClickListener(this);

        virtualAssist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent  intentVirtualAssist= new Intent(getApplicationContext(), VirtualAssistantActivity.class);
                startActivity(intentVirtualAssist);
                finish();
            }
        });

    }
    @Override
    public void onClick(View v) {
        Intent  intentSearch;
        switch (v.getId()) {

            case R.id.today_selected2_x:

                //  intentSearch= new Intent(getApplicationContext(), WrittenActivity.class);
                // startActivity(intentSearch);
                changeButtonBackground("Today");
                // todayPerformanceButton.setBackgroundResource(changedButtonBackground);
                // finish();

                break;

            case R.id.week_selected3_x:
                // intentSearch= new Intent(getApplicationContext(), WrittenActivity.class);
                // startActivity(intentSearch);
                changeButtonBackground("Week");
                // finish();


                break;

            case R.id.month_selected4_x:
                // intentSearch= new Intent(getApplicationContext(), WrittenActivity.class);
                // startActivity(intentSearch);
                changeButtonBackground("Month");
                // finish();

                break;

            case R.id.year_selected_x:
                //  intentSearch= new Intent(getApplicationContext(), WrittenActivity.class);
                // startActivity(intentSearch);
                changeButtonBackground("Year");
                // finish();

                break;

            case R.id.unhighlighted_delivered_tab_x:
                intentSearch= new Intent(getApplicationContext(), DelieveredActivity.class);
                startActivity(intentSearch);
                // changeButtonBackground("Year");
                finish();
                break;

        }
    }

    // method to change button background on click
    public void changeButtonBackground(String buttonSelected){

        switch(buttonSelected){
            case "Today":
                todayPerformanceButton.setBackgroundResource(R.drawable.selected_today_button_pp);
                weekPerformanceButton.setBackgroundResource(R.drawable.unselected_week_button_pp);
                monthPerformanceButton.setBackgroundResource(R.drawable.unselected_month_button_pp);
                yearPerformanceButton.setBackgroundResource(R.drawable.unselected_year_button_pp);



                break;

            case "Week":

                todayPerformanceButton.setBackgroundResource(R.drawable.unselected_today_button_pp);
                weekPerformanceButton.setBackgroundResource(R.drawable.selected_week_button_pp);
                monthPerformanceButton.setBackgroundResource(R.drawable.unselected_month_button_pp);
                yearPerformanceButton.setBackgroundResource(R.drawable.unselected_year_button_pp);
                break;

            case "Month":
                todayPerformanceButton.setBackgroundResource(R.drawable.unselected_today_button_pp);
                weekPerformanceButton.setBackgroundResource(R.drawable.unselected_week_button_pp);
                monthPerformanceButton.setBackgroundResource(R.drawable.selected_month_button_pp);
                yearPerformanceButton.setBackgroundResource(R.drawable.unselected_year_button_pp);
                break;

            case "Year":
                todayPerformanceButton.setBackgroundResource(R.drawable.unselected_today_button_pp);
                weekPerformanceButton.setBackgroundResource(R.drawable.unselected_week_button_pp);
                monthPerformanceButton.setBackgroundResource(R.drawable.unselected_month_button_pp);
                yearPerformanceButton.setBackgroundResource(R.drawable.selected_year_button_pp);
                break;
        }

    }
}
