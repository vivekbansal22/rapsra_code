package com.example.gns076.rapsra.activities;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.Environment;
import android.os.RemoteException;
import android.util.Log;

import com.example.gns076.rapsra.model.AppPreference;
import com.example.gns076.rapsra.model.BeaconLogInfo;
import com.example.gns076.rapsra.model.BeaconModel;
import com.example.gns076.rapsra.model.BeaconModelFloorPlan;
import com.example.gns076.rapsra.model.BeaconParameters;
import com.example.gns076.rapsra.model.MerchandiseBeaconInfoResponse;
import com.google.android.gms.common.util.ArrayUtils;
import com.google.gson.Gson;

import org.altbeacon.beacon.Beacon;
import org.altbeacon.beacon.BeaconConsumer;
import org.altbeacon.beacon.BeaconManager;
import org.altbeacon.beacon.BeaconParser;
import org.altbeacon.beacon.RangeNotifier;
import org.altbeacon.beacon.Region;
import org.altbeacon.beacon.service.ArmaRssiFilter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class BeaconScanningApp extends Application implements BeaconConsumer/*, SensorEventListener*/ {

    private static final String TAG = BeaconScanningApp.class.getSimpleName();

    RangeNotifier rangeNotifier = null;

    private static float pathLossParameter = 2.3f;

    private static float measuredRssi =-73;

    private BeaconManager mBeaconManager;
    private BeaconModelFloorPlan beaconModelFloorPlan;

    private List<BeaconModelFloorPlan> beaconInfoList;
    private HashMap<String, BeaconModelFloorPlan> beaconHashmap =  new HashMap<String, BeaconModelFloorPlan>();

    private BeaconParameters beaconParameters;

    private  int[] rssis;

    private float meanRssi;

    private Double calculatedDistance;



    SharedPreferences sharedPreferences;

    Gson gson = new Gson();
    String json = "";


    private ArrayList<BeaconLogInfo> beaconLogInfoArrayList = new ArrayList<>();
    BeaconLogInfo beaconLogInfo;
    private String agent_id;
    private AppPreference mAppPreference;

    public APIInterface apiInterface;
    private String role;


    @Override
    public void onCreate() {
        super.onCreate();

        mAppPreference = new AppPreference(this);
        // mainActivity =  new Intent(getApplicationContext(),MainActivity.class);
        apiInterface = APIClient.getClient().create(APIInterface.class);
        role =  mAppPreference.getRole();

        mBeaconManager = BeaconManager.getInstanceForApplication(getApplicationContext());
        mBeaconManager.getBeaconParsers().add(new BeaconParser().
                setBeaconLayout(BeaconParser.EDDYSTONE_UID_LAYOUT));

        mBeaconManager.getBeaconParsers().add(new BeaconParser()
                .setBeaconLayout("m:2-3=0215,i:4-19,i:20-21,i:22-23,p:24-24"));
        mBeaconManager.setBackgroundScanPeriod(60000l);
        mBeaconManager.setBackgroundBetweenScanPeriod(60000l);
        mBeaconManager.setRssiFilterImplClass(ArmaRssiFilter.class);
        mBeaconManager.bind(this);
        try {
            mBeaconManager.updateScanPeriods();
        } catch (RemoteException e) {
            //e.printStackTrace();
            // ignore updation
        }



    }



    @Override
    public void onBeaconServiceConnect() {
        mBeaconManager.removeAllRangeNotifiers();

        rangeNotifier = new RangeNotifier() {


            @Override
            public void didRangeBeaconsInRegion(Collection<Beacon> beacons, Region region) {
                String beaconId = "";
                String beaconRssi = "";
                Double beaconDistance ;
                String tempBeaconId = "";


                StringBuilder logString;

                sharedPreferences = getSharedPreferences("rapsra_pref_beacon", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor =  sharedPreferences.edit();

                Date c = Calendar.getInstance().getTime();
                System.out.println("Current time => " + c);

                SimpleDateFormat df = new SimpleDateFormat("YYYY-MM-dd hh:mm:ss zzz");
                String formattedDate = df.format(c);

                agent_id = mAppPreference.getAgentId();
                if (beacons.size() > 0) {

                    for(Beacon beacon:beacons) {


                        beaconId = beacon.getId1().toString()+beacon.getId2().toString();


                        beaconRssi = Integer.toString(beacon.getRssi());
                        beaconDistance = beacon.getDistance();
                        beaconParameters =  new BeaconParameters(Double.toString(beaconDistance),beaconRssi);

                        if(beaconHashmap.containsKey(beaconId)) {
                            beaconModelFloorPlan = beaconHashmap.get(beaconId);
                            beaconModelFloorPlan.addParameter(beaconParameters);

                           rssis = getRssisFromBeaconList(beaconModelFloorPlan.getBeaconParametersList());

                           /* Code added to remove 2 max and 2 min values from list*/
                            rssis = removeMaxAndMinValues(rssis);

                          meanRssi = calculateMean(rssis);
                           Log.i(TAG,"Mean RSSI value:::"+meanRssi);

                           calculatedDistance = calculateDistancetemp(meanRssi, measuredRssi, pathLossParameter);

                           Log.i(TAG, beaconId + "  is :  " + beacon.getDistance() + " for rssi " + beacon.getRssi());

                            Log.i(TAG, "Calculated Distance for beacon ID   " + beaconId + "  is :  " + calculatedDistance);
                            // logString.append(" Calculated Distance is "+ calculatedDistance);
                            if(calculatedDistance!=null) {
                                beaconModelFloorPlan.setMeanDistance(calculatedDistance);
                            }



                        }
                        //         }
                        else{

                            beaconModelFloorPlan = new BeaconModelFloorPlan(beaconId);
                            beaconModelFloorPlan.addParameter(beaconParameters);
                            /*Added code to check for specific beacon ID's*/
                         /*   if(beaconIDList.contains(beaconId))
                            {*/
                            beaconHashmap.put(beaconId, beaconModelFloorPlan);

                            //   }
                            /*Added code to check for specific beacon ID's*/
                            //     beaconHashmap.put(beaconId, beaconModelFloorPlan);
                            Log.i(TAG,"New Beacon added with : "+beaconId);


                        }


                    }



                    beaconInfoList =  new ArrayList<>(beaconHashmap.values());

                    json =  gson.toJson(beaconInfoList);
                    editor.putString("BeaconInfoList",json);
                    editor.commit();


                }
            }
        };
        mBeaconManager.addRangeNotifier(rangeNotifier);


        try {
            mBeaconManager.startRangingBeaconsInRegion(new Region("myRangingUniqueId", null, null, null));
        } catch (RemoteException e) {    }

        if(beaconLogInfoArrayList!=null){
            this.setBeaconLogInfoArrayList(beaconLogInfoArrayList);
        }

    }

    public int[]removeMaxAndMinValues(int arrayRssis[]){
        ArrayList<Integer>arrayList = new ArrayList<Integer>();
        Arrays.sort(arrayRssis);
        if(arrayRssis.length >4) {
            for (int a : arrayRssis) {
                arrayList.add(a);
            }
            arrayList.remove(0);
            arrayList.remove(0);
            arrayList.remove(arrayList.size() - 1);
            arrayList.remove(arrayList.size() - 1);


            arrayRssis = ArrayUtils.toPrimitiveArray(arrayList);
        }

        return arrayRssis;

    }



    public int[] getRssisFromBeaconList(List<BeaconParameters> beaconList) {
        int[] rssis = new int[beaconList.size()];
        for (int i = 0; i < beaconList.size(); i++) {
            rssis[i] = Integer.parseInt(beaconList.get(i).getBeaconRssi());
        }
        return rssis;
    }



    public float calculateMean(int[] values) {
        int sum = 0;
        for (int i = 0; i < values.length; i++) {
            sum += values[i];
        }
        return sum / (float) values.length;
    }



    public static Double calculateDistancetemp(float meanrssi, float calibratedRssi, float pathLossParameter) {
        float distance = (float) Math.pow(10, (calibratedRssi - meanrssi) / (10 * pathLossParameter));
        return (Double) Math.pow(10, (calibratedRssi - meanrssi) / (10 * pathLossParameter));
    }


    @Override
    public void onTerminate() {
        super.onTerminate();

    }

    public void bind(){
        mBeaconManager = BeaconManager.getInstanceForApplication(getApplicationContext());
        mBeaconManager.getBeaconParsers().add(new BeaconParser().
                setBeaconLayout(BeaconParser.EDDYSTONE_UID_LAYOUT));
        mBeaconManager.getBeaconParsers().add(new BeaconParser()
                .setBeaconLayout("m:2-3=0215,i:4-19,i:20-21,i:22-23,p:24-24"));
        mBeaconManager.setBackgroundScanPeriod(60000l);
        mBeaconManager.setBackgroundBetweenScanPeriod(60000l);
        mBeaconManager.setRssiFilterImplClass(ArmaRssiFilter.class);
        mBeaconManager.bind(this);
        try {
            mBeaconManager.updateScanPeriods();
        } catch (RemoteException e) {
            //e.printStackTrace();
            // ignore updation
        }

    }
    public  void terminateScan(){
        mBeaconManager.unbind(this);
        mBeaconManager.removeRangeNotifier(rangeNotifier);
        mBeaconManager.disableForegroundServiceScanning();

    }

    public ArrayList<BeaconLogInfo> getBeaconLogInfoArrayList() {
        return beaconLogInfoArrayList;
    }

    public void setBeaconLogInfoArrayList(ArrayList<BeaconLogInfo> beaconLogInfoArrayList) {
        this.beaconLogInfoArrayList = beaconLogInfoArrayList;
    }


}
