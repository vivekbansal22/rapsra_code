package com.example.gns076.rapsra.adapters;

import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import android.view.View;

import com.example.gns076.rapsra.R;
import com.example.gns076.rapsra.model.User;

import java.util.List;

/**
 * Created by .
 */

public class MyStatusRecyclerAdapter //extends RecyclerView.Adapter<MyStatusRecyclerAdapter.UserViewHolder>
{

    private List<User> listUsers;

    public MyStatusRecyclerAdapter(List<User> listUsers) {
        this.listUsers = listUsers;
    }

    //@Override
   /* public MyStatusRecyclerAdapter onCreateViewHolder(ViewGroup parent, int viewType) {
        // inflating recycler item view
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_user_recycler, parent, false);

        return new MyStatusRecyclerAdapter(itemView);
    }*/

    //@Override
    public void onBindViewHolder(MyStatusRecyclerAdapter holder, int position) {
        //holder.textViewName.setText(listUsers.get(position).getName());
        //holder.textViewEmail.setText(listUsers.get(position).getEmail());
        //holder.textViewPassword.setText(listUsers.get(position).getPassword());
    }
/*

    @Override
    public int getItemCount() {
        Log.v(MyStatusRecyclerAdapter.class.getSimpleName(),""+listUsers.size());
        return listUsers.size();
    }
*/


    /**
     * ViewHolder class
     */
    public class MyStatusViewHolder extends RecyclerView.ViewHolder {

        public AppCompatTextView textViewName;
        public AppCompatTextView textViewEmail;
        public AppCompatTextView textViewPassword;

        public MyStatusViewHolder(View view) {
            super(view);
            textViewName = (AppCompatTextView) view.findViewById(R.id.textViewName);
            textViewEmail = (AppCompatTextView) view.findViewById(R.id.textViewEmail);
            textViewPassword = (AppCompatTextView) view.findViewById(R.id.textViewPassword);
        }
    }


}
