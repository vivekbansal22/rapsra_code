package com.example.gns076.rapsra.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.gns076.rapsra.R;
//import com.example.gns076.rapsra.activities.OnItemClickListener;
import com.example.gns076.rapsra.model.AppPreference;
import com.example.gns076.rapsra.model.ProspectFollowDataModel;

import java.util.ArrayList;


//public class ProspectDataListAdapter extends ArrayAdapter<ProspectFollowDataModel> {
public class ProspectDataListAdapter extends RecyclerView.Adapter<ProspectDataListAdapter.ProspectDataHolder> {
    private AppPreference mAppPreference;
    private static final String TAG= "ProspectDataListAdapter";
    private Context mcContext;
    private LayoutInflater mInflater;
    private final OnItemClickListener listener;
    private ArrayList<ProspectFollowDataModel> prospectFollowDataModelsList ;
    public interface OnItemClickListener {
        void onItemClick(ProspectFollowDataModel item);
    }

    public ProspectDataListAdapter(@NonNull Context context, int resource, @NonNull ArrayList<ProspectFollowDataModel> prospectFollowDataModelsList,OnItemClickListener listener) {
        //    super(context, resource, objects);
        this.mcContext = context;
        mInflater = LayoutInflater.from(mcContext);
        this.prospectFollowDataModelsList = prospectFollowDataModelsList;
        mAppPreference = new AppPreference(context);
        this.listener = listener;
    }

    public void setProspectFollowDataModelsList(ArrayList<ProspectFollowDataModel> prospectFollowDataModelsList) {
        this.prospectFollowDataModelsList = prospectFollowDataModelsList;
    }

/*
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView,@NonNull ViewGroup parent) {
        ProspectFollowDataModel prospectFollowDataModel = getItem(position);
        View view = convertView;
        if(view == null){
            view = mInflater.inflate(R.layout.list_prospects_data,parent,false);
        }
         TextView fullName = view.findViewById(R.id.fullName);
        TextView phoneNo = view.findViewById(R.id.phoneNo);
        TextView prospectsNotes = view.findViewById(R.id.prospect_notes);
   //     TextView followUpDate = view.findViewById(R.id.follow_up_date);
        TextView emailId = view.findViewById(R.id.prospectsCalendarEmailId);
        fullName.setText(prospectFollowDataModel.getFirst_name()+prospectFollowDataModel.getLast_name());
        phoneNo.setText(prospectFollowDataModel.getPhoneNo());
        prospectsNotes.setText(prospectFollowDataModel.getDateNotes());
        emailId.setText(prospectFollowDataModel.getEmail());
     //   followUpDate.setText(prospectFollowDataModel.followDate);

        return view;

    }*/

    @NonNull
    @Override
    public ProspectDataHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout, parent, false);

        return new ProspectDataHolder(itemView);
    }


    @Override
    public int getItemCount() {
        return prospectFollowDataModelsList.size();
    }

    @Override
    public void onBindViewHolder(@NonNull ProspectDataHolder holder, int position) {
        // super.onBindViewHolder(holder, position);
        holder.fullName.setText("Name :"+prospectFollowDataModelsList.get(position).getFirst_name()+" "+prospectFollowDataModelsList.get(position).getLast_name());
        holder.emailId.setText("EmailId :"+prospectFollowDataModelsList.get(position).getEmail());

        holder.phoneNo.setText("PhoneNo :"+prospectFollowDataModelsList.get(position).getPhoneNo());
        holder.prospectCity.setText("City :"+ prospectFollowDataModelsList.get(position).getCity() );
        holder.prospectState.setText("State :" + prospectFollowDataModelsList.get(position).getState());
        holder.bind(prospectFollowDataModelsList.get(position),listener);
    }


    class ProspectDataHolder extends RecyclerView.ViewHolder{
        private TextView fullName;
        private TextView phoneNo;

        private TextView emailId;
        private TextView prospectState;
        private TextView prospectCity;

        public ProspectDataHolder(@NonNull View itemView) {
            super(itemView);
            fullName = itemView.findViewById(R.id.prospectFullName);
            phoneNo = itemView.findViewById(R.id.prospectPhoneNo);

            //     TextView followUpDate = view.findViewById(R.id.follow_up_date);
            emailId = itemView.findViewById(R.id.prospectsCalendarEmailId);
            prospectState = itemView.findViewById(R.id.prospectDataCity);
            prospectCity = itemView.findViewById(R.id.prospectDataState);

        }

        public void bind(final ProspectFollowDataModel item, final OnItemClickListener listener) {

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {
                    listener.onItemClick(item);
                }
            });
        }
    }

}
