package com.example.gns076.rapsra.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.RelativeSizeSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.example.gns076.rapsra.R;

import com.example.gns076.rapsra.model.AppPreference;
import com.example.gns076.rapsra.model.ChatAgentModel;
import com.example.gns076.rapsra.model.ChatApiResponse;
import com.example.gns076.rapsra.model.Message;
import com.example.gns076.rapsra.model.MessageParcelable;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;


import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChatActivity extends BaseActivity implements View.OnClickListener
{
    public APIInterface apiInterface;
    private AppPreference mAppPreference;
    private FirebaseAuth mFirebaseAuth;
    private Button btnSend;
    private EditText edtMessage;
    private RecyclerView rvMessage;
    private Date msgDate;
    DateFormat dateFormat;
    String formattedDate;

    Button menuButton;
    TextView newsCrawler;
    // TextView currentDate;
    private EditText etMessage;
    private Button btSend;
    private RecyclerView rvChat;
    private ArrayList<MessageParcelable> mMessages;
    private Button virtualAssist;


    private FirebaseDatabase mFirebaseDatabase;
    private DatabaseReference mDatabaseReference;

    private     String email  = "gns@gns.com";
    private   String password  = "gns123";

    private  ArrayList<ChatAgentModel> chatAgentData;

    private FirebaseRecyclerAdapter<Message, MessageItemViewHolder> adapter;
    int msgposition=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_group_chat_x);
        initializeVariables();
        //  etMessage.setSelection(2);

        //  System.out.println(" Store Id :"+ mAppPreference.getStoreId());
        mFirebaseAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {

                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()){

                            Toast.makeText(ChatActivity.this, "Chat",Toast.LENGTH_SHORT).show();

                        }else {
                            Toast.makeText(ChatActivity.this, "Chat",Toast.LENGTH_SHORT).show();
                        }
                    }
                });

        // Setting the LayoutManager.
        final LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        layoutManager.scrollToPosition(0);

        //Set LayoutManager to RecyclerView
        rvChat.setLayoutManager(layoutManager);


        mFirebaseDatabase = FirebaseDatabase.getInstance();
        mDatabaseReference = mFirebaseDatabase.getReference();


        adapter = new FirebaseRecyclerAdapter<Message, MessageItemViewHolder>(
                Message.class,
                R.layout.rap_chat,
                MessageItemViewHolder.class,
                //mDatabaseReference.child("chat")
                mDatabaseReference.child("chat_dealer"+mAppPreference.getDealerId())
        ) {


            @Override
            protected void populateViewHolder(final MessageItemViewHolder viewHolder,final Message model, int position) {

                if(chatAgentData == null){

                    try {
                        Call<ChatApiResponse> call1 = apiInterface.getChatData(mAppPreference.getDealerId());


                        call1.enqueue(new Callback<ChatApiResponse>() {
                            @Override
                            public void onResponse(Call<ChatApiResponse> call, Response<ChatApiResponse> response) {

                                ChatApiResponse res=response.body();
                                chatAgentData = res.getChatAgentModelArrayList();

                                if (!res.isError()) {

                                    String userStatus = null;
                                    for(ChatAgentModel chatAgentModel:chatAgentData){
                                        if(chatAgentModel.getSenderId().trim().equalsIgnoreCase(model.getSenderId().trim())){
                                            userStatus = chatAgentModel.getSenderStatus();
                                            break;
                                        }
                                    }
                                    if(userStatus == null){
                                        userStatus = "Off";
                                    }

                                    final boolean isMe = model.getSenderId().trim().equalsIgnoreCase(mAppPreference.getAgentId().trim());
                                    // Show-hide image based on the logged-in user.
                                    // Display the profile image to the right for our user, left for other users.
                                    if (isMe) {
                                        viewHolder.body.setGravity(Gravity.CENTER_VERTICAL | Gravity.LEFT);

                                        //  viewHolder.tvmsglayoutcontent.setBackgroundColor(getResources().getColor(R.color.colorForChatSender));
                                        viewHolder.tvmsglayoutcontent.setBackgroundResource(R.drawable.rounded_rectangle_sender_chat);

                                        viewHolder.tvmsglayoutcontent.setGravity(Gravity.RIGHT);
                                        viewHolder.tvmsglayoutmain.setGravity(Gravity.RIGHT);
                                        viewHolder.tvmsglayout.setGravity(Gravity.RIGHT);

                                        viewHolder.body.setText(model.getMessage());

                                        viewHolder.msgTime.setText(model.getMsgtime());
                                        //    viewHolder.body.setTextColor(getResources().getColor(R.color.colorYellow));

                                        viewHolder.rightUser.setTextColor(Color.WHITE);
                                        viewHolder.rightUser.setVisibility(View.VISIBLE);
                                        int index = model.getSendermailId().indexOf("@");
                                        viewHolder.rightUser.setText(model.getSendermailId().substring(0,index).toUpperCase());

                                        viewHolder.leftUser.setVisibility(View.INVISIBLE);
                                        viewHolder.usericon1.setVisibility(View.INVISIBLE);

                                        viewHolder.usericon2.setTextColor(Color.WHITE);
                                        viewHolder.usericon2.setBackgroundResource(getIconResource(userStatus.trim()));
                                        viewHolder.usericon2.setText(model.getSendermailId().substring(0,2).toUpperCase());
                                        viewHolder.usericon2.setVisibility(View.VISIBLE);//commented to make red and blue dots invisible




                                    } else {
                                        viewHolder.body.setGravity(Gravity.CENTER_VERTICAL | Gravity.LEFT);

                                        viewHolder.tvmsglayoutcontent.setBackgroundResource(R.drawable.rounded_rectangle_other_chat);

                                        //    viewHolder.tvmsglayoutcontent.setBackgroundColor(getResources().getColor(R.color.colorForChatOthers));
                                        viewHolder.tvmsglayoutcontent.setGravity(Gravity.LEFT);
                                        viewHolder.tvmsglayoutmain.setGravity(Gravity.LEFT);
                                        viewHolder.tvmsglayout.setGravity(Gravity.LEFT);


                                        viewHolder.body.setText(model.getMessage());
                                        viewHolder.msgTime.setText(model.getMsgtime());
                                        //      viewHolder.body.setTextColor(getResources().getColor(R.color.colorYellow));


                                        int index = model.getSendermailId().indexOf("@");

                                        viewHolder.leftUser.setText(model.getSendermailId().substring(0,index).toUpperCase());
                                        viewHolder.leftUser.setTextColor(Color.WHITE);
                                        viewHolder.leftUser.setVisibility(View.VISIBLE);

                                        viewHolder.usericon1.setTextColor(Color.WHITE);
                                        viewHolder.usericon1.setBackgroundResource(getIconResource(userStatus.trim()));
                                        viewHolder.usericon1.setVisibility(View.VISIBLE);
                                        viewHolder.usericon1. setText(model.getSendermailId().substring(0,2).toUpperCase());

                                        viewHolder.usericon2.setVisibility(View.INVISIBLE);
                                        viewHolder.rightUser.setVisibility(View.INVISIBLE);



                                    }


                                    msgposition++;
                                    setposition(msgposition);

                                } else {

                                }
                            }

                            @Override
                            public void onFailure(Call<ChatApiResponse> call, Throwable t) {
                                call.cancel();
                            }
                        });
                    }catch (Exception e){
                        e.printStackTrace();
                    }

                }
                else {
                    final boolean isMe = model.getSenderId().trim().equalsIgnoreCase(mAppPreference.getAgentId().trim());
                    // Show-hide image based on the logged-in user.
                    // Display the profile image to the right for our user, left for other users.
                    String userStatus = null;
                    for(ChatAgentModel chatAgentModel:chatAgentData){
                        if(chatAgentModel.getSenderId().trim().equalsIgnoreCase(model.getSenderId().trim())){
                            userStatus = chatAgentModel.getSenderStatus();
                            break;
                        }
                    }
                    if(userStatus == null){
                        userStatus = "Off";
                    }
                    if (isMe) {

                        viewHolder.body.setGravity(Gravity.CENTER_VERTICAL | Gravity.LEFT);

                        //  viewHolder.tvmsglayoutcontent.setBackgroundColor(getResources().getColor(R.color.colorForChatSender));
                        viewHolder.tvmsglayoutcontent.setBackgroundResource(R.drawable.rounded_rectangle_sender_chat);

                        viewHolder.tvmsglayoutcontent.setGravity(Gravity.RIGHT);
                        viewHolder.tvmsglayoutmain.setGravity(Gravity.RIGHT);
                        viewHolder.tvmsglayout.setGravity(Gravity.RIGHT);

                        viewHolder.body.setText(model.getMessage());

                        viewHolder.msgTime.setText(model.getMsgtime());
                        //   viewHolder.body.setTextColor(Color.BLACK);

                        viewHolder.rightUser.setTextColor(Color.WHITE);
                        viewHolder.rightUser.setVisibility(View.VISIBLE);
                        int index = model.getSendermailId().indexOf("@");
                        viewHolder.rightUser.setText(model.getSendermailId().substring(0,index).toUpperCase());

                        viewHolder.leftUser.setVisibility(View.INVISIBLE);
                        viewHolder.usericon1.setVisibility(View.INVISIBLE);

                        viewHolder.usericon2.setTextColor(Color.WHITE);
                        viewHolder.usericon2.setBackgroundResource(getIconResource(userStatus.trim()));
                        viewHolder.usericon2.setVisibility(View.VISIBLE);//
                        viewHolder.usericon2.setText(model.getSendermailId().substring(0,2).toUpperCase());



                    } else {
                        viewHolder.body.setGravity(Gravity.CENTER_VERTICAL | Gravity.LEFT);

                        viewHolder.tvmsglayoutcontent.setBackgroundResource(R.drawable.rounded_rectangle_other_chat);

                        //    viewHolder.tvmsglayoutcontent.setBackgroundColor(getResources().getColor(R.color.colorForChatOthers));

                        viewHolder.tvmsglayoutcontent.setGravity(Gravity.LEFT);
                        viewHolder.tvmsglayoutmain.setGravity(Gravity.LEFT);
                        viewHolder.tvmsglayout.setGravity(Gravity.LEFT);

                        viewHolder.body.setText(model.getMessage());
                        viewHolder.msgTime.setText(model.getMsgtime());
                        //      viewHolder.body.setTextColor(Color.BLACK);


                        int index = model.getSendermailId().indexOf("@");
                        viewHolder.leftUser.setText(model.getSendermailId().substring(0,index).toUpperCase());
                        viewHolder.leftUser.setTextColor(Color.WHITE);
                        viewHolder.leftUser.setVisibility(View.VISIBLE);

                        viewHolder.usericon1.setVisibility(View.VISIBLE);
                        viewHolder.usericon1.setTextColor(Color.WHITE);
                        viewHolder.usericon1.setText(model.getSendermailId().substring(0,2).toUpperCase());
                        viewHolder.usericon1.setBackgroundResource(getIconResource(userStatus.trim()));

                        viewHolder.usericon2.setVisibility(View.INVISIBLE);
                        viewHolder.rightUser.setVisibility(View.INVISIBLE);

                        //       viewHolder.tvUser.setTextColor(Color.WHITE);




                    }


                    msgposition++;
                    setposition(msgposition);
                }// main else

            }

        };
        rvChat.setAdapter(adapter);
        menuButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Creating the instance of PopupMenu
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);


                PopupMenu popup = new PopupMenu(ChatActivity.this, menuButton);
                //Inflating the Popup using xml file

                menuButton.setBackgroundResource(R.drawable.exit_menu);

                Menu gamesMenu =popup.getMenu();


                popup.getMenuInflater()
                        .inflate(R.menu.menu_home, gamesMenu);
                gamesMenu.removeItem(R.id.chat_menu);

                gamesMenu.removeItem(R.id.logout);
                gamesMenu.removeItem(R.id.add_edit_so);
                gamesMenu.removeItem(R.id.scheduled_followups);

                for(int i = 0; i < gamesMenu.size(); i++) {
                    MenuItem item = gamesMenu.getItem(i);


                    SpannableString spanString = new SpannableString(gamesMenu.getItem(i).getTitle().toString());
                    int end = spanString.length();

                    //   spanString.setSpan(new RelativeSizeSpan(1.5f), 0, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    spanString.setSpan(new RelativeSizeSpan(0.04f), 0, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    item.setTitle(spanString);

                }


                popup.setOnDismissListener(new PopupMenu.OnDismissListener() {
                    @Override
                    public void onDismiss(PopupMenu popupMenu) {
                        menuButton.setBackgroundResource(R.drawable.menu);
                    }
                });


                //registering popup with OnMenuItemClickListener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {

                        Intent intentButtonClick;
                        switch (item.getItemId()){

                            case R.id.main_page:
                                intentButtonClick = new Intent(getApplicationContext(), SalesRadarActivityNew.class);
                                startActivity(intentButtonClick);
                                break;
/*
                            case R.id.add_edit_so:
                                intentButtonClick = new Intent(getApplicationContext(), AddEditSoActivity.class);
                                startActivity(intentButtonClick);
                                break;*/

                            case R.id.add_edit_prospect:
                                intentButtonClick = new Intent(getApplicationContext(), ProspectorActivity.class);
                                intentButtonClick.putExtra("Token",mAppPreference.getToken() );
                                startActivity(intentButtonClick);
                                break;

                            case R.id.rap_gamify:
                                intentButtonClick = new Intent(getApplicationContext(), GamificationActivity.class);
                                startActivity(intentButtonClick);
                                break;
                            case R.id.my_rank:
                                intentButtonClick = new Intent(getApplicationContext(), WrittenActivity.class);
                                startActivity(intentButtonClick);
                                break;
                            case R.id.fun_games:
                                intentButtonClick = new Intent(getApplicationContext(), FunGames.class);
                                startActivity(intentButtonClick);
                                break;

                            case R.id.ar_mode:
                                intentButtonClick = new Intent(getApplicationContext(), ArViewActivityNew.class);
                                startActivity(intentButtonClick);
                                break;

                            case R.id.floor_map_mode:
                                intentButtonClick = new Intent(getApplicationContext(), FloorActivity.class);
                                startActivity(intentButtonClick);
                                break;

                        }
                        return true;
                    }
                });

                popup.show(); //showing popup menu


            }
        }); //closi


    }
    public int getIconResource(String statusName){
        int statusResource = 0;

        switch(statusName.toString()){
            case "Available":
                statusResource =  R.drawable.chaticonavailable;
                break;

            case "New Client":
                statusResource = R.drawable.chaticonnewclient;
                break;

            case "Existing Client":
                statusResource = R.drawable.chaticonexistingclient;
                break;

            case "Meeting":
                statusResource = R.drawable.chaticonmeeting;
                break;

            case "Training":
                statusResource = R.drawable.chaticontraining;
                break;

            case "Non Selling Activity":
                statusResource = R.drawable.chaticonnonsellingactivity;
                break;

            case "On Break":
                statusResource = R.drawable.chaticononbreak;

                break;

            case "Off":
                statusResource = R.drawable.chaticonoff;
                break;

        }


        return statusResource;
    }
    public void initializeVariables(){

        apiInterface = APIClient.getClient().create(APIInterface.class);
        mAppPreference = new AppPreference(this);
        mFirebaseAuth = FirebaseAuth.getInstance();
        etMessage = (EditText) findViewById(R.id.etMessage);
        btSend = (Button) findViewById(R.id.btSend);
        btSend.setOnClickListener(this);
        rvChat = (RecyclerView) findViewById(R.id.rvChat);
        virtualAssist = (Button)findViewById(R.id.chatVirtualAssist);
        virtualAssist.setOnClickListener(this);

        Calendar cal = Calendar. getInstance();
        msgDate=cal.getTime();
        String strDateFormat = "hh:mm aaa";
        dateFormat = new SimpleDateFormat(strDateFormat);
        formattedDate= dateFormat.format(msgDate);

        //  currentDate = findViewById(R.id.currentDateChat);
        SimpleDateFormat sdf = new SimpleDateFormat("MMM d,yyyy");
        //   currentDate.setText(sdf.format(new Date()));

        newsCrawler = findViewById(R.id.chatNewsCrwaler);
        newsCrawler.setText(mAppPreference.getCrawler());
        newsCrawler.setSelected(true);

        menuButton = findViewById(R.id.chatMenuButton);
        menuButton.setOnClickListener(this);
    }

    public void setposition(int position){
        rvChat.smoothScrollToPosition(position);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btSend:
                String message = etMessage.getText().toString().trim();
                if (!TextUtils.isEmpty(message)){
                    Map<String, Object> param = new HashMap<>();
                    param.put("sender", mAppPreference.getUser());
                    param.put("message", message);

                    //        System.out.println("Dealer Id  :"+mAppPreference.getDealerId()+ " Agent Id  :" + mAppPreference.getAgentId());
                    param.put("msgtime", formattedDate);

                    //mDatabaseReference.child("chat").push().setValue(param)
                    //   mDatabaseReference.child("chat_dealer"+mAppPreference.getDealerId()).push().setValue(param)

                    mDatabaseReference.child("chat_dealer"+mAppPreference.getDealerId()).push().setValue(new Message(mAppPreference.getUser().trim(),mAppPreference.getAgentId().trim(),message,formattedDate,mAppPreference.getEmail().trim()))
                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    etMessage.setText("");
                                    if(task.isSuccessful()){
                                        Log.d("SendMessage", "Success"); }else{
                                        Log.d("SendMessage", "failed ");
                                    }
                                }
                            });
                    rvChat.smoothScrollToPosition(rvChat.getAdapter().getItemCount());
                }
                break;


            case R.id.chatVirtualAssist:
                Intent  intentVirtualAssist= new Intent(getApplicationContext(), VirtualAssistantActivity.class);
                startActivity(intentVirtualAssist);
                finish();
                break;
        }

    }

    public static class MessageItemViewHolder extends RecyclerView.ViewHolder
    {

        public TextView leftUser;
        public TextView rightUser;
        public TextView body;
        public TextView msgTime;
        public LinearLayout tvmsglayout;
        public LinearLayout tvmsglayoutcontent;
        public LinearLayout tvmsglayoutmain;
        public TextView usericon1;
        public TextView usericon2;
        //    public ImageView usericon3;
        //  public ImageView usericon4;

        public MessageItemViewHolder(View itemView)
        {
            super(itemView);
            leftUser = (TextView) itemView.findViewById(R.id.leftUserName);
            rightUser = (TextView) itemView.findViewById(R.id.rightUserName);

            body = (TextView) itemView.findViewById(R.id.tvBody);
            //  body.setTextColor(this.getResources().getDrawable(R.color.transprentColor));
            msgTime=(TextView) itemView.findViewById(R.id.tvTime);
            tvmsglayoutmain=(LinearLayout) itemView.findViewById(R.id.tvmsglayoutmain);
            tvmsglayoutcontent=(LinearLayout) itemView.findViewById(R.id.tvmsglayoutcontent);
            tvmsglayout=(LinearLayout) itemView.findViewById(R.id.tvmsglayout);
            usericon1 =( TextView)itemView.findViewById(R.id.usericon1);
            usericon2 = (TextView)itemView.findViewById(R.id.usericon2);

        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater= getMenuInflater();
        inflater.inflate(R.menu.menu_home,menu);
        menu.removeItem(R.id.chat_menu);
        menu.removeItem(R.id.logout);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        return super.onOptionsItemSelected(item);
    }

}
