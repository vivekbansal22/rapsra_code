package com.example.gns076.rapsra.activities;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;

import com.example.gns076.rapsra.model.Message;
import com.firebase.ui.database.FirebaseListAdapter;
import com.google.firebase.database.Query;

public class MessageAdapter extends FirebaseListAdapter<Message> {
    public MessageAdapter(Activity activity, Class<Message> modelClass, int modelLayout, Query ref) {
        super(activity, modelClass, modelLayout, ref);
    }

    @Override
    protected void populateView(View v, Message model, int position) {

    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        return super.getView(position, view, viewGroup);
    }
}
