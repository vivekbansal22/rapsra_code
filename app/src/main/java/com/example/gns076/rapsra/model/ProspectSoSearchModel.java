package com.example.gns076.rapsra.model;

import com.google.gson.annotations.SerializedName;

public class ProspectSoSearchModel implements  java.io.Serializable{

    @SerializedName("id")
    private String id;
    @SerializedName("sales_order")
    private String sales_order_no;
    @SerializedName("first_name")
    private String first_name;
    @SerializedName("last_name")
    private String last_name;
    @SerializedName("email")
    private String email;
    @SerializedName("phone")
    private String phNo;
    @SerializedName("street1")
    private String address;
    @SerializedName("street2")
    private String unit;
    @SerializedName("zip")
    private String zip;
    @SerializedName("dealer_id")
    private String dealer_id;
    @SerializedName("city")
    private String city;
    @SerializedName("state")
    private String state;
    @SerializedName("store_id")
    private String store_id;
    @SerializedName("agent_id")
    private String agent_id;
    @SerializedName("notes")
    private String notes;
    @SerializedName("merchant_category")
    private String merchant_category;
    @SerializedName("next_step")
    private String next_step;
    @SerializedName("type")
    private String type;
    @SerializedName("created_at")
    private String created_at;
    @SerializedName("updated_at")
    private String updated_at;


    public ProspectSoSearchModel() {
    }

    public ProspectSoSearchModel(String first_name, String email, String phNo) {
        this.first_name = first_name;
        this.email = email;
        this.phNo = phNo;
    }

    public String getId() {
        return id;
    }

    public String getCreated_at() {
        return created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getSales_order_no() {
        return sales_order_no;
    }

    public void setSales_order_no(String sales_order_no) {
        this.sales_order_no = sales_order_no;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhNo() {
        return phNo;
    }

    public void setPhNo(String phNo) {
        this.phNo = phNo;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getDealer_id() {
        return dealer_id;
    }

    public void setDealer_id(String dealer_id) {
        this.dealer_id = dealer_id;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getStore_id() {
        return store_id;
    }

    public void setStore_id(String store_id) {
        this.store_id = store_id;
    }

    public String getAgent_id() {
        return agent_id;
    }

    public void setAgent_id(String agent_id) {
        this.agent_id = agent_id;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getMerchant_category() {
        return merchant_category;
    }

    public void setMerchant_category(String merchant_category) {
        this.merchant_category = merchant_category;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getNext_step() {
        return next_step;
    }

    public void setNext_step(String next_step) {
        this.next_step = next_step;
    }
}
