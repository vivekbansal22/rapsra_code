package com.example.gns076.rapsra.model;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class CustomBeaconList<BeaconInfo> extends ArrayList<BeaconInfo> {

    private int maxSize;
    private List<BeaconInfo> list ;

    public CustomBeaconList(int maxSize) {
        this.maxSize = maxSize;
    }

    /*@Override
    public boolean add(BeaconInfoTemp e) {
        super.addFirst(e);
        if (size() > maxSize){
            removeLast();
        }
        return true;
    }*/

    @Override
    public boolean add(BeaconInfo beaconInfo) {

        if(size()>maxSize){
            super.remove(0);
        }

       super.add(beaconInfo);
        return true;
    }
}
