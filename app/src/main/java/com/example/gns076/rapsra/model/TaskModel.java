package com.example.gns076.rapsra.model;

import java.io.Serializable;

public class TaskModel implements Serializable {

    private int taskId;

    private String taskName;

    private String taskDate;

    private String taskAction;

    private String taskContactMethod;

    private String taskNotes;

    public int getTaskId() {
        return taskId;
    }

    public void setTaskId(int taskId) {
        this.taskId = taskId;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public String getTaskDate() {
        return taskDate;
    }

    public void setTaskDate(String taskDate) {
        this.taskDate = taskDate;
    }

    public String getTaskAction() {
        return taskAction;
    }

    public void setTaskAction(String taskAction) {
        this.taskAction = taskAction;
    }

    public String getTaskContactMethod() {
        return taskContactMethod;
    }

    public void setTaskContactMethod(String taskContactMethod) {
        this.taskContactMethod = taskContactMethod;
    }

    public String getTaskNotes() {
        return taskNotes;
    }

    public void setTaskNotes(String taskNotes) {
        this.taskNotes = taskNotes;
    }
}
