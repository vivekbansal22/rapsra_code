package com.example.gns076.rapsra.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class SpinnerStateApiResponse {
    @SerializedName("error")
    private boolean error;
    @SerializedName("message")
    private String message;

    @SerializedName("beacon_list")
    private ArrayList<StateModel> stateSpinnerList;

    public ArrayList<StateModel> getStateSpinnerList() {
        return stateSpinnerList;
    }

    public void setStateSpinnerList(ArrayList<StateModel> stateSpinnerList) {
        this.stateSpinnerList = stateSpinnerList;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }



}
