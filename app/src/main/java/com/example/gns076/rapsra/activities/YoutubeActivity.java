package com.example.gns076.rapsra.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import com.example.gns076.rapsra.model.AppPreference;
import com.google.android.gms.nearby.messages.BleSignal;
import com.google.android.gms.nearby.messages.Distance;
import com.google.android.gms.nearby.messages.Message;
import com.google.android.gms.nearby.messages.MessageListener;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;
import com.example.gns076.rapsra.R;

import java.util.Timer;

public class YoutubeActivity extends YouTubeBaseActivity
{
    private AppPreference mAppPreference;
    YouTubePlayerView youTubePlayerView;
   // Button play;
    YouTubePlayer.OnInitializedListener onInitializedListener;
    private MessageListener mMessageListener;
    private Message mMessage;
    private static final String TAG = YoutubeActivity.class.getSimpleName();
    String onlyId = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_youtube);
        mAppPreference = new AppPreference(this);

        Bundle bundle = this.getIntent().getExtras();
        onlyId = bundle.getString("video");
        youTubePlayerView = (YouTubePlayerView) findViewById(R.id.youtube);
        onInitializedListener = new YouTubePlayer.OnInitializedListener() {
            @Override
            public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b)
            {
                youTubePlayer.loadVideo(onlyId);
                Handler vhandler =new Handler();
                vhandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                      //  Intent intentRegister8 = new Intent(getApplicationContext(), DashboardActivity.class);
                     //   startActivity(intentRegister8);
                    }
                },60000 );
            }

            @Override
            public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {

            }
        };
        youTubePlayerView.initialize(PlayerConfig.API_KEY,onInitializedListener);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        stopService(new Intent(this,BackgroundSubscribeIntentService.class));
    }

    /*private Intent getBackgroundSubscribeServiceIntent() {
        return new Intent(this, BackgroundSubscribeIntentService.class);
    }*/
}