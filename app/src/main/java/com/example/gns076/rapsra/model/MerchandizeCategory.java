package com.example.gns076.rapsra.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class MerchandizeCategory implements Serializable {

    @SerializedName("id")
    private String merchanizeId;

    @SerializedName("merchandize_category_name")
    private String merchandize_Category_Name;

    public String getMerchanizeId() {
        return merchanizeId;
    }

    public void setMerchanizeId(String merchanizeId) {

        this.merchanizeId = merchanizeId;
    }

    public String getMerchandize_category_name() {
        return merchandize_Category_Name;
    }

    public void setMerchandize_category_Name(String merchandize_category_Name) {
        this.merchandize_Category_Name = merchandize_category_Name;
    }
}
