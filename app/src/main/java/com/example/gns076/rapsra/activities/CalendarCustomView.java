package com.example.gns076.rapsra.activities;

import android.content.Context;
import android.content.Intent;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.gns076.rapsra.R;
import com.example.gns076.rapsra.adapters.GridAdapter;
import com.example.gns076.rapsra.model.AppPreference;
import com.example.gns076.rapsra.model.CalendarDataResponse;
import com.example.gns076.rapsra.model.ProspectFollowDataModel;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;



public class CalendarCustomView  extends LinearLayout{
    private static final String TAG = CalendarCustomView.class.getSimpleName();
    private ImageView previousButton, nextButton;
    private TextView currentDate;
    private GridView calendarGridView;
    private Button addEventButton;
    private static final int MAX_CALENDAR_COLUMN = 42;
    private int month, year;
    private SimpleDateFormat formatter = new SimpleDateFormat("MMMM yyyy", Locale.ENGLISH);
    private Calendar cal = Calendar.getInstance(Locale.ENGLISH);
    private Context context;
    private GridAdapter mAdapter;
    private APIInterface apiInterface;
    private AppPreference mAppPreference;
    private  ProspectFollowDataModel followDataModels[];
    // private DatabaseQuery mQuery;
    public CalendarCustomView(Context context) {
        super(context);
    }
    public CalendarCustomView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        initializeUILayout();

        setPreviousButtonClickEvent();
        setNextButtonClickEvent();
        setGridCellClickEvents();
        mAppPreference = new AppPreference(context);
        //Added code
        apiInterface = APIClient.getClient().create(APIInterface.class);
        try {

            Call<CalendarDataResponse> call2 = apiInterface.getCalendarData(mAppPreference.getAgentId(), mAppPreference.getDealerId(),String.valueOf(cal.get(Calendar.MONTH )+1), String.valueOf(cal.get(Calendar.YEAR)));

            call2.enqueue(new Callback<CalendarDataResponse>() {
                @Override
                public void onResponse(Call<CalendarDataResponse> call, Response<CalendarDataResponse> response) {

                    CalendarDataResponse res = response.body();
                    if (res != null) {
                        followDataModels=res.getObj();

                        setUpCalendarAdapter();
                    }
                    else{
                        setUpCalendarAdapter();
                    }

                }

                @Override
                public void onFailure(Call<CalendarDataResponse> call, Throwable t) {
                    setUpCalendarAdapter();
                    call.cancel();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
        //Added code
        setUpCalendarAdapterForNoData();

        //setUpCalendarAdapter();
        Log.d(TAG, "I need to call this method");
    }
    public CalendarCustomView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
    /**
     *   This method is initialize layout
     */

    private void initializeUILayout(){
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.calendar_layout, this);
        previousButton = (ImageView)view.findViewById(R.id.previous_month);
        nextButton = (ImageView)view.findViewById(R.id.next_month);
        currentDate = (TextView)view.findViewById(R.id.display_current_date);

        calendarGridView = (GridView)view.findViewById(R.id.calendar_grid);
    }

    /*
    * this method is executed OnCLick of back button on calendar
    */
    private void setPreviousButtonClickEvent(){
        previousButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                cal.add(Calendar.MONTH, -1);
                //Added code
                apiInterface = APIClient.getClient().create(APIInterface.class);
                try {

                    Call<CalendarDataResponse> call2 = apiInterface.getCalendarData(mAppPreference.getAgentId(),mAppPreference.getDealerId(),String.valueOf(cal.get(Calendar.MONTH )+1), String.valueOf(cal.get(Calendar.YEAR)));

                    call2.enqueue(new Callback<CalendarDataResponse>() {
                        @Override
                        public void onResponse(Call<CalendarDataResponse> call, Response<CalendarDataResponse> response) {

                            CalendarDataResponse res = response.body();
                            if (res != null) {
                                followDataModels=res.getObj();
                                setUpCalendarAdapter();
                            }
                            else {
                                setUpCalendarAdapter();
                            }

                        }

                        @Override
                        public void onFailure(Call<CalendarDataResponse> call, Throwable t) {
                            setUpCalendarAdapterForNoData();
                            call.cancel();
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
                //Added code

                setUpCalendarAdapterForNoData();
            }
        });
    }
    /*
     * this method is executed OnCLick of next button on calendar
     */
    private void setNextButtonClickEvent(){
        nextButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                cal.add(Calendar.MONTH, 1);

                apiInterface = APIClient.getClient().create(APIInterface.class);
                try {

                    Call<CalendarDataResponse> call2 = apiInterface.getCalendarData(mAppPreference.getAgentId(),mAppPreference.getDealerId(),String.valueOf(cal.get(Calendar.MONTH)+1), String.valueOf(cal.get(Calendar.YEAR)));

                    call2.enqueue(new Callback<CalendarDataResponse>() {
                        @Override
                        public void onResponse(Call<CalendarDataResponse> call, Response<CalendarDataResponse> response) {

                            CalendarDataResponse res = response.body();
                            if (res != null) {
                                followDataModels=res.getObj();

                                setUpCalendarAdapter();
                            }
                            else{
                                setUpCalendarAdapter();
                            }

                        }

                        @Override
                        public void onFailure(Call<CalendarDataResponse> call, Throwable t) {
                            setUpCalendarAdapterForNoData();
                            call.cancel();
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
                //Added code
                setUpCalendarAdapterForNoData();

            }
        });
    }
    private void setGridCellClickEvents() {
        onAttachedToWindow();
    }
    /*
     * this method is executed OnCLick of date
     */
    @Override
    public void onAttachedToWindow(){

        super.onAttachedToWindow();
        calendarGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                TextView prospectName3 = (TextView) view.findViewById(R.id.calendar_date);
                /*   DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");*/

                Intent intentProspectDataActivity = new Intent(context, ProspectsDataActivity.class);

                intentProspectDataActivity.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intentProspectDataActivity.putExtra("Date",prospectName3.getText());
                intentProspectDataActivity.putExtra("monthInString",String.valueOf(new SimpleDateFormat("MMM").format(cal.getTime())));
                String str = new SimpleDateFormat("MM").format(cal.getTime());
                intentProspectDataActivity.putExtra("monthInInt",new SimpleDateFormat("MM").format(cal.getTime()));
                intentProspectDataActivity.putExtra("year",cal.get(Calendar.YEAR));
                //  intentRegister10.putExtra("previousDate",dateFormat.format(cal.getTime()));
                intentProspectDataActivity.putExtra("prospectFollowDataModelArray",followDataModels);
                context.startActivity(intentProspectDataActivity);

            }
        });
    }
    /*
     * this method is used to set up calendar with data
     */
    private void setUpCalendarAdapter(){
        List<Date> dayValueInCells = new ArrayList<Date>();
        //    mQuery = new DatabaseQuery(context);
        //     List<EventObjects> mEvents = mQuery.getAllFutureEvents();
        Calendar mCal = (Calendar)cal.clone();
        mCal.set(Calendar.DAY_OF_MONTH, 1);
        int firstDayOfTheMonth = mCal.get(Calendar.DAY_OF_WEEK) - 1;
        mCal.add(Calendar.DAY_OF_MONTH, -firstDayOfTheMonth);
        while(dayValueInCells.size() < MAX_CALENDAR_COLUMN){
            dayValueInCells.add(mCal.getTime());
            mCal.add(Calendar.DAY_OF_MONTH, 1);
        }
        Log.d(TAG, "Number of date " + dayValueInCells.size());
        String sDate = formatter.format(cal.getTime());
        currentDate.setText(sDate);
        mAdapter = new GridAdapter(context, dayValueInCells, cal,followDataModels);

        calendarGridView.setAdapter(mAdapter);
    }
    /*
     * this method is used to set up calendar with No data
     */
    private void setUpCalendarAdapterForNoData(){
        List<Date> dayValueInCells = new ArrayList<Date>();
        Calendar mCal = (Calendar)cal.clone();
        mCal.set(Calendar.DAY_OF_MONTH, 1);
        int firstDayOfTheMonth = mCal.get(Calendar.DAY_OF_WEEK) - 1;
        mCal.add(Calendar.DAY_OF_MONTH, -firstDayOfTheMonth);
        while(dayValueInCells.size() < MAX_CALENDAR_COLUMN){
            dayValueInCells.add(mCal.getTime());
            mCal.add(Calendar.DAY_OF_MONTH, 1);
        }
        Log.d(TAG, "Number of date " + dayValueInCells.size());
        String sDate = formatter.format(cal.getTime());
        currentDate.setText(sDate);
        mAdapter = new GridAdapter(context, dayValueInCells, cal,null);

        calendarGridView.setAdapter(mAdapter);
    }



}
