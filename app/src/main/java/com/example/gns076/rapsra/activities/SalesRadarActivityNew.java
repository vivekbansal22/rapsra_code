package com.example.gns076.rapsra.activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.example.gns076.rapsra.R;
import com.example.gns076.rapsra.model.AppPreference;
import com.example.gns076.rapsra.model.BeaconLogInfo;
import com.example.gns076.rapsra.model.BeaconLogResponse;
import com.example.gns076.rapsra.model.BeaconModel;
import com.example.gns076.rapsra.model.LogoutResponse;
import com.example.gns076.rapsra.model.MerchandiseBeaconInfoResponse;
import com.example.gns076.rapsra.model.NewsCrawler;
import com.example.gns076.rapsra.sql.DatabaseHelper;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class SalesRadarActivityNew extends BaseActivity  implements  View.OnClickListener {

    private static final String TAG = SalesRadarActivityNew.class.getSimpleName();
    private Button performanceButton;
    private Button prospectorButton;
    private Button gamesButton;
    private Button groupChatButton;
    private Button floorMapButton;
    private Button arModeButton;
    private Button gamificationButton;
    private Button logOut;

    private Button availableButton;
    private Button newClientButton;
    private Button existingClientButton;
    private Button meetingButton;
    private Button trainingButton;
    private Button nonSellingActivityButton;
    private Button onBreakButton;
    private  Button offStatusButton;

    private String[] newsCrawler;
    private String crawler1="";
    private AppPreference mAppPreference;
    private String dealer_id;
    private String store_id;
    private String agent_id;
    private APIInterface apiInterface;
    private TextView marque;
    private String token;
    boolean doubleBackToExitPressedOnce = false;

    private MerchandiseBeaconInfoResponse merchandiseBeaconInfoResponse;
    private List<BeaconModel> beaconInfoList = new ArrayList<BeaconModel>();

    private DatabaseHelper databaseHelper;

    private BeaconScanningApp scanningApp;

   private BeaconLogInfo beaconLogInfoAftersqldatadd = new BeaconLogInfo();

    SimpleDateFormat simpleDateFormat;
   private StringBuilder afterAddingsqldata = new StringBuilder();

    private String logFlag = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sales_radar_x);

        simpleDateFormat = new SimpleDateFormat("YYYY-MM-dd hh:mm:ss");
        intializeObjects();

        int permissionCheck = ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION);
        int permissionCheck1 = ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION);
        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
                Toast.makeText(this, "The permission to get BLE location data is required", Toast.LENGTH_SHORT).show();
            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
            }
        } else {
            Toast.makeText(this, "Location permissions already granted", Toast.LENGTH_SHORT).show();
        }


        if (permissionCheck1 != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_COARSE_LOCATION)) {
                Toast.makeText(this, "The permission to get BLE location data is required", Toast.LENGTH_SHORT).show();
            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, 1);
            }
        } else {
            Toast.makeText(this, "Location permissions already granted", Toast.LENGTH_SHORT).show();
        }

        initializeListeners();
        enableDisableButtons();
        setButtonStatus();
        if(!mAppPreference.getUserStatus().equalsIgnoreCase("Off")){
            offStatusButton.setBackgroundResource(R.drawable.off_status_not_selected);
        }

        Date chosenTime=  Calendar.getInstance().getTime();

        SimpleDateFormat dateFormatter = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss");
        String currentTime = dateFormatter.format(chosenTime);

        try {
            Call<NewsCrawler> callForCrawler = apiInterface.getcrawler(dealer_id,currentTime);
            callForCrawler.enqueue(new Callback<NewsCrawler>() {
                @Override
                public void onResponse(Call<NewsCrawler> call, Response<NewsCrawler> response) {
                    //   System.out.println(response.body().toString());
                    NewsCrawler res=response.body();
                    if (!res.isError()) {
                        newsCrawler=res.getCrawler();
                        for(String s:newsCrawler){
                            crawler1=crawler1.concat("  "+s);
                        }
                        mAppPreference.setCrawler(crawler1);
                        marque.setText(crawler1);
                        marque.setSelected(true);
                    } else {
                        // newsCrawler="NA";

                    }
                }

                @Override
                public void onFailure(Call<NewsCrawler> call, Throwable t) {

                    call.cancel();
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }

        databaseHelper.deleteBeaconInfo();

        beaconLogInfoAftersqldatadd.setAgentID(mAppPreference.getAgentId());
        beaconLogInfoAftersqldatadd.setDistance("0");
        beaconLogInfoAftersqldatadd.setBeaconIdentifier("0");
        Date date = Calendar.getInstance().getTime();
        beaconLogInfoAftersqldatadd.setTimestamp(simpleDateFormat.format(date));




        try {
            Call<MerchandiseBeaconInfoResponse> call2 = apiInterface.getMerchandiseBeaconInfo(mAppPreference.getAgentId());
            call2.enqueue(new Callback<MerchandiseBeaconInfoResponse>() {
                @Override
                public void onResponse(Call<MerchandiseBeaconInfoResponse> call, Response<MerchandiseBeaconInfoResponse> apiResponse) {
                    if(!apiResponse.body().isError()) {
                        // handleResponceFromBeacon(call, apiResponse);
                        merchandiseBeaconInfoResponse = apiResponse.body();
                        beaconInfoList = merchandiseBeaconInfoResponse.getBeaconInfo();

                        mAppPreference.setBeaconLogFlag(merchandiseBeaconInfoResponse.getLogFlag());
                        afterAddingsqldata.append("Login with user::  "+mAppPreference.getUser()+
                                "   and Role: "+mAppPreference.getRole()+ "----------------------------------------------------");

                        afterAddingsqldata.append("\n");
                        afterAddingsqldata.append("Adding SQLite Data for user::");
                        for(BeaconModel beaconModel:beaconInfoList){
                            databaseHelper.addBeaconInfo(beaconModel);
                            afterAddingsqldata.append(" For Beacon ID:"+beaconModel.getBeaconId()+ " With Attachment "
                                    + beaconModel.getBeaconAttachment());
                        }
                        beaconLogInfoAftersqldatadd.setLogString(afterAddingsqldata.toString());
                        if(mAppPreference.getBeaconLogFlag().equalsIgnoreCase("y")){
                            setUserBeaconLogs(beaconLogInfoAftersqldatadd);
                        }
                    }
                }

                @Override
                public void onFailure(Call<MerchandiseBeaconInfoResponse> call, Throwable t) {
                    Toast.makeText(getApplicationContext(), "No Network Connection", Toast.LENGTH_LONG).show();
                    call.cancel();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }



    }
    // method to set status
    public void setButtonStatus(){
        switch (mAppPreference.getUserStatus()){

            case "Available":
                availableButton.setBackgroundResource(R.drawable.available_status_selected);
                break;

            case "New Client":

                newClientButton.setBackgroundResource(R.drawable.new_client_status_selected);
                break;

            case "Off":
                offStatusButton.setBackgroundResource(R.drawable.off_status_selected);
                break;

            case "On Break":
                onBreakButton.setBackgroundResource(R.drawable.on_break_selected);
                break;

            case "Training":
                trainingButton.setBackgroundResource(R.drawable.training_status_selected);
                break;

            case "Non Selling Activity":
                nonSellingActivityButton.setBackgroundResource(R.drawable.non_selling_activity_selectes);
                break;


            case "Meeting":
                meetingButton.setBackgroundResource(R.drawable.meeting_status_selected);

                break;


            case "Existing Client":
                existingClientButton.setBackgroundResource(R.drawable.existing_client_status_selected);
                break;


        }


    }
    //method to initialize Objects
    public void intializeObjects(){
        performanceButton = (Button)findViewById(R.id.performance_button);
        prospectorButton = (Button)findViewById(R.id.prospector_button);
        gamesButton = (Button)findViewById(R.id.games_button);
        groupChatButton = (Button)findViewById(R.id.group_chat_button);
        floorMapButton = (Button)findViewById(R.id.floor_map_button);
        arModeButton = (Button)findViewById(R.id.ar_mode_button);
        gamificationButton = (Button)findViewById(R.id.gamification_button);


        logOut = (Button)findViewById(R.id.log_out_button_sales_radar);
        mAppPreference = new AppPreference(this);
        marque = (TextView)findViewById(R.id.sales_radar_news_crawler_text_view);

        availableButton = (Button)findViewById(R.id.available_status_button);
        newClientButton  = (Button)findViewById(R.id.new_client_button);
        existingClientButton = (Button)findViewById(R.id.existing_client_button);
        meetingButton  = (Button)findViewById(R.id.meeting_button);
        trainingButton = (Button)findViewById(R.id.training_button);
        nonSellingActivityButton = (Button)findViewById(R.id.non_selling_activity_button);
        onBreakButton = (Button)findViewById(R.id.on_break_button);
        offStatusButton = (Button)findViewById(R.id.off_status_button);


        dealer_id=mAppPreference.getDealerId();
        store_id=mAppPreference.getStoreId();
        agent_id=mAppPreference.getAgentId();
        token=mAppPreference.getToken();
        apiInterface = APIClient.getClient().create(APIInterface.class);
        //Added SQlite code for adding Beacon Info
        databaseHelper = new DatabaseHelper(this);

        //Added scanning code after login appliction for RAPSRA proximity changes.
        scanningApp = (BeaconScanningApp)getApplicationContext();
        scanningApp.bind();

    }

    // method to initialize listeners
    public void initializeListeners(){
        performanceButton.setOnClickListener(this);
        prospectorButton.setOnClickListener(this);
        gamesButton.setOnClickListener(this);
        groupChatButton.setOnClickListener(this);
        floorMapButton.setOnClickListener(this);
        arModeButton.setOnClickListener(this);
        gamificationButton.setOnClickListener(this);
        logOut.setOnClickListener(this);

        availableButton.setOnClickListener(this);
        newClientButton.setOnClickListener(this);
        existingClientButton.setOnClickListener(this);
        meetingButton.setOnClickListener(this);
        trainingButton.setOnClickListener(this);
        nonSellingActivityButton.setOnClickListener(this);
        onBreakButton.setOnClickListener(this);
        offStatusButton.setOnClickListener(this);

        availableButton.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                // TODO Auto-generated method stub
                if(!mAppPreference.getUserStatus().equalsIgnoreCase("Available")){
                    changeEarlierStatusBackground();

                    availableButton.setBackgroundResource(R.drawable.available_status_selected);
                    mAppPreference.setUserStatus("Available");
                    enableDisableButtons();
                    setStatus("Available");

                    //     Toast.makeText(getApplicationContext(), "Long click on Available Button" , Toast.LENGTH_LONG).show();
                }
                // Toast.makeText(getApplicationContext(), "Long click on Available Button" , Toast.LENGTH_LONG).show();
                return true;
            }
        });
        newClientButton.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                // TODO Auto-generated method stub

                if(!mAppPreference.getUserStatus().equalsIgnoreCase("New Client")) {
                    changeEarlierStatusBackground();

                    newClientButton.setBackgroundResource(R.drawable.new_client_status_selected);
                    mAppPreference.setUserStatus("New Client");
                    enableDisableButtons();
                    setStatus("New Client");

                    //   Toast.makeText(getApplicationContext(), "Long click on New Client Button" , Toast.LENGTH_LONG).show();
                }
                // Toast.makeText(getApplicationContext(), "Long click on New Client Button" , Toast.LENGTH_LONG).show();
                return true;
            }
        });

        existingClientButton.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                // TODO Auto-generated method stub

                if(!mAppPreference.getUserStatus().equalsIgnoreCase("Existing Client")) {
                    changeEarlierStatusBackground();

                    existingClientButton.setBackgroundResource(R.drawable.existing_client_status_selected);
                    mAppPreference.setUserStatus("Existing Client");
                    enableDisableButtons();
                    setStatus("Existing Client");

                    // Toast.makeText(getApplicationContext(), "Long click on Existing Client Button" , Toast.LENGTH_LONG).show();
                }
                return true;
            }
        });
        meetingButton.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                // TODO Auto-generated method stub

                if(!mAppPreference.getUserStatus().equalsIgnoreCase("Meeting")) {
                    changeEarlierStatusBackground();
                    meetingButton.setBackgroundResource(R.drawable.meeting_status_selected);
                    mAppPreference.setUserStatus("Meeting");
                    enableDisableButtons();
                    setStatus("Meeting");

                    // Toast.makeText(getApplicationContext(), "Long click on Meeting Button", Toast.LENGTH_LONG).show();
                }
                return true;
            }
        });

        trainingButton.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                // TODO Auto-generated method stub

                if(!mAppPreference.getUserStatus().equalsIgnoreCase("Training")) {
                    changeEarlierStatusBackground();

                    trainingButton.setBackgroundResource(R.drawable.training_status_selected);
                    mAppPreference.setUserStatus("Training");
                    enableDisableButtons();
                    setStatus("Training");

                    //Toast.makeText(getApplicationContext(), "Long click on Training Button", Toast.LENGTH_LONG).show();
                }
                return true;
            }
        });

        nonSellingActivityButton.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                // TODO Auto-generated method stub

                if(!mAppPreference.getUserStatus().equalsIgnoreCase("Non Selling Activity")) {

                    changeEarlierStatusBackground();
                    nonSellingActivityButton.setBackgroundResource(R.drawable.non_selling_activity_selectes);
                    mAppPreference.setUserStatus("Non Selling Activity");
                    enableDisableButtons();
                    setStatus("Non Selling Activity");

                    //Toast.makeText(getApplicationContext(), "Long click on Non Selling Button", Toast.LENGTH_LONG).show();
                }
                return true;
            }
        });

        onBreakButton.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                // TODO Auto-generated method stub
                if(!mAppPreference.getUserStatus().equalsIgnoreCase("On Break")) {
                    changeEarlierStatusBackground();
                    onBreakButton.setBackgroundResource(R.drawable.on_break_selected);
                    mAppPreference.setUserStatus("On Break");
                    enableDisableButtons();
                    setStatus("On Break");
                    //Toast.makeText(getApplicationContext(), "Long click on On Break Button", Toast.LENGTH_LONG).show();
                }
                return true;
            }
        });

        offStatusButton.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                // TODO Auto-generated method stub
                if(!mAppPreference.getUserStatus().equalsIgnoreCase("Off")) {
                    changeEarlierStatusBackground();
                    offStatusButton.setBackgroundResource(R.drawable.off_status_selected);
                    mAppPreference.setUserStatus("Off");
                    enableDisableButtons();
                    setStatus("Off");

                    // Toast.makeText(getApplicationContext(), "Long click on Off Status Button", Toast.LENGTH_LONG).show();
                }
                return true;
            }
        });



    }
    //method to set status
    public void setStatus(String userStatus){
        try {

            Call<LogoutResponse> callToSetStatus = apiInterface.setstatus(agent_id,userStatus,dealer_id,store_id);
            callToSetStatus.enqueue(new Callback<LogoutResponse>() {
                @Override
                public void onResponse(Call<LogoutResponse> call, Response<LogoutResponse> response) {
                    //  System.out.println(response.body().toString());
                    LogoutResponse res=response.body();
                    if (!res.isError()) {
                        //  Toast.makeText(getApplicationContext(),"Status Set",Toast.LENGTH_SHORT).show();
                    } else {
                        // newsCrawler="NA";

                    }
                }

                @Override
                public void onFailure(Call<LogoutResponse> call, Throwable t) {
                    Toast.makeText(getApplicationContext(),"Unable To Set Status",Toast.LENGTH_SHORT).show();
                    call.cancel();
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void changeEarlierStatusBackground(){

        switch (mAppPreference.getUserStatus()){

            case "Available":
                availableButton.setBackgroundResource(R.drawable.active_status_not_selected);

                break;

            case "New Client":

                newClientButton.setBackgroundResource(R.drawable.new_client_status_not_selected);
                break;

            case "Off":
                offStatusButton.setBackgroundResource(R.drawable.off_status_not_selected);
                break;

            case "On Break":
                onBreakButton.setBackgroundResource(R.drawable.on_break_status_not_selected);
                break;

            case "Training":
                trainingButton.setBackgroundResource(R.drawable.training_status_not_selected);
                break;

            case "Non Selling Activity":
                nonSellingActivityButton.setBackgroundResource(R.drawable.non_selling_activity_status_not_selected);
                break;


            case "Meeting":
                meetingButton.setBackgroundResource(R.drawable.meeting_status_not_selected);

                break;


            case "Existing Client":
                existingClientButton.setBackgroundResource(R.drawable.existing_client_not_selected);
                break;


        }

    }

    public void enableDisableButtons(){

        if(mAppPreference.getUserStatus().equalsIgnoreCase("Off")){
            performanceButton.setEnabled(false);
            prospectorButton.setEnabled(false);
            gamesButton.setEnabled(false);
            groupChatButton.setEnabled(false);
            floorMapButton.setEnabled(false);
         //   arModeButton.setEnabled(false);
            gamificationButton.setEnabled(false);


        }
        else{
            performanceButton.setEnabled(true);
            prospectorButton.setEnabled(true);
            gamesButton.setEnabled(true);
            groupChatButton.setEnabled(true);
            floorMapButton.setEnabled(true);
            arModeButton.setEnabled(true);
            gamificationButton.setEnabled(true);
        }


    }

    @Override
    public void onClick(View view)
    {
        Intent intentButtonClick;
        switch (view.getId()) {
            case R.id.performance_button:

                intentButtonClick = new Intent(getApplicationContext(), WrittenActivity.class);
                startActivity(intentButtonClick);
                break;

            case R.id.prospector_button:
                intentButtonClick = new Intent(getApplicationContext(), ProspectorActivity.class);
                intentButtonClick.putExtra("Token",mAppPreference.getToken() );
                startActivity(intentButtonClick);
                break;

            case R.id.games_button:
                intentButtonClick = new Intent(getApplicationContext(), FunGames.class);
                startActivity(intentButtonClick);
                break;

            case R.id.group_chat_button:
                intentButtonClick = new Intent(getApplicationContext(), ChatActivity.class);
                startActivity(intentButtonClick);
                break;

            case R.id.floor_map_button:
                intentButtonClick = new Intent(getApplicationContext(), FloorMapActivity.class);
                startActivity(intentButtonClick);
                break;

            case R.id.ar_mode_button:
                intentButtonClick = new Intent(getApplicationContext(), ArViewActivityNew.class);
               /* Bundle bundle = new Bundle();
                bundle.putParcelableArrayList("dataList", (ArrayList<? extends Parcelable>) beaconInfoList);
                intentButtonClick.putExtras(bundle);*/

                startActivity(intentButtonClick);
                break;

            case R.id.gamification_button:
                intentButtonClick = new Intent(getApplicationContext(), GamificationActivity.class);
                startActivity(intentButtonClick);
                break;

            case R.id.log_out_button_sales_radar:

                try {
                    Call<LogoutResponse> call2 = apiInterface.setstatus(agent_id,"Off",dealer_id,store_id);
                    call2.enqueue(new Callback<LogoutResponse>() {
                        @Override
                        public void onResponse(Call<LogoutResponse> call, Response<LogoutResponse> response) {
                            //  System.out.println(response.body().toString());
                            LogoutResponse res=response.body();
                            if (!res.isError()) {
                                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                finish();
                            } else {
                                // newsCrawler="NA";
                                Toast.makeText(getApplicationContext(),"Failed To LogOut",Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<LogoutResponse> call, Throwable t) {
                            Toast.makeText(getApplicationContext(),"Failed To LogOut.Check net Connection",Toast.LENGTH_SHORT).show();
                            call.cancel();
                        }
                    });
                }catch (Exception e){
                    e.printStackTrace();
                }

                databaseHelper.deleteBeaconInfo();

                break;

            // Do something
        }
    }
    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            try {
                Call<LogoutResponse> call2 = apiInterface.setstatus(agent_id,"Off",dealer_id,store_id);
                call2.enqueue(new Callback<LogoutResponse>() {
                    @Override
                    public void onResponse(Call<LogoutResponse> call, Response<LogoutResponse> response) {
                        //  System.out.println(response.body().toString());
                        LogoutResponse res=response.body();
                        if (!res.isError()) {
                            // Toast.makeText(getApplicationContext(),res.getMessage(),Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            finish();
                        } else {
                            // newsCrawler="NA";

                        }
                    }

                    @Override
                    public void onFailure(Call<LogoutResponse> call, Throwable t) {

                        call.cancel();
                    }
                });
            }catch (Exception e){
                e.printStackTrace();
            }
            databaseHelper.deleteBeaconInfo();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to logout.", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);

             scanningApp.terminateScan();
        //  beaconDistancesAdditionApp.onTerminate();
    }



    public void setUserBeaconLogs(BeaconLogInfo beaconLogs){


        //   for(BeaconLogInfo beaconLogInfo:beaconLogInfos){

        try {
            //      Log.i(TAG, "Adding Beacon ID:"+beaconLogInfo.getBeaconIdentifier()+ " with Logs:::::"+ beaconLogInfo.getLogString());
            Call<BeaconLogResponse> call2 = apiInterface.setBeaconlogs(beaconLogs.getBeaconIdentifier(),beaconLogs.getDistance(),beaconLogs.getLogString(),beaconLogs.getAgentID(),beaconLogs.getTimestamp());

            call2.enqueue(new Callback<BeaconLogResponse>() {
                @Override
                public void onResponse(Call<BeaconLogResponse> call, Response<BeaconLogResponse> response) {
                    System.out.println(response.body().toString());
                    BeaconLogResponse res=response.body();
                    if (!res.isError()) {
                        //Toast.makeText(getApplicationContext(), "Beacon Logs added successfully" , Toast.LENGTH_SHORT).show();
                        Log.i(TAG,"Beacon Logs added successfully");
                    } else{
                       // Toast.makeText(getApplicationContext(), "Beacon logs invalid parameters" , Toast.LENGTH_LONG).show();
                        Log.i(TAG,"Beacon logs invalid parameters");

                    }


                }

                @Override
                public void onFailure(Call<BeaconLogResponse> call, Throwable t) {

                    call.cancel();
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }



        //  }
    }


}
