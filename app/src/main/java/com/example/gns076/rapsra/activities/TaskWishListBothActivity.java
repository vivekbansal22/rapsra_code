package com.example.gns076.rapsra.activities;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.RelativeSizeSpan;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.example.gns076.rapsra.R;
import com.example.gns076.rapsra.model.AppPreference;

public class TaskWishListBothActivity extends AppCompatActivity implements View.OnClickListener {

    private AppPreference mAppPreference;

    private Button basketUnselected;
    private Button taskUnselected;
    private Button bothReturn;
    private String token;
    private Button menuButton;
    private Button virtualAssist;

    private Button editButton;

    private TextView wishListPhoneNo;

    private TextView firstLastNameText;

    private String prospectorId;
    private String prospectFirstName;
    private String prospectLastName;
    private String prospectPhNo;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_wishlist_both_x);

        initializeObjects();

        setListeners();

        menuButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Creating the instance of PopupMenu
                PopupMenu popup = new PopupMenu(TaskWishListBothActivity.this, menuButton);
                //Inflating the Popup using xml file

                menuButton.setBackgroundResource(R.drawable.exit_menu);

                Menu gamesMenu =popup.getMenu();
                popup.getMenuInflater()
                        .inflate(R.menu.menu_home, gamesMenu);
                gamesMenu.removeItem(R.id.logout);

                gamesMenu.removeItem(R.id.add_edit_so);
                gamesMenu.removeItem(R.id.scheduled_followups);


                for(int i = 0; i < gamesMenu.size(); i++) {
                    MenuItem item = gamesMenu.getItem(i);


                    SpannableString spanString = new SpannableString(gamesMenu.getItem(i).getTitle().toString());
                    int end = spanString.length();

                    //   spanString.setSpan(new RelativeSizeSpan(1.5f), 0, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    spanString.setSpan(new RelativeSizeSpan(0.04f), 0, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    item.setTitle(spanString);

                }


                popup.setOnDismissListener(new PopupMenu.OnDismissListener() {
                    @Override
                    public void onDismiss(PopupMenu popupMenu) {
                        menuButton.setBackgroundResource(R.drawable.menu);
                    }
                });


                //registering popup with OnMenuItemClickListener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {

                        Intent intentViewScheduledFollowUps;
                        switch (item.getItemId()){

                            case R.id.main_page:
                                intentViewScheduledFollowUps = new Intent(getApplicationContext(), SalesRadarActivityNew.class);
                                startActivity(intentViewScheduledFollowUps);
                                break;

                          /*  case R.id.add_edit_so:
                                intentViewScheduledFollowUps = new Intent(getApplicationContext(), AddEditSoActivity.class);
                                startActivity(intentViewScheduledFollowUps);
                                break;
*/
                            case R.id.add_edit_prospect:
                                intentViewScheduledFollowUps = new Intent(getApplicationContext(), ProspectorActivity.class);
                                intentViewScheduledFollowUps.putExtra("Token",mAppPreference.getToken() );
                                startActivity(intentViewScheduledFollowUps);
                                break;

                            case R.id.rap_gamify:
                                intentViewScheduledFollowUps = new Intent(getApplicationContext(), GamificationActivity.class);
                                startActivity(intentViewScheduledFollowUps);
                                break;
                            case R.id.my_rank:
                                intentViewScheduledFollowUps = new Intent(getApplicationContext(), WrittenActivity.class);
                                startActivity(intentViewScheduledFollowUps);
                                break;
                            case R.id.chat_menu:
                                intentViewScheduledFollowUps = new Intent(getApplicationContext(), ChatActivity.class);
                                startActivity(intentViewScheduledFollowUps);
                                break;

                            case R.id.ar_mode:
                                intentViewScheduledFollowUps = new Intent(getApplicationContext(), ArViewActivityNew.class);
                                startActivity(intentViewScheduledFollowUps);
                                break;

                            case R.id.floor_map_mode:
                                intentViewScheduledFollowUps = new Intent(getApplicationContext(), FloorActivity.class);
                                startActivity(intentViewScheduledFollowUps);
                                break;

                            case R.id.fun_games:
                                intentViewScheduledFollowUps = new Intent(getApplicationContext(), FunGames.class);
                                startActivity(intentViewScheduledFollowUps);
                                break;

                        }
                        return true;
                    }
                });

                popup.show(); //showing popup menu


            }
        });

    }

    public void setListeners() {

        taskUnselected.setOnClickListener(this);
        basketUnselected.setOnClickListener(this);
        bothReturn.setOnClickListener(this);
        virtualAssist.setOnClickListener(this);
        editButton.setOnClickListener(this);
    }

    public void initializeObjects(){
        mAppPreference = new AppPreference(this);
        token=mAppPreference.getToken();


        taskUnselected = (Button) findViewById(R.id.task_unselect_bothPage);
        basketUnselected = (Button) findViewById(R.id.wishlist_Unselect_BothPage);
        bothReturn = (Button)findViewById(R.id.both_return_taskwishlist_page);
        menuButton =(Button)findViewById(R.id.both_Menu_Button);
        virtualAssist = (Button)findViewById(R.id.both_Virtual_assist);

        editButton = (Button)findViewById(R.id.edit_button_taskwishlist_both_page);

        prospectorId =  getIntent().getExtras().getString("prospect_id");
        prospectFirstName =  getIntent().getExtras().getString("prospector_firstname");
        prospectLastName =  getIntent().getExtras().getString("prospector_lastname");
        prospectPhNo =  getIntent().getExtras().getString("prospector_phone");

        wishListPhoneNo = (TextView) findViewById(R.id.phoneno_both_x);
        firstLastNameText = (TextView) findViewById(R.id.firstname_lastname_both_x);
        firstLastNameText.setText(prospectFirstName+" "+prospectLastName);

        wishListPhoneNo.setText(prospectPhNo);
    }
    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.task_unselect_bothPage:
                Intent intentTaskUnselect = new Intent(this, TaskActivity.class);
                intentTaskUnselect.putExtra("Token",mAppPreference.getToken() );

                intentTaskUnselect.putExtra("prospect_id",prospectorId );
                intentTaskUnselect.putExtra("prospector_firstname",prospectFirstName);
                intentTaskUnselect.putExtra("prospector_lastname",prospectLastName);
                intentTaskUnselect.putExtra("prospector_phone",prospectPhNo);
                startActivity(intentTaskUnselect);


                break;

            case R.id.wishlist_Unselect_BothPage:
                Intent intentBothUnselect = new Intent(this, WishListActivity.class);

                intentBothUnselect.putExtra("prospect_id",prospectorId );
                intentBothUnselect.putExtra("prospector_firstname",prospectFirstName);
                intentBothUnselect.putExtra("prospector_lastname",prospectLastName);
                intentBothUnselect.putExtra("prospector_phone",prospectPhNo);
                startActivity(intentBothUnselect);
                break;

            case R.id.both_return_taskwishlist_page: Intent intentProspector = new Intent(getApplicationContext(), ProspectorActivity.class);
                intentProspector.putExtra("Token",token );
                startActivity(intentProspector);
                finish();
                break;

            case R.id.both_Virtual_assist:
                Intent  intentVirtualAssist= new Intent(getApplicationContext(), VirtualAssistantActivity.class);
                startActivity(intentVirtualAssist);
                finish();
                break;

            case R.id.edit_button_taskwishlist_both_page:

                Intent intentTask1 = new Intent(getApplicationContext(), ProspectorActivity.class);
                intentTask1.putExtra("prospect_id",prospectorId );
                startActivity(intentTask1);
                finish();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        Intent intentProspector = new Intent(getApplicationContext(), ProspectorActivity.class);
        intentProspector.putExtra("Token",token );
        startActivity(intentProspector);
        finish();
    }
}
