package com.example.gns076.rapsra.activities;

import android.content.Intent;
import android.net.Uri;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.example.gns076.rapsra.model.AppPreference;

import com.example.gns076.rapsra.R;

public class BaseActivity extends AppCompatActivity {

    private AppPreference mAppPreference;
    private String token;
    public APIInterface apiInterface;
    private String dealer_id;
    private String store_id;
    private String agent_id;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        mAppPreference = new AppPreference(this);
        token=mAppPreference.getToken();
        dealer_id=mAppPreference.getDealerId();
        store_id=mAppPreference.getStoreId();
        agent_id=mAppPreference.getAgentId();
        apiInterface = APIClient.getClient().create(APIInterface.class);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater= getMenuInflater();
        inflater.inflate(R.menu.menu_home,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

         /*   case R.id.myrankweekly:
                Intent intentRegister5 = new Intent(getApplicationContext(), MyStatusWeeklyActivity.class);
                startActivity(intentRegister5);
                break;*/
          /*  case R.id.add_edit_so:
                Intent intentRegister = new Intent(getApplicationContext(), AddEditSoActivity.class);
                intentRegister.putExtra("Token",token );
                startActivity(intentRegister);
                break;
            case R.id.add_edit_prospect:
                Intent intentRegister1 = new Intent(getApplicationContext(), AddEditProspectActivity.class);
                intentRegister1.putExtra("Token",token );
                startActivity(intentRegister1);
                break;*/
            case R.id.rap_gamify:
                Intent intent = new Intent(Intent.ACTION_VIEW);
                String rapURL="http://halphp.raptns.com/sso/"+token;
                System.out.println(rapURL);
                intent.setData(Uri.parse(rapURL));
                startActivity(intent);
           /*     break;
            case R.id.my_rank:
                Intent intentRegister5 = new Intent(getApplicationContext(), MyStatusActivity.class);
                startActivity(intentRegister5);
                break;*/
            case R.id.fun_games:
                Intent intentRegister6 = new Intent(getApplicationContext(), FunGames.class);
                intentRegister6.putExtra("Token",token );
                startActivity(intentRegister6);
                break;
            case R.id.chat_menu:
                Intent intentRegister7 = new Intent(getApplicationContext(), ChatActivity.class);
                intentRegister7.putExtra("Token",token );
                startActivity(intentRegister7);
                break;
            case R.id.ar_mode:
                /*  Intent intentRegister8 = new Intent(getApplicationContext(), NearbyActivity.class);*/
                Intent intentRegister8 = new Intent(getApplicationContext(), CameraNearbyYoutubeActivity.class);
                intentRegister8.putExtra("Token",token );
                startActivity(intentRegister8);
                break;
            case R.id.floor_map_mode:
           /*     Intent intentRegister9 = new Intent(getApplicationContext(), FloorMapActivity.class);
                *//* Intent intentRegister9 = new Intent(getApplicationContext(), CameraNearbyYoutubeActivity.class);*//*
                startActivity(intentRegister9);*/
                break;
            case R.id.scheduled_followups:
                /* Intent intentRegister9 = new Intent(getApplicationContext(), FloorMapActivity.class);*/
                Intent intentRegister10 = new Intent(getApplicationContext(), CustomCalendarActivity.class);
                startActivity(intentRegister10);
                break;
          /*  case R.id.main_page:
                *//* Intent intentRegister9 = new Intent(getApplicationContext(), FloorMapActivity.class);*//*
                Intent intentRegister11 = new Intent(getApplicationContext(), DashboardActivity.class);
                startActivity(intentRegister11);
                break;*/
            default:
                return super.onOptionsItemSelected(item);
        }
        return  true;
    }

}
