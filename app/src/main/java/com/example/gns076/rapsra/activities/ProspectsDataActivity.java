package com.example.gns076.rapsra.activities;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.widget.TextView;

import com.example.gns076.rapsra.R;
import com.example.gns076.rapsra.adapters.ProspectDataListAdapter;
import com.example.gns076.rapsra.model.ProspectFollowDataModel;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class ProspectsDataActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    private String previousDate;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prospects_data);
        recyclerView = findViewById(R.id.calendarProspectDataList);

        String selectedListFromCalendar = getIntent().getExtras().get("Date").toString();

        ArrayList<ProspectFollowDataModel> prospectFollowDataModelsList = new ArrayList<ProspectFollowDataModel>();
        final Calendar c = Calendar.getInstance();
        CharSequence strDate = null;
        int year = (Integer) getIntent().getExtras().get("year");
        String str =  (String) getIntent().getExtras().get("monthInInt");
        int monthOfYear  = Integer.parseInt(str);
        String s = (String) getIntent().getExtras().get("Date");
        int dayOfMonth = Integer.parseInt(s);

        c.set(year,monthOfYear-1,dayOfMonth);
        Date chosenDate =  c.getTime();

        //Date d = new Date(year, monthOfYear, dayOfMonth);
        SimpleDateFormat dateFormatter = new SimpleDateFormat(
                "yyyy-MM-dd");
        strDate = dateFormatter.format(chosenDate);
        previousDate = strDate.toString();


        TextView prospectsDate = (TextView)findViewById(R.id.currentDate);
        prospectsDate.setText(String.valueOf(getIntent().getExtras().get("Date"))+"-"+String.valueOf(getIntent().getExtras().get("monthInString"))+"-"+String.valueOf(getIntent().getExtras().get("year")));
        //   previousDate = String.valueOf(getIntent().getExtras().get("year"))+"-"+String.valueOf(getIntent().getExtras().get("monthInInt"))+"-"+String.valueOf(getIntent().getExtras().get("Date"));


        ProspectDataListAdapter adapter = new ProspectDataListAdapter(this, R.layout.layout, getDataListDate(selectedListFromCalendar), new ProspectDataListAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(ProspectFollowDataModel item) {
                // Toast.makeText(this, "Item Clicked", Toast.LENGTH_LONG).show();
                Intent intentRegister = new Intent(getApplicationContext(), CalendarProspectsUpdateData.class);
                intentRegister.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intentRegister.putExtra("selectedProspect",item);
                intentRegister.putExtra("previousDate",previousDate);
                getApplicationContext().startActivity(intentRegister);
            }
        });



        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        recyclerView.setAdapter(adapter);

    }
    /*
    *  This method is to get events day wise
    * */
    public  ArrayList<ProspectFollowDataModel> getDataListDate(String selectedDate){

        ProspectFollowDataModel followDataModels[]= (ProspectFollowDataModel[])getIntent().getExtras().get("prospectFollowDataModelArray")  ;
        ArrayList<ProspectFollowDataModel> prospectFollowDataModelList = new  ArrayList<ProspectFollowDataModel>() ;
        //     Map<Integer,LinkedList<String>>prospectDateMap = new HashMap<Integer,LinkedList<String>>();
        if(followDataModels != null) {
            for (int i = 0; i < followDataModels.length; i++) {
                String stringDate = followDataModels[i].getFollowDate().substring(8);
                if(Integer.parseInt(stringDate) == Integer.parseInt(selectedDate)){
                    prospectFollowDataModelList.add(followDataModels[i]);
                }
            }

        }
        return prospectFollowDataModelList;
    }
}
