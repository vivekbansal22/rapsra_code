package com.example.gns076.rapsra.model;


import java.util.ArrayList;
import java.util.List;

public class BeaconModelFloorPlan  implements Comparable< BeaconModelFloorPlan >{

    private String beaconId;
    /*  private String beaconRssi;
      private String beaconDistance;*/
    private Double meanDistance;
    private List<BeaconParameters> beaconParametersList;


    public BeaconModelFloorPlan(String beaconId, Double meanDistance) {
        this.beaconId = beaconId;
        this.meanDistance = meanDistance;
    }

    public BeaconModelFloorPlan(String beaconId) {
        this.beaconId = beaconId;
        beaconParametersList = new CustomBeaconList<>(10);
    }

    public String getBeaconId() {
        return beaconId;
    }

    public void setBeaconId(String beaconId) {
        this.beaconId = beaconId;
    }

    public void addParameter(BeaconParameters beaconParameter){
        beaconParametersList.add(beaconParameter);
    }

    public List<BeaconParameters> getBeaconParametersList() {
        return beaconParametersList;
    }

    public void setBeaconParametersList(List<BeaconParameters> beaconParametersList) {
        this.beaconParametersList = beaconParametersList;
    }

    public Double getMeanDistance() {
        return meanDistance;
    }

    public void setMeanDistance(Double meanDistance) {
        this.meanDistance = meanDistance;
    }
    @Override
    public int compareTo(BeaconModelFloorPlan o) {
        return this.getMeanDistance().compareTo(o.getMeanDistance());


    }
}
