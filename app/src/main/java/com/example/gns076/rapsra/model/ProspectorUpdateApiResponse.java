package com.example.gns076.rapsra.model;

import com.google.gson.annotations.SerializedName;

public class ProspectorUpdateApiResponse {
    @SerializedName("error")
    private boolean error;
    @SerializedName("message")
    private String message;
    @SerializedName("data")
    private ProspectorUpdateModel prospectorUpdateModel[];

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ProspectorUpdateModel[] getProspectorUpdateModel() {
        return prospectorUpdateModel;
    }

    public void setProspectorUpdateModel(ProspectorUpdateModel prospectorUpdateModel[]) {
        this.prospectorUpdateModel = prospectorUpdateModel;
    }
}
