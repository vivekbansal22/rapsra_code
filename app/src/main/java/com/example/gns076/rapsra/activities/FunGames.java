package com.example.gns076.rapsra.activities;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.RelativeSizeSpan;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.example.gns076.rapsra.R;
import com.example.gns076.rapsra.model.AppPreference;

public class FunGames extends BaseActivity implements View.OnClickListener {
    //  public class FunGames extends BaseActivity{
    private Button texaHodemButton;
    private Button solitareButton;
    private Button triviaButton;
    private Button candyCrushButton;
    private Button menuButton;
    private TextView newsCrawler;
    private AppPreference mAppPreference;
    private ConstraintLayout gamesLayout;
    private boolean isCross = false;
    private Button virtualAssist;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mAppPreference = new AppPreference(this);
        setContentView(R.layout.activity_games_x);
        texaHodemButton = (Button) findViewById(R.id.gameTexasButton);
        solitareButton = (Button) findViewById(R.id.gameSolitaireButton);
        triviaButton = (Button) findViewById(R.id.gameTriviaButton);
        candyCrushButton = (Button) findViewById(R.id.gameCandyCrashButton);
        gamesLayout = (ConstraintLayout) findViewById(R.id.gamesPageLayout);
        virtualAssist = (Button)findViewById(R.id.gamesVirtualassist);

        menuButton = findViewById(R.id.gamesMenuButton);
        newsCrawler = findViewById(R.id.gameNewsCrawler);
        newsCrawler.setText(mAppPreference.getCrawler());

        newsCrawler.setSelected(true);
        initListeners();

        menuButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Creating the instance of PopupMenu
                PopupMenu popup = new PopupMenu(FunGames.this, menuButton);
                //Inflating the Popup using xml file

                menuButton.setBackgroundResource(R.drawable.exit_menu);
                isCross = true;
                Menu gamesMenu =popup.getMenu();


                popup.getMenuInflater()
                        .inflate(R.menu.menu_home, gamesMenu);
                gamesMenu.removeItem(R.id.fun_games);
                gamesMenu.removeItem(R.id.logout);
                gamesMenu.removeItem(R.id.add_edit_so);
                gamesMenu.removeItem(R.id.scheduled_followups);

                for(int i = 0; i < gamesMenu.size(); i++) {
                    MenuItem item = gamesMenu.getItem(i);


                    SpannableString spanString = new SpannableString(gamesMenu.getItem(i).getTitle().toString());
                    int end = spanString.length();

                    //   spanString.setSpan(new RelativeSizeSpan(1.5f), 0, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    spanString.setSpan(new RelativeSizeSpan(0.04f), 0, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    item.setTitle(spanString);

                }


                popup.setOnDismissListener(new PopupMenu.OnDismissListener() {
                    @Override
                    public void onDismiss(PopupMenu popupMenu) {
                        menuButton.setBackgroundResource(R.drawable.menu);
                    }
                });


                //registering popup with OnMenuItemClickListener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {

                        Intent intentButtonClick;
                        switch (item.getItemId()){

                            case R.id.main_page:
                                intentButtonClick = new Intent(getApplicationContext(), SalesRadarActivityNew.class);
                                startActivity(intentButtonClick);
                                break;

                            /*case R.id.add_edit_so:
                                intentButtonClick = new Intent(getApplicationContext(), AddEditSoActivity.class);
                                startActivity(intentButtonClick);
                                break;*/

                            case R.id.add_edit_prospect:
                                intentButtonClick = new Intent(getApplicationContext(), ProspectorActivity.class);
                                intentButtonClick.putExtra("Token",mAppPreference.getToken() );
                                startActivity(intentButtonClick);
                                break;

                            case R.id.rap_gamify:
                                intentButtonClick = new Intent(getApplicationContext(), GamificationActivity.class);
                                startActivity(intentButtonClick);
                                break;
                            case R.id.my_rank:
                                intentButtonClick = new Intent(getApplicationContext(), WrittenActivity.class);
                                startActivity(intentButtonClick);
                                break;
                            case R.id.chat_menu:
                                intentButtonClick = new Intent(getApplicationContext(), ChatActivity.class);
                                startActivity(intentButtonClick);
                                break;

                            case R.id.ar_mode:
                                intentButtonClick = new Intent(getApplicationContext(), ArViewActivityNew.class);
                                startActivity(intentButtonClick);
                                break;

                            case R.id.floor_map_mode:
                                intentButtonClick = new Intent(getApplicationContext(), FloorActivity.class);
                                startActivity(intentButtonClick);
                                break;

                        }
                        return true;
                    }
                });

                popup.show(); //showing popup menu


            }
        }); //closi

    }

    /*  public void screenTapped(View view) {
          // Your code here
          if(isCross == true){
              isCross = false;
              menuButton.setBackgroundResource(R.drawable.games_menu_button_icon);
          }
      }
  */
    public void startNewActivity(Context context, String packageName) {
        Intent intent = context.getPackageManager().getLaunchIntentForPackage(packageName);
        if (intent == null) {
            // Bring user to the market or let them choose an app?
            intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse("market://details?id=" + packageName));
        }
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater= getMenuInflater();
        inflater.inflate(R.menu.menu_home,menu);
        for(int i = 0; i < menu.size(); i++) {
            MenuItem item = menu.getItem(i);


            SpannableString spanString = new SpannableString(menu.getItem(i).getTitle().toString());
            int end = spanString.length();

            //   spanString.setSpan(new RelativeSizeSpan(1.5f), 0, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            spanString.setSpan(new RelativeSizeSpan(50f), 0, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            item.setTitle(spanString);

        }
        /*    menu.removeItem(R.id.fun_games);*/
        /*    menu.removeItem(R.id.logout);*/
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //    menuButton.setBackgroundResource(R.drawable.games_exit_button_icon);

        return super.onOptionsItemSelected(item);

    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {

        return super.onPrepareOptionsMenu(menu);

    }

    private void initListeners() {
        texaHodemButton.setOnClickListener(this);
        solitareButton.setOnClickListener(this);
        triviaButton.setOnClickListener(this);
        candyCrushButton.setOnClickListener(this);
        menuButton.setOnClickListener(this);
        gamesLayout.setOnClickListener(this);
        virtualAssist.setOnClickListener(this);
    }
    @Override
    public void onClick(View v){
        switch (v.getId()) {
            case R.id.gameTexasButton:
                startNewActivity(getApplicationContext(),"com.geaxgame.gamezone.texas");
                break;
            case R.id.gameSolitaireButton:
                startNewActivity(getApplicationContext(),"com.mobilityware.solitaire");
                break;
            case R.id.gameTriviaButton:
                startNewActivity(getApplicationContext(),"com.etermax.preguntados.lite");
                break;
            case R.id.gameCandyCrashButton:
                startNewActivity(getApplicationContext(),"com.king.candycrushsaga");
                break;
            case R.id.gamesVirtualassist:
                Intent  intentVirtualAssist= new Intent(getApplicationContext(), VirtualAssistantActivity.class);
                startActivity(intentVirtualAssist);
                finish();
                break;

        }
    }



}

