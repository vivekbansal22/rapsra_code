package com.example.gns076.rapsra.model;

import com.google.gson.annotations.SerializedName;

public class CalendarDataResponse {


    @SerializedName("error")
    private boolean error;


    @SerializedName("message")
    private String message;

    @SerializedName("followups")
    private ProspectFollowDataModel obj[];

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ProspectFollowDataModel[] getObj() {
        return obj;
    }

    public void setObj(ProspectFollowDataModel[] obj) {
        this.obj = obj;
    }



}
