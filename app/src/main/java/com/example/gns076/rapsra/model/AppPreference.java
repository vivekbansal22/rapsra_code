package com.example.gns076.rapsra.model;

import android.content.Context;
import android.content.SharedPreferences;


public class AppPreference {

    private SharedPreferences mPreferences;
    private String PREF_NAME = "rapsra_pref";

    private String KEY_EMAIL = "email";
    private String KEY_TOKEN = "token";
    private String KEY_USER = "user";
    private String KEY_DEALER_ID = "dealer_id";
    public String KEY_CRAWLER= "crawler";
    private String KEY_STORE_ID = "store_id";
    private String KEY_AGENT_ID = "agent_id";
    private String KEY_ROLE = "role";
    private String KEY_USER_STATUS = "user_status";
    private String KEY_LOCK_STATUS = "lock_status";

    private String KEY_SHOW_USER_COORDINATES_FLAG = "user_coordinates_flag";
    private String KEY_BEACON_LOG_FLAG = "beacon_log_flag";

    private SharedPreferences.Editor editor;

    public AppPreference(Context mContext){
        mPreferences = mContext.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        editor = mPreferences.edit();
    }


    public void setShowUserCoordinateFlag(String userCoordinateFlag) {
        editor.putString(KEY_SHOW_USER_COORDINATES_FLAG, userCoordinateFlag);
        editor.commit();
    }

    public String getShowUserCoordinateFlag() {
        return  mPreferences.getString(KEY_SHOW_USER_COORDINATES_FLAG, null);
    }


    public void setDealerId(String dealerId){
        editor.putString(KEY_DEALER_ID, dealerId);
        editor.commit();
    }

    public String getDealerId(){

        return  mPreferences.getString(KEY_DEALER_ID, null);
    }
    public String getUserStatus() {
        return  mPreferences.getString(KEY_USER_STATUS, null);
    }

    public void setUserStatus(String userStatus) {
        editor.putString(KEY_USER_STATUS, userStatus);
        editor.commit();
    }

    public String getLockStatus() {
        return  mPreferences.getString(KEY_LOCK_STATUS, null);
    }

    public void setLockStatus(String buttonShade) {
        editor.putString(KEY_LOCK_STATUS, buttonShade);
        editor.commit();
    }

    public String getRole() {
        return  mPreferences.getString(KEY_ROLE, null);
    }

    public void setRole(String role) {
        editor.putString(KEY_ROLE, role);
        editor.commit();
    }
    public void setStoreId(String storeId){
        editor.putString(KEY_STORE_ID, storeId);
        editor.commit();
    }

    public String getStoreId(){

        return  mPreferences.getString(KEY_STORE_ID, null);
    }

    public void setAgentId(String agentId){
        editor.putString(KEY_AGENT_ID, agentId);
        editor.commit();
    }

    public String getAgentId(){

        return  mPreferences.getString(KEY_AGENT_ID, null);
    }

    public void setToken(String token){
        editor.putString(KEY_TOKEN, token);
        editor.commit();
    }

    public String getToken(){
        return  mPreferences.getString(KEY_TOKEN, null);
    }

    public String getUser(){
        return  mPreferences.getString(KEY_USER, null);
    }

    public void setUser(String user){
        editor.putString(KEY_USER, user);
        editor.commit();
    }

    public void setEmail(String email){
        editor.putString(KEY_EMAIL, email);
        editor.commit();
    }

    public String getEmail(){
        return  mPreferences.getString(KEY_EMAIL, null);
    }

    public void setCrawler(String crawler){
        editor.putString(KEY_CRAWLER, crawler);
        editor.commit();
    }

    public String getCrawler(){
        return  mPreferences.getString(KEY_CRAWLER, null);
    }

    public void setBeaconLogFlag(String beaconLogFlag){
        editor.putString(KEY_BEACON_LOG_FLAG, beaconLogFlag);
        editor.commit();
    }

    public String getBeaconLogFlag(){

        return  mPreferences.getString(KEY_BEACON_LOG_FLAG, null);
    }

    public void clear(){
        editor.clear();
        editor.commit();
    }
}

