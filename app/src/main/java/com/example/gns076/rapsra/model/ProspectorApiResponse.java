package com.example.gns076.rapsra.model;

import com.google.gson.annotations.SerializedName;

public class ProspectorApiResponse {
    @SerializedName("error")
    private boolean error;
    @SerializedName("message")
    private String message;

    @SerializedName("insertId")
    private String prospectId;


    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getProspectId() {
        return prospectId;
    }

    public void setProspectId(String prospectId) {
        this.prospectId = prospectId;
    }

}
