package com.example.gns076.rapsra.activities;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.IBinder;
import androidx.annotation.Nullable;
import android.widget.Toast;

import com.example.gns076.rapsra.model.BeaconModelFloorPlan;
import com.example.gns076.rapsra.model.BeaconParameters;
import com.google.gson.Gson;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

public class BeaconDistancesAdditionApp extends Service {
    Random r = new Random();
    double fromWhite, fromMarron, fromPink;
    SharedPreferences.Editor editor;
    SharedPreferences sharedPreferences;
    Gson gson = new Gson();
    String json = "";
    private HashMap<String, BeaconModelFloorPlan> beaconHashmap = new HashMap<String, BeaconModelFloorPlan>();
    private List<BeaconModelFloorPlan> beaconInfoList;
    private BeaconParameters beaconParametersWhite,beaconParametersMarron,beaconParametersPink;
    private BeaconModelFloorPlan beaconModelFloorPlanWhite,beaconModelFloorPlanMarron,beaconModelFloorPlanPink;
    final String beaconIdWhite = "0xedd1ebeac04e5defa0170xe576c84ebafb";
    final String beaconIdMarron = "0xedd1ebeac04e5defa0170xdac64a8d245e";
    final String beaconIdPink = "0xedd1ebeac04e5defa0170x14f32f9cb3b8";
    static double temp=0;

    @Override
    public void onCreate() {
        super.onCreate();
        Timer t = new Timer();
        sharedPreferences = getSharedPreferences("rapsra_pref_beacon", Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        t.scheduleAtFixedRate(new TimerTask() {

            @Override
            public void run() {


                fromWhite = r.nextDouble() + r.nextInt(2);
                fromMarron = r.nextDouble() + r.nextInt(3) + 10;
                fromPink = r.nextDouble() + r.nextInt(2) + 10;

                if(temp==10){
                    temp = 0;
                }
                temp++;

                beaconParametersWhite = new BeaconParameters(Double.toString(fromWhite), "7357");
                beaconModelFloorPlanWhite = new BeaconModelFloorPlan(beaconIdWhite);


               beaconModelFloorPlanWhite.setMeanDistance(fromWhite);
              //  beaconModelFloorPlanWhite.setMeanDistance(Double.toString(10.30));

                beaconModelFloorPlanWhite.addParameter(beaconParametersWhite);
                beaconHashmap.put(beaconIdWhite, beaconModelFloorPlanWhite);


                beaconParametersMarron = new BeaconParameters(Double.toString(fromMarron), "7357");
                beaconModelFloorPlanMarron = new BeaconModelFloorPlan(beaconIdMarron);

              //  beaconModelFloorPlanMarron.setMeanDistance(Double.toString(temp+10));
                beaconModelFloorPlanMarron.setMeanDistance(fromMarron);
              //  beaconModelFloorPlanMarron.setMeanDistance(Double.toString(7.57));


                beaconModelFloorPlanMarron.addParameter(beaconParametersMarron);
                beaconHashmap.put(beaconIdMarron, beaconModelFloorPlanMarron);


                beaconParametersPink = new BeaconParameters(Double.toString(fromPink), "7357");
                beaconModelFloorPlanPink = new BeaconModelFloorPlan(beaconIdPink);

              //  beaconModelFloorPlanPink.setMeanDistance(Double.toString(temp+10));
                beaconModelFloorPlanPink.setMeanDistance(fromMarron);
            //    beaconModelFloorPlanPink.setMeanDistance(Double.toString(8.66));

                beaconModelFloorPlanPink.addParameter(beaconParametersPink);
                beaconHashmap.put(beaconIdPink, beaconModelFloorPlanPink);


                beaconInfoList = new ArrayList<>(beaconHashmap.values());
                json = gson.toJson(beaconInfoList);
                editor.putString("BeaconInfoList", json);
                System.out.println("Inside BeaconDistancesAdditionApp.........");
                editor.commit();
            }
        }, 0, 1000);
    }


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
    @Override
    public void onDestroy() {
        /* IF YOU WANT THIS SERVICE KILLED WITH THE APP THEN UNCOMMENT THE FOLLOWING LINE */
        //handler.removeCallbacks(runnable);
        Toast.makeText(this, "Service stopped", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onStart(Intent intent, int startid) {
        Toast.makeText(this, "Service started by user.", Toast.LENGTH_LONG).show();
    }
}
