package com.example.gns076.rapsra.activities;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebView;

import com.example.gns076.rapsra.R;
import com.example.gns076.rapsra.model.AppPreference;

public class RapGamifyActivity extends AppCompatActivity {


    private AppPreference mAppPreference;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rapgamify);

        WebView mywebview= (WebView)findViewById(R.id.webview);

        mywebview.loadUrl("http://halphp.raptns.com/sso/{token}");
    }

}
