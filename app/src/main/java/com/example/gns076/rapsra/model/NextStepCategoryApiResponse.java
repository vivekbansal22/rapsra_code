package com.example.gns076.rapsra.model;

import com.google.gson.annotations.SerializedName;

public class NextStepCategoryApiResponse {
    @SerializedName("error")
    private boolean error;


    @SerializedName("message")
    private String message;

    public NextStepModel[] getObj() {
        return obj;
    }

    public void setObj(NextStepModel[] obj) {
        this.obj = obj;
    }

    @SerializedName("next_step")
    private NextStepModel obj[];

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
