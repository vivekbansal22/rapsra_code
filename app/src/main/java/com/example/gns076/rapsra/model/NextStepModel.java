package com.example.gns076.rapsra.model;

import com.google.gson.annotations.SerializedName;

public class NextStepModel {
    public String getNextStepId() {
        return nextStepId;
    }

    public void setNextStepId(String nextStepId) {
        this.nextStepId = nextStepId;
    }

    public String getNextStepName() {
        return nextStepName;
    }

    public void setNextStepName(String nextStepName) {
        this.nextStepName = nextStepName;
    }

    @SerializedName("id")
    private String nextStepId;

    @SerializedName("next_step_name")
    private String nextStepName;

}
