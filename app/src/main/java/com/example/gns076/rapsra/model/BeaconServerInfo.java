package com.example.gns076.rapsra.model;

public class BeaconServerInfo {
    private String beaconId;
    private String beaconXCoordinate;
    private String getBeaconYCoordinate;

    public String getBeaconId() {
        return beaconId;
    }

    public BeaconServerInfo(String beaconXCoordinate, String getBeaconYCoordinate) {
        this.beaconXCoordinate = beaconXCoordinate;
        this.getBeaconYCoordinate = getBeaconYCoordinate;
    }

    public BeaconServerInfo(String beaconId, String beaconXCoordinate, String getBeaconYCoordinate) {
        this.beaconId = beaconId;
        this.beaconXCoordinate = beaconXCoordinate;
        this.getBeaconYCoordinate = getBeaconYCoordinate;
    }

    public void setBeaconId(String beaconId) {
        this.beaconId = beaconId;
    }

    public String getBeaconXCoordinate() {
        return beaconXCoordinate;
    }

    public void setBeaconXCoordinate(String beaconXCoordinate) {
        this.beaconXCoordinate = beaconXCoordinate;
    }

    public String getGetBeaconYCoordinate() {
        return getBeaconYCoordinate;
    }

    public void setGetBeaconYCoordinate(String getBeaconYCoordinate) {
        this.getBeaconYCoordinate = getBeaconYCoordinate;
    }


}
