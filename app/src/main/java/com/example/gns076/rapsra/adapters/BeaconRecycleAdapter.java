package com.example.gns076.rapsra.adapters;

import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.gns076.rapsra.R;
import com.example.gns076.rapsra.model.BeaconInfo;

import java.util.List;

public class BeaconRecycleAdapter extends RecyclerView.Adapter<BeaconRecycleAdapter.BeaconHolder> {

    private List<BeaconInfo> listBeacon;

    public BeaconRecycleAdapter(List<BeaconInfo> listBeacon) {
        this.listBeacon = listBeacon;
    }

    @Override
    public BeaconHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // inflating recycler item view
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.beacon_recycler, parent, false);

        return new BeaconHolder(itemView);
    }

    @Override
    public void onBindViewHolder(BeaconHolder holder, int position) {

        holder.textViewNamespace.setText(listBeacon.get(position).getBeaconNamespace());
        holder.textViewType.setText(listBeacon.get(position).getBeaconType());
        holder.textViewMessage.setText(listBeacon.get(position).getBeaconMessage());
        holder.textViewDistance.setText(listBeacon.get(position).getBeaconDistance().toString());
    }

    @Override
    public int getItemCount() {
        Log.v(UsersRecyclerAdapter.class.getSimpleName(),""+listBeacon.size());
        return listBeacon.size();
    }


    /**
     * ViewHolder class
     */
    public class BeaconHolder extends RecyclerView.ViewHolder {

        public TextView textViewNamespace;
        public TextView textViewType;
        public TextView textViewMessage;
        public TextView textViewDistance;

        public BeaconHolder(View view) {
            super(view);
            textViewNamespace =  view.findViewById(R.id.textViewNamespace);
            textViewType =  view.findViewById(R.id.textViewType);
            textViewMessage =  view.findViewById(R.id.textViewMessage);
            textViewDistance =  view.findViewById(R.id.textViewDistance);

        }
    }


}
