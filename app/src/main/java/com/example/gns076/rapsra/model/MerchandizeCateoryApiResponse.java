package com.example.gns076.rapsra.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class MerchandizeCateoryApiResponse {
    @SerializedName("error")
    private boolean error;


    @SerializedName("message")
    private String message;

    @SerializedName("merchandize_categories")
    private ArrayList<MerchandizeCategoryDetails> merchandizeCategories =  new ArrayList<>();

    @SerializedName("merchandize_category")
    private MerchandizeCategory obj[];

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public MerchandizeCategory[] getObj() {
        return obj;
    }

    public void setObj(MerchandizeCategory[] obj) {
        this.obj = obj;
    }

    public ArrayList<MerchandizeCategoryDetails> getMerchandizeCategories() {
        return merchandizeCategories;
    }

    public void setMerchandizeCategories(ArrayList<MerchandizeCategoryDetails> merchandizeCategories) {
        this.merchandizeCategories = merchandizeCategories;
    }
}
