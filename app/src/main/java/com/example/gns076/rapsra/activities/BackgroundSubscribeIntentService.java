/**
 * Copyright 2016 Google Inc. All Rights Reserved.
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.gns076.rapsra.activities;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;

import com.google.android.gms.nearby.Nearby;
import com.google.android.gms.nearby.messages.Message;
import com.google.android.gms.nearby.messages.MessageListener;
/*
import com.google.android.gms.nearby.messages.zzb;
*/
import java.util.List;
import com.example.gns076.rapsra.R;


public class BackgroundSubscribeIntentService extends IntentService
{
    private static final String TAG = "BackSubIntentService";

    private static final int MESSAGES_NOTIFICATION_ID = 1;
    private static final int NUM_MESSAGES_IN_NOTIFICATION = 5;

    public BackgroundSubscribeIntentService() {
        super("BackgroundSubscribeIntentService");
    }
    public MessageListener mMessageListener;
    @Override
    public void onCreate() {
        super.onCreate();
        //updateNotification();
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Log.i(TAG,"innside onHandleIntent");
        if (intent != null) {

            Nearby.Messages.handleIntent(intent, new MessageListener() {
/*
                @Override
                public void zza(Message message, zza zza) {
                    super.zza(message, zza);
                }
*/

/*
                @Override
                public void zza(Message message, zzb zzb) {
                    super.zza(message, zzb);
                }
*/

                @Override
                public void onFound(Message message) {
                    Log.i(TAG,"innside onFound");
                    Utils.saveFoundMessage(getApplicationContext(), message);
                    if(message.getType().equals("video"))
                    {
                        Bundle bundle = new Bundle();
                        String youtubeVideoString = new String(message.getContent());
                        String onlyId = youtubeVideoString.replace("https://www.youtube.com/watch?v=", "");


                        bundle.putString("video", onlyId );
                        Intent intent = new Intent(getApplicationContext(),YoutubeActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.putExtras(bundle);
                        Log.i(TAG,"Starting activity");
                        startActivity(intent);
                    }
                    //updateNotification();
                }

                @Override
                public void onLost(Message message) {
                    Log.i(TAG,"innside onLost");
                    Utils.removeLostMessage(getApplicationContext(), message);
                    //updateNotification();

                }
            });
        }
    }


    /*private void updateNotification() {
        List<String> messages = Utils.getCachedMessages(getApplicationContext());
        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        Intent launchIntent = new Intent(getApplicationContext(), ArViewActivity.class);
        launchIntent.setAction(Intent.ACTION_MAIN);
        launchIntent.addCategory(Intent.CATEGORY_LAUNCHER);
        PendingIntent pi = PendingIntent.getActivity(getApplicationContext(), 0,
                launchIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        String contentTitle = getContentTitle(messages);
        String contentText = getContentText(messages);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(android.R.drawable.star_on)
                .setContentTitle(contentTitle)
                .setContentText(contentText)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(contentText))
                .setOngoing(true)
                .setContentIntent(pi);
        notificationManager.notify(MESSAGES_NOTIFICATION_ID, notificationBuilder.build());
    }*/

    private String getContentTitle(List<String> messages) {
        switch (messages.size()) {
            case 0:
                return getResources().getString(R.string.scanning);
            case 1:
                return getResources().getString(R.string.one_message);
            default:
                return getResources().getString(R.string.many_messages, messages.size());
        }
    }

    private String getContentText(List<String> messages) {
        String newline = System.getProperty("line.separator");
        if (messages.size() < NUM_MESSAGES_IN_NOTIFICATION) {
            return TextUtils.join(newline, messages);
        }
        return TextUtils.join(newline, messages.subList(0, NUM_MESSAGES_IN_NOTIFICATION)) +
                newline + "&#8230;";
    }
}
