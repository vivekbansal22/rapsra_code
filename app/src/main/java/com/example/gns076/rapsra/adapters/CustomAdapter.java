package com.example.gns076.rapsra.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import com.example.gns076.rapsra.R;
import com.example.gns076.rapsra.model.MerchandizeSubCategory;
import com.example.gns076.rapsra.model.SubCategoryPopUpModel;

import java.util.ArrayList;

/**
 * Created by hardik on 9/1/17.
 */
public class CustomAdapter extends BaseAdapter {

    private Context context;
    public static ArrayList<MerchandizeSubCategory> subCategoryPopUpModelArrayList;


    public CustomAdapter(Context context, ArrayList<MerchandizeSubCategory> subCategoryPopUpModelArrayList) {

        this.context = context;
        this.subCategoryPopUpModelArrayList = subCategoryPopUpModelArrayList;

    }

    @Override
    public int getViewTypeCount() {
        return getCount();
    }
    @Override
    public int getItemViewType(int position) {

        return position;
    }

    @Override
    public int getCount() {
        return subCategoryPopUpModelArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return subCategoryPopUpModelArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;

        if (convertView == null) {
            holder = new ViewHolder(); LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.lv_item, null, true);

            holder.checkBox = (CheckBox) convertView.findViewById(R.id.cb);
            holder.tvAnimal = (TextView) convertView.findViewById(R.id.subCategoryListName);

            convertView.setTag(holder);
        }else {
            // the getTag returns the viewHolder object set as a tag to the view
            holder = (ViewHolder)convertView.getTag();
        }


   //     holder.checkBox.setText("Checkbox "+position);
        holder.tvAnimal.setText(subCategoryPopUpModelArrayList.get(position).getMerchandize_sub_category_name());

        holder.checkBox.setChecked(subCategoryPopUpModelArrayList.get(position).isSelected());

        holder.checkBox.setTag(R.integer.btnplusview, convertView);
        holder.checkBox.setTag( position);
        holder.checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                View tempview = (View) holder.checkBox.getTag(R.integer.btnplusview);
                Integer pos = (Integer)  holder.checkBox.getTag();

               // Toast.makeText(context, "Checkbox "+pos+" clicked!", Toast.LENGTH_SHORT).show();

                if(subCategoryPopUpModelArrayList.get(pos).isSelected()){
                    subCategoryPopUpModelArrayList.get(pos).setSelected(false);
                  //  holder.checkBox.setBackgroundResource(R.drawable.checkbox_unselected);

                }else {
                    subCategoryPopUpModelArrayList.get(pos).setSelected(true);
                 //   holder.checkBox.setBackgroundResource(R.drawable.checkbox_selected);
                }

            }
        });

        return convertView;
    }

    private class ViewHolder {

        protected CheckBox checkBox;
        private TextView tvAnimal;

    }

}
