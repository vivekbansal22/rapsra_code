package com.example.gns076.rapsra.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.gns076.rapsra.adapters.SearchProspectSoAdapter;
import com.example.gns076.rapsra.model.AppPreference;
import com.example.gns076.rapsra.model.ProspectSoResponse;
import com.example.gns076.rapsra.model.ProspectSoSearchModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.gns076.rapsra.R.id;
import static com.example.gns076.rapsra.R.layout;

public class SearchProspectSoActivity extends AppCompatActivity {

    private static final String TAG = SearchProspectSoActivity.class.getSimpleName();
    private AppPreference mAppPreference;
    private TextView searchText;
    private Button searchButton;
    // List view
    private RecyclerView recyclerView;

    // Listview Adapter
    SearchProspectSoAdapter adapter;

    // Search EditText
    EditText inputSearch;
    String search_data ="";


    // ArrayList for Listview
    ArrayList<HashMap<String, String>> prospectsoList;
    public APIInterface apiInterface;
    private String dealer_id;
    private String searchString;
    List<ProspectSoSearchModel> responseList = new ArrayList<ProspectSoSearchModel>();
    List<ProspectSoSearchModel> tempresponseList = new ArrayList<ProspectSoSearchModel>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(layout.activity_search_prospect_so_new);

        ProspectSoSearchModel tempSearchModel;
        recyclerView = (RecyclerView)findViewById(id.recycler_view_prospect_so) ;
        apiInterface = APIClient.getClient().create(APIInterface.class);
        mAppPreference = new AppPreference(this);
        dealer_id=mAppPreference.getDealerId();
        search_data =  getIntent().getExtras().get("Search_data").toString();


        try {
            Call<ProspectSoResponse> call2 = apiInterface.getProspectSoDetails(dealer_id,search_data);
            call2.enqueue(new Callback<ProspectSoResponse>() {
                @Override
                public void onResponse(Call<ProspectSoResponse> call, Response<ProspectSoResponse> response) {
                    //System.out.println(response.body().toString());
                    Log.i(TAG,"Response:+response.body().toString()");
                    ProspectSoResponse res=response.body();
                    if (!res.isError()) {
                        responseList = res.getFollowups();
                        if(responseList.isEmpty()){
                            Toast.makeText(getApplicationContext(), "No Records Available " , Toast.LENGTH_LONG).show();
                        }
                        adapter.setListProspectSoSearch(responseList);
                        adapter.notifyDataSetChanged();
                        recyclerView.invalidate();
                    } else {
                        // newsCrawler="NA";

                    }
                }

                @Override
                public void onFailure(Call<ProspectSoResponse> call, Throwable t) {
                    Log.i(TAG,"inside onFailure of API call");
                    call.cancel();
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }


        adapter = new SearchProspectSoAdapter(responseList,new SearchProspectSoAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(ProspectSoSearchModel item) {
                // Toast.makeText(this, "Item Clicked", Toast.LENGTH_LONG).show();
                Intent intentRegister = new Intent();
                intentRegister.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                intentRegister.putExtra("selectedProspectsodata",item);
                setResult(Activity.RESULT_OK, intentRegister);
                onBackPressed();
            }
        });
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        recyclerView.setScrollContainer(true);
        recyclerView.setAdapter(adapter);

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}

